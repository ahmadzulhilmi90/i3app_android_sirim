/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.m3tech.app_100_1474;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.m3tech.app_100_1142_v4";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "1.0.1";
}
