package com.m3tech.content;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.collection.Collection_content;
import com.m3tech.comments.Comment;
import com.m3tech.favorite.Favorite;
import com.m3tech.header.Header;
import com.m3tech.web.WebBrowser;
import com.m3tech.widget.Plugin_Content_T6_Widget;

public class Plugin_Content_T6 extends Plugin_Content_T6_Widget implements LocationListener {

	private static final String LOG_TAG = "PLUGIN_CONTENT_T6";
	content_Adapter adapter_content=null;
	Location currentLocation;
	LocationManager locationManager;
	public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
	public static final long MIN_TIME_BW_UPDATES = 1000*60*1; // 1 minute

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING CONTENT T6 ");
		
		Intent j = getIntent();
		cont_cat_name = j.getStringExtra("cont_cat_name");
		cont_category_id =  j.getStringExtra("cont_category_id");
		list_template = j.getStringExtra("list_template");
		plugin_id = j.getStringExtra("plugin_id");
		allow_favorite =  j.getStringExtra("allow_favorite");
		allow_comments =  j.getStringExtra("allow_comments");
		
		Log.d(LOG_TAG,"cont_cat_name="+ cont_cat_name);	
		Log.d(LOG_TAG,"cont_category_id="+ cont_category_id);	
		Log.d(LOG_TAG,"customerID ="+ cuid);
		Log.d(LOG_TAG,"allow_favorite="+ allow_favorite);
		Log.d(LOG_TAG,"allow_comments="+ allow_comments);		

		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, cont_cat_name,"");
		
		layouteditsearch.setVisibility(View.VISIBLE);

		//*** text search change *******//

		editTextsearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					GetSearch Task = new GetSearch();
					Task.execute(new String[] { currentTextFilter, cont_category_id });
					return true;
				}
				return false;
			}


		});

		editTextsearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
				// When user changed the Text

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {

			}

			@Override
			public void afterTextChanged(Editable s) { 

				Log.i(LOG_TAG, "Typed=" + s.toString());
				currentTextFilter = s.toString();
				Log.i(LOG_TAG, "currentTextFilter == " + currentTextFilter);
				Log.i(LOG_TAG, "currentTextFilterLength == " + currentTextFilter.length());

				if (currentTextFilter != null || currentTextFilter.length() > 0) {
					GetSearch Task = new GetSearch();
					Task.execute(new String[] { currentTextFilter, cont_category_id });

				} else {
					GetContentList Task = new GetContentList();
					Task.execute(new String[] { cont_category_id }); 
				}

			}

		});
		
		/*** Call Function ***/
		onGetTopID(cont_category_id);
		
		GetContentList Task = new GetContentList();
		Task.execute(new String[] { cont_category_id }); 

	}
	
	/*** GET TOP ID ***/
	public void onGetTopID(String cont_category_id){
		
		Log.d(LOG_TAG, "cat id : "+ cont_category_id);
		Cursor d = Helper_Contentcategory.getByCatId(cont_category_id);
		Log.d(LOG_TAG, "d.getCount()=" + d.getCount());
	
		if (d.moveToLast() != false) {
			topid = Helper_Contentcategory.gettop_id(d).toString();
			Log.d(LOG_TAG, "topid == " + topid);
		}
		d.close();
		
		favorite = new Favorite(context,topid);
		comment = new Comment(context,topid);
		
	}

	void InitializeLocationManager() {

		currentLocation = getLocation();

		if (currentLocation != null) {
			currentlat = String.valueOf(currentLocation.getLatitude());
			currentlong = String.valueOf(currentLocation.getLongitude());
		} 

		Log.d(LOG_TAG,"currentlat="+currentlat);
		Log.d(LOG_TAG,"currentlong="+currentlong);

		GetContentList Task = new GetContentList();
		Task.execute(new String[] { cont_category_id}); 
	}

	public Location getLocation() {
		Location location = null;
		locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);

		try {
			locationManager = (LocationManager) Plugin_Content_T6.this.getSystemService(LOCATION_SERVICE);

			// getting GPS status
			boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// getting network status
			boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSEnabled && !isNetworkEnabled) {
				// no network provider is enabled
				Toast.makeText(Plugin_Content_T6.this,getResources().getString(R.string.please_turnon_gps), Toast.LENGTH_LONG).show();

			} else {
				this.canGetLocation = true;
				if (isNetworkEnabled) {
					locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
					Log.d("Network", "Network Enabled");

					if (locationManager != null) {
						location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					}
				}
				// if GPS Enabled get lat/long using GPS Services
				if (isGPSEnabled) {
					if (location == null) {
						locationManager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER,
								MIN_TIME_BW_UPDATES,
								MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
						Log.d("GPS", "GPS Enabled");
						if (locationManager != null) {
							location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return location;
	}

	/**** start function get content list ****/
	private class GetContentList extends AsyncTask<String, Void, String> {

		String cont_cat_id = "0";
		String map_lat = "";
		String map_lon = "";
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			cont_cat_id = arg0[0];

			try{

				adapter_content=new content_Adapter();
				adapter_content.clear();
				Log.d(LOG_TAG,"cont_cat_id="+cont_cat_id);

				//content = Helper_Content.getByCategoryId(cont_cat_id);
				content = Helper_Content.getByCategoryIdPOI(cont_cat_id, currentlat, currentlong);

				Log.d(LOG_TAG, "c=" + content.getCount());
				if (content.moveToLast() != false) {
					// Get from DB
					content.moveToFirst();
					do {
						current_content=new Collection_content();

						String strdistance = "";

						if (Helper_Content.getmap_lat(content).toString().length()>0 
								&& Helper_Content.getmap_lon(content).toString().length()>0
								&& currentlat.length()>0 && currentlong.length()>0) {
							
							map_lat = Helper_Content.getmap_lat(content).toString();
							map_lon = Helper_Content.getmap_lon(content).toString();
							strdistance = slotAction.getStrDistance(currentlat, currentlong, map_lat, map_lon);	
						}

						Log.d(LOG_TAG, Helper_Content.gettitle(content).toString() + "=" + strdistance);

						current_content.setcontent_id(Helper_Content.getcontent_id(content).toString());
						current_content.settitle(Helper_Content.gettitle(content).toString());
						current_content.setimage(Helper_Content.getimage(content).toString());
						current_content.setthumbnail(Helper_Content.getthumbnail(content).toString());
						current_content.setmap_lat(Helper_Content.getmap_lat(content).toString());
						current_content.setmap_lon(Helper_Content.getmap_lon(content).toString());
						current_content.setdistance(strdistance);
						current_content.setvar1(Helper_Content.getvar1(content).toString());
						current_content.setvar1_title(Helper_Content.getvar1_title(content).toString());
						current_content.setlink_url(Helper_Content.getlink_url(content).toString());

						adapter_content.add(current_content);

					} while (content.moveToNext());

				}

				content.close();

				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetContentList-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				ListView listView2 = (ListView) findViewById(R.id.listcontent);

				if (result.equals("SUCCESS")){

					listView2.setAdapter(adapter_content);			
					listView2.setOnItemClickListener(onListClick3);	

				}else{
					Toast.makeText(Plugin_Content_T6.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "Getcontentlist-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}


	class content_Adapter extends ArrayAdapter<Collection_content> {
		content_Adapter() {
			super(Plugin_Content_T6.this, R.layout.plugin_content_template6_row, model_content);
		}

		public View getView(int position, View convertView,
				ViewGroup parent) {
			View row=convertView;
			NotificationHolder3 holder=null;

			if (row==null) {													
				LayoutInflater inflater=getLayoutInflater();

				row=inflater.inflate(R.layout.plugin_content_template6_row, parent, false);
				holder=new NotificationHolder3(row);
				row.setTag(holder);
			}
			else {
				holder=(NotificationHolder3)row.getTag();
			}

			holder.populateFrom3(model_content.get(position));
			return(row);
		}
	}


	class NotificationHolder3 {
		@SuppressWarnings("unused")
		private View row=null;
		private TextView titlecontent=null;
		private TextView distance = null;
		private TextView var1 = null;
		private LinearLayout layout_info = null;
		


		NotificationHolder3(View row) {

			this.row=row;	

			titlecontent=(TextView) row.findViewById(R.id.text);
			titlecontent.setTextColor(Color.parseColor(colorcode));

			distance=(TextView) row.findViewById(R.id.distance);
			var1=(TextView) row.findViewById(R.id.var1);
			layout_info = (LinearLayout) row.findViewById(R.id.layout_info);
			
			
		}
		void populateFrom3(final Collection_content r) {
			String distances = "";
			Log.d(LOG_TAG,"titlecontent = "+r.gettitle() + " " + r.getdistance());

			if (r.getmap_lat().length()>0 && r.getmap_lon().length()>0) {
				distances = r.getdistance();
			}

			titlecontent.setText(r.gettitle());

			if (distances.length()>0) {
				distance.setText(distances);	
				distance.setVisibility(View.VISIBLE);
			} else {
				distance.setVisibility(View.GONE);
			}

			String strvar1 = r.getvar1();
			if (strvar1.length()>0) {
				var1.setText(strvar1);	
				var1.setVisibility(View.VISIBLE);
			} else {
				var1.setVisibility(View.GONE);
			}

			myClickListener listener = new myClickListener(r.getcontent_id());
			layout_info.setOnClickListener(listener);
			

		}	
	}

	public class myClickListener implements LinearLayout.OnClickListener {

		String myVar;
		public myClickListener(String myVar) {
			this.myVar = myVar;
		}

		public void onClick(View v) {
			try{
				Log.d(LOG_TAG,"myClickListener:"+myVar);
				String link_url = current_content.getlink_url();
				
				if (plugin_id.equals("20") || plugin_id.equals("21")) {
					Intent i = new Intent(Plugin_Content_T6.this, Plugin_Content_Gallery.class);
					i.putExtra("contentcategorytitle", cont_cat_name);
					i.putExtra("contentid", myVar);
					i.putExtra("list_template", list_template);
					i.putExtra("plugin_id", plugin_id);
					i.putExtra("contentdetails", current_content.getdetails());
					i.putExtra("contenttitle", current_content.gettitle());
					startActivity(i);
					
				} else if (plugin_id.equals("45") && link_url.length()>14) {
					if (!link_url.startsWith("http://") && !link_url.startsWith("https://")){
						link_url = "http://" + link_url;
					}

					Intent i=new Intent(getApplicationContext(), WebBrowser.class);
					i.putExtra("url", link_url);
					i.putExtra("openweb", "yes");
					i.putExtra("pagetitle", current_content.gettitle());
					startActivity(i);

				} else {
					Intent i = new Intent(Plugin_Content_T6.this, Plugin_Content_Detail.class);
					i.putExtra("contentcategorytitle", cont_cat_name);
					i.putExtra("contentid", myVar);
					i.putExtra("list_template", list_template);
					i.putExtra("plugin_id", plugin_id);
					i.putExtra("top_id", topid);
					i.putExtra("allow_comments", allow_comments);
					i.putExtra("allow_favorite", allow_favorite);
					i.putExtra("from", "content");
					startActivity(i);
				}

			} catch (Throwable t) { 
				Log.e(LOG_TAG, "myClickListener-Error:" + t.getMessage(), t); 

			}
		}

	};

	private AdapterView.OnItemClickListener onListClick3=new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent,
				View view, int position,
				long id) {
			current_content=model_content.get(position);	
			Log.d(LOG_TAG,"onListClick-"+current_content.getcontent_id());

			if (current_content.getmap_lat().length()>0 && current_content.getmap_lon().length()>0) {
				//String uri = "geo:" + current_content.getmap_lat() + ","+ current_content.getmap_lon();
				//startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri)));

				Intent i=new Intent(Plugin_Content_T6.this, Plugin_Content_Maps.class);
				i.putExtra("contentcategorytitle", cont_cat_name);
				i.putExtra("contentid", current_content.getcontent_id());
				i.putExtra("list_template", list_template);
				i.putExtra("plugin_id", plugin_id);
				startActivity(i);

			} else {
				Intent i=new Intent(Plugin_Content_T6.this, Plugin_Content_Detail.class);
				i.putExtra("contentcategorytitle", cont_cat_name);
				i.putExtra("contentid", current_content.getcontent_id());
				i.putExtra("list_template", list_template);
				i.putExtra("plugin_id", plugin_id);
				i.putExtra("top_id", topid);
				i.putExtra("allow_comments", allow_comments);
				i.putExtra("allow_favorite", allow_favorite);
				i.putExtra("from", "content");
				startActivity(i);

			}

		}
	};


	/****************************search function*******************************/



	private class GetSearch extends AsyncTask<String, Void, String> {
		String cont_cat_id = "0";
		String map_lat = "";
		String map_lon = "";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;

			key = arg0[0];
			cont_cat_id = arg0[1];

			try {
				Log.d(LOG_TAG, "KEY =" + key);
				response = "SUCCESS";

			} catch (Throwable t) {
				response = "FAILED";
				Log.e(LOG_TAG, "GetSearch-doInBackground-Error:" + t.getMessage(), t);
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				adapter_content=new content_Adapter();
				adapter_content.clear();

				if (result.equals("SUCCESS")) {

					if (key.length()>0) {
						content = Helper_Content.getByCatAndKeyPOI(cont_cat_id, key, currentlat, currentlong);
					} else {
						content = Helper_Content.getByCategoryIdPOI(cont_cat_id, currentlat, currentlong);
					}
					int count_ = content.getCount();
					Log.d(LOG_TAG, "count_product="+ count_);

					if(count_ == 0){

						Toast.makeText(Plugin_Content_T6.this,getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG).show();
					}
					else{
						Log.d(LOG_TAG, "count_Product_Mode="+ count_);
						if (content.moveToLast() != false ){
							//Get from DB
							content.moveToFirst();
							do {
								current_content=new Collection_content();
								String strdistance = "";

								if (Helper_Content.getmap_lat(content).toString().length()>0 
										&& Helper_Content.getmap_lon(content).toString().length()>0
										&& currentlat.length()>0 && currentlong.length()>0) {
									
									map_lat = Helper_Content.getmap_lat(content).toString();
									map_lon = Helper_Content.getmap_lon(content).toString();
									strdistance = slotAction.getStrDistance(currentlat, currentlong, map_lat, map_lon);	
								}

								Log.d(LOG_TAG, Helper_Content.gettitle(content).toString() + "=" + strdistance);

								current_content.setcontent_id(Helper_Content.getcontent_id(content).toString());
								current_content.settitle(Helper_Content.gettitle(content).toString());
								current_content.setimage(Helper_Content.getimage(content).toString());
								current_content.setthumbnail(Helper_Content.getthumbnail(content).toString());
								current_content.setmap_lat(Helper_Content.getmap_lat(content).toString());
								current_content.setmap_lon(Helper_Content.getmap_lon(content).toString());
								current_content.setdistance(strdistance);
								current_content.setvar1(Helper_Content.getvar1(content).toString());
								current_content.setvar1_title(Helper_Content.getvar1_title(content).toString());
								adapter_content.add(current_content);

							} while (content.moveToNext());


						}
					}

				} else {
					Toast.makeText(Plugin_Content_T6.this,getResources().getString(R.string.item_not_found),Toast.LENGTH_SHORT).show();
				}
				listView.setAdapter(adapter_content);
				listView.setOnItemClickListener(onListClick3);	



			} catch (Throwable t) {
				Log.e(LOG_TAG,"GetSalesOrder-onPostExecute-Error:" + t.getMessage(),t);

			}
		}
	}

	/*************************search function**********************************/

	public void onBackPressed() {

		super.onBackPressed(); // allows standard use of backbutton for page 1
		this.finish();
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d(LOG_TAG,"onResume");
		InitializeLocationManager();
	}

	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();


		Helper_Content.close();		

	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub

	}


}
