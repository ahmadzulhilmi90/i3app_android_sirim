package com.m3tech.content;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;

import com.m3tech.app_100_1474.R;
import com.m3tech.collection.Collection_content;
import com.m3tech.header.Header;
import com.m3tech.widget.Plugin_Content_CatT1_Widget;

public class Plugin_Content_CatT1 extends Plugin_Content_CatT1_Widget {

	private static final String LOG_TAG = "PLUGIN_CONTENT_CATT1";
	
	ArrayAdapter<Collection_content> adapter_parent = null;
	ArrayAdapter<Collection_content> adapter_child = null;
	protected Collection_content current_content_parent = null;	
	protected Collection_content current_content_child = null;
	protected List<Collection_content> model_content_child=new ArrayList<Collection_content>();
	private String parameter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING CONTENT CAT T1 ");

		Intent j = getIntent();
		prod_cat_name = j.getStringExtra("prod_cat_name");
		parent_name =  j.getStringExtra("parent_name");
		child_name = j.getStringExtra("child_name");
		top_id = j.getStringExtra("top_id");
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome,img_back,img_home, texttitlebar, prod_cat_name,"");
		
		/*** Setting Text Spinner ***/
		text_parent.setText(parent_name.toString());
		text_child.setText(child_name.toString());

		/*** Setting Spinner Parent ***/
		GetParentContent();
		
		/*** Setting Button submit ***/
		submit.setOnClickListener(onSubmit);
	}
	
	public void GetParentContent(){
		
		adapter_parent = new ArrayAdapter<Collection_content>(context,android.R.layout.simple_spinner_item,model_content);
		adapter_parent.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		Cursor content = Helper_Contentcategory.getByTopId(top_id);
		Log.d(LOG_TAG, "c=" + content.getCount());
		if (content.moveToLast() != false) {
			// Get from DB
			content.moveToFirst();
			do {
				current_content_parent=new Collection_content();
				String cont_cat_id = Helper_Contentcategory.getcontentcat_id(content).toString();
				current_content_parent.setcategory_id(Helper_Contentcategory.gettop_id(content).toString());
				current_content_parent.setcontent_id(cont_cat_id);
				current_content_parent.settitle(Helper_Contentcategory.getname(content).toString());
				
				detail = Helper_Content.getByCategoryId_Latest(cont_cat_id);
				if (detail.moveToLast() != false) {
					detail.moveToFirst();
					do {
						current_content_parent.setimage(Helper_Content.getimage(detail).toString());
						current_content_parent.setthumbnail(Helper_Content.getthumbnail(detail).toString());								
					} while (detail.moveToNext());
				}
				
				adapter_parent.add(current_content_parent);


			} while (content.moveToNext());

		}
		detail.close();
		content.close();
		spinner_parent.setAdapter(adapter_parent);
		spinner_parent.setOnItemSelectedListener(new SpinnerParentSelected());
		
	}
	
	public class SpinnerParentSelected implements OnItemSelectedListener{

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			
			try{
				String selected = parent.getItemAtPosition(position).toString();
				current_content=model_content.get(position);
				Log.d(LOG_TAG,"SpinnerParentSelected ID : "+current_content.getcontent_id());
				
				if(selected.length()>0 && !selected.equals("Please Select")){
					Log.d(LOG_TAG, "Parent :"+selected);
					GetChildContent(current_content.getcontent_id().toString());
				}else{
					Log.d(LOG_TAG, "Parent Error :"+selected);
				}
				
			}catch(Throwable t){
				Log.e(LOG_TAG, "SpinnerParentSelected :"+t.getMessage());
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub
			spinner_parent.setPrompt("Please Select");
		}
		
	}

	public void GetChildContent(String id){
		
		adapter_child = new ArrayAdapter<Collection_content>(context,android.R.layout.simple_spinner_item,model_content_child);
		adapter_child.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adapter_child.clear();
		
		Cursor content = Helper_Contentcategory.getByParentId(id);
		Log.d(LOG_TAG, "c=" + content.getCount());
		
		if (content.moveToLast() != false) {
			// Get from DB
			content.moveToFirst();
			do {
				current_content_child=new Collection_content();
				String cont_cat_id = Helper_Contentcategory.getcontentcat_id(content).toString();
				current_content_child.setcategory_id(Helper_Contentcategory.gettop_id(content).toString());
				current_content_child.setcontent_id(cont_cat_id);
				current_content_child.settitle(Helper_Contentcategory.getname(content).toString());
				
				detail = Helper_Content.getByCategoryId_Latest(cont_cat_id);
				if (detail.moveToLast() != false) {
					detail.moveToFirst();
					do {
						current_content_child.setimage(Helper_Content.getimage(detail).toString());
						current_content_child.setthumbnail(Helper_Content.getthumbnail(detail).toString());								
					} while (detail.moveToNext());
				}
				adapter_child.add(current_content_child);

			} while (content.moveToNext());

		}
		detail.close();
		content.close();
		spinner_child.setAdapter(adapter_child);
		spinner_child.setOnItemSelectedListener(new SpinnerChildSelected());
		
	}
	
	public class SpinnerChildSelected implements OnItemSelectedListener{

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			
			try{
				String selected = parent.getItemAtPosition(position).toString();
				current_content_child=model_content_child.get(position);
				Log.d(LOG_TAG,"SpinnerChildSelected ID : "+current_content_child.getcontent_id());
				
				if(selected.length()>0 && !selected.equals("Please Select")){
					Log.d(LOG_TAG, "Child :"+selected);
					
					String top_id = current_content_child.getcategory_id();
					String top_title = current_content_child.gettitle();
					parameter = top_id+"|"+top_title;
					
				}else{
					Log.d(LOG_TAG, "Child Error :"+selected);
				}
				
			}catch(Throwable t){
				Log.e(LOG_TAG, "SpinnerChildSelected :"+t.getMessage());
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub
			spinner_child.setPrompt("Please Select");
		}
		
	}
	
	private View.OnClickListener onSubmit = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Log.d(LOG_TAG, "parameter : " + parameter);
			GetLoading Task = new GetLoading();
			Task.execute();
			
		}
	};
	
	private class GetLoading extends AsyncTask<String, Void, String> {

		String response;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressdialog = ProgressDialog.show(context,"",getResources().getString(R.string.please_wait));
		}
		@Override
		protected String doInBackground(String... arg0) {		
			try{
				response="SUCCESS";
			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetContentList-doInBackground-Error:" + t.getMessage(), t); 
			}
			progressdialog.dismiss();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				if (result.equals("SUCCESS")){
					
					String[] parts = parameter.split("\\|",-1);
					String top_id = parts[0];
					String top_title = parts[1];
					Log.d(LOG_TAG, "Top ID Child : " + top_id);
					Log.d(LOG_TAG, "Top Title Child : " + top_title);
					
					if(top_id.length()>0 && top_title.length()>0){
						slotAction.CreatePluginPage(top_id, "content", top_title, "", "");
					}
				}else{
					Toast.makeText(Plugin_Content_CatT1.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}
			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "GetLoading-onPostExecute-Error:" + t.getMessage(), t);
			}
		}
	}

	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
	}

	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();
		Helper_Content.close();	
		Helper_Contentcategory.close();
	}
}
