package com.m3tech.content;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.collection.Collection_content;
import com.m3tech.header.Header;
import com.m3tech.widget.Plugin_Content_CatT2_Widget;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class Plugin_Content_CatT5 extends Plugin_Content_CatT2_Widget {

	private static final String LOG_TAG = "PLUGIN_CONTENT_CATT5";
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();
	content_Adapter adapter_content=null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING CONTENT CAT T5 ");

		Intent j = getIntent();
		cont_cat_name = j.getStringExtra("cont_cat_name");
		cont_category_id =  j.getStringExtra("cont_category_id");
		list_template = j.getStringExtra("list_template");
		top_id = j.getStringExtra("top_id");
		
		Log.d(LOG_TAG,"cont_cat_name="+ cont_cat_name);	
		Log.d(LOG_TAG,"cont_category_id="+ cont_category_id);	
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome,img_back,img_home,texttitlebar, cont_cat_name,"");

		GetContentList Task = new GetContentList();
		Task.execute(new String[] { cont_category_id }); 

	}


	/**** start function get content list ****/
	private class GetContentList extends AsyncTask<String, Void, String> {

		String cont_cat_id;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			cont_cat_id = arg0[0];
			try{

				adapter_content=new content_Adapter();
				adapter_content.clear();
				Log.d(LOG_TAG,"cont_cat_id="+cont_cat_id);

				content = Helper_Contentcategory.getByCatIdList(cont_cat_id);
				Log.d(LOG_TAG, "c=" + content.getCount());
				if (content.moveToLast() != false) {
					// Get from DB
					content.moveToFirst();
					do {


						current_content=new Collection_content();

						String cont_cat_id = Helper_Contentcategory.getcontentcat_id(content).toString();
						current_content.setcategory_id(Helper_Contentcategory.gettop_id(content).toString());
						current_content.setcontent_id(cont_cat_id);
						current_content.settitle(Helper_Contentcategory.getname(content).toString());
						
						detail = Helper_Content.getByCategoryId_Latest(cont_cat_id);
						if (detail.moveToLast() != false) {
							detail.moveToFirst();
							do {
								current_content.setimage(Helper_Content.getimage(detail).toString());
								current_content.setthumbnail(Helper_Content.getthumbnail(detail).toString());								
							} while (detail.moveToNext());
						}
						
						adapter_content.add(current_content);


					} while (content.moveToNext());

				}

				content.close();
				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetContentList-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				if (result.equals("SUCCESS")){
					listview.setAdapter(adapter_content);			
					listview.setOnItemClickListener(onListClick3);
				}else{
					Toast.makeText(Plugin_Content_CatT5.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}
			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "Getcontentlist-onPostExecute-Error:" + t.getMessage(), t);
			}
		}
	}


	class content_Adapter extends ArrayAdapter<Collection_content> {
		content_Adapter() {
			super(Plugin_Content_CatT5.this, R.layout.plugin_content_template7_row, model_content);
		}

		public View getView(int position, View convertView,
				ViewGroup parent) {
			View row=convertView;
			NotificationHolder3 holder=null;

			if (row==null) {													
				LayoutInflater inflater=getLayoutInflater();

				row=inflater.inflate(R.layout.plugin_content_template7_row, parent, false);
				holder=new NotificationHolder3(row);
				row.setTag(holder);
			}
			else {
				holder=(NotificationHolder3)row.getTag();
			}

			holder.populateFrom3(model_content.get(position));
			return(row);
		}
	}


	class NotificationHolder3 {
		@SuppressWarnings("unused")
		private View row=null;
		private TextView titlecontent=null;
		private ImageView image =null;
		private ProgressBar spinner = null;
		@SuppressWarnings("unused")
		private LinearLayout row_image;

		NotificationHolder3(View row) {
			this.row=row;	
			titlecontent=(TextView) row.findViewById(R.id.text);
			image = (ImageView) row.findViewById(R.id.image_url);
			spinner = (ProgressBar) row.findViewById(R.id.progress);
			row_image = (LinearLayout) row.findViewById(R.id.row_image);
		}
		
		void populateFrom3(Collection_content r) {

			titlecontent.setText(r.gettitle());
			Log.d(LOG_TAG,"titlecontent="+r.gettitle());

			imageLoader.displayImage(r.getimage(), image, displayOptions, new ImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {
					spinner.setVisibility(View.VISIBLE);
				}
				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					spinner.setVisibility(View.GONE);
				}
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					spinner.setVisibility(View.GONE);
				}
				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					spinner.setVisibility(View.GONE);
				}

			});
		}	
	}


	private AdapterView.OnItemClickListener onListClick3=new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent,
				View view, int position,
				long id) {
				current_content=model_content.get(position);	
				Log.d(LOG_TAG,"onListClick-"+current_content.getcontent_id());
				String top_id = current_content.getcategory_id();
				String top_title = current_content.gettitle();
				slotAction.CreatePluginPage(top_id, "content", top_title, "", "");
				finish();
		}
	};


	@SuppressWarnings("deprecation")
	public static DisplayImageOptions getDisplayImageOptions()
	{
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}

	public void onBackPressed() {
		super.onBackPressed(); 
		this.finish();

	}

	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();
		Helper_Content.close();	
		Helper_Contentcategory.close();
	}
}
