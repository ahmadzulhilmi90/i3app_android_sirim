package com.m3tech.content;

import org.json.JSONException;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.collection.Collection_content;
import com.m3tech.comments.Comment;
import com.m3tech.comments.CommentList;
import com.m3tech.customer.Customer_MainPage;
import com.m3tech.favorite.Favorite;
import com.m3tech.header.Header;
import com.m3tech.web.WebBrowser;
import com.m3tech.widget.Plugin_Content_T2_Widget;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class Plugin_Content_T2 extends Plugin_Content_T2_Widget {

	private static final String LOG_TAG = "PLUGIN_CONTENT_T2";
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();
	content_Adapter adapter_content=null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LOG(LOG_TAG,"Class",LOG_TAG);
		
		Intent j = getIntent();
		cont_cat_name = j.getStringExtra("cont_cat_name");
		cont_category_id =  j.getStringExtra("cont_category_id");
		list_template = j.getStringExtra("list_template");
		plugin_id = j.getStringExtra("plugin_id");
		allow_favorite =  j.getStringExtra("allow_favorite");
		allow_comments =  j.getStringExtra("allow_comments");
		
		Log.d(LOG_TAG,"cont_cat_name="+ cont_cat_name);	
		Log.d(LOG_TAG,"cont_category_id="+ cont_category_id);	
		Log.d(LOG_TAG,"cuid ="+ cuid);
		Log.d(LOG_TAG,"allow_favorite="+ allow_favorite);
		Log.d(LOG_TAG,"allow_comments="+ allow_comments);
		Log.d(LOG_TAG,"colorcode="+ colorcode);

		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome,img_back,img_home,texttitlebar, cont_cat_name,"");

		/*** Call Function ***/
		onGetTopID(cont_category_id);
		
		GetContentList Task = new GetContentList();
		Task.execute(new String[] { cont_category_id });
		
	}
	
	/*** GET TOP ID ***/
	public void onGetTopID(String cont_category_id){
		
		Log.d(LOG_TAG, "cat id : "+ cont_category_id);
		Cursor d = Helper_Contentcategory.getByCatId(cont_category_id);
		Log.d(LOG_TAG, "d.getCount()=" + d.getCount());
	
		if (d.moveToLast() != false) {
			topid = Helper_Contentcategory.gettop_id(d).toString();
			Log.d(LOG_TAG, "topid == " + topid);
		}
		d.close();
		
		favorite = new Favorite(context,topid);
		comment = new Comment(context,topid);
	}


	/**** start function get content list ****/
	private class GetContentList extends AsyncTask<String, Void, String> {

		String cont_cat_id;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			cont_cat_id = arg0[0];
			try{

				adapter_content=new content_Adapter();
				adapter_content.clear();
				Log.d(LOG_TAG,"cont_cat_id="+cont_cat_id);

				content = Helper_Content.getByCategoryId(cont_cat_id);
				Log.d(LOG_TAG, "c=" + content.getCount());
				if (content.moveToLast() != false) {
					// Get from DB
					content.moveToFirst();
					do {
						current_content=new Collection_content();
						current_content.setcontent_id(Helper_Content.getcontent_id(content).toString());
						current_content.settitle(Helper_Content.gettitle(content).toString());
						current_content.setimage(Helper_Content.getimage(content).toString());
						current_content.setthumbnail(Helper_Content.getthumbnail(content).toString());
						current_content.setdetails(Helper_Content.getdetails(content).toString());
						current_content.setlink_url(Helper_Content.getlink_url(content).toString());
						adapter_content.add(current_content);
					} while (content.moveToNext());

				}

				content.close();

				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetContentList-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				
				if (result.equals("SUCCESS")){
					listview.setAdapter(adapter_content);			
					listview.setOnItemClickListener(onListClick3);	

				}else{
					Toast.makeText(Plugin_Content_T2.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "Getcontentlist-onPostExecute-Error:" + t.getMessage(), t);
			}
		}
	}

	class content_Adapter extends ArrayAdapter<Collection_content> {
		content_Adapter() {
			super(Plugin_Content_T2.this, R.layout.plugin_content_template2_row, model_content);
		}

		public View getView(int position, View convertView,
				ViewGroup parent) {
			View row=convertView;
			NotificationHolder3 holder=null;

			if (row==null) {													
				LayoutInflater inflater=getLayoutInflater();

				row=inflater.inflate(R.layout.plugin_content_template2_row, parent, false);
				holder=new NotificationHolder3(row);
				row.setTag(holder);
			} else {
				holder=(NotificationHolder3)row.getTag();
			}

			try {
				holder.populateFrom3(model_content.get(position));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return(row);
		}
	}


	class NotificationHolder3 {
		@SuppressWarnings("unused")
		private View row=null;
		private TextView titlecontent=null;
		private ImageView image =null;
		private ProgressBar spinner = null;
		private ImageView img_fav = null;
		private ImageView img_comment = null;
		private TextView text_total_fav = null;
		private TextView text_total_comment = null;
		private LinearLayout layout_fav_comment = null;

		NotificationHolder3(View row) {
			this.row=row;	

			titlecontent=(TextView) row.findViewById(R.id.text);
			image = (ImageView) row.findViewById(R.id.image_url);
			spinner = (ProgressBar) row.findViewById(R.id.progress);
			
			/*** layout fav and comment declaration ***/
			
			img_fav = (ImageView)row.findViewById(R.id.img_fav);
			img_comment = (ImageView)row.findViewById(R.id.img_comment);
			text_total_fav = (TextView)row.findViewById(R.id.text_total_fav);
			text_total_comment = (TextView)row.findViewById(R.id.text_total_comment);
			layout_fav_comment = (LinearLayout)row.findViewById(R.id.layout_fav_comment);

		}
		
		void populateFrom3(final Collection_content r) throws JSONException {

			String colorC = colorcode.replace("#", "#BB");
			Log.d(LOG_TAG,"colorC="+colorC);
			titlecontent.setBackgroundColor(Color.parseColor(colorC));
			titlecontent.setText(r.gettitle());
			Log.d(LOG_TAG,"titlecontent="+r.gettitle());
			
			/*** Check Allow Favorite and Comments ***/
			onCheckAllowFavoriteAndComments(img_comment,img_fav,text_total_comment,text_total_fav,layout_fav_comment);
			
			/*** Set Total Comment ***/
			
			if(text_total_comment.getVisibility() == View.VISIBLE){
				Log.d(LOG_TAG,"Array Counter Comment : " + comment.onReturnTotalComment(r.getcontent_id().toString()));
				String total = comment.onReturnTotalComment(r.getcontent_id().toString());
				
				if(total.equals("")|| total.equals(null) || total.equals("0") || total.equals("null")){
					text_total_comment.setText("0");
				}else{
					text_total_comment.setText(total);
				}
			}
			
			/*** Set Total Favorite ***/
			
			if(text_total_fav.getVisibility() == View.VISIBLE){
				
				try {
					String total = favorite.onReturnTotalFavorite(r.getcontent_id().replaceAll("\\s+",""));
					if(total.equals("0")){
						text_total_fav.setText("0");
					}else{
						text_total_fav.setText(total);
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			/*** Set Image Favorite Icon ***/
			
			if(favorite.onFavoriteList(r.getcontent_id())){
				img_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_fav_red));
			}else{
				img_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_fav_grey));
			}
			
			/*** Image Comment onClick ***/
			
			img_comment.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					if(cuid.equals("")){

						final AlertDialog.Builder alert = new AlertDialog.Builder(context);

						alert.setTitle(context.getResources().getString(R.string.login_toproceed));

						alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								
								finish();
								SharedPreferences.Editor editor = settings.edit();
								editor.remove("afterLogin");
								editor.putString("afterLogin","Comment");
								editor.putString("topid_comment",topid);
								editor.putString("cid_comment",r.getcontent_id().toString());
								editor.commit();
								Intent i=new Intent(context.getApplicationContext(), Customer_MainPage.class);
								context.startActivity(i);
								
							}

						});
						alert.show();

					}else{
						Intent toCommentList = new Intent(Plugin_Content_T2.this,CommentList.class);
						toCommentList.putExtra("cid",r.getcontent_id().toString());
						toCommentList.putExtra("topid",topid);
						startActivity(toCommentList);
					}
					
					
				}
			});
			
			/*** Image Favorite onClick ***/
			
			img_fav.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					if(cuid.equals("")){

						final AlertDialog.Builder alert = new AlertDialog.Builder(context);

						alert.setTitle(context.getResources().getString(R.string.login_toproceed));

						alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								
								finish();
								SharedPreferences.Editor editor = settings.edit();
								editor.remove("afterLogin");
								editor.putString("afterLogin","Favorite");
								editor.putString("topid_favorite",topid);
								editor.putString("cid_favorite",r.getcontent_id().toString());
								editor.commit();
								Intent i=new Intent(context.getApplicationContext(), Customer_MainPage.class);
								context.startActivity(i);
								
							}

						});
						alert.show();

					}else{
						//Call Function Process Favorite
						try{
							String response = favorite.onProcessFavorites(topid,r.getcontent_id().toString());
							
							String[] parts = response.split("-");
							String status = parts[0];
							String desc = parts[1];
							Log.d(LOG_TAG, "status : "+status + " |desc : "+desc);
							
							if(status.equals("1")){
								/*** Call Function ***/
								onGetTopID(cont_category_id);
								
								GetContentList Task = new GetContentList();
								Task.execute(new String[] { cont_category_id });
							}
						}catch(Exception x){
							
						}
						
					}
					
					
				}
			});

			imageLoader.displayImage(r.getimage(), image, displayOptions, new ImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {					
					spinner.setVisibility(View.VISIBLE);
				}
				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					spinner.setVisibility(View.GONE);
				}
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					spinner.setVisibility(View.GONE);
				}
				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					spinner.setVisibility(View.GONE);
				}

			});
		}	
	}


	private AdapterView.OnItemClickListener onListClick3=new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent,
				View view, int position,
				long id) {
			current_content=model_content.get(position);	
			Log.d(LOG_TAG,"onListClick-"+current_content.getcontent_id());
			String link_url = current_content.getlink_url();

			if (plugin_id.equals("20") || plugin_id.equals("21")) {
				Intent i = new Intent(Plugin_Content_T2.this, Plugin_Content_Gallery.class);
				i.putExtra("contentcategorytitle", cont_cat_name);
				i.putExtra("contentid", current_content.getcontent_id());
				i.putExtra("list_template", list_template);
				i.putExtra("plugin_id", plugin_id);
				i.putExtra("contentdetails", current_content.getdetails());
				i.putExtra("contenttitle", current_content.gettitle());

				startActivity(i);
				
			} else if (plugin_id.equals("45") && link_url.length()>14) {
				if (!link_url.startsWith("http://") && !link_url.startsWith("https://")){
					link_url = "http://" + link_url;
				}

				Intent i=new Intent(getApplicationContext(), WebBrowser.class);
				i.putExtra("url", link_url);
				i.putExtra("openweb", "yes");
				i.putExtra("pagetitle", current_content.gettitle());
				startActivity(i);

			} else {			
				
				Intent i=new Intent(Plugin_Content_T2.this, Plugin_Content_Detail.class);
				i.putExtra("contentcategorytitle", cont_cat_name);
				i.putExtra("contentid", current_content.getcontent_id());
				i.putExtra("list_template", list_template);
				i.putExtra("plugin_id", plugin_id);
				i.putExtra("top_id", topid);
				i.putExtra("allow_comments", allow_comments);
				i.putExtra("allow_favorite", allow_favorite);
				i.putExtra("from", "content");
				startActivity(i);
				//finish();
			}



		}
	};

	/*** Check Allow Favorite and Comments ***/
	
	public void onCheckAllowFavoriteAndComments(ImageView img_comment,ImageView img_fav,TextView text_total_comments,TextView text_total_fav,LinearLayout layout_fav_comment){
		
		if(allow_comments.equals("0") && allow_favorite.equals("0")){
			layout_fav_comment.setVisibility(View.GONE);
		}else{
			layout_fav_comment.setVisibility(View.VISIBLE);
		}
		
		if(allow_comments == "1" || allow_comments.equals("1")){
			img_comment.setVisibility(View.VISIBLE);
			text_total_comments.setVisibility(View.VISIBLE);
		}else{
			img_comment.setVisibility(View.INVISIBLE);
			text_total_comments.setVisibility(View.INVISIBLE);
		}
		
		if(allow_favorite == "1" || allow_favorite.equals("1")){
			img_fav.setVisibility(View.VISIBLE);
			text_total_fav.setVisibility(View.VISIBLE);
		}else{
			img_fav.setVisibility(View.INVISIBLE);
			text_total_fav.setVisibility(View.INVISIBLE);
		}
		
	}
	

	@SuppressWarnings("deprecation")
	public static DisplayImageOptions getDisplayImageOptions() {
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}

	public void onBackPressed() {
		super.onBackPressed(); // allows standard use of backbutton for page 1
		this.finish();
	}

	public void onDestroy() {
		Helper_Contentcategory.close();
		Helper_Content.close();	
		Helper_Themeobject.close();
		this.finish();
		super.onStop();
		super.onDestroy();
			
	}
}
