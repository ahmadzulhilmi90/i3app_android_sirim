package com.m3tech.content;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.PlayerStyle;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;
import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.collection.Collection_themeobject;
import com.m3tech.data.Helper_Content;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;
import com.m3tech.tools.MyTagHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

@SuppressWarnings("deprecation")
public class Plugin_Content_Gallery  extends YouTubeBaseActivity implements OnInitializedListener {

	private static final String LOG_TAG = "PLUGIN_CONTENT_GALLERY";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	Helper_Content Helper_Content = null;
	Helper_Themeobject Helper_Themeobject = null;
	Cursor a,c,g;
	Context context = this;
	String app_title,contentcategorytitle,themecolor,colorcode,colorcode1,cont_category_id,contentid,plugin_id;
	String contenttitle,image,details,video_url,imageUri;
	ImageView tabheader;
	String orderno,var1,home_pagename;
	TextView texttitlebar,text_title;
	SharedPreferences settings;
	ProgressBar spinner;
	String app_user,app_db;
	//Progress
	static ProgressDialog progressDialog;

	Gallery gallery;
	Runnable runnable;
	boolean rungallery = true, pausegallery=false;
	Handler handler;
	private int PicPosition;

	List<Collection_themeobject> galleryData=new ArrayList<Collection_themeobject>();
	Collection_themeobject current_list=null;	

	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();

	SlotAction slotAction = null;
	private YouTubePlayerView playerView;
	@SuppressWarnings("unused")
	private YouTubePlayer player;
	Header Header = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING CONTENT DETAIL ");
		setContentView(R.layout.plugin_content_gallery);

		slotAction = new SlotAction(this);
		Helper_Themeobject = new Helper_Themeobject(this);
		Helper_Content = new Helper_Content(this);
		settings = this.getSharedPreferences(PREFS_NAME, 0);
		home_pagename = settings.getString("home_pagename","");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		colorcode1 = settings.getString("colorcode1", "");
		app_user = context.getResources().getString(R.string.app_user);
		app_db = context.getResources().getString(R.string.app_db);

		Intent j = getIntent();
		contentcategorytitle = j.getStringExtra("contentcategorytitle");
		contenttitle = j.getStringExtra("contenttitle");
		cont_category_id = j.getStringExtra("cont_category_id");
		plugin_id = j.getStringExtra("plugin_id");
		contentid =  j.getStringExtra("contentid");
		details =  j.getStringExtra("contentdetails");
		
		
		/*** layout linear declaration *****/

		tabheader = (ImageView) findViewById(R.id.tabheader);
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome,img_back,img_home,texttitlebar, contentcategorytitle,"");

		gallery = (Gallery) findViewById(R.id.gallery1);   
		text_title = (TextView) findViewById(R.id.text_title);
		text_title.setText(contenttitle);
		text_title.setTextColor(Color.parseColor(colorcode1));
		
		playerView = (YouTubePlayerView) findViewById(R.id.video);


		if(!details.isEmpty() && !details.equals("null")){ //***content detail

			LinearLayout layout_content = (LinearLayout) findViewById(R.id.layout_content);
			layout_content.setVisibility(View.VISIBLE);

			TextView content = (TextView) findViewById(R.id.content);
			//Spanned text = Html.fromHtml(details);
			Spanned text = Html.fromHtml(details, null, new MyTagHandler());
			content.setText(text);
			
		}

		if (plugin_id.equals("21")) {
			gallery.setVisibility(View.GONE);
			GetVideoDetails Task = new GetVideoDetails();
			Task.execute(new String[] { contentid });
		} else {
			int dps = 420;
			final float scale = this.getResources().getDisplayMetrics().density;
			int pixels = (int) (dps * scale + 0.5f);
			
			RelativeLayout toppart = (RelativeLayout) findViewById(R.id.toppart);
			toppart.getLayoutParams().height= pixels;

			GetGalleryData Task = new GetGalleryData();
			Task.execute(new String[] { contentid });

		}
	}

	/****  function get GetGalleryData  *****/
	public class GetGalleryData extends AsyncTask<String, Void, String> {
		int handledelay = 10000;
		String cont_id;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//progressDialog = ProgressDialog.show(Theme1.this, "", getResources().getString(R.string.please_wait));
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			try{

				cont_id = arg0[0];

				//c = Helper_Content.getByCategoryId(getResources().getString(R.string.cat_photo_gallery));
				c = Helper_Content.getContentGallery(cont_id);
				Log.d(LOG_TAG, "Loading Gallery.. " + cont_id + ": " + c.getCount());

				for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
					current_list=new Collection_themeobject();
					current_list.setobject_value(Helper_Content.getimage(c).toString());

					galleryData.add(current_list);

				}

				response="SUCCESS";

				//progressDialog.dismiss();	
			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetGalleryData-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				/***** gallery function ****************/
				if (galleryData.size()>0) {
					gallery.setAdapter(new ImageAdapter(Plugin_Content_Gallery.this));
					if (galleryData.size()==1) {
						Log.d(LOG_TAG, "Gallery: NO RUNNABLE");
						gallery.setSelection(0); 

					} else {
						runnable = new Runnable() {	
							public void run() {	
								if (PicPosition >=  galleryData.size()) {           
									PicPosition=0; //start again 
								}

								if (rungallery) {
									if (pausegallery) {
										Log.d(LOG_TAG, "pausegallery:"+ handledelay);
										pausegallery = false;
									} else {
										gallery.setSelection(PicPosition);
										gallery.setOnTouchListener(new OnTouchListener() {
										    @Override
										    public boolean onTouch(View v, MotionEvent event) {
										    	pausegallery=true;
										        return false;
										    }
										});
									
									}
									PicPosition = gallery.getSelectedItemPosition()+1;  
								}										
								handler.postDelayed(runnable, handledelay);											
							}
						};

						handler = new Handler();
						handler.postDelayed(runnable, handledelay);
					} 
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "GetGalleryData-onPostExecute-Error:" + t.getMessage(), t);

			}
		}

	}


	/****  gallery image *********/	
	public class ImageAdapter extends BaseAdapter {
		private Context context;

		public ImageAdapter(Context c) {
			context = c;
		}

		// ---returns the number of images---
		public int getCount() {

			if(galleryData.toArray().length > 1){
				return galleryData.toArray().length;
			}else{
				return 1;
			}
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		// ---returns an ImageView view---
		public View getView(final int position, View convertView, ViewGroup parent) {
			ImageView imageView = new ImageView(context);
			final ProgressBar spinner = (ProgressBar) findViewById(R.id.progressbar);

			String imgurl=null;
			imgurl=galleryData.get(position).getobject_value();
			imageUri = slotAction.getImageURI(imgurl);			


			String draw = "drawable://";
			if (imageUri.toLowerCase().contains(draw.toLowerCase())) {
				imageUri = imageUri.replace("drawable://", "");
				imageView.setImageResource(Integer.parseInt(imageUri));
				spinner.setVisibility(View.GONE);
			} else {

				imageLoader.displayImage(imageUri,  imageView, displayOptions, new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {
						spinner.setVisibility(View.VISIBLE);
					}
					@Override
					public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
						spinner.setVisibility(View.GONE);
					}
					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
						spinner.setVisibility(View.GONE);
					}
					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						spinner.setVisibility(View.GONE);
					}
				}); 	

			}

			imageView.setLayoutParams(new Gallery.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);

			return imageView;
		}
	}

	private class GetVideoDetails extends AsyncTask<String, Void, String> {

		String cont_id;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Plugin_Content_Gallery.this, "",
					getResources().getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;

			cont_id = arg0[0];
			try {


				c = Helper_Content.getByContID(cont_id);
				Log.d(LOG_TAG, "c.getCount()=" + c.getCount());

				if (c.moveToLast() != false) {
					details = Helper_Content.getdetails(c).toString();
					video_url = Helper_Content.getvideo_url(c).toString();
				}

				c.close();
				response = "SUCCESS";
				// Log.d(LOG_TAG,"responseBody="+ responseBody);

			} catch (Throwable t) {
				response = "FAILED";
				Log.e(LOG_TAG,
						"GetSchedule-doInBackground-Error:" + t.getMessage(), t);
			}
			return response;
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				if (result.equals("SUCCESS")) {

					if (c.getCount()>0) {

						if(!video_url.isEmpty() && !video_url.equals("null")){  //***video detail
							PlayYoutubeVideo();

						}
					}


				} else {
					Toast.makeText(
							Plugin_Content_Gallery.this,
							getResources().getString(
									R.string.please_retry_again),
									Toast.LENGTH_LONG).show();
				}

				Plugin_Content_Gallery.progressDialog.dismiss();

			} catch (Throwable t) {
				Log.e(LOG_TAG,
						"GetVideoDetails-onPostExecute-Error:" + t.getMessage(), t);
				Plugin_Content_Gallery.progressDialog.dismiss();
			}
		}

	}

	public void PlayYoutubeVideo() {
		playerView.initialize(getResources().getString(R.string.api_key), this);
		playerView.setVisibility(View.VISIBLE);
	}



	@Override
	public void onInitializationFailure(Provider arg0,
			YouTubeInitializationResult arg1) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
		this.player = player;
		player.setPlayerStyle(PlayerStyle.MINIMAL);
		if (!wasRestored) {
			Uri myUri = Uri.parse(video_url);
			String videoId = myUri.getQueryParameter("v");
			Log.d(LOG_TAG, "video_url: " + video_url);
			Log.d(LOG_TAG, "videoId: " + videoId);
			
			if (videoId==null) {
				String[] segments = myUri.getPath().split("/");
				videoId = segments[segments.length-1];
				Log.d(LOG_TAG, "videoId: " + videoId);
			}
			
			if (videoId!=null) {
				player.cueVideo(videoId);
			}
		}
	}

	protected YouTubePlayer.Provider getYouTubePlayerProvider() {
		return playerView;
	}





	public static DisplayImageOptions getDisplayImageOptions()
	{
		return new DisplayImageOptions.Builder()
		//	.showImageOnLoading(R.drawable.ic_stub)
		//.showImageForEmptyUri(R.drawable.ic_empty)
		//	.showImageOnFail(R.drawable.ic_error)

		//.showStubImage(R.drawable.loading)

		.cacheInMemory(true)
		.cacheOnDisc(true)


		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();


	}


	@Override
    protected void onPause() {
		rungallery = false;
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		rungallery = true;
	}


	public void onBackPressed() {

		super.onBackPressed(); // allows standard use of backbutton for page 1
		this.finish();

		overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right);
	}

	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();


		Helper_Content.close();
	}

}
