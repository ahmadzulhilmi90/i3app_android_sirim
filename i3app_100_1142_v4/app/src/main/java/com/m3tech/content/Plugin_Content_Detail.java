package com.m3tech.content;

import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.comments.Comment;
import com.m3tech.comments.CommentList;
import com.m3tech.customer.Customer_MainPage;
import com.m3tech.favorite.Favorite;
import com.m3tech.header.Header;
import com.m3tech.tools.MyTagHandler;
import com.m3tech.web.WebBrowser;
import com.m3tech.widget.Plugin_Content_Detail_Widget;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class Plugin_Content_Detail extends Plugin_Content_Detail_Widget {

	private static final String LOG_TAG = "PLUGIN_CONTENT_DETAIL";
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING CONTENT DETAIL ");

		Intent j = getIntent();
		contentcategorytitle = j.getStringExtra("contentcategorytitle");
		list_template = j.getStringExtra("list_template");
		contentid =  j.getStringExtra("contentid");
		plugin_id =  j.getStringExtra("plugin_id");
		topid =  j.getStringExtra("top_id");
		allow_favorite =  j.getStringExtra("allow_favorite");
		allow_comments =  j.getStringExtra("allow_comments");
		from =  j.getStringExtra("from");
		
		Log.d(LOG_TAG,"plugin_id="+ plugin_id);	
		Log.d(LOG_TAG,"content_id="+ contentid);	
		Log.d(LOG_TAG,"topid="+ topid);
		Log.d(LOG_TAG,"allow_favorite="+ allow_favorite);
		Log.d(LOG_TAG,"allow_comments="+ allow_comments);
		Log.d(LOG_TAG,"contentcategorytitle="+ contentcategorytitle);	

		/*** onClick *****/
		img_comment.setOnClickListener(onGetCommentList);
		img_fav.setOnClickListener(onSetFavorite);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome,img_back,img_home,texttitlebar, contentcategorytitle,"");
		
		GetContentDetail Task = new GetContentDetail();
		Task.execute(new String[] { contentid });
		
		comment = new Comment(this,topid);
		favorite = new Favorite(this,topid);
		
		/*** Call Function Favorite & Comment ***/
		try {
			onCheckAllowFavoriteAndComments(allow_comments,allow_favorite);
			onGetTotalComment();
			onGetTotalFavorite();
			onGetFavoriteItem();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*** Check Allow Favorite and Comments ***/
	
	public void onCheckAllowFavoriteAndComments(String allow_comments,String allow_favorite){
		
		if(allow_comments == "0" || allow_comments.equals("0")){
			layout_fav_comment.setVisibility(View.GONE);
		}else{ 
			layout_fav_comment.setVisibility(View.VISIBLE);
		}
		
		if(allow_comments == "1" || allow_comments.equals("1")){
			img_comment.setVisibility(View.VISIBLE);
			text_total_comment.setVisibility(View.VISIBLE);
		}else{
			img_comment.setVisibility(View.INVISIBLE);
			text_total_comment.setVisibility(View.INVISIBLE);
		}
		
		if(allow_favorite == "1" || allow_favorite.equals("1")){
			img_fav.setVisibility(View.VISIBLE);
			text_total_fav.setVisibility(View.VISIBLE);
		}else{
			img_fav.setVisibility(View.INVISIBLE);
			text_total_fav.setVisibility(View.INVISIBLE);
		}
		
	}
	
	
	/*** Call onGetFavoriteItem TextView ***/
	
	public void onGetFavoriteItem() throws JSONException{
		
		if(favorite.onFavoriteList(contentid)){
			img_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_fav_red));
		}else{
			img_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_fav_grey));
		}
	}
	
	/*** Call onGetTotalFavorite TextView ***/
	
	public void onGetTotalFavorite() throws JSONException{
		
		String total = favorite.onReturnTotalFavorite(contentid);
		if(total.equals("0")||total.equals("")||total.equals(null)){
			text_total_fav.setText("0");
		}else{
			text_total_fav.setText(total);
		}
	}
	
	
	/*** Call onGetTotalComment TextView ***/
	
	public void onGetTotalComment() throws JSONException{
		
		String total = comment.onReturnTotalComment(contentid);
		if(total.equals("")|| total.equals(null)){
			text_total_comment.setText("0");
		}else{
			text_total_comment.setText(total);
		}
	}
	
	
	/***  OnClick Image Comment ***/
	
	private View.OnClickListener onGetCommentList=new View.OnClickListener() {
		public void onClick(View v) {
			try{
				
				if(cuid.equals("")){

					final AlertDialog.Builder alert = new AlertDialog.Builder(context);

					alert.setTitle(context.getResources().getString(R.string.login_toproceed));

					alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							
							SharedPreferences.Editor editor = settings.edit();
							editor.remove("afterLogin");
							editor.putString("afterLogin","Comment");
							editor.putString("topid_comment",topid);
							editor.putString("cid_comment",contentid);
							editor.commit();
							Intent i=new Intent(Plugin_Content_Detail.this, Customer_MainPage.class);
							startActivity(i);
							finish();
							
						}

					});
					alert.show();

				}else{
					
					Intent toCommentList = new Intent(Plugin_Content_Detail.this,CommentList.class);
					toCommentList.putExtra("cid",contentid);
					toCommentList.putExtra("topid",topid);
					startActivity(toCommentList);
				}

			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onCommentList-Error:" + t.getMessage(), t); 

			}
		}

	};
	
	
	/*** OnClick Image Favorite ***/
	
	private View.OnClickListener onSetFavorite=new View.OnClickListener() {
		
		public void onClick(View v) {
			try{
				
				if(cuid.equals("")){

					final AlertDialog.Builder alert = new AlertDialog.Builder(context);

					alert.setTitle(context.getResources().getString(R.string.login_toproceed));

					alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							
							SharedPreferences.Editor editor = settings.edit();
							editor.remove("afterLogin");
							editor.putString("afterLogin","Favorite");
							editor.putString("topid_favorite",topid);
							editor.putString("cid_favorite",contentid);
							editor.commit();
							Intent i=new Intent(context.getApplicationContext(), Customer_MainPage.class);
							context.startActivity(i);
							finish();
							
						}

					});
					alert.show();

				}else{
					
					//Call ProcessFavorite API
					try{
						String response = favorite.onProcessFavorites(topid,contentid);
						Log.d(LOG_TAG,"response : " + response);
						
						if(response.length()>0){
							
							String[] parts = response.split("-");
							String status = parts[0];
							String desc = parts[1];
							Log.d(LOG_TAG, "status : "+status + " |desc : "+desc);
							
							if(status.equals("1") && desc.equals("Successfully Add")){
								img_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_fav_red));
								int text = Integer.parseInt(text_total_fav.getText().toString())+1;
								text_total_fav.setText(String.valueOf(text));
							}else if(status.equals("1") && desc.equals("Successfully Delete")){
								img_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_fav_grey));
								int text = Integer.parseInt(text_total_fav.getText().toString())-1;
								text_total_fav.setText(String.valueOf(text));
							}else{
								img_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_fav_grey));
							}
							
							
						}else{
							Log.d(LOG_TAG, "Image Favorite onClick Error");
						}
						
					}catch(Exception x){
						
					}
				}

			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onCommentList-Error:" + t.getMessage(), t); 

			}
		}

	};


	private class GetContentDetail extends AsyncTask<String, Void, String> {

		String cont_id;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Plugin_Content_Detail.this, "",
					getResources().getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String cat_id = "";

			cont_id = arg0[0];
			try {

				c = Helper_Content.getByContID(cont_id);
				Log.d(LOG_TAG, "c.getCount()=" + c.getCount());

				if (c.moveToLast() != false) {

					title = Helper_Content.gettitle(c).toString();
					image = Helper_Content.getimage(c).toString();
					thumbnail = Helper_Content.getthumbnail(c).toString();
					call_title = Helper_Content.getcall_title(c).toString();
					telephone = Helper_Content.gettelephone(c).toString();
					email_title = Helper_Content.getemail_title(c).toString();				
					email = Helper_Content.getemail(c).toString();
					details = Helper_Content.getdetails(c).toString();
					link_title = Helper_Content.getlink_title(c).toString();
					link_url = Helper_Content.getlink_url(c).toString();
					map_title = Helper_Content.getmap_title(c).toString();
					
					map_lat = Helper_Content.getmap_lat(c).toString();
					map_lon = Helper_Content.getmap_lon(c).toString();
					video_title = Helper_Content.getvideo_title(c).toString();
					video_url = Helper_Content.getvideo_url(c).toString();
					fb_title = Helper_Content.getfb_title(c).toString();
					fb_url = Helper_Content.getfb_url(c).toString();
					orderno = Helper_Content.getorderno(c).toString();
					price_title2 = Helper_Content.getprice_title(c).toString();
					price = Helper_Content.getprice(c).toString();
					
					var1_title = Helper_Content.getvar1_title(c).toString();
					var1 = Helper_Content.getvar1(c).toString();
					var2_title = Helper_Content.getvar2_title(c).toString();
					var2 = Helper_Content.getvar2(c).toString();
					var3_title = Helper_Content.getvar3_title(c).toString();
					var3 = Helper_Content.getvar3(c).toString();
					var4_title = Helper_Content.getvar4_title(c).toString();
					var4 = Helper_Content.getvar4(c).toString();
					var5_title = Helper_Content.getvar5_title(c).toString();
					var5 = Helper_Content.getvar5(c).toString();

					cat_id = Helper_Content.getcategory_id(c).toString();
					

				}

				c.close();
				
				if (plugin_id.length()==0) {
				
					d = Helper_Contentcategory.getByCatId(cat_id);
					Log.d(LOG_TAG, "d.getCount()=" + d.getCount());

					if (d.moveToLast() != false) {
						plugin_id = Helper_Contentcategory.getplugin_id(d).toString();
					}
				
					d.close();
				}
				
				if (plugin_id.equals("20") || plugin_id.equals("21")) {
					response = "GALLERY";
					
				} else {
					response = "SUCCESS";
				}

			} catch (Throwable t) {
				response = "FAILED";
				Log.e(LOG_TAG,
						"GetSchedule-doInBackground-Error:" + t.getMessage(), t);
			}
			return response;
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				if (result.equals("GALLERY")) {
					Intent i = new Intent(Plugin_Content_Detail.this, Plugin_Content_Gallery.class);
					i.putExtra("contentcategorytitle", contentcategorytitle);
					i.putExtra("contentid", contentid);
					i.putExtra("list_template", list_template);
					i.putExtra("plugin_id", plugin_id);
					i.putExtra("contentdetails", details);
					i.putExtra("contenttitle", title);

					startActivity(i);
					finish();
					
				} else if (result.equals("SUCCESS")) {
					

					LinearLayout layout_body = (LinearLayout) findViewById(R.id.layout_body);
					LinearLayout layout_call = (LinearLayout) findViewById(R.id.layout_call);
					LinearLayout layout_email = (LinearLayout) findViewById(R.id.layout_email);
					LinearLayout layout_content = (LinearLayout) findViewById(R.id.layout_content);
					LinearLayout layout_ketiga = (LinearLayout) findViewById(R.id.layout_ketiga);
					LinearLayout layout_website = (LinearLayout) findViewById(R.id.layout_website);
					LinearLayout layout_map = (LinearLayout) findViewById(R.id.layout_map);
					LinearLayout layout_video = (LinearLayout) findViewById(R.id.layout_video);
					LinearLayout layout_fb = (LinearLayout) findViewById(R.id.layout_fb);
					LinearLayout layout2column = (LinearLayout) findViewById(R.id.layout2column);
					LinearLayout layout_price = (LinearLayout) findViewById(R.id.layout_price);
					LinearLayout layout_var1 = (LinearLayout) findViewById(R.id.layout_var1);
					LinearLayout layout_var2 = (LinearLayout) findViewById(R.id.layout_var2);
					LinearLayout layout_var3 = (LinearLayout) findViewById(R.id.layout_var3);
					LinearLayout layout_var4 = (LinearLayout) findViewById(R.id.layout_var4);
					LinearLayout layout_var5 = (LinearLayout) findViewById(R.id.layout_var5);
					

					if (c.getCount()>0) {

						layout_body.setVisibility(View.VISIBLE);	

						if(!image.isEmpty() && !image.equals("null")){  //***image detail

							LinearLayout layoutImage = (LinearLayout) findViewById(R.id.layoutImage);
							layoutImage.setVisibility(View.VISIBLE);
							ImageView image_url = (ImageView) findViewById(R.id.image_url);
							spinner = (ProgressBar) findViewById(R.id.progressbar);

							imageLoader.displayImage(image, image_url, displayOptions, new ImageLoadingListener() {
								@Override
								public void onLoadingStarted(String imageUri, View view) {
									spinner.setVisibility(View.VISIBLE);
								}
								@Override
								public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
									spinner.setVisibility(View.GONE);

								}
								@Override
								public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
									spinner.setVisibility(View.GONE);
								}
								@Override
								public void onLoadingCancelled(String imageUri, View view) {
									spinner.setVisibility(View.GONE);
								}

							});

						}



						if(!title.isEmpty() && !title.equals("null")){
							int myNum = 0;

							try {
							    myNum = Integer.parseInt(list_template);
							} catch(NumberFormatException nfe) {
							   //System.out.println("Could not parse " + nfe);
							} 
							
							if (myNum>3) {
								TextView title1 = (TextView) findViewById(R.id.title1);
								title1.setVisibility(View.VISIBLE);
								title1.setTextColor(Color.parseColor(colorcode1));
								title1.setText(title);
							} else {
								TextView title2 = (TextView) findViewById(R.id.title2);
								title2.setVisibility(View.VISIBLE);
								title2.setBackgroundColor(Color.parseColor(colorcode1));
								title2.setText(title);								
							}
						}



						if(!telephone.isEmpty() || !email.isEmpty()){

							layout2column.setVisibility(View.VISIBLE);

							if(!telephone.isEmpty() && !telephone.equals("null")){  //***call

								layout_call.setVisibility(View.VISIBLE);
								TextView texttitlecall = (TextView) findViewById(R.id.texttitlecall);
								TextView textcall = (TextView) findViewById(R.id.textcall);

								texttitlecall.setText(call_title);
								textcall.setText(telephone);

								layout_call.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View view) {

										Uri uri = Uri.fromParts("tel", telephone, null);
										Intent callIntent = new Intent(Intent.ACTION_CALL, uri);
										callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
										startActivity(callIntent);
									}

								});


							}

							if(!email.isEmpty() && !email.equals("null")){ //*** email

								layout_email.setVisibility(View.VISIBLE);
								TextView texttitleemail = (TextView) findViewById(R.id.texttitleemail);
								TextView textemail = (TextView) findViewById(R.id.textemail);

								texttitleemail.setText(email_title);
								textemail.setText(email);

								layout_email.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View view) {

										Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
										emailIntent.setType("plain/text");
										emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
												new String[] { email });
										emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
										emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
												Html.fromHtml(""));

										startActivity(Intent.createChooser(emailIntent, "Send mail..."));
									}

								});
							}

						}


						if(!price.equals("null")&&!price.isEmpty()&&!price.equals("0.00")){ //***price

							layout_price.setVisibility(View.VISIBLE);
							TextView pricetext = (TextView) findViewById(R.id.price);
							
							Float pricef= Float.parseFloat(price);						
							String price_total = String.format(" %s %,.2f", price_title2, pricef);
							pricetext.setText(price_total);
						}


						if(!var1.isEmpty() && !var1.equals("null")){ //***layout var 1
							layout_var1.setVisibility(View.VISIBLE);
							TextView var1titletxt = (TextView) findViewById(R.id.var1title);
							if (var1_title.length()>0) {
								var1titletxt.setText(var1_title);
							} else {
								var1titletxt.setVisibility(View.GONE);
							}
							TextView var1txt = (TextView) findViewById(R.id.var1);
							var1txt.setText(var1);
						}

						if(!var2.isEmpty() && !var2.equals("null")){ //***layout var 2
							layout_var2.setVisibility(View.VISIBLE);
							TextView var2titletxt = (TextView) findViewById(R.id.var2title);
							if (var2_title.length()>0) {
								var2titletxt.setText(var2_title);
							} else {
								var2titletxt.setVisibility(View.GONE);
							}
							TextView var2txt = (TextView) findViewById(R.id.var2);
							var2txt.setText(var2);
						}


						if(!var3.isEmpty() && !var3.equals("null")){ //***layout var 3
							layout_var3.setVisibility(View.VISIBLE);
							TextView var3titletxt = (TextView) findViewById(R.id.var3title);
							if (var3_title.length()>0) {
								var3titletxt.setText(var3_title);
							} else {
								var3titletxt.setVisibility(View.GONE);
							}
							TextView var3txt = (TextView) findViewById(R.id.var3);
							var3txt.setText(var3);
						}

						if(!var4.isEmpty() && !var4.equals("null")){ //***layout var 4
							layout_var4.setVisibility(View.VISIBLE);
							TextView var4titletxt = (TextView) findViewById(R.id.var4title);
							if (var4_title.length()>0) {
								var4titletxt.setText(var4_title);
							} else {
								var4titletxt.setVisibility(View.GONE);
							}
							TextView var4txt = (TextView) findViewById(R.id.var4);
							var4txt.setText(var4);
						}

						if(!var5.isEmpty() && !var5.equals("null")){ //***layout var 5
							layout_var5.setVisibility(View.VISIBLE);
							TextView var5titletxt = (TextView) findViewById(R.id.var5title);
							if (var5_title.length()>0) {
								var5titletxt.setText(var5_title);
							} else {
								var5titletxt.setVisibility(View.GONE);
							}
							TextView var5txt = (TextView) findViewById(R.id.var5);
							var5txt.setText(var5);
						}

						if(!details.isEmpty() && !details.equals("null")){ //***content detail

							layout_content.setVisibility(View.VISIBLE);
							TextView content = (TextView) findViewById(R.id.content);
							//Spanned text = Html.fromHtml(details);
							Spanned text = Html.fromHtml(details, null, new MyTagHandler());
							content.setText(text);
						}

						if(!link_url.isEmpty() || (!map_lat.isEmpty() && !map_lon.isEmpty()) || !video_url.isEmpty() || !fb_url.isEmpty()){

							layout_ketiga.setVisibility(View.VISIBLE);

							if(!link_url.isEmpty() && !link_url.equals("null")){  //*** website

								String vUrl = link_url;
								vUrl = vUrl.replace("http://", "");
								vUrl = vUrl.replace("https://", "");
								
								layout_website.setVisibility(View.VISIBLE);
								TextView webtitle = (TextView) findViewById(R.id.webtitle);
								webtitle.setText(link_title);

								TextView website = (TextView) findViewById(R.id.website);
								website.setText(vUrl);

								if((map_lat.length() < 1 && map_lon.length() < 1) && video_url.length() < 1 && fb_url.length()< 1){

									ImageView lineweb = (ImageView) findViewById(R.id.lineweb);
									lineweb.setVisibility(View.GONE);

								}


								layout_website.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View view) {

										if (!link_url.startsWith("http://") && !link_url.startsWith("https://")){
											link_url = "http://" + link_url;
										}

										Intent i=new Intent(getApplicationContext(), WebBrowser.class);
										i.putExtra("url", link_url);
										i.putExtra("openweb", "yes");
										i.putExtra("pagetitle", title);
										startActivity(i);
									}

								});
							}

							if(!map_lat.isEmpty() && !map_lon.isEmpty() && !map_lat.equals("null") && !map_lon.equals("null")){ //*** map

								layout_map.setVisibility(View.VISIBLE);
								TextView maptitle = (TextView) findViewById(R.id.maptitle);
								maptitle.setText(map_title);

								TextView map_lat_text = (TextView) findViewById(R.id.map_lat);
								map_lat_text.setText(map_lat+", "+map_lon);

								if(video_url.length() <1 && fb_url.length()<1){

									ImageView linemap = (ImageView) findViewById(R.id.linemap);
									linemap.setVisibility(View.GONE);

								}

								layout_map.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View view) {

										//String uri = "geo:" + map_lat + ","+ map_lon;
										//startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri)));
										
										Intent i=new Intent(Plugin_Content_Detail.this, Plugin_Content_Maps.class);
										i.putExtra("contentcategorytitle", contentcategorytitle);
										i.putExtra("contentid", contentid);
										i.putExtra("list_template", list_template);
										i.putExtra("plugin_id", plugin_id);
										startActivity(i);

									}

								});

							}

							if(!video_url.isEmpty() && !video_url.equals("null")){ //*** video

								String vUrl = video_url;
								vUrl = vUrl.replace("http://www.", "");
								vUrl = vUrl.replace("http://", "");
								vUrl = vUrl.replace("https://www.", "");
								vUrl = vUrl.replace("https://", "");

								layout_video.setVisibility(View.VISIBLE);
								TextView videotext = (TextView) findViewById(R.id.videotext);
								videotext.setText(video_title);

								TextView video = (TextView) findViewById(R.id.video);
								video.setText(vUrl);

								if( fb_url.length()<1){

									ImageView linevideo = (ImageView) findViewById(R.id.linevideo);
									linevideo.setVisibility(View.GONE);

								}

								layout_video.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View view) {

										if (!video_url.startsWith("http://") && !video_url.startsWith("https://")){
											video_url = "http://" + video_url;
										}

										Intent i=new Intent(getApplicationContext(), WebBrowser.class);
										i.putExtra("url", video_url);
										i.putExtra("openweb", "yes");
										i.putExtra("pagetitle", title);
										startActivity(i);
									}

								});


							}

							if(!fb_url.isEmpty() && !fb_url.equals("null")){  //*** fb

								String vUrl = fb_url;
								vUrl = vUrl.replace("http://www.", "");
								vUrl = vUrl.replace("http://", "");
								vUrl = vUrl.replace("https://www.", "");
								vUrl = vUrl.replace("https://", "");

								layout_fb.setVisibility(View.VISIBLE);
								TextView fbtext = (TextView) findViewById(R.id.fbtext);
								fbtext.setText(fb_title);

								TextView fb = (TextView) findViewById(R.id.fb);
								fb.setText(vUrl);

								layout_fb.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View view) {

										if (!fb_url.startsWith("http://") && !fb_url.startsWith("https://")){
											fb_url = "http://" + fb_url;
										}

										if (!link_url.startsWith("http://") && !link_url.startsWith("https://")){
											link_url = "http://" + link_url;
										}

										Intent i=new Intent(getApplicationContext(), WebBrowser.class);
										i.putExtra("url", fb_url);
										i.putExtra("openweb", "yes");
										i.putExtra("pagetitle", title);
										startActivity(i);
									}

								});


							}


						}
						

					} else {
						//TO DO GO BACK TO LISTING
						Toast.makeText(
								Plugin_Content_Detail.this,
								getResources().getString(
										R.string.record_not_found),
										Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(
							Plugin_Content_Detail.this,
							getResources().getString(
									R.string.please_retry_again),
									Toast.LENGTH_LONG).show();
				}

				Plugin_Content_Detail.progressDialog.dismiss();

			} catch (Throwable t) {
				Log.e(LOG_TAG,
						"Getwhatsnew-onPostExecute-Error:" + t.getMessage(), t);
				Plugin_Content_Detail.progressDialog.dismiss();
			}
		}
	}


	@SuppressWarnings("deprecation")
	public static DisplayImageOptions getDisplayImageOptions()
	{
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}


	public void onBackPressed() {
		this.finish();
	}

	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();
		Helper_Content.close();
	}

}
