package com.m3tech.content;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.data.Helper_Content;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/*
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;*/


public class Plugin_Content_Maps extends FragmentActivity implements LocationListener {

	private static final String LOG_TAG = "PLUGIN_CONTENT Maps";
	public static final String PREFS_NAME = "MyPrefsFile";
	Helper_Content Helper_Content = null;
	Helper_Themeobject Helper_Themeobject = null;
	Cursor a, c, g;
	Context context = this;
	String app_title, themecolor, colorcode, colorcode1, list_template, contentcategorytitle, contentid,plugin_id;
	String title, map_lat, map_lon, var1, home_pagename;
	LinearLayout tabback,tabhome;
	String app_user,app_db;
	TextView texttitlebar, texthome;
	SharedPreferences settings;
	ProgressBar spinner;
	ImageView tabheader;

	Location currentLocation;
	LocationManager locationManager;
	String currentlat, currentlong, strdistance;
	//GoogleMap mMap;
	public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
	public static final long MIN_TIME_BW_UPDATES = 1000*60*1; // 1 minute

	SlotAction slotAction = null;
	Header Header = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG, "ENTERING CONTENT DETAIL ");
		setContentView(R.layout.plugin_content_maps);

		slotAction = new SlotAction(this);
		Helper_Themeobject = new Helper_Themeobject(this);
		Helper_Content = new Helper_Content(this);
		settings = this.getSharedPreferences(PREFS_NAME, 0);
		home_pagename = settings.getString("home_pagename", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		colorcode1 = settings.getString("colorcode1", "");
		app_user = context.getResources().getString(R.string.app_user);
		app_db = context.getResources().getString(R.string.app_db);

		Intent j = getIntent();
		contentcategorytitle = j.getStringExtra("contentcategorytitle");
		contentid = j.getStringExtra("contentid");
		list_template = j.getStringExtra("list_template");
		plugin_id = j.getStringExtra("plugin_id");

		Log.d(LOG_TAG, "contentcategorytitle=" + contentcategorytitle);

		/*** layout linear declaration *****/
tabheader=(ImageView)findViewById(R.id.tabheader);
		texttitlebar=(TextView)findViewById(R.id.texttitlebar);
		/**** home & back button *****/
		tabback=(LinearLayout)findViewById(R.id.tabback);
		tabhome=(LinearLayout)findViewById(R.id.tabhome);
		ImageView img_back=(ImageView)findViewById(R.id.back);
		ImageView img_home=(ImageView)findViewById(R.id.home);

		/*** Setting Header ***/
		Header=new Header(this,tabheader,tabback,tabhome,img_back,img_home,texttitlebar,contentcategorytitle,"");

		map_lat="";
		map_lon="";
		currentlat="";
		currentlong="";

		GetContentDetail Task=new GetContentDetail();
		Task.execute(new String[]{contentid});

		//InitializeLocationManager();

		}

		/*void InitializeLocationManager(){

		currentLocation=getLocation();

		if(currentLocation!=null){
		currentlat=String.valueOf(currentLocation.getLatitude());
		currentlong=String.valueOf(currentLocation.getLongitude());

		if(map_lat.length()>0&&map_lon.length()>0){
		strdistance=slotAction.getStrDistance(currentlat,currentlong,map_lat,map_lon);

		TextView kmaway=(TextView)findViewById(R.id.kmaway);
		if(strdistance.length()>0){
		kmaway.setText(strdistance+" "+getResources().getString(R.string.away));
		}else{
		kmaway.setVisibility(View.GONE);
		}
		}

		}

		}*/

/*
public Location getLocation() {
		Location location = null;
		locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);

		try {
			locationManager = (LocationManager) Plugin_Content_Maps.this.getSystemService(LOCATION_SERVICE);

			// getting GPS status
			boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// getting network status
			boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSEnabled && !isNetworkEnabled) {
				// no network provider is enabled
				Toast.makeText(Plugin_Content_Maps.this,getResources().getString(R.string.please_turnon_gps), Toast.LENGTH_LONG).show();

			} else {
				if (isNetworkEnabled) {
					locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
					Log.d("Network", "Network Enabled");

					if (locationManager != null) {
						location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					}
				}
				// if GPS Enabled get lat/long using GPS Services
				if (isGPSEnabled) {
					if (location == null) {
						locationManager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER,
								MIN_TIME_BW_UPDATES,
								MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
						Log.d("GPS", "GPS Enabled");
						if (locationManager != null) {
							location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return location;
	}


	private void setUpMap(String clat, String clong, String locname) {
		Log.d(LOG_TAG, "Entering setUpMap: " + locname);

		if (mMap == null) {
			FragmentManager myFM = Plugin_Content_Maps.this.getSupportFragmentManager();
			final SupportMapFragment myMAPF = (SupportMapFragment) myFM.findFragmentById(R.id.maps);

			mMap = myMAPF.getMap();
		}

		double pointLat = Double.parseDouble(clat);
		double pointLon = Double.parseDouble(clong);

		LatLng newLoc = new LatLng(pointLat, pointLon);

		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newLoc, 17));
		mMap.addMarker(new MarkerOptions().position(new LatLng(pointLat, pointLon)).title(locname));

	}
*/

	private class GetContentDetail extends AsyncTask<String, Void, String> {

		String cont_id;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;

			cont_id = arg0[0];
			try {

				c = Helper_Content.getByContID(cont_id);
				Log.d(LOG_TAG, "c.getCount()=" + c.getCount());

				if (c.moveToLast() != false) {

					title = Helper_Content.gettitle(c).toString();
					var1 = Helper_Content.getvar1(c).toString();

					Log.d(LOG_TAG, "Title: " + title);
					strdistance = "";

					if (Helper_Content.getmap_lat(c).toString().length() > 0
							&& Helper_Content.getmap_lon(c).toString().length() > 0) {
						map_lat = Helper_Content.getmap_lat(c).toString();
						map_lon = Helper_Content.getmap_lon(c).toString();
						
						if (currentlat.length()>0 && currentlong.length()>0) {
							strdistance = slotAction.getStrDistance(currentlat, currentlong, map_lat, map_lon);												
							Log.d(LOG_TAG, "Distance: " + strdistance);
						}
					}

				}

				c.close();
				response = "SUCCESS";
				// Log.d(LOG_TAG,"responseBody="+ responseBody);

			} catch (Throwable t) {
				response = "FAILED";
				Log.e(LOG_TAG,
						"GetSchedule-doInBackground-Error:" + t.getMessage(), t);
			}
			return response;
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				if (result.equals("SUCCESS")) {

					if (c.getCount() > 0) {

						if (!title.isEmpty() && !title.equals("null")) {

							TextView text = (TextView) findViewById(R.id.texttitle);
							text.setText(title);
						}

						TextView var1txt = (TextView) findViewById(R.id.textarea);
						if (!var1.isEmpty() && !var1.equals("null")) { // ***layout
							// var 1
							var1txt.setText(var1);
						} else {
							var1txt.setVisibility(View.GONE);
						}

						LinearLayout btninfo = (LinearLayout) findViewById(R.id.btninfo);
						btninfo.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View view) {

								Intent i=new Intent(Plugin_Content_Maps.this, Plugin_Content_Detail.class);
								i.putExtra("contentcategorytitle", contentcategorytitle);
								i.putExtra("contentid", contentid);
								i.putExtra("list_template", list_template);
								i.putExtra("plugin_id", plugin_id);
								i.putExtra("from", "map");
								startActivity(i);
							}

						});

						LinearLayout layout_map = (LinearLayout) findViewById(R.id.layout_maps);
						LinearLayout tabbottom = (LinearLayout) findViewById(R.id.tabbottom);
						LinearLayout btndirection = (LinearLayout) findViewById(R.id.btndirection);

						if (map_lat.length()>0 && map_lon.length()>0) { // *** map

							TextView kmaway = (TextView) findViewById(R.id.kmaway);
							if (strdistance.length()>0) {
								kmaway.setText(strdistance + " " + getResources().getString(R.string.away));
							} else {
								kmaway.setVisibility(View.GONE);
							}

							btndirection.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View view) {

									String uri = "geo:" + map_lat + "," + map_lon;
									startActivity(new Intent( android.content.Intent.ACTION_VIEW, Uri.parse(uri)));
								}

							});

							//setUpMap(map_lat,map_lon,title);

						} else {
							layout_map.setVisibility(View.GONE);
							tabbottom.setVisibility(View.GONE);
						}

					} else {

						Toast.makeText(
								Plugin_Content_Maps.this,
								getResources().getString(
										R.string.record_not_found),
										Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(
							Plugin_Content_Maps.this,
							getResources().getString(
									R.string.please_retry_again),
									Toast.LENGTH_LONG).show();
				}

			} catch (Throwable t) {
				Log.e(LOG_TAG,
						"Getwhatsnew-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}

	

	@SuppressWarnings("deprecation")
	public static DisplayImageOptions getDisplayImageOptions() {
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true).cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565).build();

	}

	public void onBackPressed() {

		super.onBackPressed(); // allows standard use of backbutton for page 1
		this.finish();

	}

	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();

		Helper_Content.close();
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

}
