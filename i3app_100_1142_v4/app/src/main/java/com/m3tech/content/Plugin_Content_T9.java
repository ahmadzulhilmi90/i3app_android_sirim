package com.m3tech.content;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.collection.Collection_content;
import com.m3tech.header.Header;
import com.m3tech.web.WebBrowser;
import com.m3tech.widget.Plugin_Content_T9_Widget;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class Plugin_Content_T9 extends Plugin_Content_T9_Widget {

	private static final String LOG_TAG = "PLUGIN_CONTENT_T9";
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();
	content_Adapter adapter_content=null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING CONTENT T9 ");
		
		Intent j = getIntent();
		cont_cat_name = j.getStringExtra("cont_cat_name");
		cont_category_id =  j.getStringExtra("cont_category_id");
		list_template = j.getStringExtra("list_template");
		plugin_id = j.getStringExtra("plugin_id");
		allow_favorite =  j.getStringExtra("allow_favorite");
		allow_comments =  j.getStringExtra("allow_comments");
		
		Log.d(LOG_TAG,"cont_cat_name="+ cont_cat_name);	
		Log.d(LOG_TAG,"cont_category_id="+ cont_category_id);	
		Log.d(LOG_TAG,"customerID ="+ cuid);
		Log.d(LOG_TAG,"allow_favorite="+ allow_favorite);
		Log.d(LOG_TAG,"allow_comments="+ allow_comments);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, cont_cat_name,"");
		
		rowLayout_full = R.layout.plugin_content_template9_full;
		rowLayout_list = R.layout.plugin_content_template9_list;
		
		/*** Call Function ***/
		onGetTopID(cont_category_id);

		GetContentList Task = new GetContentList();
		Task.execute(new String[] { cont_category_id }); 

	}
	
	/*** GET TOP ID ***/
	public void onGetTopID(String cont_category_id){
		
		Log.d(LOG_TAG, "cat id : "+ cont_category_id);
		Cursor d = Helper_Contentcategory.getByCatId(cont_category_id);
		Log.d(LOG_TAG, "d.getCount()=" + d.getCount());
	
		if (d.moveToLast() != false) {
			topid = Helper_Contentcategory.gettop_id(d).toString();
			Log.d(LOG_TAG, "topid == " + topid);
		}
		d.close();
	}


	/**** start function get content list ****/
	private class GetContentList extends AsyncTask<String, Void, String> {

		String cont_cat_id;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			cont_cat_id = arg0[0];
			try{

				adapter_content=new content_Adapter();
				adapter_content.clear();
				Log.d(LOG_TAG,"cont_cat_id="+cont_cat_id);
				String curr_date = "0";
				String prev_date = "0";
				String row_layout = "full";

				content = Helper_Content.getByTimeline(cont_cat_id);
				Log.d(LOG_TAG, "c=" + content.getCount());
				if (content.moveToLast() != false) {
					// Get from DB
					content.moveToFirst();
					do {

						current_content=new Collection_content();

						curr_date = Helper_Content.getdate_updated(content).toString();						
						current_content.setdate_updated(curr_date);
						
						curr_date = curr_date.substring(0, 10); 
						if (curr_date.equals(prev_date)) {
							row_layout = "list";							
						} else {
							row_layout = "full";
						}
						current_content.setcontent_id(Helper_Content.getcontent_id(content).toString());
						current_content.settitle(Helper_Content.gettitle(content).toString());
						current_content.setimage(Helper_Content.getimage(content).toString());
						current_content.setthumbnail(Helper_Content.getthumbnail(content).toString());
						current_content.setdetails(Helper_Content.getdetails(content).toString());	
						current_content.setlink_url(Helper_Content.getlink_url(content).toString());

						//using field var5 to set the row layout
						current_content.setvar5(row_layout);
						
						adapter_content.add(current_content);
						prev_date = curr_date;

					} while (content.moveToNext());

				}

				content.close();

				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetContentList-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				ListView listView2 = (ListView) findViewById(R.id.listcontent);

				if (result.equals("SUCCESS")){

					listView2.setAdapter(adapter_content);			
					listView2.setOnItemClickListener(onListClick3);	

				}else{
					Toast.makeText(Plugin_Content_T9.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "Getcontentlist-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}


	class content_Adapter extends ArrayAdapter<Collection_content> {
		content_Adapter() {
			super(Plugin_Content_T9.this, rowLayout_list, model_content);
		}


		@Override
		public int getItemViewType(int position) {
			String row_layout = model_content.get(position).getvar5();
			int row_lay = rowLayout_full;
			
			if (row_layout.equals("full")) {
				row_lay = rowLayout_full;
			} else {
				row_lay = rowLayout_list;
			}

			Log.d(LOG_TAG,"getItemViewType (" + position + ") : " + row_layout);

		    return row_lay;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View row=convertView;
			NotificationHolder holder=null;

			if (row==null) {													
				LayoutInflater inflater=getLayoutInflater();
				
				row = inflater.inflate(getItemViewType(position), null);
				holder = new NotificationHolder(row);
				row.setTag(holder);
			}
			else {
				holder=(NotificationHolder)row.getTag();
			}

			holder.populateFrom3(model_content.get(position));

			return(row);
		}
	}


	class NotificationHolder {
		private TextView contenttitle=null;
		private TextView textdetails=null;
		private TextView contentdate=null;
		private ImageView image =null;
		private ProgressBar spinner = null;

		NotificationHolder(View row) {
			contenttitle = (TextView) row.findViewById(R.id.text);
			textdetails  = (TextView) row.findViewById(R.id.textdetails);
			contentdate  = (TextView) row.findViewById(R.id.contentdate);
			image = (ImageView) row.findViewById(R.id.image_url);
			spinner = (ProgressBar) row.findViewById(R.id.progress);			
		}
		
		void populateFrom3(Collection_content r) {

			Spanned details = Html.fromHtml(r.getdetails());
			String txtdetails = slotAction.getWords(details.toString(),25);
			
			contenttitle.setText(r.gettitle());
			textdetails.setText(txtdetails);
						
			String date = r.getdate_updated();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
			Date newDate = null;
			try {
				newDate = sdf.parse(date);
			}catch(Exception ex){
				ex.printStackTrace();
			}

			SimpleDateFormat formatter = new SimpleDateFormat("EEE, MMM dd, yyyy hh:mm a");
			String msgdate = formatter.format(newDate);
			
			contentdate.setText(msgdate);
			
			Log.d(LOG_TAG,r.getvar5() + " : " + r.gettitle());

			imageLoader.displayImage(r.getimage(), image, displayOptions, new ImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {
					spinner.setVisibility(View.VISIBLE);
				}
				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					spinner.setVisibility(View.GONE);
				}
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					spinner.setVisibility(View.GONE);
				}
				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					spinner.setVisibility(View.GONE);
				}

			});
		}	
	}


	private AdapterView.OnItemClickListener onListClick3=new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent,
				View view, int position,
				long id) {
			current_content=model_content.get(position);	
			Log.d(LOG_TAG,"onListClick-"+current_content.getcontent_id());
			String link_url = current_content.getlink_url();

			if (plugin_id.equals("20") || plugin_id.equals("21")) {
				Intent i = new Intent(Plugin_Content_T9.this, Plugin_Content_Gallery.class);
				i.putExtra("contentcategorytitle", cont_cat_name);
				i.putExtra("contentid", current_content.getcontent_id());
				i.putExtra("list_template", list_template);
				i.putExtra("plugin_id", plugin_id);
				i.putExtra("contentdetails", current_content.getdetails());
				i.putExtra("contenttitle", current_content.gettitle());

				startActivity(i);
				
			} else if (plugin_id.equals("45") && link_url.length()>14) {
				if (!link_url.startsWith("http://") && !link_url.startsWith("https://")){
					link_url = "http://" + link_url;
				}

				Intent i=new Intent(getApplicationContext(), WebBrowser.class);
				i.putExtra("url", link_url);
				i.putExtra("openweb", "yes");
				i.putExtra("pagetitle", current_content.gettitle());
				startActivity(i);

			} else {			
				Intent i=new Intent(Plugin_Content_T9.this, Plugin_Content_Detail.class);
				i.putExtra("contentcategorytitle", cont_cat_name);
				i.putExtra("contentid", current_content.getcontent_id());
				i.putExtra("list_template", list_template);
				i.putExtra("plugin_id", plugin_id);
				i.putExtra("top_id", topid);
				i.putExtra("allow_comments", allow_comments);
				i.putExtra("allow_favorite", allow_favorite);
				i.putExtra("from", "content");
				startActivity(i);
				
			}
		}
	};

	

	@SuppressWarnings("deprecation")
	public static DisplayImageOptions getDisplayImageOptions()
	{
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}

	public void onBackPressed() {
		super.onBackPressed(); // allows standard use of backbutton for page 1
		this.finish();
	}

	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();
		Helper_Content.close();		
	}
}
