package com.m3tech.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.header.Header;
import com.m3tech.widget.WebBrowser_Widget;


@SuppressLint("SetJavaScriptEnabled")
public class WebBrowser extends WebBrowser_Widget {
	private static final String LOG_TAG = "WEB BROWSER";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING WEB BROWSER ");
		
		Intent i = getIntent();
		url = i.getStringExtra("url");
		openweb = i.getStringExtra("openweb");
		pagetitle = i.getStringExtra("pagetitle");
		Log.i(LOG_TAG, "pagetitle: " + pagetitle);
		Log.i(LOG_TAG, "openweb: " + openweb);
		
		
		if (pagetitle.length()==0) {
			pagetitle = app_title;
		}

		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome,img_back,img_home, texttitlebar, pagetitle,"");
		tabbottom.setBackgroundColor(Color.parseColor(colorcode1));
		
		if (openweb.equals("yes")) {
			btnopen.setVisibility(View.VISIBLE);
		} else {
			btnopen.setVisibility(View.GONE);			
		}
		
		if (url != null) {
			
			if(url.contains("mailto:")){
				Log.d(LOG_TAG, "--in--");
				
				String[] parts = url.split("body=");
				String subject = parts[0];
				String body = parts[1];
				Log.d(LOG_TAG, subject + ","+ body);
				
				Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
				emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				emailIntent.setType("plain/text");
				emailIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
				emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{""});
				emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
				emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
				startActivity(emailIntent);
				finish();
				
			}else if (url.toLowerCase().startsWith("http://sms:")) { 
				
				Log.d(LOG_TAG, "http://sms:" + url);
				
				String[] parts = url.split("body=");
				String subject = parts[0];
				String text = parts[1];
				String message = "";
				
				String[] num = subject.split("sms:");
				String phone = num[1];
				phone = phone.replace("?", "");
				
				try {
					message = URLDecoder.decode(text, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Log.d(LOG_TAG, phone + ","+ message);
				
				Intent smsIntent = new Intent(Intent.ACTION_VIEW);
			      
		      smsIntent.setData(Uri.parse("smsto:"));
		      smsIntent.setType("vnd.android-dir/mms-sms");
		      smsIntent.putExtra("address"  , new String (phone));
		      smsIntent.putExtra("sms_body"  , message);
		      startActivity(smsIntent);
		      finish();
			    
				
			}else{
			
				progressbar.setVisibility(View.VISIBLE);
				spinner.setVisibility(View.VISIBLE);
				
				Log.i(LOG_TAG, "Loading url: " + url);
				//final ProgressDialog pd = ProgressDialog.show(this, "", "Loading...", true);
				
	
				WebSettings webViewSettings = webView.getSettings();
				webViewSettings.setAppCacheEnabled(true);
	
				webViewSettings.setJavaScriptEnabled(true);
				webViewSettings.setSupportZoom(true);  
				//webViewSettings.setBuiltInZoomControls(true);
				webViewSettings.setSaveFormData(true);
				webViewSettings.setUseWideViewPort(true);
				webViewSettings.setLoadWithOverviewMode(true);
	
				webView.canGoBack();
				webView.loadUrl(url);
	
				webView.setWebChromeClient(new WebChromeClient() {
	                public void onProgressChanged(WebView view, int progress) {
	        			Log.i(LOG_TAG, "setWebChromeClient - onProgressChanged: " + progress);
	
	                	progressbar.setProgress(progress);
	                    if (progress == 100) {
	                    	progressbar.setVisibility(View.GONE);
	                    	spinner.setVisibility(View.GONE);
	                    	progressbar.setProgress(0);
	                    	if (!openweb.equals("yes")) {
	                			tabbottom.setVisibility(View.GONE);			
	                		}
	                    } else {
	                    	progressbar.setVisibility(View.VISIBLE);
	                    	spinner.setVisibility(View.VISIBLE);
	                    }
	                }
	            });
	
	
				// prevent opening new window
				webView.setWebViewClient(new WebViewClient() {
					@Override
					public boolean shouldOverrideUrlLoading(WebView view, String url) {
						
						if (url.toLowerCase().startsWith("tel:")) { 
							//Log.d(LOG_TAG, "tel:" + url);
			                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url)); 
			                startActivity(intent); 
			                return true;	
						} 
						else if (url.toLowerCase().startsWith("whatsapp:")) { 
							//Log.d(LOG_TAG, "whatsapp:" + url);
							
							String[] parts = url.split("text=");
							String subject = parts[0];
							String text = parts[1];
							String message = "";
							try {
								message = URLDecoder.decode(text, "UTF-8");
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							Log.d(LOG_TAG, subject + ","+ message);
							
							boolean isWhatsappInstalled = whatsappInstalledOrNot("com.whatsapp");
						    if (isWhatsappInstalled) {
						    	final ComponentName name = new ComponentName("com.whatsapp", "com.whatsapp.ContactPicker");
						    	Intent oShareIntent = new Intent();
						    	oShareIntent.setComponent(name);
						    	oShareIntent.setType("text/plain");
						    	oShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, message.toString());
						    	startActivity(oShareIntent);
						    	
						    } else {
						        Toast.makeText(WebBrowser.this, "WhatsApp not Installed",Toast.LENGTH_SHORT).show();
						        Uri uri = Uri.parse("market://details?id=com.whatsapp");
						        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
						        startActivity(goToMarket);
						    }
						    return true;
							
						}
						else if (url.toLowerCase().startsWith("http://sms:")) { 
							
							Log.d(LOG_TAG, "http://sms:" + url);
							
							
							return false;
							
						}else if (url.toLowerCase().startsWith("sms:")) { 
							//Log.d(LOG_TAG, "sms:" + url);
							
							String[] parts = url.split("body=");
							String subject = parts[0];
							String text = parts[1];
							String message = "";
							try {
								message = URLDecoder.decode(text, "UTF-8");
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							Log.d(LOG_TAG, subject + ","+ message);
							
							Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( "sms:" + "" ) ); 
							intent.putExtra( "sms_body", message ); 
							context.startActivity(intent);
							return false;
							
						}/*else if(url.toLowerCase().startsWith("https://www.linkedin.com")) { 
							Log.d(LOG_TAG, "URL Load :" + url);
							//Uri uri = Uri.parse(url);
							//startActivity(new Intent(Intent.ACTION_VIEW, uri));
							return true;
						}*/
						else if (url.toLowerCase().startsWith("https://twitter.com")) { 
							//Log.d(LOG_TAG, "twitter.com:" + url);
							
							String[] parts = url.split("text=");
							String subject = parts[0];
							String text = parts[1];
							String message = "";
							
							try {
								message = URLDecoder.decode(text, "UTF-8");
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							Log.d(LOG_TAG, subject + ","+ message);
							
							boolean isTwitterInstalled = twitterInstalledOrNot("com.twitter.android");
						    if (isTwitterInstalled) {
						    	   Intent intent = new Intent(Intent.ACTION_SEND);
						        intent.setClassName("com.twitter.android", "com.twitter.android.composer.ComposerActivity");
						        intent.setType("text/plain");
						        intent.putExtra(Intent.EXTRA_TEXT, message);
						        startActivity(intent);
						    	
						    } else {
						        Toast.makeText(WebBrowser.this, "Twitter not Installed",Toast.LENGTH_SHORT).show();
						        Uri uri = Uri.parse("market://details?id=com.twitter.android");
						        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
						        startActivity(goToMarket);
						    }
						    progressbar.setVisibility(View.GONE);
	                    	spinner.setVisibility(View.GONE);
						    return true;
			
						}
						else if (url.toLowerCase().startsWith("mailto:")) { 
						
							String message = "";
							try {
								message = URLDecoder.decode(url, "UTF-8");
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							
							String[] parts = message.split("body=");
							String subject = parts[0];
							String getbody = parts[1];
							Log.d(LOG_TAG,"message : "+ message);
							Log.d(LOG_TAG,"subject : "+ subject);
							Log.d(LOG_TAG,"getbody : "+ getbody);
								
						    MailTo mt = MailTo.parse(url);
			                Intent i = newEmailIntent(WebBrowser.this, mt.getTo(), mt.getSubject(), getbody, mt.getCc());
			                startActivity(i);
			                view.reload();
			                return true;
							
						} 
	
						 view.loadUrl(url);
				         return false;
					}
	
					@Override
					public void onPageFinished(WebView view, String url) {
						isPageLoadedComplete = true;
						progressbar.setVisibility(View.GONE);
						spinner.setVisibility(View.GONE);
						super.onPageFinished(view, url);
					}
	
					@Override
					public void onReceivedError(WebView view, int errorCod,String description, String failingUrl) {
						progressbar.setVisibility(View.GONE);
						spinner.setVisibility(View.GONE);
						Toast.makeText(getApplicationContext(), 
								getResources().getString(R.string.MSG_NO_INTERNET), 
								Toast.LENGTH_LONG).show();
					}
					
				});
				
				btnopen.setOnClickListener(onclickopenbutton);
			}
			
		} else {
			progressbar.setVisibility(View.GONE);
			spinner.setVisibility(View.GONE);
		}

	}

	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_DOWN){
            switch(keyCode)
            {
            case KeyEvent.KEYCODE_BACK:
                if(webView.canGoBack()){
                    webView.goBack();
                }else{
                    finish();
                }
                return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }
	
	private View.OnClickListener onclickopenbutton=new View.OnClickListener() {
		public void onClick(View v) {
			try{
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			    startActivity(intent);


			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onclickopenbutton-Error:" + t.getMessage(), t); 

			}
		}

	};
	
	public static Intent newEmailIntent(Context context, String address, String subject, String body, String cc) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_CC, cc);
        intent.setType("message/rfc822");
        return intent;
    }
	
	private boolean whatsappInstalledOrNot(String uri) {
		PackageManager pm = getPackageManager();
		boolean app_installed = false;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}
	
	private boolean twitterInstalledOrNot(String uri) {
		PackageManager pm = getPackageManager();
		boolean app_installed = false;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
        finish();
 	}

	public void onDestroy() {
		webView.destroy();
		this.finish();
		super.onStop();
		super.onDestroy();	
	}


}
