package com.m3tech.clock;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.header.Header;
import com.m3tech.widget.Plugin_Clock_T1_Widget;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class Plugin_Clock_T1 extends Plugin_Clock_T1_Widget implements LocationListener{

	private static final String LOG_TAG = "PLUGIN_CLOCK_T1";
	static ProgressDialog progressDialog;
	SimpleDateFormat curr_dateFormat;
	CountDownTimer countdowntimer;
	boolean isRunning = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LOG(LOG_TAG, "Class", LOG_TAG);

		Intent j = getIntent();
		pagetitle = j.getStringExtra("pagetitle");

		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome,img_back,img_home, texttitlebar,pagetitle, "");
				
		/*** Setting Customer Name ***/
		text_customername.setText("Hi "+customerName);
		
		/*** GetFirstTimeClock ***/
		GetFirstTimeClock();
		
		image_status_in.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				
				/*** Get Data Shared Preferences ***/
				SharedPreferences s = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
				String remark = s.getString("remark","");
				String text_remarks = text_remark.getText().toString();
				
				
				
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {
					
					if(remark.equals("1")){
						
						//if (!Character.isWhitespace(text_remarks.charAt(0))) {
							if(text_remarks.length()>3){

								//if(onCheckCanClockIn(IN,GetCurrentDate())){
									if(onTurnGPS()){
										Toast.makeText(Plugin_Clock_T1.this,getResources().getString(R.string.please_turnon_gps), Toast.LENGTH_LONG).show();
										final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
										startActivity(intent);
									}else{
									
										/*** Call LocationManager ***/
										InitializeLocationManager();
										
										/*** Call Function ***/
										GetInOut(IN);
									}
//								}else{
//									Toast.makeText(Plugin_Clock_T1.this,"User can Clock In once per day. Thank you", Toast.LENGTH_LONG).show();
//								}
							}else{
								Toast.makeText(Plugin_Clock_T1.this,"Late clock in, please enter remarks.", Toast.LENGTH_LONG).show();
							}
//						}else{
//							Toast.makeText(Plugin_Clock_T1.this,"Late clock in, please enter remarks.", Toast.LENGTH_LONG).show();
//						}
						
						
					}else{
						//if(onCheckCanClockIn(IN,GetCurrentDate())){
							if(onTurnGPS()){
								Toast.makeText(Plugin_Clock_T1.this,getResources().getString(R.string.please_turnon_gps), Toast.LENGTH_LONG).show();
								final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
								startActivity(intent);
							}else{
							
								/*** Call LocationManager ***/
								InitializeLocationManager();
								
								/*** Call Function ***/
								GetInOut(IN);
							}
//						}else{
//							Toast.makeText(Plugin_Clock_T1.this,"User can Clock In once per day. Thank you", Toast.LENGTH_LONG).show();
//						}
					}
				}else{
					Toast.makeText(Plugin_Clock_T1.this, getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_SHORT).show();
				}
				
			}
		});

		image_status_out.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {
				
					String user_clock_in = text_timer_in.getText().toString();
					if(user_clock_in.length() > 0){
						
						/*** If Total hour less than 8, user cant clock out ***/
						
						long curr_total_hour = twoDatesBetweenTime(user_clock_in);
						int hours   = (int) ((curr_total_hour / (1000*60*60)) % 24);
						
						/*** Get Data Shared Preferences ***/
						SharedPreferences s = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
						String max = s.getString("max","");
						//String remark = s.getString("remark","");
						String text_remarks = text_remark.getText().toString();
						
						if(hours<Integer.parseInt(max)){
							if(text_remarks.length()>3){
								if(onTurnGPS()){
									Toast.makeText(Plugin_Clock_T1.this,getResources().getString(R.string.please_turnon_gps), Toast.LENGTH_LONG).show();
									final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
									startActivity(intent);
								}else{
								
									/*** Call LocationManager ***/
									InitializeLocationManager();
									
									/*** Call Function ***/
									GetInOut(OUT);
								}
							}else{
								Toast.makeText(Plugin_Clock_T1.this,"Early clock out, please enter remarks.", Toast.LENGTH_SHORT).show();
							}
						}else{
							if(onTurnGPS()){
								Toast.makeText(Plugin_Clock_T1.this,getResources().getString(R.string.please_turnon_gps), Toast.LENGTH_LONG).show();
								final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
								startActivity(intent);
							}else{
							
								/*** Call LocationManager ***/
								InitializeLocationManager();
								
								/*** Call Function ***/
								GetInOut(OUT);
							}
						}
					}
				}else{
					Toast.makeText(Plugin_Clock_T1.this, getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_SHORT).show();
				}
			}

		});
	}
	
	/*** Check If user already clock in ***/
	
	public void GetFirstTimeClock(){
		Boolean Network_status = isNetworkAvailable();
		if (Network_status == true) {
			/*** Call Function GetClockInStatus ***/
			
			GetClockInStatus task = new GetClockInStatus();
			task.execute(new String[] {IN});
			
		}else{
			Toast.makeText(Plugin_Clock_T1.this, getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_SHORT).show();
			onBackPressed();
		}
	}
	
	public void InitialiseDataInOut(){
		
		if(getDate(IN).length()>0){
			
			/*** Setting text_status ***/
			text_status.setText("Clock In");
			
			/*** Setting Button ***/
			image_status_in.setVisibility(View.GONE);
			image_status_out.setVisibility(View.VISIBLE);
			
			/*** Setting text_desc ***/
			text_desc.setText("Successfully Clock-In.");
			
		}else{
			
			/*** Setting text_status ***/
			text_status.setText("Please Clock In");
			
			/*** Setting Button ***/
			image_status_in.setVisibility(View.VISIBLE);
			image_status_out.setVisibility(View.GONE);
		}
		text_timer_in.setText(getDate(IN));
		text_timer_out.setText(getDate(OUT));
		
		/*** Setting Timer ****/
		try {
			onStartTimer();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*** Check Only 1 Clock In Per day ***/
	
	public boolean onCheckCanClockIn(String type,String log){
		Cursor c = Helper_ClockLog.getByLog(type,log);
		int total = c.getCount();
		c.close();
		if(total>0){
			if(total > 10){
				/*** Delete data clock log if > 10 ***/
				Helper_ClockLog.DeleteAllExceptToday(log);
			}
			return false;
		}else{
			return true;
		}
	}
	
	public void onWidgetTurnOn(String clockin_date) throws ParseException{
		
		/*** Setting Timer ***/
		if(clockin_date.length()>0){
			text_timer_in.setText(clockin_date);
			Helper_Clock.insert(IN, clockin_date, GetCurrentDate());
			Helper_ClockLog.insert(IN, clockin_date, GetCurrentDate());
		}else{
			text_timer_in.setText(GetCurrentDateTime());
			Helper_Clock.insert(IN, GetCurrentDateTime(), GetCurrentDate());
			Helper_ClockLog.insert(IN, GetCurrentDateTime(), GetCurrentDate());
		}
		text_timer_out.setText("");
		
		/*** Setting text_status ***/
		text_status.setText("Clock In");
		
		/*** Setting Button ***/
		image_status_in.setVisibility(View.GONE);
		image_status_out.setVisibility(View.VISIBLE);
		
		/*** Setting Timer ****/
		onStartTimer();
	}
	
	public void onWidgetTurnOff(){
		/*** Setting text_status ***/
		text_status.setText("Please Clock In");
		
		/*** Setting text_timer_out ***/
		text_timer_out.setText(GetCurrentDateTime());
		
		/*** Setting Helper_Clock ***/
		Helper_Clock.Delete();
		
		/*** Setting Button ***/
		image_status_in.setVisibility(View.VISIBLE);
		image_status_out.setVisibility(View.GONE);
		
		/*** Setting Timer ***/
		onStopTimer();
	}
	
	public String getDate(String type){
		String data = "";
		Cursor c = Helper_Clock.getByType(type);
		int total = c.getCount();
		if(total>0){
			if (c.moveToLast() != false ){
		 		//Get from DB
		 		c.moveToFirst();
		 		do {
		 			data = Helper_Clock.getdata(c).toString();
		 		} while (c.moveToNext());		
		 	}
		 	c.close();
		 	return data;
		}else{
			return data = "";
		}
	}
	
	public String GetCurrentDateTime(){
		 curr_dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 String formattedDate = curr_dateFormat.format(new Date()).toString();
		 currentDateTime = formattedDate;
		 return String.valueOf(currentDateTime);
	}
	
	public String GetCustCurrentDateTime(){
		 SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
		 String formattedDate = dateFormat.format(new Date()).toString();
		 currentDateTime = formattedDate;
		 Log.d(LOG_TAG, "currentDateTime : " + currentDateTime);
		 return String.valueOf(currentDateTime);
	}
	
	public String GetCurrentDate(){
		 SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		 String formattedDate = dateFormat.format(new Date()).toString();
		 String currentDateTime = formattedDate;
		 Log.d(LOG_TAG, "currentDateTime : " + currentDateTime);
		 return String.valueOf(currentDateTime);
	}
	
	public boolean onTurnGPS(){
		locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
		locationManager = (LocationManager) Plugin_Clock_T1.this.getSystemService(LOCATION_SERVICE);
		// getting GPS status
		boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (!isGPSEnabled) {
			return true;
		} else {
			return false;
		}
	}
	
	public void InitializeLocationManager() {
		/*currentLocation = getLocation();
		if (currentLocation != null) {
			currentlat = String.valueOf(currentLocation.getLatitude());
			currentlong = String.valueOf(currentLocation.getLongitude());
		} 
		Log.d(LOG_TAG,"currentlat="+currentlat);
		Log.d(LOG_TAG,"currentlong="+currentlong);*/
	}
	
	/*public Location getLocation() {
		Location location = null;
		locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);

		try {
			locationManager = (LocationManager) Plugin_Clock_T1.this.getSystemService(LOCATION_SERVICE);

			// getting GPS status
			boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// getting network status
			boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSEnabled) { // && !isNetworkEnabled
				// no network provider is enabled
				Toast.makeText(Plugin_Clock_T1.this,getResources().getString(R.string.please_turnon_gps), Toast.LENGTH_LONG).show();
				final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);

			} else {
				this.canGetLocation = true;
				if (isNetworkEnabled) {
					locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
					Log.d("Network", "Network Enabled");

					if (locationManager != null) {
						location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					}
				}
				// if GPS Enabled get lat/long using GPS Services
				if (isGPSEnabled) {
					if (location == null) {
						locationManager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER,
								MIN_TIME_BW_UPDATES,
								MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
						Log.d("GPS", "GPS Enabled");
						if (locationManager != null) {
							location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return location;
	}
	*/
	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String arg0) {
		// 
		//http://app.getsnapps.com/api/plugin.php?mode=verifyclock&db=105&userid=232&udid=80f20d8b2968a9f8&cdate=20160303051308&deviceid=354017050761895&tag=android&cid=215&lat=&lng=&status=in
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub

	}
	
	/*** Call Clock_IN_OUT ***/
	public void GetInOut(String id){
		GetInOutTASK task = new GetInOutTASK();
		task.execute(new String[] {id});
	}
	
	
	/*** User :: GetClockInStatus ***/
	
	private class GetClockInStatus extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Plugin_Clock_T1.this, "", getResources()
					.getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String status = null,desc = null,currentstatus=null,remark="",max="";
			
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {

					currentstatus = arg0[0];
					
					/*** LOG DATA ***/
					Log.d(LOG_TAG, "cdate :"+String.valueOf(GetCustCurrentDateTime()));
					Log.d(LOG_TAG, "macid :"+imei);
					Log.d(LOG_TAG, "lat :"+currentlat);
					Log.d(LOG_TAG, "lng :"+currentlong);
					Log.d(LOG_TAG, "status :"+currentstatus);
					
					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

					settings = getSharedPreferences(PREFS_NAME, 0);
					String strURL = getResources().getString(R.string.CLOCK_IN_OUT_API)
							+ "mode="+ URLEncoder.encode("verifyclock", "UTF-8")
							+ "&db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&cdate=" + URLEncoder.encode(String.valueOf(GetCustCurrentDateTime()), "UTF-8")
							+ "&deviceid=" + URLEncoder.encode(imei, "UTF-8")
							+ "&tag=" + URLEncoder.encode("android", "UTF-8")
							+ "&cid=" + URLEncoder.encode(cuid, "UTF-8")
							+ "&lat=" + URLEncoder.encode(currentlat, "UTF-8")
							+ "&lng=" + URLEncoder.encode(currentlong, "UTF-8")
							+ "&status=" + URLEncoder.encode(currentstatus, "UTF-8");
					

					HttpPost httppost = new HttpPost(strURL);
					Log.d(LOG_TAG, "onGetStatusLockIn-url:" + strURL);

					String responseBody = null;
					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					responseBody = httpclient.execute(httppost, responseHandler);

					Log.d(LOG_TAG, "onGetStatusLockIn-responseBody:" + responseBody);

					JSONArray StatusJSONArray = new JSONArray(responseBody);
					Log.d(LOG_TAG, "StatusJSONArray.length():"+ StatusJSONArray.length());
					if (StatusJSONArray.length() > 0) {
						status = StatusJSONArray.getJSONObject(0).getString("status").toString();
						desc = StatusJSONArray.getJSONObject(0).getString("desc").toString();
						remark = StatusJSONArray.getJSONObject(0).getString("remark").toString();
						max = StatusJSONArray.getJSONObject(0).getString("max").toString();
						Log.d(LOG_TAG, "remark:"+remark+" max:"+max);
					}
					
				}else{
					status = "-1";
					desc = getResources().getString(R.string.MSG_NO_INTERNET);
				}

			} catch (Throwable t) {
				status = "-1";
				desc = getResources().getString(R.string.please_retry_again);
				Log.e(LOG_TAG, "doInBackground-Error:" + t.getMessage(), t);
			}
			progressDialog.dismiss();
			return status+"~"+desc+"~"+remark+"~"+max;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			String[] parts = result.split("~");
			String status = parts[0];
			String desc = parts[1];
			String remark = parts[2];
			String max = parts[3];
			
			Log.d(LOG_TAG, "result: "+status+"|"+desc);
			
			if(status.equals("1") && !desc.equals("Successful")){
				
				Helper_Clock.Delete();
				Helper_ClockLog.Delete();
				
				/*** Setting text_desc ***/
				text_desc.setText("Successful Clock In.");
				
				try {
					onWidgetTurnOn(desc);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				/*** Save Value remark and max to SharedPreferences ***/
				SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
				editor.remove("remark");
				editor.remove("max");
				editor.putString("remark", remark);
				editor.putString("max", max);
				editor.commit();
					
			}else if(status.equals("1") && desc.equals("Successful")){ 
				text_desc.setText("Please Clock In.");
				
				/*** Save Value remark and max to SharedPreferences ***/
				SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
				editor.remove("remark");
				editor.remove("max");
				editor.putString("remark", remark);
				editor.putString("max", max);
				editor.commit();
				
			}else{
				text_desc.setText(desc);
			}

			/*** Call LocationManager ***/
			InitializeLocationManager();
			
			Plugin_Clock_T1.progressDialog.dismiss();
		}
	}
	

	private class GetInOutTASK extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Plugin_Clock_T1.this, "", getResources()
					.getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String status = null,desc = null,currentstatus=null;
			
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {

					currentstatus = arg0[0];
					
					/*** LOG DATA ***/
					Log.d(LOG_TAG, "cdate :"+String.valueOf(GetCustCurrentDateTime()));
					Log.d(LOG_TAG, "macid :"+imei);
					Log.d(LOG_TAG, "lat :"+currentlat);
					Log.d(LOG_TAG, "lng :"+currentlong);
					Log.d(LOG_TAG, "status :"+currentstatus);
					
					String remarks = text_remark.getText().toString();
					if(remarks.length()>0){
						remarks = text_remark.getText().toString();
					}else{
						remarks = "";
					}
					
					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

					settings = getSharedPreferences(PREFS_NAME, 0);
					String strURL = getResources().getString(R.string.CLOCK_IN_OUT_API)
							+ "mode="+ URLEncoder.encode("clock", "UTF-8")
							+ "&db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&cdate=" + URLEncoder.encode(String.valueOf(GetCustCurrentDateTime()), "UTF-8")
							+ "&deviceid=" + URLEncoder.encode(imei, "UTF-8")
							+ "&tag=" + URLEncoder.encode("android", "UTF-8")
							+ "&cid=" + URLEncoder.encode(cuid, "UTF-8")
							+ "&lat=" + URLEncoder.encode(currentlat, "UTF-8")
							+ "&lng=" + URLEncoder.encode(currentlong, "UTF-8")
							+ "&status=" + URLEncoder.encode(currentstatus, "UTF-8")
							+ "&txt=" + URLEncoder.encode(remarks, "UTF-8");
					

					HttpPost httppost = new HttpPost(strURL);					
					Log.d(LOG_TAG, "onLogin-url:" + strURL);

					String responseBody = null;
					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					responseBody = httpclient.execute(httppost, responseHandler);

					Log.d(LOG_TAG, "onLogin-responseBody:" + responseBody);

					JSONArray StatusJSONArray = new JSONArray(responseBody);
					Log.d(LOG_TAG, "StatusJSONArray.length():"+ StatusJSONArray.length());
					if (StatusJSONArray.length() > 0) {
						status = StatusJSONArray.getJSONObject(0).getString("status").toString();
						desc = StatusJSONArray.getJSONObject(0).getString("desc").toString();
					}
					
				}else{
					status = "-1";
					desc = getResources().getString(R.string.MSG_NO_INTERNET);
				}

			} catch (Throwable t) {
				status = "-1";
				desc = getResources().getString(R.string.please_retry_again);
				Log.e(LOG_TAG, "doInBackground-Error:" + t.getMessage(), t);
			}
			progressDialog.dismiss();
			return status+"~"+desc+"~"+currentstatus;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			String[] parts = result.split("~");
			String status = parts[0];
			String desc = parts[1];
			String currentstatus = parts[2];
			text_desc.setText(desc.toString());
			Log.d(LOG_TAG, "result: "+status+"|"+desc);
			
			if(status.equals("1") || status == "1"){
				if(currentstatus.equals("in")){
					try {
						onWidgetTurnOn("");
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					onWidgetTurnOff();
				}
				text_desc.setText("");
			}
			Plugin_Clock_T1.progressDialog.dismiss();
		}
	}
	
	/**** Call Service Timer 
	 * @throws ParseException ***/
	
	public void onStartTimer() throws ParseException{
		String user_clock_in = text_timer_in.getText().toString();
		if(user_clock_in.length()>0){
			newTimerFunction(user_clock_in);
		}
	}
	
	/*** Function to get mili between two datetime ***/
	public long twoDatesBetweenTime(String oldtime)
    {
        // TODO Auto-generated method stub
        int day = 0;
        int hh = 0;
        int mm = 0;
        int ss = 0;
        Long timeDiff = null;
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date oldDate = dateFormat.parse(oldtime);
            Date cDate = new Date();
            timeDiff = cDate.getTime() - oldDate.getTime();
            day = (int) TimeUnit.MILLISECONDS.toDays(timeDiff);
            hh = (int) (TimeUnit.MILLISECONDS.toHours(timeDiff) - TimeUnit.DAYS.toHours(day));
            mm = (int) (TimeUnit.MILLISECONDS.toMinutes(timeDiff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeDiff)));
            ss = (int) (TimeUnit.MILLISECONDS.toSeconds(timeDiff) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeDiff)));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        Log.d(LOG_TAG, "=" + day + " days " + hh + " hour " + mm + " min " + ss + "seconds" + " | milisecond : "+ timeDiff);
        return timeDiff;
    }
	
	/*** Function Timer ***/
	public void newTimerFunction(final String user_clock_in) {
		Timer t = new Timer();
		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {

						if (isRunning) {

							long initial_value;
							initial_value = twoDatesBetweenTime(user_clock_in);
							int seconds = (int) (initial_value / 1000) % 60;
							int minutes = (int) ((initial_value / (1000 * 60)) % 60);
							int hours = (int) ((initial_value / (1000 * 60 * 60)) % 24);

							/*** Setting if Total Hour mode than 8 ***/
							if (hours >= 8) {
								text_desc.setText("Hardworking Staff!");
							}

							/*** Set total hour to Text Hour ***/
							if (hours == 0) {
								text_total_hour.setText(minutes + " minutes "+ seconds + " seconds");
							} else if (minutes == 0) {
								text_total_hour.setText(seconds + " seconds");
							} else {
								text_total_hour.setText(hours + " hours "+ minutes + " minutes " + seconds+ " seconds");
							}
						}
					}
				});
			}
		};
		t.scheduleAtFixedRate(task, 0, 1000);
	}
	
	public void onStopTimer(){
		isRunning = false;
	}

	public boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}
	
	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
	}

	public void onDestroy() {
		Helper_Clock.close();
		Helper_ClockLog.close();
		this.finish();
		super.onStop();
		super.onDestroy();
	}
}
