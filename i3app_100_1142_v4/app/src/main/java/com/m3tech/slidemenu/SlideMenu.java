package com.m3tech.slidemenu;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.m3tech.adapter.SlideMenu_Adapter;
import com.m3tech.collection.Collection_themeobject;
import com.m3tech.app_100_1474.R;
import com.m3tech.widget.SlideMenu_Widget;

public class SlideMenu extends SlideMenu_Widget{

	private static final String LOG_TAG = "SLIDE MENU";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING SLIDE MENU LIST");
		
		//*** Call Function ***//
		GetMenuVerList Task = new GetMenuVerList();
		Task.execute();
		
		//*** Call onClick ***//
		layout_darkslidemenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_left);
			}
		});
	}
	
	/**** start function get content list ****/
	private class GetMenuVerList extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			try{

				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetMenuVerList-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				if (result.equals("SUCCESS")){
					
					SlideMenu_Adapter slidemenu_adapter = new SlideMenu_Adapter(context, R.layout.plugin_content_slidemenu_row,model_themeobject);
					slidemenu_adapter.clear();
					slidemenu_adapter.notifyDataSetChanged();
					
					c = Helper_Themeobject.getByMenuVer(app_user);
					Log.d(LOG_TAG, "c=" + c.getCount());
					if (c.moveToLast() != false) {
						// Get from DB
						c.moveToFirst();
						do {
							
							current_themeobject = new Collection_themeobject();
							current_themeobject.setthemeobject_id(Helper_Themeobject.getthemeobject_id(c).toString());
							current_themeobject.settheme_code(Helper_Themeobject.gettheme_code(c).toString());
							current_themeobject.setslot_code(Helper_Themeobject.getslot_code(c).toString());
							current_themeobject.setpage_id(Helper_Themeobject.getpage_id(c).toString());
							current_themeobject.setobject_type(Helper_Themeobject.getobject_type(c).toString());
							current_themeobject.setobject_value(Helper_Themeobject.getobject_value(c).toString());
							current_themeobject.setaction_type(Helper_Themeobject.getaction_type(c).toString());
							current_themeobject.setaction_value(Helper_Themeobject.getaction_value(c).toString());
							current_themeobject.setuser_id(Helper_Themeobject.getuser_id(c).toString());
							current_themeobject.setcurrency(Helper_Themeobject.getremark(c).toString());
							current_themeobject.setdate_updated(Helper_Themeobject.getdate_updated(c).toString());
							current_themeobject.setlastupdate(Helper_Themeobject.getlastupdate(c).toString());
							current_themeobject.setattribute(Helper_Themeobject.getattribute(c).toString());

							slidemenu_adapter.add(current_themeobject);

						} while (c.moveToNext());
					}
					c.close();
					listview.setAdapter(slidemenu_adapter);
					listview.setOnItemClickListener(onListClickMenu);
					
				}else{
					Toast.makeText(SlideMenu.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}
			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "Getcontentlist-onPostExecute-Error:" + t.getMessage(), t);
			}
		}
	}
	
	private AdapterView.OnItemClickListener onListClickMenu=new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent,
			View view, int position,
			long id) {
			
				current_themeobject=model_themeobject.get(position);	
				Log.d(LOG_TAG,"onListClick-"+current_themeobject.getaction_type());
				
				if(!current_themeobject.getaction_type().equals("") || current_themeobject.getaction_type().length() > 0){
					slotAction.CreateNextPage(current_themeobject);
					finish();
				}
		}
	};
	
	public void onBackPressed() {
		super.onBackPressed(); 
		this.finish();
	}

	public void onDestroy() {
		Helper_Content.close();
		Helper_Themeobject.close();
		Helper_Contentcategory.close();
		this.finish();
		super.onStop();
		super.onDestroy();	
	}
}
