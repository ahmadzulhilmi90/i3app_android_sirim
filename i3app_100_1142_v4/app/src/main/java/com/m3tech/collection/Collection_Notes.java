package com.m3tech.collection;

public class Collection_Notes {

	private String id="";
	private String note_id="";	
	private String note_title="";
	private String note_desc="";
	
	public String getid() {
		return(id);
	}
	
	public void setid(String id) {
		this.id=id;
	}

	public String getnote_id() {
		return(note_id);
	}
	
	public void setnote_id(String note_id) {
		this.note_id=note_id;
	}

	public String getnote_title() {
		return(note_title);
	}
	
	public void setnote_title(String note_title) {
		this.note_title=note_title;
	}
	
	public String getnote_desc() {
		return(note_desc);
	}
	
	public void setnote_desc(String note_desc) {
		this.note_desc=note_desc;
	}
}