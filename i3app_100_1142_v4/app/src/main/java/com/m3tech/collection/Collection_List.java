package com.m3tech.collection;

public class Collection_List {

	private String title="";	
	private String image="";	
	private String code1="";
	private String code2="";	
	private String code3="";	
	private String code4="";	
	private String code5="";	
	
	public String get_title() {
		return(title);
	}
	
	public void set_title(String title) {
		this.title=title;
	}

	public String get_image() {
		return(image);
	}
	
	public void set_image(String image) {
		this.image=image;
	}

	public String get_code1() {
		return(code1);
	}
	
	public void set_code1(String code1) {
		this.code1=code1;
	}

	public String get_code2() {
		return(code2);
	}
	
	public void set_code2(String code2) {
		this.code2=code2;
	}

	public String get_code3() {
		return(code3);
	}
	
	public void set_code3(String code3) {
		this.code3=code3;
	}

	public String get_code4() {
		return(code4);
	}
	
	public void set_code4(String code4) {
		this.code4=code4;
	}

	public String get_code5() {
		return(code5);
	}
	
	public void set_code5(String code5) {
		this.code5=code5;
	}


	public String toString() {
		return(get_title());
	}
	
}