package com.m3tech.collection;

public class Collection_BookSet {

	private String max_guesstotal="";	
	private String v_product="";
	private String v_branch="";
	private String v_datestart="";
	private String v_timestart="";
	private String v_dateend="";
	private String v_timeend="";
	private String v_guesstotal="";
	private String v_notes="";
	private String tq_message="";
	private String book_image_name="";
	private String book_image="";
	private String booksetid="";
	private String avai_date="";
	private String avai_time="";
	private String avai_time_start="";
	private String avai_time_end="";
	private String top_id="";
	private String booking_title="";
	private String multiple_product="";
	
	public String gettop_id() {
		return(top_id);
	}
	
	public void settop_id(String top_id) {
		this.top_id=top_id;
	}
	
	public String getbooking_title() {
		return(booking_title);
	}
	
	public void setbooking_title(String booking_title) {
		this.booking_title=booking_title;
	}
	
	public String getmultiple_product() {
		return(multiple_product);
	}
	
	public void setmultiple_product(String multiple_product) {
		this.multiple_product=multiple_product;
	}
	
	public String getmax_guesstotal() {
		return(max_guesstotal);
	}
	
	public void setmax_guesstotal(String max_guesstotal) {
		this.max_guesstotal=max_guesstotal;
	}
	
	public String geticon_name() {
		return(v_product);
	}
	
	public void setv_product(String v_product) {
		this.v_product=v_product;
	}
	
	public String getv_branch() {
		return(v_branch);
	}
	
	public void setv_branch(String v_branch) {
		this.v_branch=v_branch;
	}
	
	public String getv_datestart() {
		return(v_datestart);
	}
	
	public void setv_datestart(String v_datestart) {
		this.v_datestart=v_datestart;
	}
	
	public String getv_timestart() {
		return(v_timestart);
	}
	
	public void setv_timestart(String v_timestart) {
		this.v_timestart=v_timestart;
	}
	
	public String getv_dateend() {
		return(v_dateend);
	}
	
	public void setv_dateend(String v_dateend) {
		this.v_dateend=v_dateend;
	}
	
	public String getv_timeend() {
		return(v_timeend);
	}
	
	public void setv_timeend(String v_timeend) {
		this.v_timeend=v_timeend;
	}
	
	public String getv_guesstotal() {
		return(v_guesstotal);
	}
	
	public void setv_guesstotal(String v_guesstotal) {
		this.v_guesstotal=v_guesstotal;
	}
	
	public String getv_notes() {
		return(v_notes);
	}
	
	public void setv_notes(String v_notes) {
		this.v_notes=v_notes;
	}
	
	public String gettq_message() {
		return(tq_message);
	}
	
	public void settq_message(String tq_message) {
		this.tq_message=tq_message;
	}
	
	public String getbook_image_name() {
		return(book_image_name);
	}
	
	public void setbook_image_name(String book_image_name) {
		this.book_image_name=book_image_name;
	}
	
	public String getbook_image() {
		return(book_image);
	}
	
	public void setbook_image(String book_image) {
		this.book_image=book_image;
	}
	
	public String getbooksetid() {
		return(booksetid);
	}
	
	public void setbooksetid(String booksetid) {
		this.booksetid=booksetid;
	}
	
	public String getavai_date() {
		return(avai_date);
	}
	
	public void setavai_date(String avai_date) {
		this.avai_date=avai_date;
	}
	
	public String getavai_time() {
		return(avai_time);
	}
	
	public void setavai_time(String avai_time) {
		this.avai_time=avai_time;
	}
	
	public String getavai_time_start() {
		return(avai_time_start);
	}
	
	public void setavai_time_start(String avai_time_start) {
		this.avai_time_start=avai_time_start;
	}
	
	public String getavai_time_end() {
		return(avai_time_end);
	}
	
	public void setavai_time_end(String avai_time_end) {
		this.avai_time_end=avai_time_end;
	}
	
	public String toString() {
		return(getavai_date());
	}
	
}