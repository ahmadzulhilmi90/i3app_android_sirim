package com.m3tech.collection;

public class Collection_Loyalty {

	private String id="";	
	private String user_id="";
	private String title="";
	private String short_description="";
	private String secret_code="";
	private String start_date="";
	private String end_date="";
	private String total_stamp="";
	private String icon="";
	private String bg_img="";
	private String status="";
	private String last_updated="";
	private String icon_name="";
	private String stamp_icon_name="";
	private String stamp_icon_bg_name="";
	private String stamp_icon_type="";
	private String stamp_icon_url="";
	private String stamp_icon_bg_url="";
	
	public String getid() {
		return(id);
	}
	
	public void setid(String id) {
		this.id=id;
	}
	
	public String getuser_id() {
		return(user_id);
	}
	
	public void setuser_id(String user_id) {
		this.user_id=user_id;
	}
	
	public String gettitle() {
		return(title);
	}
	
	public void settitle(String title) {
		this.title=title;
	}
	
	public String getshort_description() {
		return(short_description);
	}
	
	public void setshort_description(String short_description) {
		this.short_description=short_description;
	}
	
	public String getsecret_code() {
		return(secret_code);
	}
	
	public void setsecret_code(String secret_code) {
		this.secret_code=secret_code;
	}
	
	public String getstart_date() {
		return(start_date);
	}
	
	public void setstart_date(String start_date) {
		this.start_date=start_date;
	}
	
	public String getend_date() {
		return(end_date);
	}
	
	public void setend_date(String end_date) {
		this.end_date=end_date;
	}
	
	public String gettotal_stamp() {
		return(total_stamp);
	}
	
	public void settotal_stamp(String total_stamp) {
		this.total_stamp=total_stamp;
	}
	
	public String geticon() {
		return(icon);
	}

	public void seticon(String icon) {
		this.icon=icon;
	}

	public String getbg_img() {
		return(bg_img);
	}

	public void setbg_img(String bg_img) {
		this.bg_img=bg_img;
	}


	public String getstatus() {
		return(status);
	}
	
	public void setstatus(String status) {
		this.status=status;
	}
	
	public String getlast_updated() {
		return(last_updated);
	}
	
	public void setlast_updated(String last_updated) {
		this.last_updated=last_updated;
	}
	
	public String geticon_name() {
		return(icon_name);
	}
	
	public void seticon_name(String icon_name) {
		this.icon_name=icon_name;
	}
	
	public String getstamp_icon_name() {
		return(stamp_icon_name);
	}
	
	public void setstamp_icon_name(String stamp_icon_name) {
		this.stamp_icon_name=stamp_icon_name;
	}
	
	public String getstamp_icon_bg_name() {
		return(stamp_icon_bg_name);
	}
	
	public void setstamp_icon_bg_name(String stamp_icon_bg_name) {
		this.stamp_icon_bg_name=stamp_icon_bg_name;
	}
	
	public String getstamp_icon_type() {
		return(stamp_icon_type);
	}
	
	public void setstamp_icon_type(String stamp_icon_type) {
		this.stamp_icon_type=stamp_icon_type;
	}
	
	public String getstamp_icon_url() {
		return(stamp_icon_url);
	}
	
	public void setstamp_icon_url(String stamp_icon_url) {
		this.stamp_icon_url=stamp_icon_url;
	}
	
	public String getstamp_icon_bg_url() {
		return(stamp_icon_bg_url);
	}
	
	public void setstamp_icon_bg_url(String stamp_icon_bg_url) {
		this.stamp_icon_bg_url=stamp_icon_bg_url;
	}
	
	public String toString() {
		return(gettitle());
	}
	
}