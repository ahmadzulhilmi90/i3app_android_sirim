package com.m3tech.collection;

public class Collection_Ibeaconxref {

	private String ibeaconxref_id="";	
	private String device_id="";
	private String content_id="";
	private String user_id="";
	private String date_updated="";
	private String lastupdate="";
	
	
	public String getibeaconxref_id() {
		return(ibeaconxref_id);
	}
	
	public void setibeaconxref_id(String ibeaconxref_id) {
		this.ibeaconxref_id=ibeaconxref_id;
	}
	
	public String getdevice_id() {
		return(device_id);
	}
	
	public void setdevice_id(String device_id) {
		this.device_id=device_id;
	}
	
	public String getcontent_id() {
		return(content_id);
	}
	
	public void setcontent_id(String content_id) {
		this.content_id=content_id;
	}
	
	public String getuser_id() {
		return(user_id);
	}
	
	public void setuser_id(String user_id) {
		this.user_id=user_id;
	}
	
	public String getdate_updated() {
		return(date_updated);
	}
	
	public void setdate_updated(String date_updated) {
		this.date_updated=date_updated;
	}
	
	public String getlastupdate() {
		return(lastupdate);
	}
	
	public void setlastupdate(String lastupdate) {
		this.lastupdate=lastupdate;
	}
	
	
	
	public String toString() {
		return(getdevice_id());
	}
	
}