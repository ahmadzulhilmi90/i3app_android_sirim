package com.m3tech.collection;

public class Collection_Event {

	private String num="";	
	private String row_id="";	
	private String event_name="";	
	private String event_description="";
	private String venue="";	
	private String date_start="";	
	private String date_end="";	
	private String time_start="";
	
	private String time_end="";	
	private String pax_limit="";	
	private String latitude="";
	private String longitude="";	
	private String url="";	
	private String cutoff="";	
	private String notification="";
	
	private String status="";	
	private String cdate="";	
	private String mdate="";
	private String user_id="";	
	private String top_id="";	
	private String schedule_date="";	
	private String page_id="";
	
	public String getnum() {
		return(num);
	}
	
	public void setnum(String num) {
		this.num=num;
	}
	
	public String getrow_id() {
		return(row_id);
	}
	
	public void setrow_id(String row_id) {
		this.row_id=row_id;
	}
	
	public String getevent_name() {
		return(event_name);
	}
	
	public void setevent_name(String event_name) {
		this.event_name=event_name;
	}
	
	public String getevent_description() {
		return(event_description);
	}
	
	public void setevent_description(String event_description) {
		this.event_description=event_description;
	}
	
	public String getvenue() {
		return(venue);
	}
	
	public void setvenue(String venue) {
		this.venue=venue;
	}
	
	public String getdate_start() {
		return(date_start);
	}
	
	public void setdate_start(String date_start) {
		this.date_start=date_start;
	}
	
	public String getdate_end() {
		return(date_end);
	}
	
	public void setdate_end(String date_end) {
		this.date_end=date_end;
	}
	
	public String gettime_start() {
		return(time_start);
	}
	
	public void settime_start(String time_start) {
		this.time_start=time_start;
	}
	
	public String gettime_end() {
		return(time_end);
	}
	
	public void settime_end(String time_end) {
		this.time_end=time_end;
	}
	
	public String getpax_limit() {
		return(pax_limit);
	}
	
	public void setpax_limit(String pax_limit) {
		this.pax_limit=pax_limit;
	}
	
	public String getlatitude() {
		return(latitude);
	}
	
	public void setlatitude(String latitude) {
		this.latitude=latitude;
	}

	public String getlongitude() {
		return(longitude);
	}
	
	public void setlongitude(String longitude) {
		this.longitude=longitude;
	}
	
	public String geturl() {
		return(url);
	}
	
	public void seturl(String url) {
		this.url=url;
	}
	
	public String getcutoff() {
		return(cutoff);
	}
	
	public void setcutoff(String cutoff) {
		this.cutoff=cutoff;
	}
	
	public String getnotification() {
		return(notification);
	}
	
	public void setnotification(String notification) {
		this.notification=notification;
	}
	
	public String getstatus() {
		return(status);
	}
	
	public void setstatus(String status) {
		this.status=status;
	}
	
	public String getcdate() {
		return(cdate);
	}
	
	public void setcdate(String cdate) {
		this.cdate=cdate;
	}
	
	public String getmdate() {
		return(mdate);
	}
	
	public void setmdate(String mdate) {
		this.mdate=mdate;
	}
	
	public String getuser_id() {
		return(user_id);
	}
	
	public void setuser_id(String user_id) {
		this.user_id=user_id;
	}
	
	public String gettop_id() {
		return(top_id);
	}
	
	public void settop_id(String top_id) {
		this.top_id=top_id;
	}
	
	public String getschedule_date() {
		return(schedule_date);
	}
	
	public void setschedule_date(String schedule_date) {
		this.schedule_date=schedule_date;
	}
	
	public String getpage_id() {
		return(page_id);
	}
	
	public void setpage_id(String page_id) {
		this.page_id=page_id;
	}
	
	public String toString() {
		return(getevent_name());
	}
	
}