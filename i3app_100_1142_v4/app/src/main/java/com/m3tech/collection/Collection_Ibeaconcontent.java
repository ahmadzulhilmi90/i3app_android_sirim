package com.m3tech.collection;

public class Collection_Ibeaconcontent {

	private String id="";	
	private String device_id="";
	private String welcome_msg="";
	private String welcome_img="";
	private String page_id="";
	private String start_date="";
	private String end_date="";
	private String user_id="";
	private String status="";
	private String date_updated="";
	private String last_updated="";
	private String limitperday="";

	
	
	public String getid() {
		return(id);
	}
	
	public void setid(String id) {
		this.id=id;
	}
	
	public String getdevice_id() {
		return(device_id);
	}
	
	public void setdevice_id(String device_id) {
		this.device_id=device_id;
	}
	
	public String getmajor() {
		return(welcome_msg);
	}
	
	public void setwelcome_msg(String welcome_msg) {
		this.welcome_msg=welcome_msg;
	}
	
	public String getwelcome_img() {
		return(welcome_img);
	}
	
	public void setwelcome_img(String welcome_img) {
		this.welcome_img=welcome_img;
	}
	
	public String getpage_id() {
		return(page_id);
	}
	
	public void setpage_id(String page_id) {
		this.page_id=page_id;
	}
	
	public String getstart_date() {
		return(start_date);
	}
	
	public void setstart_date(String start_date) {
		this.start_date=start_date;
	}
	
	public String getend_date() {
		return(end_date);
	}
	
	public void setend_date(String end_date) {
		this.end_date=end_date;
	}
	
	public String getuser_id() {
		return(user_id);
	}
	
	public void setuser_id(String user_id) {
		this.user_id=user_id;
	}
	
	
	public String getstatus() {
		return(status);
	}
	
	public void setstatus(String status) {
		this.status=status;
	}
	
	public String getdate_updated() {
		return(date_updated);
	}
	
	public void setdate_updated(String date_updated) {
		this.date_updated=date_updated;
	}
	
	public String getlast_updated() {
		return(last_updated);
	}
	
	public void setlimitperday(String limitperday) {
		this.limitperday=limitperday;
	}

	public String getlimitperday() {
		return(limitperday);
	}
	
	public String toString() {
		return(getdevice_id());
	}
	
}