package com.m3tech.collection;

public class Collection_themeobjectplugin {

	private String themeobjectplugin_id="";	
	private String page_id="";
	private String object_id="";
	private String plugin_value1="";
	private String plugin_value2="";
	private String plugin_value3="";
	private String remark="";
	private String date_updated="";
	private String lastupdate="";
	
	
	public String getthemeobjectplugin_id() {
		return(themeobjectplugin_id);
	}
	
	public void setthemeobjectplugin_id(String themeobjectplugin_id) {
		this.themeobjectplugin_id=themeobjectplugin_id;
	}
	
	public String getpage_id() {
		return(page_id);
	}
	
	public void setpage_id(String page_id) {
		this.page_id=page_id;
	}
	
	public String getobject_id() {
		return(object_id);
	}
	
	public void setobject_id(String object_id) {
		this.object_id=object_id;
	}
	
	public String getplugin_value1() {
		return(plugin_value1);
	}
	
	public void setplugin_value1(String plugin_value1) {
		this.plugin_value1=plugin_value1;
	}
	
	public String getplugin_value2() {
		return(plugin_value2);
	}
	
	public void setplugin_value2(String plugin_value2) {
		this.plugin_value2=plugin_value2;
	}
	
	public String getplugin_value3() {
		return(plugin_value3);
	}
	
	public void setplugin_value3(String plugin_value3) {
		this.plugin_value3=plugin_value3;
	}
	
	public String getremark() {
		return(remark);
	}
	
	public void setcurrency(String remark) {
		this.remark=remark;
	}
	
	public String getdate_updated() {
		return(date_updated);
	}
	
	public void setdate_updated(String date_updated) {
		this.date_updated=date_updated;
	}
	
	public String getlastupdate() {
		return(lastupdate);
	}
	
	public void setlastupdate(String lastupdate) {
		this.lastupdate=lastupdate;
	}
	
	
	
	public String toString() {
		return(getpage_id());
	}
	
}