package com.m3tech.collection;

public class Collection_Warrantylist {

	private String id="";	
	private String user_id="";
	private String customer_id="";
	private String unregister_id="";
	private String title="";
	private String dealer="";
	private String serial_number="";
	private String model_number="";
	private String purchase_date="";
	private String warranty_period="";
	private String icon="";
	private String img_product="";
	private String img_receipt="";
	private String img_warranty="";
	private String comments="";
	private String date_inserted="";
	private String date_updated="";
	private String status="";
	private String brand="";
	private String category="";
	private String expirystatus="";
	
	public String getid() {
		return(id);
	}
	
	public void setid(String id) {
		this.id=id;
	}
	
	public String getuser_id() {
		return(user_id);
	}
	
	public void setuser_id(String user_id) {
		this.user_id=user_id;
	}
	
	public String getcustomer_id() {
		return(customer_id);
	}
	
	public void setcustomer_id(String customer_id) {
		this.customer_id=customer_id;
	}
	
	public String getunregister_id() {
		return(unregister_id);
	}
	
	public void setunregister_id(String unregister_id) {
		this.unregister_id=unregister_id;
	}
	
	public String gettitle() {
		return(title);
	}
	
	public void settitle(String title) {
		this.title=title;
	}
	
	public String getdealer() {
		return(dealer);
	}
	
	public void setdealer(String dealer) {
		this.dealer=dealer;
	}
	
	public String getserial_number() {
		return(serial_number);
	}
	
	public void setserial_number(String serial_number) {
		this.serial_number=serial_number;
	}
	
	public String getmodel_number() {
		return(model_number);
	}
	
	public void setmodel_number(String model_number) {
		this.model_number=model_number;
	}
	
	public String getpurchase_date() {
		return(purchase_date);
	}
	
	public void setpurchase_date(String purchase_date) {
		this.purchase_date=purchase_date;
	}
	
	public String getwarranty_period() {
		return(warranty_period);
	}
	
	public void setwarranty_period(String warranty_period) {
		this.warranty_period=warranty_period;
	}
	
	public String geticon() {
		return(icon);
	}
	
	public void seticon(String icon) {
		this.icon=icon;
	}
	
	public String getimg_product() {
		return(img_product);
	}
	
	public void setimg_product(String img_product) {
		this.img_product=img_product;
	}
	
	public String getimg_receipt() {
		return(img_receipt);
	}
	
	public void setimg_receipt(String img_receipt) {
		this.img_receipt=img_receipt;
	}
	
	public String getimg_warranty() {
		return(img_warranty);
	}
	
	public void setimg_warranty(String img_warranty) {
		this.img_warranty=img_warranty;
	}
	
	public String getcomments() {
		return(comments);
	}
	
	public void setcomments(String comments) {
		this.comments=comments;
	}
	
	public String getdate_inserted() {
		return(date_inserted);
	}
	
	public void setdate_inserted(String date_inserted) {
		this.date_inserted=date_inserted;
	}
	
	public String getdate_updated() {
		return(date_updated);
	}
	
	public void setdate_updated(String date_updated) {
		this.date_updated=date_updated;
	}
	
	public String getstatus() {
		return(status);
	}
	
	public void setstatus(String status) {
		this.status=status;
	}
	
	public String getbrand() {
		return(brand);
	}
	
	public void setbrand(String brand) {
		this.brand=brand;
	}
	public String getcategory() {
		return(category);
	}
	
	public void setcategory(String category) {
		this.category=category;
	}
	
	public String toString() {
		return(gettitle());
	}

	public String getexpirystatus() {
		return(expirystatus);
	}
	public void setexpirystatus(String status) {
		this.expirystatus=status;
	}
	

}