package com.m3tech.collection;

public class Collection_Inbox {

	private String id="";
	private String mdate="";	
	private String message="";	
	private String sender_id="";
	private String sender_name="";
	private String image_url="";
	
	public String getid() {
		return(id);
	}
	
	public void setid(String id) {
		this.id=id;
	}

	public String getsender_id() {
		return(sender_id);
	}
	
	public void setsender_id(String sender_id) {
		this.sender_id=sender_id;
	}

	public String getmdate() {
		return(mdate);
	}
	
	public void setmdate(String mdate) {
		this.mdate=mdate;
	}

	public String getmessage() {
		return(message);
	}
	
	public void setmessage(String message) {
		this.message=message;
	}
	
	
	public String getsender_name() {
		return(sender_name);
	}
	
	public void setsender_name(String sender_name) {
		this.sender_name=sender_name;
	}

	public String getimage_url() {
		return(image_url);
	}
	
	public void setimage_url(String image_url) {
		this.image_url=image_url;
	}
	
}