package com.m3tech.collection;

public class Collection_AppSetup {

	private String name="";	
	private String icon_name="";
	private String icon_pic="";
	private String background="";
	private String splash_page="";
	private String user_id="";
	private String currency="";
	private String product_title="";
	private String profile_title="";
	private String booking_title="";
	private String booking_status="";
	private String loyalty_title="";
	private String loyalty_status="";
	private String branch_title="";
	private String customer_title="";
	
	public String getname() {
		return(name);
	}
	
	public void setname(String name) {
		this.name=name;
	}
	
	public String geticon_name() {
		return(icon_name);
	}
	
	public void seticon_name(String icon_name) {
		this.icon_name=icon_name;
	}
	
	public String geticon_pic() {
		return(icon_pic);
	}
	
	public void seticon_pic(String icon_pic) {
		this.icon_pic=icon_pic;
	}
	
	public String getbackground() {
		return(background);
	}
	
	public void setbackground(String background) {
		this.background=background;
	}
	
	public String getsplash_page() {
		return(splash_page);
	}
	
	public void setsplash_page(String splash_page) {
		this.splash_page=splash_page;
	}
	
	public String getsuser_id() {
		return(user_id);
	}
	
	public void setuser_id(String user_id) {
		this.user_id=user_id;
	}
	
	public String getcurrency() {
		return(currency);
	}
	
	public void setcurrency(String currency) {
		this.currency=currency;
	}
	
	public String getproduct_title() {
		return(product_title);
	}
	
	public void setproduct_title(String product_title) {
		this.product_title=product_title;
	}
	
	public String getprofile_title() {
		return(profile_title);
	}
	
	public void setprofile_title(String profile_title) {
		this.profile_title=profile_title;
	}
	
	public String getbooking_title() {
		return(booking_title);
	}
	
	public void setbooking_title(String booking_title) {
		this.booking_title=booking_title;
	}
	
	public String getbooking_status() {
		return(booking_status);
	}
	
	public void setbooking_status(String booking_status) {
		this.booking_status=booking_status;
	}
	
	public String getloyalty_title() {
		return(loyalty_title);
	}
	
	public void setloyalty_title(String loyalty_title) {
		this.loyalty_title=loyalty_title;
	}
	
	public String getloyalty_status() {
		return(loyalty_status);
	}
	
	public void setloyalty_status(String loyalty_status) {
		this.loyalty_status=loyalty_status;
	}
	
	public String getbranch_title() {
		return(branch_title);
	}
	
	public void setbranch_title(String branch_title) {
		this.branch_title=branch_title;
	}
	
	public String getcustomer_title() {
		return(customer_title);
	}
	
	public void setcustomer_title(String customer_title) {
		this.customer_title=customer_title;
	}
	
	public String toString() {
		return(getname());
	}
	
}