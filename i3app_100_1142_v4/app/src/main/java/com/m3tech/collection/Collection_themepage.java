package com.m3tech.collection;

public class Collection_themepage {

	private String themepage_id="";	
	private String theme_code="";
	private String name="";
	private String prev_page="";
	private String user_id="";
	private String remark="";
	private String date_updated="";
	private String lastupdate="";
	
	
	public String getthemepage_id() {
		return(themepage_id);
	}
	
	public void setthemepage_id(String themepage_id) {
		this.themepage_id=themepage_id;
	}
	
	public String gettheme_code() {
		return(theme_code);
	}
	
	public void settheme_code(String theme_code) {
		this.theme_code=theme_code;
	}
	
	public String getname() {
		return(name);
	}
	
	public void setname(String name) {
		this.name=name;
	}
	
	public String getprev_page() {
		return(prev_page);
	}
	
	public void setprev_page(String prev_page) {
		this.prev_page=prev_page;
	}
	
	public String getuser_id() {
		return(user_id);
	}
	
	public void setuser_id(String user_id) {
		this.user_id=user_id;
	}
	
	public String getremark() {
		return(remark);
	}
	
	public void setcurrency(String remark) {
		this.remark=remark;
	}
	
	public String getdate_updated() {
		return(date_updated);
	}
	
	public void setdate_updated(String date_updated) {
		this.date_updated=date_updated;
	}
	
	public String getlastupdate() {
		return(lastupdate);
	}
	
	public void setlastupdate(String lastupdate) {
		this.lastupdate=lastupdate;
	}
	
	
	
	public String toString() {
		return(gettheme_code());
	}
	
}