package com.m3tech.collection;

public class Collection_themeobject {

	private String themeobject_id="";	
	private String theme_code="";
	private String slot_code="";
	private String page_id="";
	private String object_type="";
	private String object_value="";
	private String action_type="";
	private String action_value="";
	private String user_id="";
	private String remark="";
	private String date_updated="";
	private String lastupdate="";
	private String attribute="";
	
	public String getthemeobject_id() {
		return(themeobject_id);
	}
	
	public void setthemeobject_id(String themeobject_id) {
		this.themeobject_id=themeobject_id;
	}
	
	public String gettheme_code() {
		return(theme_code);
	}
	
	public void settheme_code(String theme_code) {
		this.theme_code=theme_code;
	}
	
	public String getslot_code() {
		return(slot_code);
	}
	
	public void setslot_code(String slot_code) {
		this.slot_code=slot_code;
	}
	
	public String getpage_id() {
		return(page_id);
	}
	
	public void setpage_id(String page_id) {
		this.page_id=page_id;
	}
	
	public String getobject_type() {
		return(object_type);
	}
	
	public void setobject_type(String object_type) {
		this.object_type=object_type;
	}
	
	public String getobject_value() {
		return(object_value);
	}
	
	public void setobject_value(String object_value) {
		this.object_value=object_value;
	}
	
	
	
	public String getaction_type() {
		return(action_type);
	}
	
	public void setaction_type(String action_type) {
		this.action_type=action_type;
	}
	
	
	public String getaction_value() {
		return(action_value);
	}
	
	public void setaction_value(String action_value) {
		this.action_value=action_value;
	}
		
	public String getuser_id() {
		return(user_id);
	}
	
	public void setuser_id(String user_id) {
		this.user_id=user_id;
	}
	
	
	public String getremark() {
		return(remark);
	}
	
	public void setcurrency(String remark) {
		this.remark=remark;
	}
	
	public String getdate_updated() {
		return(date_updated);
	}
	
	public void setdate_updated(String date_updated) {
		this.date_updated=date_updated;
	}
	
	public String getlastupdate() {
		return(lastupdate);
	}
	
	public void setlastupdate(String lastupdate) {
		this.lastupdate=lastupdate;
	}
	
	public String getattribute() {
		return(attribute);
	}
	
	public void setattribute(String attribute) {
		this.attribute=attribute;
	}
	
	
	public String toString() {
		return(gettheme_code());
	}
	
}