package com.m3tech.collection;

public class Collection_BookingDetail {

	private String id="";
	private String booking_no="";	
	private String customer_id="";
	private String product_id="";
	private String product_name="";
	private String branch_id="";
	private String branch_name="";
	private String date_start="";
	private String time_start="";
	private String date_end="";
	private String time_end="";
	private String guess_total="";
	private String notes="";
	private String status="";
	private String status_desc="";
	private String user_id="";
	private String cdate="";
	private String mdate="";
	private String name="";
	private String top_id="";
	private String booking_title="";
	private String multiple_product="";
	
	private String price_list="";
	private String price_total="";
	private String qtt_list="";
	
	public String getprice_list() {
		return(price_list);
	}
	
	public void setprice_list(String price_list) {
		this.price_list=price_list;
	}
	
	public String getprice_total() {
		return(price_total);
	}
	
	public void setprice_total(String price_total) {
		this.price_total=price_total;
	}
	
	public String getqtt_list() {
		return(qtt_list);
	}
	
	public void setqtt_list(String qtt_list) {
		this.qtt_list=qtt_list;
	}
	
	
	public String gettop_id() {
		return(top_id);
	}
	
	public void settop_id(String top_id) {
		this.top_id=top_id;
	}
	
	public String getbooking_title() {
		return(booking_title);
	}
	
	public void setbooking_title(String booking_title) {
		this.booking_title=booking_title;
	}
	
	public String getmultiple_product() {
		return(multiple_product);
	}
	
	public void setmultiple_product(String multiple_product) {
		this.multiple_product=multiple_product;
	}
	
	public String getid() {
		return(id);
	}
	
	public void setid(String id) {
		this.id=id;
	}
	
	public String getbooking_no() {
		return(booking_no);
	}
	
	public void setbooking_no(String booking_no) {
		this.booking_no=booking_no;
	}
	
	
	public String getcustomer_id() {
		return(customer_id);
	}
	
	public void setcustomer_id(String customer_id) {
		this.customer_id=customer_id;
	}
	
	public String getproduct_id() {
		return(product_id);
	}
	
	public void setproduct_id(String product_id) {
		this.product_id=product_id;
	}
	
	public String getproduct_name() {
		return(product_name);
	}
	
	public void setproduct_name(String product_name) {
		this.product_name=product_name;
	}
	
	public String getbranch_id() {
		return(branch_id);
	}
	
	public void setbranch_id(String branch_id) {
		this.branch_id=branch_id;
	}
	
	public String getbranch_name() {
		return(branch_name);
	}
	
	public void setbranch_name(String branch_name) {
		this.branch_name=branch_name;
	}
	
	public String getdate_start() {
		return(date_start);
	}
	
	public void setdate_start(String date_start) {
		this.date_start=date_start;
	}
	
	public String gettime_start() {
		return(time_start);
	}
	
	public void settime_start(String time_start) {
		this.time_start=time_start;
	}
	
	public String getdate_end() {
		return(date_end);
	}
	
	public void setdate_end(String date_end) {
		this.date_end=date_end;
	}
	
	
	
	public String gettime_end() {
		return(time_end);
	}
	
	public void settime_end(String time_end) {
		this.time_end=time_end;
	}
	
	public String getguess_total() {
		return(guess_total);
	}
	
	public void setguess_total(String guess_total) {
		this.guess_total=guess_total;
	}
	
	public String getnotes() {
		return(notes);
	}
	
	public void setnotes(String notes) {
		this.notes=notes;
	}
	
	public String getstatus() {
		return(status);
	}
	
	public void setstatus(String status) {
		this.status=status;
	}
	
	public String getstatus_desc() {
		return(status_desc);
	}
	
	public void setstatus_desc(String status_desc) {
		this.status_desc=status_desc;
	}
	
	
	public String getuser_id() {
		return(user_id);
	}
	
	public void setuser_id(String user_id) {
		this.user_id=user_id;
	}
	
	
	public String getcdate() {
		return(cdate);
	}
	
	
	
	public void setcdate(String cdate) {
		this.cdate=cdate;
	}
	
	
	public String getmdate() {
		return(mdate);
	}
	
	
	
	public void setmdate(String mdate) {
		this.mdate=mdate;
	}
	
	public String getname() {
		return(name);
	}
	
	
	
	public void setname(String name) {
		this.name=name;
	}
	
}