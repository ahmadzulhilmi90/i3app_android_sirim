package com.m3tech.collection;

public class Collection_contentcategory {

	private String contentcat_id="";	
	private String name="";
	private String plugin_id="";
	private String list_template="";
	private String user_id="";
	private String is_hidden="";
	private String date_updated="";
	private String lastupdate="";
	
	public String getcontentcat_id() {
		return(contentcat_id);
	}
	
	public void setcontentcat_id(String contentcat_id) {
		this.contentcat_id=contentcat_id;
	}
	
	public String getname() {
		return(name);
	}
	
	public void setname(String name) {
		this.name=name;
	}
	
	public String getplugin_id() {
		return(plugin_id);
	}
	
	public void setplugin_id(String plugin_id) {
		this.plugin_id=plugin_id;
	}
	
	public String getlist_template() {
		return(list_template);
	}
	
	public void setlist_template(String list_template) {
		this.list_template=list_template;
	}
	
	public String getuser_id() {
		return(user_id);
	}
	
	public void setuser_id(String user_id) {
		this.user_id=user_id;
	}
	
	public String getis_hidden() {
		return(is_hidden);
	}
	
	public void setis_hidden(String is_hidden) {
		this.is_hidden=is_hidden;
	}
	
	
	public String getdate_updated() {
		return(date_updated);
	}
	
	public void setdate_updated(String date_updated) {
		this.date_updated=date_updated;
	}
	
	public String getlastupdate() {
		return(lastupdate);
	}
	
	public void setlastupdate(String lastupdate) {
		this.lastupdate=lastupdate;
	}
	
	
	public String toString() {
		return(getname());
	}
	
}