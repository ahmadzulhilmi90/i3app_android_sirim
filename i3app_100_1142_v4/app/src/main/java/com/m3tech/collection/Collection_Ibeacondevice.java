package com.m3tech.collection;

public class Collection_Ibeacondevice {

	private String id="";	
	private String uuid="";
	private String major="";
	private String minor="";
	private String name="";
	private String error_msg="";
	private String user_id="";
	private String status="";
	private String date_updated="";
	private String last_updated="";
	
	
	public String getid() {
		return(id);
	}
	
	public void setid(String id) {
		this.id=id;
	}
	
	public String getuuid() {
		return(uuid);
	}
	
	public void setuuid(String uuid) {
		this.uuid=uuid;
	}
	
	public String getmajor() {
		return(major);
	}
	
	public void setmajor(String major) {
		this.major=major;
	}
	
	public String getminor() {
		return(minor);
	}
	
	public void setminor(String minor) {
		this.minor=minor;
	}
	
	public String getname() {
		return(name);
	}
	
	public void setname(String name) {
		this.name=name;
	}
	
	public String geterror_msg() {
		return(error_msg);
	}
	
	public void seterror_msg(String error_msg) {
		this.error_msg=error_msg;
	}
	
	public String getuser_id() {
		return(user_id);
	}
	
	public void setuser_id(String user_id) {
		this.user_id=user_id;
	}
	
	
	public String getstatus() {
		return(status);
	}
	
	public void setstatus(String status) {
		this.status=status;
	}
	
	public String getdate_updated() {
		return(date_updated);
	}
	
	public void setdate_updated(String date_updated) {
		this.date_updated=date_updated;
	}
	
	public String getlast_updated() {
		return(last_updated);
	}
	
	public void setlast_updated(String last_updated) {
		this.last_updated=last_updated;
	}
	
	
	
	public String toString() {
		return(getuuid());
	}
	
}