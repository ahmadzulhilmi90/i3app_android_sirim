package com.m3tech.collection;

public class Collection_content {

	private String content_id="";	
	private String title="";
	private String category_id="";
	private String image="";
	private String thumbnail="";
	private String call_title="";
	private String telephone="";
	private String email_title="";
	private String email="";
	private String details="";
	private String link_title="";
	private String link_url="";
	private String map_title="";
	private String map_lat="";
	private String map_lon="";
	private String video_title="";
	private String video_url="";
	private String fb_title="";
	private String fb_url="";
	private String orderno;
	private String price_title;
	private String price;
	private String var1_title;
	private String var1;
	private String var2_title;
	private String var2;
	private String var3_title;
	private String var3;
	private String var4_title;
	private String var4;
	private String var5_title;
	private String var5;
	private String user_id="";	
	private String date_updated="";
	private String lastupdate="";
	private String distance="";
	
	
	public String getcontent_id() {
		return(content_id);
	}
	
	public void setcontent_id(String content_id) {
		this.content_id=content_id;
	}
	
	public String gettitle() {
		return(title);
	}
	
	public void settitle(String title) {
		this.title=title;
	}
	
	public String getcategory_id() {
		return(category_id);
	}
	
	public void setcategory_id(String category_id) {
		this.category_id=category_id;
	}
	
	public String getimage() {
		return(image);
	}
	
	public void setimage(String image) {
		this.image=image;
	}
	
	public String getthumbnail() {
		return(thumbnail);
	}
	
	public void setthumbnail(String thumbnail) {
		this.thumbnail=thumbnail;
	}
	
	public String getcall_title() {
		return(call_title);
	}
	
	public void setcall_title(String call_title) {
		this.call_title=call_title;
	}
	
	
	
	public String gettelephone() {
		return(telephone);
	}
	
	public void settelephone(String telephone) {
		this.telephone=telephone;
	}
	
	
	public String getemail_title() {
		return(email_title);
	}
	
	public void setemail_title(String email_title) {
		this.email_title=email_title;
	}
		
	public String getemail() {
		return(email);
	}
	
	public void setemail(String email) {
		this.email=email;
	}
	
	
	public String getdetails() {
		return(details);
	}
	
	public void setdetails(String details) {
		this.details=details;
	}
	
	
	public String getlink_title() {
		return(link_title);
	}
	
	public void setlink_title(String link_title) {
		this.link_title=link_title;
	}
	
	public String getlink_url() {
		return(link_url);
	}
	
	public void setlink_url(String link_url) {
		this.link_url=link_url;
	}
	
	public String getmap_title() {
		return(map_title);
	}
	
	public void setmap_title(String map_title) {
		this.map_title=map_title;
	}
	
	public String getmap_lat() {
		return(map_lat);
	}
	
	public void setmap_lat(String map_lat) {
		this.map_lat=map_lat;
	}
	
	public String getmap_lon() {
		return(map_lon);
	}
	
	public void setmap_lon(String map_lon) {
		this.map_lon=map_lon;
	}
	
	public String getvideo_title() {
		return(video_title);
	}
	
	public void setvideo_title(String video_title) {
		this.video_title=video_title;
	}
	
	public String getvideo_url() {
		return(video_url);
	}
	
	public void setvideo_url(String video_url) {
		this.video_url=video_url;
	}
	
	public String getattribute() {
		return(fb_title);
	}
	
	public void setfb_title(String fb_title) {
		this.fb_title=fb_title;
	}
	
	public String getfb_url() {
		return(fb_url);
	}
	
	public void setfb_url(String fb_url) {
		this.fb_url=fb_url;
	}
	
	public String getorderno() {
		return(orderno);
	}
	
	public void setorderno(String orderno) {
		this.orderno=orderno;
	}
	
	public String getprice_title() {
		return(price_title);
	}
	
	public void setprice_title(String price_title) {
		this.price_title=price_title;
	}
	
	public String getprice() {
		return(price);
	}
	
	public void setprice(String price) {
		this.price=price;
	}
	
	public String getvar1_title() {
		return(var1_title);
	}
	
	public void setvar1_title(String var1_title) {
		this.var1_title=var1_title;
	}
	
	public String getvar1() {
		return(var1);
	}
	
	public void setvar1(String var1) {
		this.var1=var1;
	}
	
	public String getvar2_title() {
		return(var2_title);
	}
	
	public void setvar2_title(String var2_title) {
		this.var2_title=var2_title;
	}
	
	public String getvar2() {
		return(var2);
	}
	
	public void setvar2(String var2) {
		this.var2=var2;
	}
	
	public String getvar3_title() {
		return(var3_title);
	}
	
	public void setvar3_title(String var3_title) {
		this.var3_title=var3_title;
	}
	
	public String getvar3() {
		return(var3);
	}
	
	public void setvar3(String var3) {
		this.var3=var3;
	}
	
	public String getvar4_title() {
		return(var4_title);
	}
	
	public void setvar4_title(String var4_title) {
		this.var4_title=var4_title;
	}
	
	public String getvar4() {
		return(var4);
	}
	
	public void setvar4(String var4) {
		this.var4=var4;
	}
	
	public String getvar5_title() {
		return(var5_title);
	}
	
	public void setvar5_title(String var5_title) {
		this.var5_title=var5_title;
	}
	
	public String getvar5() {
		return(var5);
	}
	
	public void setvar5(String var5) {
		this.var5=var5;
	}
	
	public String getuser_id() {
		return(user_id);
	}
	
	public void setuser_id(String user_id) {
		this.user_id=user_id;
	}
	
	public String getdate_updated() {
		return(date_updated);
	}
	
	public void setdate_updated(String date_updated) {
		this.date_updated=date_updated;
	}
	
	public String getlastupdate() {
		return(lastupdate);
	}
	
	public void setlastupdate(String lastupdate) {
		this.lastupdate=lastupdate;
	}
		
	public String getdistance() {
		return(distance);
	}
	
	public void setdistance(String distance) {
		this.distance=distance;
	}
	
	
	public String toString() {
		return(gettitle());
	}
	
}