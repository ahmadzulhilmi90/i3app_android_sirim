package com.m3tech.collection;

public class Collection_PointSettings {
	
	private String setting_id="";	
	private String plugin_id="";	
	private String plugin_code="";	
	private String title="";
	private String details="";	
	private String image_url="";	
	private String point_value="";	
	private String thankyou_msg="";	
	private String waiting_msg="";	
	private String waiting_interval="";	
	
	public String get_title() {
		return(title);
	}
	
	public void set_title(String title) {
		this.title=title;
	}

	public String get_setting_id() {
		return(setting_id);
	}
	
	public void set_setting_id(String setting_id) {
		this.setting_id=setting_id;
	}

	public String get_plugin_id() {
		return(plugin_id);
	}
	
	public void set_plugin_id(String plugin_id) {
		this.plugin_id=plugin_id;
	}

	public String get_plugin_code() {
		return(plugin_code);
	}
	
	public void set_plugin_code(String plugin_code) {
		this.plugin_code=plugin_code;
	}

	public String get_details() {
		return(details);
	}
	
	public void set_details(String details) {
		this.details=details;
	}

	public String get_image_url() {
		return(image_url);
	}
	
	public void set_image_url(String image_url) {
		this.image_url=image_url;
	}

	public String get_point_value() {
		return(point_value);
	}
	
	public void set_point_value(String point_value) {
		this.point_value=point_value;
	}

	public String get_thankyou_msg() {
		return(thankyou_msg);
	}
	
	public void set_thankyou_msg(String thankyou_msg) {
		this.thankyou_msg=thankyou_msg;
	}

	public String get_waiting_msg() {
		return(waiting_msg);
	}
	
	public void set_waiting_msg(String waiting_msg) {
		this.waiting_msg=waiting_msg;
	}

	public String get_waiting_interval() {
		return(waiting_interval);
	}
	
	public void set_waiting_interval(String waiting_interval) {
		this.waiting_interval=waiting_interval;
	}

	public String toString() {
		return(get_title());
	}
	
}