package com.m3tech.collection;

public class Collection_themecolor {

	private String color_id="";	
	private String name="";
	private String color="";
	private String color1="";
	private String color2="";
	private String color3="";
	private String color4="";
	private String lastupdate="";
	
	
	public String getcolor_id() {
		return(color_id);
	}
	
	public void setcolor_id(String color_id) {
		this.color_id=color_id;
	}
	
	public String getname() {
		return(name);
	}
	
	public void setname(String name) {
		this.name=name;
	}
	
	public String getcolor() {
		return(color);
	}
	
	public void setcolor(String color) {
		this.color=color;
	}
	
	public String getcolor1() {
		return(color1);
	}
	
	public void setcolor1(String color1) {
		this.color1=color1;
	}
	
	public String getcolor2() {
		return(color2);
	}
	
	public void setcolor2(String color2) {
		this.color2=color2;
	}
	
	public String getcolor3() {
		return(color3);
	}
	
	public void setcolor3(String color3) {
		this.color3=color3;
	}
	
	public String getcolor4() {
		return(color4);
	}
	
	public void setcolor4(String color4) {
		this.color4=color4;
	}
	
	public String getlastupdate() {
		return(lastupdate);
	}
	
	public void setlastupdate(String lastupdate) {
		this.lastupdate=lastupdate;
	}
	
	
	
	public String toString() {
		return(getname());
	}
	
}