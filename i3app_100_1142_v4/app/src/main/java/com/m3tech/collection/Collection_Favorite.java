package com.m3tech.collection;

public class Collection_Favorite {

	private String id="";
	private String user_id="";	
	private String customer_id="";	
	private String m_date="";
	private String content_id="";
	private String top_id="";
	
	public String getid() {
		return(id);
	}
	
	public void setid(String id) {
		this.id=id;
	}

	public String getuser_id() {
		return(user_id);
	}
	
	public void setuser_id(String user_id) {
		this.user_id=user_id;
	}

	public String getcustomer_id() {
		return(customer_id);
	}
	
	public void setcustomer_id(String customer_id) {
		this.customer_id=customer_id;
	}

	public String getm_date() {
		return(m_date);
	}
	
	public void setm_date(String m_date) {
		this.m_date=m_date;
	}
	
	public String getcontent_id() {
		return(content_id);
	}
	
	public void setcontent_id(String content_id) {
		this.content_id=content_id;
	}
	
	public String gettop_id() {
		return(top_id);
	}
	
	public void settop_id(String top_id) {
		this.top_id=top_id;
	}
}