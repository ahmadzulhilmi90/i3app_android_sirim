package com.m3tech.reservation;


import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.data.Helper_Content;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;

public class Reservation_Detail extends Activity {

	public static final String PREFS_NAME = "MyPrefsFile";
	private SharedPreferences settings;
	private static final String LOG_TAG = "RESERVATION DETAIL";

	Cursor c;
	static Boolean network_status=false;

	Helper_Content Helper_Content=null;
	SlotAction slotAction = null;
	Helper_Themeobject Helper_Themeobject = null;

	String home_pagename, layout,page, themecolor, btn_menucolor, tabbar, userid, currentdate,multiple_product,
	customerID,home_themecode,app_title,colorcode,colorcode1,app_user,app_db,udid;

	String branch_name,branch_id,call_title,telephone,email_title,email,details,map_title,map_lat,map_lon;
	TextView branchtitle,rsv_status,rsv_text,video,map,call_text,email_text,smsno_text,website,branch_address,
	reservno,cust_name,reservedate;
	LinearLayout layout_cancel,layout_map,layout_call,layout_email,layout_border;
	Drawable drawableimage;
	Cursor pdf, pdfv ;
	String product_id,bookno,custname,datebook,customer_id,status,status_desc,tq_message,bookid,display_price,display_total,display_qtt,price_list,price_total,qtt_list;
	static ProgressDialog progressDialog;

	//JSON
	JSONArray companyprofileJSONArray;

	TextView texttitlebar,texthome;
	ImageView back,tabheader;

	Context context = this;
	Header Header = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING RESERVATION DETAIL ");
		setContentView(R.layout.reservation_detail);
		
		network_status = isNetworkAvailable();
		if(network_status == true){
			

			slotAction = new SlotAction(this);
			Helper_Themeobject = new Helper_Themeobject(this);

			settings = this.getSharedPreferences(PREFS_NAME, 0);
			app_user = getResources().getString(R.string.app_user);
			app_db = getResources().getString(R.string.app_db);
			udid = settings.getString("udid", "");
			customerID = settings.getString("customerID", "");

			home_themecode = settings.getString("home_themecode", "");
			home_pagename = settings.getString("home_pagename", "");
			app_title = settings.getString("app_title", "");
			colorcode = settings.getString("colorcode", "");
			colorcode1 = settings.getString("colorcode1", "");

			String pageTitle = settings.getString("rsvPageTitle", "");
			if(pageTitle ==null||pageTitle.equals("")){
				pageTitle = app_title;
			}
			/*** layout linear declaration *****/

			tabheader = (ImageView) findViewById(R.id.tabheader);

			/*** text view *******/
			texttitlebar = (TextView) findViewById(R.id.texttitlebar);

			/**** home & back button *****/
			LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
			LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
			ImageView img_back = (ImageView) findViewById(R.id.back);
			ImageView img_home = (ImageView) findViewById(R.id.home);

			/*** Setting Header ***/
			Header = new Header(context, tabheader, tabback, tabhome,img_back,img_home, texttitlebar, pageTitle,"");

			Intent j = getIntent();

			product_id = j.getStringExtra("product_id");
			branch_id = j.getStringExtra("branch_id");
			bookno = j.getStringExtra("bookno");
			custname = j.getStringExtra("custname");
			datebook = j.getStringExtra("datebook");
			bookid = j.getStringExtra("bookid");
			status = j.getStringExtra("status");
			status_desc = j.getStringExtra("status_desc");
			tq_message = j.getStringExtra("tq_message");
			display_price = j.getStringExtra("display_price");
			display_total = j.getStringExtra("display_total");
			display_qtt = j.getStringExtra("display_qtt");
			price_list = j.getStringExtra("price_list");
			price_total = j.getStringExtra("price_total");
			qtt_list = j.getStringExtra("qtt_list");
			multiple_product = j.getStringExtra("multiple_product");
			
			Log.d(LOG_TAG, "product_id : " + product_id + " price_total : " + price_total);

			Helper_Content = new Helper_Content(this);
			LinearLayout layout3 = (LinearLayout)findViewById(R.id.layout_ketiga);
			reservno = (TextView) findViewById(R.id.reservno);
			reservno.setText(bookno);

			cust_name = (TextView) findViewById(R.id.custname);
			cust_name.setText(custname);

			rsv_status = (TextView) findViewById(R.id.status);
			String bookingstatus = getResources().getString(R.string.status)+ ": " + status_desc;
			rsv_status.setText(bookingstatus);

			
			LinearLayout layout_cancel = (LinearLayout) findViewById(R.id.layout_cancel);
			TextView canceltext = (TextView) findViewById(R.id.textcancel);

			rsv_text = (TextView) findViewById(R.id.reservtext);
			String reservtext="";
			if (status.equals("1")) {
				reservtext = getResources().getString(R.string.your_prersv_no_is);
				layout_cancel.setOnClickListener(onClickReservation);
				layout3.setVisibility(View.VISIBLE);
				
				//Setting tq message
				TextView content = (TextView) findViewById(R.id.branch_address);
				if(tq_message.length()>0){
					content.setText(tq_message);
				}else{
					content.setVisibility(View.GONE);
				}
				
			} else {
				layout3.setVisibility(View.GONE);
				reservtext = getResources().getString(R.string.your_rsv_no_is);
				canceltext.setText(getResources().getString(R.string.cannot_cancel));
			}
			rsv_text.setText(reservtext);
			
			String date = datebook;
			reservedate=(TextView) findViewById(R.id.reservedate);
			reservedate.setText(date);
			
			//Get Product List 
			onViewListProductBook(product_id);

			GetBranchDetail Task = new GetBranchDetail();
			Task.execute(new String[] { branch_id });
			
		}else{
			finish();
			Toast.makeText(Reservation_Detail.this,getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_LONG).show();
		} 
		
		
	}
	
	public void onViewListProductBook(String product_id){
		
		/*** Initialize widget ***/
		LinearLayout layout_product_single = (LinearLayout)findViewById(R.id.layout_product_single);
		LinearLayout layout_product_multiple = (LinearLayout)findViewById(R.id.layout_product_multiple);
		TextView text_product_list_single = (TextView)findViewById(R.id.text_product_list_single);
		TextView text_product_list = (TextView)findViewById(R.id.text_product_list);
		LinearLayout lay_total = (LinearLayout)findViewById(R.id.lay_total);
		TextView text_total_product = (TextView)findViewById(R.id.text_total_product);
		TextView text_product_qty = (TextView)findViewById(R.id.text_product_qty);
		TextView text_product_total_price = (TextView)findViewById(R.id.text_product_total_price);
		lay_total.setVisibility(View.GONE);
		text_product_qty.setVisibility(View.INVISIBLE);
		text_product_total_price.setVisibility(View.INVISIBLE);
		
		if(multiple_product.equals("1")){
			layout_product_single.setVisibility(View.GONE);
			layout_product_multiple.setVisibility(View.VISIBLE);
		}else{
			layout_product_multiple.setVisibility(View.GONE);
			layout_product_single.setVisibility(View.VISIBLE);
		}
		
		//1.Setting Layout Total
		if(display_total.equals("1")){
			lay_total.setVisibility(View.VISIBLE);
		}
		
		//2.Setting Layout Quantity
		if(display_qtt.equals("1")){
			text_product_qty.setVisibility(View.VISIBLE);
		}
		
		//3.Setting layout Price
		if(display_price.equals("1")){
			text_product_total_price.setVisibility(View.VISIBLE);
		}
		
		//1. Setting product list
		ArrayList<String> product = new ArrayList<String>();
		String[] parts = product_id.split(",");
        for ( int i=0; i < parts.length; i++){
	        
	    	Cursor c = Helper_Content.getByContID(parts[i]);
	    	if(c.getCount()>0){
				if (c.moveToLast() != false) {
					String title = Helper_Content.gettitle(c).toString();
					Log.d(LOG_TAG, "Title Product : " + title);
					if(title.length()>0){
						product.add(title);
					}
				}
	    	}
			c.close();
        }
        
        String text = "",nextline_product="";
        for ( int i=0; i < product.size(); i++){
        	if(i < product.size()-1){
        		nextline_product = "\n";
        	}else{
        		nextline_product = "";
        	}
        	text += String.valueOf(product.get(i)).trim() + nextline_product;
        	
        }

        
        //2.Setting Quantity List
        ArrayList<String> quantity_list = new ArrayList<String>();
        String[] qtt = qtt_list.split(",");
        for ( int qtt_a=0; qtt_a < qtt.length; qtt_a++){
        	quantity_list.add(String.valueOf(qtt[qtt_a]));
        }
        String final_quantity="",nextline_final_quantity="";
        for ( int qtt_b=0; qtt_b < quantity_list.size(); qtt_b++){
        	if(qtt_b < quantity_list.size()-1){
        		nextline_final_quantity = "\n";
        	}else{
        		nextline_final_quantity = "";
        	}
        	final_quantity += String.valueOf(quantity_list.get(qtt_b)).trim() + nextline_final_quantity;
        	
        }
        //
        //3.Setting Price List
        ArrayList<String> price_product_list = new ArrayList<String>();
        String[] p = price_list.split(",");
        for ( int pr_a=0; pr_a < qtt.length; pr_a++){
        	price_product_list.add(String.valueOf(p[pr_a]));
        }
        String final_product_price="",nextline_final_product_price="";
        for ( int pr_b=0; pr_b < price_product_list.size(); pr_b++){
        	if(pr_b < price_product_list.size()-1){
        		nextline_final_product_price = "\n";
        	}else{
        		nextline_final_product_price = "";
        	}
        	final_product_price += String.valueOf(price_product_list.get(pr_b)).trim() + nextline_final_product_price;
        	
        }
        
        //4. Setting data to textview
        text_product_list.setText(text.toString());
        text_product_list_single.setText(text.toString());
        text_product_qty.setText(final_quantity.toString());
        text_product_total_price.setText(final_product_price.toString());
        
        //5. Setting total price 2 decimal place
        DecimalFormat dec = new DecimalFormat("#.00");
        double price_total_ = Double.parseDouble(price_total);
		double Total_Price = ( (double)(price_total_)) * 1;
		Log.d(LOG_TAG, "Total Price : " + dec.format(Total_Price));
		
		text_total_product.setText(String.valueOf(dec.format(Total_Price)));
	}


	private class GetBranchDetail extends AsyncTask<String, Void, String> {

		String cont_id;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;
			cont_id = arg0[0];
			Log.d(LOG_TAG, "cont_id=" + cont_id);

			try {
				c = Helper_Content.getByContID(cont_id);
				Log.d(LOG_TAG, "Total data=" + c.getCount());

				if (c.moveToLast() != false) {

					branch_name = Helper_Content.gettitle(c).toString();
					call_title = Helper_Content.getcall_title(c).toString();
					telephone = Helper_Content.gettelephone(c).toString();
					email_title = Helper_Content.getemail_title(c).toString();				
					email = Helper_Content.getemail(c).toString();
					details = Helper_Content.getdetails(c).toString();
					map_title = Helper_Content.getmap_title(c).toString();
					map_lat = Helper_Content.getmap_lat(c).toString();
					map_lon = Helper_Content.getmap_lon(c).toString();
				}

				c.close();
				response = "SUCCESS";
				// Log.d(LOG_TAG,"responseBody="+ responseBody);

			} catch (Throwable t) {
				response = "FAILED";
				Log.e(LOG_TAG,
						"GetSchedule-doInBackground-Error:" + t.getMessage(), t);
			}
			return response;
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.d(LOG_TAG, "result=" + result);
			try {

				if (result.equals("SUCCESS")) {
					LinearLayout layout_call = (LinearLayout) findViewById(R.id.layout_call);
					LinearLayout layout_email = (LinearLayout) findViewById(R.id.layout_email);
					LinearLayout layout_map = (LinearLayout) findViewById(R.id.layout_map);
					
					
					/*
					TextView content = (TextView) findViewById(R.id.branch_address);dd
					
					Log.d(LOG_TAG, "STATUS - : "+ status);
					Log.d(LOG_TAG, "DETAILS - : "+ details);
					if(status.equals("1") && details.length() > 0){ //***content detail						
						Spanned text = Html.fromHtml(details);
						content.setText(text);
					}/*else if (status.equals("1")) {
						content.setText(tq_message);
					}*/
					/*else if(tq_message.length()>0){
						content.setText(tq_message);
					}else{
						content.setVisibility(View.GONE);
					}
					*/
					TextView lo_branch = (TextView) findViewById(R.id.branchtitle);
					Log.d(LOG_TAG, "branch_name : " + branch_name);
					if(branch_name != null){						
						lo_branch.setText(branch_name);
					} else {
						lo_branch.setVisibility(View.GONE);
					}

					if(telephone != null && telephone.length() > 0 && telephone.equals(null)){  //***call

						layout_call.setVisibility(View.VISIBLE);
						TextView texttitlecall = (TextView) findViewById(R.id.calltitle);
						TextView textcall = (TextView) findViewById(R.id.phone);

						texttitlecall.setText(call_title);
						textcall.setText(telephone);

						layout_call.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View view) {

								Uri uri = Uri.fromParts("tel", telephone, null);
								Intent callIntent = new Intent(Intent.ACTION_CALL, uri);
								callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								startActivity(callIntent);
							}

						});


					}

					if(email != null && email.length()>0 && email.equals(null)){ //*** email

						layout_email.setVisibility(View.VISIBLE);
						TextView texttitleemail = (TextView) findViewById(R.id.emailtitle);
						TextView textemail = (TextView) findViewById(R.id.email);

						texttitleemail.setText(email_title);
						textemail.setText(email);

						if(telephone.length() <1){

							ImageView linephone = (ImageView) findViewById(R.id.linephone);
							linephone.setVisibility(View.GONE);

						}

						layout_email.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View view) {

								Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
								emailIntent.setType("plain/text");
								emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
										new String[] { email });
								emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
								emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
										Html.fromHtml(""));

								startActivity(Intent.createChooser(emailIntent, "Send mail..."));
							}

						});
					}

					Log.d(LOG_TAG, "map_lat == " + map_lat);
					Log.d(LOG_TAG, "map_lat == " + map_lat);
					//if(map_lat != null && map_lon != null ){ //*** map
					if(map_lat.length() >0 && map_lon.length()>0){
						
						layout_map.setVisibility(View.VISIBLE);
						TextView maptitle = (TextView) findViewById(R.id.maptitle);
						maptitle.setText(map_title);

						TextView map_lat_text = (TextView) findViewById(R.id.map_lat);
						map_lat_text.setText(map_lat+","+map_lon);

						if(email.length() <1){

							ImageView linemap = (ImageView) findViewById(R.id.linemap);
							linemap.setVisibility(View.GONE);

						}

						layout_map.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View view) {

								String uri = "geo:" + map_lat + ","+ map_lon;
								startActivity(new Intent(android.content.Intent.ACTION_VIEW,
										Uri.parse(uri)));
							}

						});


					}else{
						layout_map.setVisibility(View.GONE);
					}
				}


			} catch (Throwable t) {
				Log.e(LOG_TAG, "GetBranchDetail-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}



	private View.OnClickListener onClickReservation=new View.OnClickListener() {

		public void onClick(View v) {
			Log.d(LOG_TAG,"reservation Click!");

			try{

				final AlertDialog.Builder alert = new AlertDialog.Builder(Reservation_Detail.this);


				alert.setTitle("Are you sure you want to cancel your reservation?");
				alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {

						try{

							HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
							HttpClient httpclient = new DefaultHttpClient();
							httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

							String strURL = getResources().getString(R.string.RESERVATION_CANCEL_API)
									+ "db=" + URLEncoder.encode(app_db, "UTF-8")
									+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
									+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
									+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
									+ "&bookid="+ URLEncoder.encode(bookid, "UTF-8"); 

							HttpPost httppost = new HttpPost(strURL);
							Log.d(LOG_TAG,"strURL:"+ strURL);
							String responseBody = null;
							ResponseHandler<String> responseHandler = new BasicResponseHandler();
							responseBody = httpclient.execute(httppost, responseHandler);

							Log.d(LOG_TAG, "onLogin-responseBody:" + responseBody);

							JSONArray StatusJSONArray = new JSONArray(responseBody);

							if (StatusJSONArray.length() > 0) {
								status = StatusJSONArray.getJSONObject(0).getString("status").toString();
								status_desc = StatusJSONArray.getJSONObject(0).getString("desc").toString();
								Log.d(LOG_TAG, "status_desc:" + status_desc);
							}

							Toast.makeText(getApplicationContext(), status_desc, Toast.LENGTH_SHORT).show();
							onBackPressed();

						} catch (Throwable t) { 
							Log.e(LOG_TAG, "oncancelreserve-Error:" + t.getMessage(), t); 

						}
					}


				});

				alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.cancel();
					}
				});
				alert.show();       


			}catch (Throwable t) {
				Log.e(LOG_TAG, "Error:" + t.getMessage(), t);

			} 

		}	
	};

//	private View.OnClickListener onBack=new View.OnClickListener() {
//		public void onClick(View v) {
//			try{
//				//finish();
//
//				Intent i=new Intent(Reservation_Detail.this, Reservation_List.class);
//				startActivity(i);
//
//				overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
//			} catch (Throwable t) { 
//				Log.e(LOG_TAG, "onRegister-Error:" + t.getMessage(), t); 
//
//			}
//		}
//
//	};
//
//
//	private View.OnClickListener onClickCall=new View.OnClickListener() {
//
//		public void onClick(View v) {
//			Log.d(LOG_TAG,"Call Click!");
//
//			Uri uri= Uri.fromParts("tel",telephone, null);
//			Intent callIntent = new Intent(Intent.ACTION_CALL,uri);
//			callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//			startActivity(callIntent);
//
//		}	
//	};
//
//	private View.OnClickListener onClickEmail=new View.OnClickListener() {
//
//		public void onClick(View v) {
//			Log.d(LOG_TAG,"Email Click!");
//
//			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
//			emailIntent.setType("plain/text");
//			emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{""});
//			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"");
//			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(""));
//
//			startActivity(Intent.createChooser(emailIntent, "Send mail..."));
//
//		}	
//	};

	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onBackPressed() {

		super.onBackPressed(); // allows standard use of backbutton for page 1
		this.finish();
		overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
	}

	public void onDestroy() {
		Helper_Content.close();

		this.finish();
		super.onStop();
		super.onDestroy();		
	}

}
