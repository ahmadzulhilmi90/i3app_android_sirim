package com.m3tech.reservation;

import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.collection.Collection_content;
import com.m3tech.collection.Collection_contentcategory;
import com.m3tech.data.Helper_BookAdded;
import com.m3tech.data.Helper_BookSet;
import com.m3tech.data.Helper_Content;
import com.m3tech.data.Helper_Contentcategory;
import com.m3tech.header.Header;

public class Reservation_New extends Activity {

	private static final String LOG_TAG = "RESERVATION NEW";
	public static final String PREFS_NAME = "MyPrefsFile";

	static Boolean network_status=false;

	Helper_Content Helper_Content=null;
	Helper_Contentcategory Helper_Contentcategory=null;
	Helper_BookSet Helper_BookSet= null;
	Helper_BookAdded Helper_BookAdded = null;
	String currency,bar_content, branchid,user_id,branchname;
	String responseBody = null;
	String pathFOLDER,max_guest,avai_date,startdate,totalperson,avai_time,tarikhstart,tarikhtamat,laststartdate;
	String prodname,productid,product_name,prodid,enddate,note,status,status_desc, firststartdate;
	String start_date,start_time,end_date,end_time,bookno,lastid,custname,date,starttime,endtime,cust_phone;
	EditText editTextnote,edit_phoneno;
	String   template,v_product,v_branch, v_datestart, v_timestart,v_dateend, v_timeend,v_guesstotal,v_notes,avai_time_start,avai_time_end, tq_message;
	LinearLayout makereservation,layoutviewlist,layouttitleheader,layout_product,layout_branch,layout_startdate,layout_starttime,layout_enddate,layout_endtime,
	layout_totalperson,layout_note,layout_phoneno;
	String home_pagename, layout,page, themecolor, btn_menucolor, tabbar, userid, currentdate,
	filename,customerid,customerID,app_title,colorcode,colorcode1,app_user,app_db,udid,top_id,booking_title,multiple_product,
	display_price,display_total,product_title,display_qtt,product_err;
	int itemCount;
	Button btnsubmit;
	TextView texttitlebar,texthome,textname;
	ImageView back,tabheader;
	ListView listview_child_product;
	ExpandableListView listview_product;
	//Progress
	static ProgressDialog progressDialog;
	private SharedPreferences settings;
	//JSON
	JSONArray booksetJSONArray;
	SlotAction slotAction = null;

	List<Collection_content> model_product = new ArrayList<Collection_content>();
	ArrayAdapter<Collection_content> adapter_product = null;
	Collection_content productlist = null;

	List<Collection_content> model_branch = new ArrayList<Collection_content>();
	ArrayAdapter<Collection_content> adapter_branch = null;
	Collection_content branchtlist = null;
	
	Collection_contentcategory Collection_contentcategory = null;
	Collection_content Collection_content = null;
	
	Header Header = null;
	Context context = this;
	
	String parent_top_id;
	String intent_price_list = "",intent_qtt_list = "",intent_price_total = "0.00";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING "+LOG_TAG);
		setContentView(R.layout.reservation_new);
		slotAction = new SlotAction(this);
		Helper_BookSet = new Helper_BookSet(this);
		Helper_Content = new Helper_Content(this);
		Helper_Contentcategory = new Helper_Contentcategory(this);
		Helper_BookAdded = new Helper_BookAdded(this);
		Helper_BookAdded.Delete();

		cust_phone = "";
		settings = this.getSharedPreferences(PREFS_NAME, 0);
		home_pagename = settings.getString("home_pagename", "");
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		customerID = settings.getString("customerID", "");
		cust_phone = settings.getString("customerPhone", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		colorcode1 = settings.getString("colorcode1", "");

		String pageTitle = settings.getString("rsvPageTitle", "");
		if(pageTitle ==null||pageTitle.equals("")){
			pageTitle = app_title;
		}

		/*** layout linear declaration *****/

		tabheader = (ImageView) findViewById(R.id.tabheader);

		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		textname = (TextView) findViewById(R.id.textname);

		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Listview product multiple ***/
		listview_product = (ExpandableListView)findViewById(R.id.listview_product);
		
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, pageTitle,"reservation");
		
		layoutviewlist =(LinearLayout) findViewById(R.id.layoutviewlist);
		layoutviewlist.setBackgroundColor(Color.parseColor(colorcode1));
		layoutviewlist.setOnClickListener(onClickListing);

		makereservation = (LinearLayout) findViewById(R.id.layoutaddnew);
		makereservation.setBackgroundColor(Color.parseColor(colorcode1));

		btnsubmit = (Button) findViewById(R.id.btnsubmit);    
		btnsubmit.setOnClickListener(onsubmitbutton);

		editTextnote = (EditText) findViewById(R.id.editTextnote); 
		edit_phoneno = (EditText) findViewById(R.id.edit_phoneno); 
		edit_phoneno.setText(cust_phone);

		layout_product=(LinearLayout) findViewById(R.id.layout_product);
		layout_branch=(LinearLayout) findViewById(R.id.layout_branch);
		layout_startdate=(LinearLayout) findViewById(R.id.layout_startdate);
		layout_starttime=(LinearLayout) findViewById(R.id.layout_starttime);
		layout_enddate=(LinearLayout) findViewById(R.id.layout_enddate);
		layout_endtime=(LinearLayout) findViewById(R.id.layout_endtime);
		layout_totalperson=(LinearLayout) findViewById(R.id.layout_totalperson);
		layout_note= (LinearLayout) findViewById(R.id.layout_note);
		layout_phoneno= (LinearLayout) findViewById(R.id.layout_phoneno);

		layout_product.setVisibility(View.GONE);
		layout_branch.setVisibility(View.GONE);
		layout_startdate.setVisibility(View.GONE); 
		layout_starttime.setVisibility(View.GONE); 
		layout_enddate.setVisibility(View.GONE); 
		layout_endtime.setVisibility(View.GONE);
		layout_totalperson.setVisibility(View.GONE);
		layout_note.setVisibility(View.GONE); 
		layout_phoneno.setVisibility(View.GONE); 
		btnsubmit.setVisibility(View.GONE); 
		listview_product.setVisibility(View.GONE);

		productid = "";
		branchid = "";
		startdate = ""; 
		starttime = "";
		enddate = "";
		endtime = "";
		totalperson = "";
		note = "";

		Intent r = getIntent();
		parent_top_id = r.getStringExtra("top_id");
		Log.d(LOG_TAG, "TOP ID RESERVATION PARENT : " + parent_top_id);
		
		//Already get the setting at the listing page. no point getting setting again.
		getReservationSettings Task = new getReservationSettings();
		Task.execute();
		//getViewData();
		
		
	}

	private View.OnClickListener onClickListing=new View.OnClickListener() {

		public void onClick(View v) {
			try{
				Helper_BookAdded.Delete();
				((Activity)context).finish();
				((Activity)context).overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onClickList-Error:" + t.getMessage(), t); 

			}
		}
	};

	private class getReservationSettings extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Reservation_New.this, "", getResources().getString(R.string.please_wait));
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;

			try{

				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_RSVSETTING;
				String strURL = getResources().getString(R.string.RESERVATION_SETTING_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&top_id=" + URLEncoder.encode(parent_top_id, "UTF-8"); ;

				HTTP_POST_RSVSETTING = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);

				ResponseHandler<String> responseHandler_RSVSETTING=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_RSVSETTING, responseHandler_RSVSETTING);	

				Log.d(LOG_TAG,responseBody);
				Log.d(LOG_TAG,"Insert into DB");

				Date d = new Date();
				currentdate = (new SimpleDateFormat("yyyy-MM-dd hh")).format(d);
				Log.d(LOG_TAG,"currentdate:"+currentdate);

				booksetJSONArray = new JSONArray(responseBody);
				Log.d(LOG_TAG,"booksetJSONArray lenght :"+ booksetJSONArray.length());

				response="SUCCESS";


			} catch (Throwable t) { 
				response="FAILED";
				getViewData();
				Log.e(LOG_TAG, "Reservation-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){

					if ( booksetJSONArray.length() > 0){

						Helper_BookSet.Delete();

						for (int n = 0; n < booksetJSONArray.length(); n++) {
							String bookset_id= booksetJSONArray.getJSONObject(n).getString("id").toString();	
							max_guest=booksetJSONArray.getJSONObject(n).getString("max_guesstotal").toString();
							v_product= booksetJSONArray.getJSONObject(n).getString("v_product").toString();	
							v_branch=booksetJSONArray.getJSONObject(n).getString("v_branch").toString();
							v_datestart= booksetJSONArray.getJSONObject(n).getString("v_datestart").toString();	
							v_timestart= booksetJSONArray.getJSONObject(n).getString("v_timestart").toString();
							v_dateend= booksetJSONArray.getJSONObject(n).getString("v_dateend").toString();
							v_timeend= booksetJSONArray.getJSONObject(n).getString("v_timeend").toString();
							v_guesstotal= booksetJSONArray.getJSONObject(n).getString("v_guesstotal").toString();
							v_notes= booksetJSONArray.getJSONObject(n).getString("v_notes").toString();
							avai_date= booksetJSONArray.getJSONObject(n).getString("avai_date").toString();
							avai_time= booksetJSONArray.getJSONObject(n).getString("avai_time").toString();
							avai_time_end= booksetJSONArray.getJSONObject(n).getString("avai_time_end").toString();
							avai_time_start= booksetJSONArray.getJSONObject(n).getString("avai_time_start").toString();
							tq_message= booksetJSONArray.getJSONObject(n).getString("tq_message").toString();
							String book_image_name= booksetJSONArray.getJSONObject(n).getString("book_image_name").toString();
							String book_image= booksetJSONArray.getJSONObject(n).getString("book_image").toString();
							// Reservation v2 : 23/5/2016 12:03 pm
							String top_id= booksetJSONArray.getJSONObject(n).getString("top_id").toString();
							String booking_title= booksetJSONArray.getJSONObject(n).getString("booking_title").toString();
							String multiple_product= booksetJSONArray.getJSONObject(n).getString("multiple_product").toString();
							// Reservation v2.1 : 26/5/2016 10:03 am
							String display_price= booksetJSONArray.getJSONObject(n).getString("display_price").toString();
							String display_total= booksetJSONArray.getJSONObject(n).getString("display_total").toString();
							String product_title= booksetJSONArray.getJSONObject(n).getString("product_title").toString();
							String display_qtt= booksetJSONArray.getJSONObject(n).getString("display_qtt").toString();
							String product_err= booksetJSONArray.getJSONObject(n).getString("product_err").toString();

							
							Log.d(LOG_TAG, "Display Price :" + display_price);
							Log.d(LOG_TAG, "Display Total :" + display_total);
							
							Log.d(LOG_TAG,"Insert into DB-start"); 
							Helper_BookSet.insert(bookset_id,max_guest,v_product,v_branch,
									v_datestart,v_timestart,v_dateend,v_timeend,v_guesstotal,v_notes,
									tq_message,book_image_name,book_image,
									avai_date,avai_time,avai_time_start,avai_time_end,currentdate,top_id,booking_title,multiple_product,
									display_price,display_total,product_title,display_qtt,product_err);
						}

						getViewData();

					}else{
						getViewData();
						Toast.makeText(Reservation_New.this,getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG).show();
					}

				}else{
					getViewData();
					Toast.makeText(Reservation_New.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}


				Reservation_New.progressDialog.dismiss();


			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "onloyalty-Error:" + t.getMessage(), t);
				Reservation_New.progressDialog.dismiss();
			}
		}

	}


	private void getViewData(){
		Cursor g = Helper_BookSet.getAll();
		if (g.moveToLast() != false){

			max_guest = Helper_BookSet.getmax_guesstotal(g).toString();
			v_product = Helper_BookSet.getv_product(g).toString();
			v_branch = Helper_BookSet.getv_branch(g).toString();
			v_datestart = Helper_BookSet.getv_datestart(g).toString();
			v_timestart = Helper_BookSet.getv_timestart(g).toString();
			v_dateend = Helper_BookSet.getv_dateend(g).toString();
			v_timeend = Helper_BookSet.getv_timeend(g).toString();
			v_guesstotal = Helper_BookSet.getv_guesstotal(g).toString();
			v_notes = Helper_BookSet.getv_notes(g).toString();
			avai_time_start=Helper_BookSet.getavai_time_start(g).toString();
			avai_time_end = Helper_BookSet.getavai_time_end(g).toString();
			avai_date = Helper_BookSet.getavai_date(g).toString();
			avai_time = Helper_BookSet.getavai_time(g).toString();
			tq_message = Helper_BookSet.gettq_message(g).toString();
			top_id = Helper_BookSet.gettop_id(g).toString();
			booking_title = Helper_BookSet.getbooking_title(g).toString();
			multiple_product = Helper_BookSet.getmultiple_product(g).toString();
			display_price = Helper_BookSet.getdisplay_price(g).toString();
			display_total = Helper_BookSet.getdisplay_total(g).toString();
			product_title = Helper_BookSet.getproduct_title(g).toString();
			display_qtt = Helper_BookSet.getdisplay_qtt(g).toString();
			product_err = Helper_BookSet.getproduct_err(g).toString();
			
		}
		g.close();
		
		//Set title booking
		textname.setText(product_title);
		
		Log.d(LOG_TAG, "multiple_product : " + multiple_product);
		if(multiple_product.equals("1")){
			listview_product.setVisibility(View.VISIBLE);
			layout_product.setVisibility(View.VISIBLE); 
			Spinner s = (Spinner) findViewById(R.id.list_product);
			s.setVisibility(View.GONE);
			onSetupExpandableListView(listview_product);
			//ParentProduct(v_product);
		}else{
			Log.d(LOG_TAG, "v_product : " + v_product);
			if(!v_product.equals("0")){
				productlistSpinner();
			}
		}

		Log.d(LOG_TAG, "v_branch : " + v_branch);
		if(!v_branch.equals("0")){
			branchlistSpinner();
		}

		Log.d(LOG_TAG, "v_datestart : " + v_datestart);
		if(v_datestart.equals("1")){
			startdatedropdown();
			layout_startdate.setVisibility(View.VISIBLE); 
		}

		Log.d(LOG_TAG, "v_timestart : " + v_timestart);
		if(v_timestart.equals("1")){
			starttimedropdown();
			layout_starttime.setVisibility(View.VISIBLE); 
		}

		Log.d(LOG_TAG, "v_dateend : " + v_dateend);
		if(v_dateend.equals("1")){
			enddatedropdown();
			layout_enddate.setVisibility(View.VISIBLE); 
		}

		Log.d(LOG_TAG, "v_timeend : " + v_timeend);
		if(v_timeend.equals("1")){
			endtimedropdwon();
			layout_endtime.setVisibility(View.VISIBLE); 
		}

		Log.d(LOG_TAG, "v_guesstotal : " + v_guesstotal);
		if(v_guesstotal.equals("1")){
			totalperson();
			layout_totalperson.setVisibility(View.VISIBLE); 
		}

		Log.d(LOG_TAG, "v_notes : " + v_notes);
		if(v_notes.equals("1")){
			layout_note.setVisibility(View.VISIBLE); 
		}
		
		//if()

		layout_phoneno.setVisibility(View.VISIBLE); 
		btnsubmit.setVisibility(View.VISIBLE); 
	}
	
	/*
	public void ParentProduct(String id){
		
		ArrayList<Collection_contentcategory> arrayOfParentProduct = new ArrayList<Collection_contentcategory>();
		String[] parts = id.split(",");
		
        for ( int i=0; i < parts.length; i++){
	        
	    	Cursor c = Helper_Contentcategory.getByCatId(parts[i]);
	    	Collection_contentcategory = new Collection_contentcategory();
				if (c.moveToLast() != false) {
					
					String name = Helper_Contentcategory.getname(c).toString();
					String contentcat_id = Helper_Contentcategory.getcontentcat_id(c).toString();
					Log.d(LOG_TAG, "contentcat_id : " + contentcat_id);
					Log.d(LOG_TAG, "Name : " + name);
					if(name.length()>0){
						Collection_contentcategory.setname(name);
						Collection_contentcategory.setcontentcat_id(contentcat_id);
						arrayOfParentProduct.add(Collection_contentcategory);
					}
				}
			c.close();
        }
        
	     // Create the adapter to convert the array to views
        ParentProductAdapter adapter = new ParentProductAdapter(this, arrayOfParentProduct);
	     // Attach the adapter to a ListView
	     listview_product.setAdapter(adapter);
	    
	     
	}
	
	public class ParentProductAdapter extends ArrayAdapter<Collection_contentcategory> {
	    public ParentProductAdapter(Context context, ArrayList<Collection_contentcategory> parentproduct) {
	       super(context, 0, parentproduct);
	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	       // Get the data item for this position
	    	final Collection_contentcategory parentproduct = getItem(position);    
	       // Check if an existing view is being reused, otherwise inflate the view
	       if (convertView == null) {
	          convertView = LayoutInflater.from(getContext()).inflate(R.layout.reservation_multiple_row, parent, false);
	       }
	       // Lookup view for data population
	       TextView text_parent_product = (TextView) convertView.findViewById(R.id.text_parent_product);
	       // Return the completed view to render on screen
	       text_parent_product.setText(parentproduct.getname().toString());
	       Log.d(LOG_TAG, "contentcat_id : " + parentproduct.getcontentcat_id().toString());
	       
	       //onClick lay_parent_product
	       LinearLayout lay_parent_product = (LinearLayout) convertView.findViewById(R.id.lay_parent_product);
	       //final LinearLayout lay_child_product = (LinearLayout) convertView.findViewById(R.id.lay_child_product);
	       final ListView listview_child_product = (ListView)convertView.findViewById(R.id.listview_child_product);
	       final ImageView img_drop_down = (ImageView)convertView.findViewById(R.id.img_drop_down);
	       
	       lay_parent_product.setOnClickListener(new View.OnClickListener() {
			
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(listview_child_product.getVisibility() == View.VISIBLE){ 
						listview_child_product.setVisibility(View.GONE);
						img_drop_down.setVisibility(View.GONE);
					}else{
						img_drop_down.setVisibility(View.VISIBLE);
						listview_child_product.setVisibility(View.VISIBLE);
						SubProductList(listview_child_product, parentproduct.getcontentcat_id().toString());
					}
					
				}
	       });
	       return convertView;
	   }
	}
	
	
	public void SubProductList(ListView listview,String category_id){
		
		SubProductAdapter adapter ;
		ArrayList<Collection_content> arrayOfsubproduct = new ArrayList<Collection_content>();
		
		Cursor d = Helper_Content.getContentCategoryID(category_id);
		int total = d.getCount();
		if(total > 0){
			for (d.moveToFirst(); !d.isAfterLast(); d.moveToNext()) {
					
					Collection_content = new Collection_content();
		 			String id = Helper_Content.getcontent_id(d).toString();
					String title = Helper_Content.gettitle(d).toString();
					String price_title = Helper_Content.getprice_title(d).toString();
					String price = Helper_Content.getprice(d).toString();
					Log.d(LOG_TAG, "id : "+ id + " title : " +title + " Price : " + price_title+price);
					Collection_content.setcontent_id(id);
					Collection_content.settitle(title);
					Collection_content.setprice_title(price_title);
					Collection_content.setprice(price);
					arrayOfsubproduct.add(Collection_content);
					
		 	}d.close();
		 	
		}else{
			//listview.setVisibility(View.GONE);
			Toast.makeText(context,"Sorry,no product added.",Toast.LENGTH_SHORT).show();
		}
		Log.d(LOG_TAG, "arrayOfsubproduct : " + arrayOfsubproduct.size());
		// Create the adapter to convert the array to views
		adapter = new SubProductAdapter(this, arrayOfsubproduct);
	     // Attach the adapter to a ListView
		listview.setAdapter(adapter);
	}*/
	
	public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                float px = 500 * (listView.getResources().getDisplayMetrics().density);
                item.measure(View.MeasureSpec.makeMeasureSpec((int)px, View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);
            // Get padding
            int totalPadding = listView.getPaddingTop() + listView.getPaddingBottom();

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight + totalPadding;
            listView.setLayoutParams(params);
            listView.requestLayout();
            return true;

        } else {
            return false;
        }

    }
	
	public boolean GetChkAdded(String id){
		Cursor c = Helper_BookAdded.getByContentID(id);
		int total = c.getCount();
		c.close();
		//onGetListBookAdded();
		if(total > 0){
			return true;
		}return false;
	}
	
	//List data separate by comma
	public String onGetListBookAddedeProductID(){
		
		String listsubproduct;
		List<String> myProductList = new ArrayList<String>();
		
		Cursor d = Helper_BookAdded.getAll();
		for (d.moveToFirst(); !d.isAfterLast(); d.moveToNext()) {
			String title = Helper_BookAdded.getcontent_id(d).toString();
			myProductList.add(title);
		}
		d.close();
		listsubproduct = myProductList.toString().replace("[", "").replace("]", "").replace(", ", ",");
		Log.d(LOG_TAG, "myProductList : " + listsubproduct);
		return listsubproduct;
	}
	
	public String onGetListBookAddedePriceList(){
		
		String listsubproduct;
		List<String> myPriceList = new ArrayList<String>();
		
		Cursor d = Helper_BookAdded.getAll();
		for (d.moveToFirst(); !d.isAfterLast(); d.moveToNext()) {
			String price = Helper_BookAdded.getprice(d).toString();
			myPriceList.add(price);
		}
		d.close();
		listsubproduct = myPriceList.toString().replace("[", "").replace("]", "").replace(", ", ",");
		Log.d(LOG_TAG, "myPriceList : " + listsubproduct);
		return listsubproduct;
	}
	
	public String onGetListBookAddedeQuantityList(){
		
		String listsubproduct;
		List<String> myQuantityList = new ArrayList<String>();
		
		Cursor d = Helper_BookAdded.getAll();
		for (d.moveToFirst(); !d.isAfterLast(); d.moveToNext()) {
			String quantity = Helper_BookAdded.getquantity(d).toString();
			myQuantityList.add(quantity);
		}
		d.close();
		listsubproduct = myQuantityList.toString().replace("[", "").replace("]", "").replace(", ", ",");
		Log.d(LOG_TAG, "myQuantityList : " + listsubproduct);
		return listsubproduct;
	}
	
	public double onGetListBookAddedeTotalPrice(){
		double total = 0.00;
		Cursor d = Helper_BookAdded.getAll();
		for (d.moveToFirst(); !d.isAfterLast(); d.moveToNext()) {
			total += Double.parseDouble(Helper_BookAdded.getprice(d).toString());
		}
		d.close();
		Log.d(LOG_TAG, "Total Price : " + total);
		return total;
	}
	/*
	public class SubProductAdapter extends ArrayAdapter<Collection_content> {
	    public SubProductAdapter(Context context, ArrayList<Collection_content> subproduct) {
	       super(context, 0, subproduct);
	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	       // Get the data item for this position
	    	final Collection_content subproduct = getItem(position);    
	       // Check if an existing view is being reused, otherwise inflate the view
	       if (convertView == null) {
	          convertView = LayoutInflater.from(getContext()).inflate(R.layout.reservation_multiple_sub_product_row, parent, false);
	       }
	       // Lookup view for data population
	       TextView text_sub_product = (TextView) convertView.findViewById(R.id.text_sub_product);
	       TextView text_sub_product_price = (TextView) convertView.findViewById(R.id.text_sub_product_price);
	       final CheckBox chk_sub_product = (CheckBox) convertView.findViewById(R.id.chk_sub_product);
	       final Spinner s = (Spinner) convertView.findViewById(R.id.list_quantity);
	       // Return the completed view to render on screen
	       text_sub_product.setText(subproduct.gettitle().toString());
	       chk_sub_product.setTag(subproduct.getcontent_id());
	       text_sub_product_price.setText(subproduct.getprice_title().toUpperCase()+subproduct.getprice());
	       
	       //Check box setting
	       if(GetChkAdded(subproduct.getcontent_id())){
	    	   chk_sub_product.setChecked(true);
	       }else{
	    	   chk_sub_product.setChecked(false);
	       }
	       
	       //Setting Spinner Quantity Sub Product
	       SetQuantitySubProduct(s,subproduct.getcontent_id());
	       s.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
					// TODO Auto-generated method stub
					if(chk_sub_product.isChecked()){
						
						Log.d(LOG_TAG, "Quantity Change : " + s.getSelectedItem());
						int quantity = Integer.parseInt(String.valueOf(s.getSelectedItem()));
						double price = Double.parseDouble(subproduct.getprice());
						DecimalFormat df = new DecimalFormat("#.00");      
						double total = Double.valueOf(df.format(price*quantity));
						Log.d(LOG_TAG, "CHANGE -  Quantity : " + quantity);
						Log.d(LOG_TAG, "CHANGE -  Price : " + price);
						Log.d(LOG_TAG, "CHANGE -  Total Price : " + total);
						Helper_BookAdded.UpdateData(String.valueOf(subproduct.getcontent_id()), String.valueOf(s.getSelectedItem()), String.valueOf(total));
						
					}else{
						Log.d(LOG_TAG, "Not Checked.");
					}
				}
	
				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub
					
				}
		
	       });
	       
	       chk_sub_product.setOnClickListener(new OnClickListener() {

			  @Override
			  public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					Log.d(LOG_TAG, "add");
					Log.d(LOG_TAG, "total_quantity_sub_product : " + s.getSelectedItem());
					int quantity = Integer.parseInt(String.valueOf(s.getSelectedItem()));
					double price = Double.parseDouble(subproduct.getprice());
					//double total = price*quantity;
					DecimalFormat df = new DecimalFormat("#.00");      
					double total = Double.valueOf(df.format(price*quantity));
					Log.d(LOG_TAG, "Quantity : " + quantity);
					Log.d(LOG_TAG, "Price : " + price);
					Log.d(LOG_TAG, "Total Price : " + total);
					
					Helper_BookAdded.insert(subproduct.getcontent_id(), subproduct.gettitle().toString(),String.valueOf(s.getSelectedItem()),String.valueOf(total));
				}else{
					Log.d(LOG_TAG, "remove");
					Helper_BookAdded.DeleteById(subproduct.getcontent_id());
				}
			  }
			});
	       
	       return convertView;
	   }
	}
	*/
	private void SetQuantitySubProduct(Spinner s,String content_id){
		Integer[] myString;

		try{
			

			int g =Integer.parseInt("10");
			Log.d(LOG_TAG, "g=" + g);

			myString = new Integer[g]; //create array
			for (int number = 0; number < g; number++) { 
				myString[number] =  number+1; 
			}

			ArrayAdapter<Integer> arr_adapter = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item, myString);
			arr_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			s.setAdapter(arr_adapter);
			
			
			int selected = 1;
			Cursor c = Helper_BookAdded.getByContentID(content_id);
			if(c.getCount()>0){  
				
				c.moveToFirst();
				do {
					selected = Integer.parseInt(Helper_BookAdded.getquantity(c).toString());
					Log.d(LOG_TAG, "SELECTED : " + String.valueOf(selected));
					if (selected > 0) {
					    int spinnerPosition = arr_adapter.getPosition(selected);
					    s.setSelection(spinnerPosition);
					}
				} while (c.moveToNext());
			}
			c.close();

		}
		catch (Throwable t) {
			Log.e(LOG_TAG, "Exception in totalperson()", t);
		}    	
	}
	

	private void productlistSpinner(){
		try {
			Spinner s = (Spinner) findViewById(R.id.list_product);

			Cursor p = Helper_Content.getContentOrderByName(v_product);
			adapter_product = new ArrayAdapter<Collection_content>(this, android.R.layout.simple_spinner_item, model_product);
			adapter_product.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			if (p.getCount()>0){
				p.moveToFirst();
				do {
					prodname = Helper_Content.gettitle(p).toString();
					prodid = Helper_Content.getcontent_id(p).toString();

					Collection_content productlist = new Collection_content();
					productlist.settitle(prodname);
					productlist.setcontent_id(prodid);
					adapter_product.add(productlist);
				} while (p.moveToNext());

				s.setAdapter(adapter_product);
				s.setOnItemSelectedListener(new product_selected());
				layout_product.setVisibility(View.VISIBLE); 

			} else {
				Log.w(LOG_TAG, "productlistSpinner= NO ITEM");
			}

		}
		catch (Throwable t) {
			Log.e(LOG_TAG, "Exception in spinner product()", t);
		}    	
	}


	public class product_selected implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

			try {
				if (pos > -1) {
					productlist = model_product.get(pos);
					productid = productlist.getcontent_id(); 
					product_name = productlist.gettitle();
					Log.d(LOG_TAG, "product_name="+ product_name);

				} else {
					Toast.makeText(parent.getContext(),"Please Choose Product",Toast.LENGTH_LONG).show();
				}

			} catch (Throwable t) {
				Log.e(LOG_TAG, "Getproduct-doInBackground-Error :" + t.getMessage(), t);
			}

		}

		@SuppressWarnings("rawtypes")
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}



	private void branchlistSpinner(){
		try{
			Spinner s = (Spinner) findViewById(R.id.list_branch);

			Cursor p = Helper_Content.getContentOrderByName(v_branch);
			adapter_branch = new ArrayAdapter<Collection_content>(this, android.R.layout.simple_spinner_item, model_branch);
			adapter_branch.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


			if (p.getCount()>0){
				p.moveToFirst();
				do {
					branchname = Helper_Content.gettitle(p).toString();
					branchid = Helper_Content.getcontent_id(p).toString();

					Collection_content branchtlist = new Collection_content();
					branchtlist.settitle(branchname);
					branchtlist.setcontent_id(branchid);
					adapter_branch.add(branchtlist);
					
				} while (p.moveToNext());

				s.setAdapter(adapter_branch);
				s.setOnItemSelectedListener(new branch_selected());
				layout_branch.setVisibility(View.VISIBLE); 

			} else {
				Log.w(LOG_TAG, "branchlistSpinner= NO ITEM");
			}

		}
		catch (Throwable t) {
			Log.e(LOG_TAG, "Exception in branch product()", t);
		}    	
	}


	public class branch_selected implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {

			try {

				if (pos > -1) {
					branchtlist = model_branch.get(pos);
					branchid = branchtlist.getcontent_id(); 
					branchname = branchtlist.gettitle();

					Log.d(LOG_TAG, "branchname="+ branchname);

				} else {
					Toast.makeText(parent.getContext(),"Please Choose Branch",Toast.LENGTH_LONG).show();
				}

			} catch (Throwable t) {
				Log.e(LOG_TAG,
						"Getbranch-doInBackground-Error :" + t.getMessage(), t);
			}
		}
		@SuppressWarnings("rawtypes")
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}


	private void startdatedropdown(){

		try{

			Spinner s = (Spinner) findViewById(R.id.list_startdate);

			List<String> dateList = Arrays.asList(avai_date.split(","));
			ArrayAdapter<String> arr_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dateList);
			arr_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			s.setAdapter(arr_adapter);
			s.setOnItemSelectedListener(new startdate_selected());
			itemCount = dateList.size();
			Log.d(LOG_TAG, "itemCount="+ itemCount);
		}
		catch (Throwable t) {
			Log.e(LOG_TAG, "Exception in startdate product()", t);
		}    	
	}


	public class startdate_selected implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {

			try {

				if (pos > -1) {
					// get spinner value

					firststartdate =  parent.getItemAtPosition(0).toString();
					Log.d(LOG_TAG, "firststartdate="+ firststartdate);
					laststartdate = parent.getItemAtPosition(itemCount-1).toString();
					Log.d(LOG_TAG, "laststartdate="+ laststartdate);
					startdate = parent.getItemAtPosition(pos).toString();


					Log.d(LOG_TAG, "startdate="+ startdate);
					//Toast.makeText(parent.getContext(),"You Choose "+ startdate + " for start date",Toast.LENGTH_LONG).show();


				} else {
					// show toast select gender
					Toast.makeText(parent.getContext(),"Please Choose StartDate",Toast.LENGTH_LONG).show();
				}

			} catch (Throwable t) {
				Log.e(LOG_TAG,
						"Getstartdate-doInBackground-Error :" + t.getMessage(), t);
			}
		}
		@SuppressWarnings("rawtypes")
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	private void enddatedropdown(){
		try{

			Spinner s = (Spinner) findViewById(R.id.list_todate);

			List<String> dateendList = Arrays.asList(avai_date.split(","));
			ArrayAdapter<String> arr_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dateendList);
			arr_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			s.setAdapter(arr_adapter);

			s.setOnItemSelectedListener(new enddate_selected());

		}
		catch (Throwable t) {
			Log.e(LOG_TAG, "Exception in startdate product()", t);
		}    	
	}


	public class enddate_selected implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {

			try {

				if (pos > -1) {
					// get spinner value

					enddate = parent.getItemAtPosition(pos).toString();


					Log.d(LOG_TAG, "enddate="+ enddate);
					//Toast.makeText(parent.getContext(),"You Choose "+ enddate +" for end date",Toast.LENGTH_LONG).show();


				} else {
					// show toast select gender
					Toast.makeText(parent.getContext(),"Please Choose enddate",Toast.LENGTH_LONG).show();
				}

			} catch (Throwable t) {

				Log.e(LOG_TAG,
						"Getenddate-doInBackground-Error :" + t.getMessage(),
						t);
			}

			// carian_project.progressDialog.dismiss();

		}
		@SuppressWarnings("rawtypes")
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}


	private void starttimedropdown(){

		try{

			Spinner s = (Spinner) findViewById(R.id.list_starttime);

			List<String> starttimelist = Arrays.asList(avai_time.split(","));
			ArrayAdapter<String> arr_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, starttimelist);
			arr_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			s.setAdapter(arr_adapter);

			s.setOnItemSelectedListener(new starttime_selected());

		}
		catch (Throwable t) {
			Log.e(LOG_TAG, "Exception in starttime()", t);
		}    	
	}


	public class starttime_selected implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {

			try {

				if (pos > -1) {
					// get spinner value

					starttime = parent.getItemAtPosition(pos).toString();


					Log.d(LOG_TAG, "starttime="+ starttime);
					//Toast.makeText(parent.getContext(),"You Choose "+ starttime + " for start time",Toast.LENGTH_LONG).show();


				} else {
					// show toast select gender
					Toast.makeText(parent.getContext(),"Please Choose Starttime",Toast.LENGTH_LONG).show();
				}

			} catch (Throwable t) {

				Log.e(LOG_TAG,
						"Getstarttime-doInBackground-Error :" + t.getMessage(),
						t);
			}

		}
		@SuppressWarnings("rawtypes")
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}



	private void endtimedropdwon(){
		try{
			Spinner s = (Spinner) findViewById(R.id.list_totime);

			List<String> starttimelist = Arrays.asList(avai_time.split(","));
			ArrayAdapter<String> arr_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, starttimelist);
			arr_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			s.setAdapter(arr_adapter);

			s.setOnItemSelectedListener(new endtime_selected());

		}
		catch (Throwable t) {
			Log.e(LOG_TAG, "Exception in starttime()", t);
		}    	
	}


	public class endtime_selected implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

			try {

				if (pos > -1) {
					// get spinner value
					endtime = parent.getItemAtPosition(pos).toString();

					Log.d(LOG_TAG, "endtime="+ endtime);
					//Toast.makeText(parent.getContext(),"You Choose "+ endtime + " for end time",Toast.LENGTH_LONG).show();

				} else {
					// show toast select gender
					Toast.makeText(parent.getContext(),"Please Choose Endtime",Toast.LENGTH_LONG).show();
				}

			} catch (Throwable t) {

				Log.e(LOG_TAG,
						"Getendtime-doInBackground-Error :" + t.getMessage(),
						t);
			}

		}
		@SuppressWarnings("rawtypes")
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}



	private void totalperson(){
		Integer[] myString;

		try{

			int g =Integer.parseInt(max_guest);
			Log.d(LOG_TAG, "g=" + g);

			myString = new Integer[g]; //create array
			for (int number = 0; number < g; number++) { 
				myString[number] =  number+1; 
			}

			Spinner s = (Spinner) findViewById(R.id.list_person);

			ArrayAdapter<Integer> arr_adapter = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item, myString);
			arr_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			s.setAdapter(arr_adapter);
			s.setOnItemSelectedListener(new tatalperson_selected());

		}
		catch (Throwable t) {
			Log.e(LOG_TAG, "Exception in totalperson()", t);
		}    	
	}

	public class tatalperson_selected implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {

			try {

				if (pos > -1) {
					// get spinner value

					totalperson = parent.getItemAtPosition(pos).toString();

					Log.d(LOG_TAG, "totalperson="+ totalperson);
					//Toast.makeText(parent.getContext(),"You Choose "+ totalperson + " for total person",Toast.LENGTH_LONG).show();


				} else {
					// show toast select gender
					Toast.makeText(parent.getContext(),"Please Choose totalperson",Toast.LENGTH_LONG).show();
				}

			} catch (Throwable t) {

				Log.e(LOG_TAG,
						"Getenddate-doInBackground-Error :" + t.getMessage(),
						t);
			}

			// carian_project.progressDialog.dismiss();

		}
		@SuppressWarnings("rawtypes")
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public static int getResourceId(Context context, String name, String resourceType) {   //this will returns you Drawable with that String Variable.
		return context.getResources().getIdentifier(name, resourceType, context.getPackageName());
	}



	@SuppressLint("SimpleDateFormat")
	private View.OnClickListener onsubmitbutton=new View.OnClickListener() {


		public void onClick(View v) {
			try{
				
				String listadded = String.valueOf(onGetListBookAddedeProductID());
				Log.d(LOG_TAG, " listadded : " + listadded);
				Log.d(LOG_TAG, " productid : " + productid);

				if(multiple_product.equals("1")){
					
					if(listadded.length() > 0){
						boolean isOk = true;

						InputMethodManager imm = (InputMethodManager)getSystemService(
								Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(editTextnote.getWindowToken(), 0);


						note =  editTextnote.getText().toString().trim(); 
						cust_phone = edit_phoneno.getText().toString().trim(); 
						if (cust_phone.length()==0) {
							isOk = false;
							Toast.makeText(Reservation_New.this,
									"Please enter your mobile phone no.", Toast.LENGTH_LONG).show();
						} else {

							SharedPreferences.Editor editor = settings.edit();
							editor.remove("customerPhone");
							editor.putString("customerPhone",cust_phone);
							editor.commit();



							if(v_dateend.equals("1")){  //check dateend available or not


								tarikhstart = startdate + " " + starttime;
								tarikhtamat = enddate + " " + endtime ;

								Log.d(LOG_TAG, "tarikhstart="+ tarikhstart);
								Log.d(LOG_TAG, "tarikhtamat="+ tarikhtamat);

								String myFormatString = "yyyy-MM-dd HH:mm"; // for example
								SimpleDateFormat df = new SimpleDateFormat(myFormatString,Locale.US);

								Date end = df.parse(tarikhtamat);
								Date start = df.parse(tarikhstart);

								String gh =  firststartdate + " " + avai_time_start;
								Date avaistartdatetime = df.parse(gh);

								if(start.hashCode() >= avaistartdatetime.hashCode()){
									Log.d(LOG_TAG, "date select valid"); 

									if(end.after(start)){
										Log.d(LOG_TAG, "OK: end.after(start)");
									} else {
										isOk = false;
										Toast.makeText(Reservation_New.this,
												"Error. Invalid end date.",
												Toast.LENGTH_LONG).show();
									}
								} else {
									isOk = false;
									Toast.makeText(Reservation_New.this,
											"Error. Invalid date.", Toast.LENGTH_SHORT).show();
								}

							} else {

								Log.d(LOG_TAG, "xde end date");
								enddate = "0000-00-00";
								endtime = "00:00:00";
							}
						}

						
						
						if (isOk) {
							
							if(multiple_product.equals("1")){
								productid = String.valueOf(onGetListBookAddedeProductID());
								intent_price_list = String.valueOf(onGetListBookAddedePriceList());
								intent_qtt_list = String.valueOf(onGetListBookAddedeQuantityList());
								intent_price_total = String.valueOf(onGetListBookAddedeTotalPrice());
								Log.d(LOG_TAG, "-- > productid : " + productid);
								Log.d(LOG_TAG, "-- > price_list : " + intent_price_list);
								Log.d(LOG_TAG, "-- > qtt_list : " + intent_qtt_list);
								Log.d(LOG_TAG, "-- > price_total : " + intent_price_total);
							}
							
							HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
							HttpClient httpclient = new DefaultHttpClient();
							httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

							String strURL = getResources().getString(R.string.RESERVATION_SUBMIT_API)
									+ "db=" + URLEncoder.encode(app_db, "UTF-8")
									+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
									+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
									+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
									+ "&product_id="+ URLEncoder.encode(productid, "UTF-8") 
									+ "&branch_id=" + URLEncoder.encode(branchid, "UTF-8") 
									+ "&date_start=" +  URLEncoder.encode(startdate, "UTF-8") 
									+ "&time_start=" + URLEncoder.encode(starttime, "UTF-8")
									+ "&date_end=" + URLEncoder.encode(enddate, "UTF-8")
									+ "&time_end=" + URLEncoder.encode(endtime, "UTF-8")
									+ "&guess_total=" + URLEncoder.encode(totalperson, "UTF-8")
									+ "&notes=" + URLEncoder.encode(note, "UTF-8")
									+ "&cust_phone=" + URLEncoder.encode(cust_phone, "UTF-8")
									+ "&price_list=" + URLEncoder.encode(intent_price_list, "UTF-8")
									+ "&qtt_list=" + URLEncoder.encode(intent_qtt_list, "UTF-8")
									+ "&price_total=" + URLEncoder.encode(intent_price_total, "UTF-8")
									+ "&top_id=" + URLEncoder.encode(top_id, "UTF-8");

							Log.d(LOG_TAG,"strURL-url: "+ strURL);
							HttpPost httppost = new HttpPost(strURL);
							String responseBody = null;
							ResponseHandler<String> responseHandler = new BasicResponseHandler();
							responseBody = httpclient.execute(httppost, responseHandler);

							Log.d(LOG_TAG, "onLogin-responseBody:" + responseBody);

							JSONArray StatusJSONArray = new JSONArray(responseBody);

							if (StatusJSONArray.length() > 0) {
								status = StatusJSONArray.getJSONObject(0).getString("status").toString();
								status_desc = StatusJSONArray.getJSONObject(0).getString("desc").toString();
								bookno = StatusJSONArray.getJSONObject(0).getString("booking_no").toString();
								lastid = StatusJSONArray.getJSONObject(0).getString("lastid").toString();
								custname = StatusJSONArray.getJSONObject(0).getString("custname").toString();
								date = StatusJSONArray.getJSONObject(0).getString("date").toString();
								Log.d(LOG_TAG, "status: " + status);
								Log.d(LOG_TAG, "status_desc: " + status_desc);
								Log.d(LOG_TAG, "bookno: " + bookno);
								Log.d(LOG_TAG, "lastid: " + lastid);
								Log.d(LOG_TAG, "custname: " + custname);
							}
							Helper_BookAdded.Delete();
							if(status.equals("1")){
								succesfulSubmit();
							}else{
								Toast.makeText(Reservation_New.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_SHORT).show();

							} 

						}
					}else{
						Toast.makeText(Reservation_New.this,product_err, Toast.LENGTH_SHORT).show();
					}
					
				}else{
					
					//if(productid.length() > 0){
						boolean isOk = true;

						InputMethodManager imm = (InputMethodManager)getSystemService(
								Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(editTextnote.getWindowToken(), 0);


						note =  editTextnote.getText().toString().trim(); 
						cust_phone = edit_phoneno.getText().toString().trim(); 
						if (cust_phone.length()==0) {
							isOk = false;
							Toast.makeText(Reservation_New.this,
									"Please enter your mobile phone no.", Toast.LENGTH_LONG).show();
						} else {

							SharedPreferences.Editor editor = settings.edit();
							editor.remove("customerPhone");
							editor.putString("customerPhone",cust_phone);
							editor.commit();



							if(v_dateend.equals("1")){  //check dateend available or not


								tarikhstart = startdate + " " + starttime;
								tarikhtamat = enddate + " " + endtime ;

								Log.d(LOG_TAG, "tarikhstart="+ tarikhstart);
								Log.d(LOG_TAG, "tarikhtamat="+ tarikhtamat);

								String myFormatString = "yyyy-MM-dd HH:mm"; // for example
								SimpleDateFormat df = new SimpleDateFormat(myFormatString,Locale.US);

								Date end = df.parse(tarikhtamat);
								Date start = df.parse(tarikhstart);

								String gh =  firststartdate + " " + avai_time_start;
								Date avaistartdatetime = df.parse(gh);

								if(start.hashCode() >= avaistartdatetime.hashCode()){
									Log.d(LOG_TAG, "date select valid"); 

									if(end.after(start)){
										Log.d(LOG_TAG, "OK: end.after(start)");
									} else {
										isOk = false;
										Toast.makeText(Reservation_New.this,
												"Error. Invalid end date.",
												Toast.LENGTH_LONG).show();
									}
								} else {
									isOk = false;
									Toast.makeText(Reservation_New.this,
											"Error. Invalid date.", Toast.LENGTH_SHORT).show();
								}

							} else {

								Log.d(LOG_TAG, "xde end date");
								enddate = "0000-00-00";
								endtime = "00:00:00";
							}
						}

						
						
						if (isOk) {
							
							if(multiple_product.equals("1")){
								productid = String.valueOf(onGetListBookAddedeProductID());
								intent_price_list = String.valueOf(onGetListBookAddedePriceList());
								intent_qtt_list = String.valueOf(onGetListBookAddedeQuantityList());
								intent_price_total = String.valueOf(onGetListBookAddedeTotalPrice());
								Log.d(LOG_TAG, "-- > productid : " + productid);
								Log.d(LOG_TAG, "-- > price_list : " + intent_price_list);
								Log.d(LOG_TAG, "-- > qtt_list : " + intent_qtt_list);
								Log.d(LOG_TAG, "-- > price_total : " + intent_price_total);
							}
							Log.d(LOG_TAG, "productid : "+ productid);
							HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
							HttpClient httpclient = new DefaultHttpClient();
							httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

							String strURL = getResources().getString(R.string.RESERVATION_SUBMIT_API)
									+ "db=" + URLEncoder.encode(app_db, "UTF-8")
									+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
									+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
									+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
									+ "&product_id="+ URLEncoder.encode(productid, "UTF-8") 
									+ "&branch_id=" + URLEncoder.encode(branchid, "UTF-8") 
									+ "&date_start=" +  URLEncoder.encode(startdate, "UTF-8") 
									+ "&time_start=" + URLEncoder.encode(starttime, "UTF-8")
									+ "&date_end=" + URLEncoder.encode(enddate, "UTF-8")
									+ "&time_end=" + URLEncoder.encode(endtime, "UTF-8")
									+ "&guess_total=" + URLEncoder.encode(totalperson, "UTF-8")
									+ "&notes=" + URLEncoder.encode(note, "UTF-8")
									+ "&cust_phone=" + URLEncoder.encode(cust_phone, "UTF-8")
									+ "&price_list=" + URLEncoder.encode(intent_price_list, "UTF-8")
									+ "&qtt_list=" + URLEncoder.encode(intent_qtt_list, "UTF-8")
									+ "&price_total=" + URLEncoder.encode(intent_price_total, "UTF-8")
									+ "&top_id=" + URLEncoder.encode(top_id, "UTF-8");

							Log.d(LOG_TAG,"strURL-url: "+ strURL);
							HttpPost httppost = new HttpPost(strURL);
							String responseBody = null;
							ResponseHandler<String> responseHandler = new BasicResponseHandler();
							responseBody = httpclient.execute(httppost, responseHandler);

							Log.d(LOG_TAG, "onLogin-responseBody:" + responseBody);

							JSONArray StatusJSONArray = new JSONArray(responseBody);

							if (StatusJSONArray.length() > 0) {
								status = StatusJSONArray.getJSONObject(0).getString("status").toString();
								status_desc = StatusJSONArray.getJSONObject(0).getString("desc").toString();
								bookno = StatusJSONArray.getJSONObject(0).getString("booking_no").toString();
								lastid = StatusJSONArray.getJSONObject(0).getString("lastid").toString();
								custname = StatusJSONArray.getJSONObject(0).getString("custname").toString();
								date = StatusJSONArray.getJSONObject(0).getString("date").toString();
								Log.d(LOG_TAG, "status: " + status);
								Log.d(LOG_TAG, "status_desc: " + status_desc);
								Log.d(LOG_TAG, "bookno: " + bookno);
								Log.d(LOG_TAG, "lastid: " + lastid);
								Log.d(LOG_TAG, "custname: " + custname);
							}
							Helper_BookAdded.Delete();
							if(status.equals("1")){
								succesfulSubmit();
							}else{
								Toast.makeText(Reservation_New.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_SHORT).show();

							} 

						}
					/*	
					}else{
						Toast.makeText(Reservation_New.this,product_err, Toast.LENGTH_SHORT).show();
					}*/
					
				}
				

			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onsubmit-Error:" + t.getMessage(), t); 

			}
		}

	};


	public void succesfulSubmit(){
		String branch_id="0";
		if(multiple_product.equals("0")){
			
			if (!branchid.equals("")) {
				branch_id = branchid;
			} else if (!productid.equals("")) {
				branch_id = productid;
			}
		}else{
			branch_id = "0";
		}

		Log.d(LOG_TAG, "product_id == " + productid);
		Log.d(LOG_TAG, "branch_id == " + branch_id);
		Log.d(LOG_TAG, "bookno == " + bookno);
		Log.d(LOG_TAG, "custname == " + custname);
		Log.d(LOG_TAG, "datebook == " + startdate);
		Log.d(LOG_TAG, "bookid == " + lastid);
		Log.d(LOG_TAG, "status == " + status);
		Log.d(LOG_TAG, "status_desc == " + status_desc);
		Log.d(LOG_TAG, "tq_message == " + tq_message);
		Log.d(LOG_TAG, "display_price == " + display_price);
		Log.d(LOG_TAG, "display_total == " + display_total);
		Log.d(LOG_TAG, "display_qtt == " + display_qtt);
		Log.d(LOG_TAG, "price_list == " + String.valueOf(intent_price_list));
		Log.d(LOG_TAG, "price_total == " + String.valueOf(intent_price_total));
		Log.d(LOG_TAG, "qtt_list == " + String.valueOf(intent_qtt_list));
		
		
		Intent i=new Intent(Reservation_New.this, Reservation_Detail.class);
		i.putExtra("product_id", productid);
		i.putExtra("branch_id",branch_id);
		i.putExtra("bookno", bookno);
		i.putExtra("custname",custname);
		i.putExtra("datebook", startdate);
		i.putExtra("bookid", lastid);
		i.putExtra("status", status);
		i.putExtra("status_desc", status_desc);					
		i.putExtra("tq_message", tq_message);
		i.putExtra("display_price", display_price);
		i.putExtra("display_total", display_total);
		i.putExtra("display_qtt", display_qtt);
		i.putExtra("price_list", String.valueOf(intent_price_list));
		i.putExtra("price_total", String.valueOf(intent_price_total));
		i.putExtra("qtt_list", String.valueOf(intent_qtt_list));
		i.putExtra("multiple_product", String.valueOf(multiple_product));
		startActivity(i);
		this.finish();
		
	}

	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;

	}

	/*********************************************************************/
	
	
	ExpandableListAdapter listAdapter;
	//ExpandableListView expListView;
	List<String> listDataHeader;
	//List<Collection_contentcategory> arrayOfParentProduct;
	HashMap<String, List<String>> listDataChild;
	
	public void onSetupExpandableListView(final ExpandableListView expListView){
		
		
		// preparing list data
			prepareListData();

			listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

			// setting list adapter
			expListView.setAdapter(listAdapter);
			setListViewHeightBasedOnItems(expListView);

			// Listview Group click listener
			expListView.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
						int groupPosition, long id) {
					// Toast.makeText(getApplicationContext(),
					// "Group Clicked " + listDataHeader.get(groupPosition),
					// Toast.LENGTH_SHORT).show();
					
					return false;
				}
			});

			// Listview Group expanded listener
			expListView.setOnGroupExpandListener(new OnGroupExpandListener() {

				@Override
				public void onGroupExpand(int groupPosition) {
					setListViewHeightBasedOnItems(expListView);
					
				}
			});

			// Listview Group collasped listener
			expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {

				@Override
				public void onGroupCollapse(int groupPosition) {
					setListViewHeightBasedOnItems(expListView);
				}
			});

			// Listview on child click listener
			expListView.setOnChildClickListener(new OnChildClickListener() {

				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
						int groupPosition, int childPosition, long id) {
				
					return false;
				}
			});
			
			
			expListView.setOnGroupExpandListener(new OnGroupExpandListener() {

			    public void onGroupExpand(int groupPosition) {


			        for(int i=0; i<listAdapter.getGroupCount(); i++) {
			            if(i != groupPosition) {
			            	expListView.collapseGroup(i);
			            }
			        }
			    }

			});
		}
		/*
		 * Preparing the list data
		 */
		private void prepareListData() {
			listDataHeader = new ArrayList<String>();
			listDataChild = new HashMap<String, List<String>>();
			
			// Adding title
			List<Collection_contentcategory> arrayOfParentProduct = new ArrayList<Collection_contentcategory>();
			String[] parts = v_product.split(",");
			
	        for ( int i=0; i < parts.length; i++){
		        
		    	Cursor c = Helper_Contentcategory.getByCatId(parts[i]);
		    	Collection_contentcategory = new Collection_contentcategory();
					if (c.moveToLast() != false) {
						
						String name = Helper_Contentcategory.getname(c).toString();
						String contentcat_id = Helper_Contentcategory.getcontentcat_id(c).toString();
						Log.d(LOG_TAG, "contentcat_id : " + contentcat_id);
						Log.d(LOG_TAG, "Name : " + name);
						if(name.length()>0){
							Collection_contentcategory.setname(name);
							Collection_contentcategory.setcontentcat_id(contentcat_id);
							arrayOfParentProduct.add(Collection_contentcategory);
							
						}
					}
				c.close();
	        }

			for(int i=0; i<arrayOfParentProduct.size(); i++){
				//listDataChild.put(listDataHeader.get(i), top250);
				Log.d(LOG_TAG, "listDataHeader.get(i) : " + arrayOfParentProduct.get(i).getname());
				listDataHeader.add(arrayOfParentProduct.get(i).getname());
				
				List<String> arrayOfsubproduct = new ArrayList<String>();
				
				Cursor d = Helper_Content.getContentCategoryID(arrayOfParentProduct.get(i).getcontentcat_id());
				int total = d.getCount();
				if(total > 0){
					for (d.moveToFirst(); !d.isAfterLast(); d.moveToNext()) {
							
							Collection_content = new Collection_content();
				 			String id = Helper_Content.getcontent_id(d).toString();
							String title = Helper_Content.gettitle(d).toString();
							if(title.equals(null)) { title="null"; }
							String price_title = Helper_Content.getprice_title(d).toString();
							if(price_title.equals(null)) { price_title="null"; }
							String price = Helper_Content.getprice(d).toString();
							if(price.equals(null)) { price="null"; }
							Log.d(LOG_TAG, "id : "+ id + " title : " +title + " Price : " + price_title+price);
							Collection_content.setcontent_id(id);
							Collection_content.settitle(title);
							Collection_content.setprice_title(price_title);
							Collection_content.setprice(price);
							arrayOfsubproduct.add(id+","+title+","+price_title+","+price);
							
				 	}d.close();
				 	
				}
				for(int b=0; b<arrayOfsubproduct.size(); b++){
					listDataChild.put(listDataHeader.get(i), arrayOfsubproduct);
				}
				
			}
			
		}
	
		public class ExpandableListAdapter extends BaseExpandableListAdapter {

			private Context _context;
			private List<String> _listDataHeader; // header titles
			// child data in format of header title, child title
			private HashMap<String, List<String>> _listDataChild;

			public ExpandableListAdapter(Context context, List<String> listDataHeader,
					HashMap<String, List<String>> listChildData) {
				this._context = context;
				this._listDataHeader = listDataHeader;
				this._listDataChild = listChildData;
			}

			@Override
			public Object getChild(int groupPosition, int childPosititon) {
				return this._listDataChild.get(this._listDataHeader.get(groupPosition))
						.get(childPosititon);
			}

			@Override
			public long getChildId(int groupPosition, int childPosition) {
				return childPosition;
			}

			@Override
			public View getChildView(int groupPosition, final int childPosition,
					boolean isLastChild, View convertView, ViewGroup parent) {

				final String childText = (String) getChild(groupPosition, childPosition);
				
				/*** Split TextString sub-product ***/
				String[] parts = childText.split(",");
				final String content_id = parts[0];
				final String title = parts[1];
				String price_title = parts[2];
				final String price_ = parts[3];
				
				if (convertView == null) {
					LayoutInflater infalInflater = (LayoutInflater) this._context
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					convertView = infalInflater.inflate(R.layout.list_item, null);
				}
				
				TextView text_sub_product_price = (TextView) convertView.findViewById(R.id.text_sub_product_price);
				final CheckBox chk_sub_product = (CheckBox) convertView.findViewById(R.id.chk_sub_product);
				TextView text_title_sub_product = (TextView) convertView.findViewById(R.id.lblListItem);
				final Spinner s = (Spinner) convertView.findViewById(R.id.list_quantity);

				//Setting Spinner Quantity Sub Product
				SetQuantitySubProduct(s,content_id.toString());
				
				text_title_sub_product.setText(title.toString());
				text_sub_product_price.setText(price_title.toUpperCase().toString()+price_.toUpperCase().toString());
				
				//Check box setting
		       if(GetChkAdded(content_id.toString())){
		    	   chk_sub_product.setChecked(true);
		       }else{
		    	   chk_sub_product.setChecked(false);
		       }
		       
		       s.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent, View view,
							int position, long id) {
							// TODO Auto-generated method stub
							if(chk_sub_product.isChecked()){
								
								Log.d(LOG_TAG, "Quantity Change : " + s.getSelectedItem());
								int quantity = Integer.parseInt(String.valueOf(s.getSelectedItem()));
								double price = Double.parseDouble(price_);
								DecimalFormat df = new DecimalFormat("###.##");      
								double total = Double.valueOf(df.format(price*quantity));
								Log.d(LOG_TAG, "CHANGE -  Quantity : " + quantity);
								Log.d(LOG_TAG, "CHANGE -  Price : " + price);
								
								DecimalFormat dec = new DecimalFormat("#.00");
								double Total_Price = ( (double)(total)) * 1;
								Log.d(LOG_TAG, "Total Price : " + dec.format(Total_Price));
								
								Helper_BookAdded.UpdateData(String.valueOf(content_id), String.valueOf(s.getSelectedItem()), String.valueOf(dec.format(Total_Price)));
								
							}else{
								Log.d(LOG_TAG, "Not Checked.");
							}
						}
			
						@Override
						public void onNothingSelected(AdapterView<?> parent) {
							// TODO Auto-generated method stub
							
						}
				
			       });
		       
		       chk_sub_product.setOnClickListener(new OnClickListener() {

					  @Override
					  public void onClick(View v) {
						if (((CheckBox) v).isChecked()) {
							Log.d(LOG_TAG, "add");
							Log.d(LOG_TAG, "total_quantity_sub_product : " + s.getSelectedItem());
							int quantity = Integer.parseInt(String.valueOf(s.getSelectedItem()));
							double price = Double.parseDouble(price_);
							//double total = price*quantity;
							DecimalFormat df = new DecimalFormat("###.##");      
							double total = Double.valueOf(df.format(price*quantity));
							Log.d(LOG_TAG, "Quantity : " + quantity);
							Log.d(LOG_TAG, "Price : " + price);
							
							
							DecimalFormat dec = new DecimalFormat("#.00");
							double Total_Price = ( (double)(total))*1;
							Log.d(LOG_TAG, "Total Price : " + dec.format(Total_Price));
							
							Helper_BookAdded.insert(content_id, title.toString(),String.valueOf(s.getSelectedItem()),String.valueOf(dec.format(Total_Price)));
						}else{
							Log.d(LOG_TAG, "remove");
							Helper_BookAdded.DeleteById(content_id.toString());
						}
					  }
					});
				
				return convertView;
			}

			@Override
			public int getChildrenCount(int groupPosition) {
				return this._listDataChild.get(this._listDataHeader.get(groupPosition))
						.size();
			}

			@Override
			public Object getGroup(int groupPosition) {
				return this._listDataHeader.get(groupPosition);
			}

			@Override
			public int getGroupCount() {
				return this._listDataHeader.size();
			}

			@Override
			public long getGroupId(int groupPosition) {
				return groupPosition;
			}

			@Override
			public View getGroupView(int groupPosition, boolean isExpanded,
					View convertView, ViewGroup parent) {
				String headerTitle = (String) getGroup(groupPosition);
				if (convertView == null) {
					LayoutInflater infalInflater = (LayoutInflater) this._context
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					convertView = infalInflater.inflate(R.layout.list_group, null);
				}

				TextView lblListHeader = (TextView) convertView
						.findViewById(R.id.lblListHeader);
				lblListHeader.setTypeface(null, Typeface.BOLD);
				lblListHeader.setText(headerTitle);

				return convertView;
			}

			@Override
			public boolean hasStableIds() {
				return false;
			}

			@Override
			public boolean isChildSelectable(int groupPosition, int childPosition) {
				return true;
			}

		}
	
	/*********************************************************************/
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onBackPressed() {
		super.onBackPressed(); // allows standard use of backbutton for page 1
		Helper_BookAdded.Delete();
		this.finish();
		overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
	}

	public void onDestroy() {
		Helper_Content.close();
		Helper_Contentcategory.close();
		Helper_BookAdded.close();
		Helper_BookSet.close();
		this.finish();
		super.onStop();
		super.onDestroy();		
	}

}
