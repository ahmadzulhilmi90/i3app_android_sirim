package com.m3tech.reservation;


import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.collection.Collection_BookingDetail;
import com.m3tech.data.Helper_BookSet;
import com.m3tech.data.Helper_Content;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;

public class Reservation_List extends Activity {

	private static final String LOG_TAG = "RESERVATION LIST";
	public static final String PREFS_NAME = "MyPrefsFile";

	private SharedPreferences settings;
	Boolean network_status;

	Helper_Content Helper_Content=null;
	Helper_BookSet Helper_BookSet= null;
	String top_id,home_pagename, layout, page, userid, currentdate, customerID,app_title,colorcode,colorcode1,app_user,app_db,udid, tq_message;
	TextView product_cat_name,texttitlebar,texthome;
	LinearLayout layoutviewlist,makereservation;
	ImageView back,tabheader;
	String display_price,display_total,display_qtt,multiple_product;

	//Progress
	static ProgressDialog progressDialog;

	//JSON
	JSONArray bookJSONArray, booksetJSONArray;
	SlotAction slotAction = null;

	List<Collection_BookingDetail> model_bookingDetail=new ArrayList<Collection_BookingDetail>();
	bookingDetail_Adapter adapter_bookingDetail=null;
	Collection_BookingDetail current_bookingDetail=null;
	
	Helper_Themeobject Helper_Themeobject = null;
	Context context = this;
	Header Header = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING RESERVATION LIST ");
		setContentView(R.layout.reservation_list);
		slotAction = new SlotAction(this);
		Helper_Content = new Helper_Content(this);
		Helper_BookSet = new Helper_BookSet(this);
		Helper_Themeobject = new Helper_Themeobject(this);

		settings = this.getSharedPreferences(PREFS_NAME, 0);
		home_pagename = settings.getString("home_pagename", "");
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		customerID = settings.getString("customerID", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		colorcode1 = settings.getString("colorcode1", "");

		String pageTitle = settings.getString("rsvPageTitle", "");
		if(pageTitle ==null||pageTitle.equals("")){
			pageTitle = app_title;
		}


		/*** layout linear declaration *****/

		tabheader = (ImageView) findViewById(R.id.tabheader);

		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);

		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, pageTitle,"");


		layoutviewlist =(LinearLayout) findViewById(R.id.layoutviewlist);
		layoutviewlist.setBackgroundColor(Color.parseColor(colorcode1));
		layoutviewlist.setOnClickListener(onClickListing);

		makereservation = (LinearLayout) findViewById(R.id.layoutaddnew);
		makereservation.setBackgroundColor(Color.parseColor(colorcode1));
		makereservation.setOnClickListener(onClickReservation);
		
		Intent r = getIntent();
		top_id = r.getStringExtra("topid_reservation");
		Log.d(LOG_TAG, "TOP ID RESERVATION PARENT : " + top_id);

		network_status = isNetworkAvailable();
		if(network_status == true){
			getReservationSettings Task = new getReservationSettings();
			Task.execute(new String[] { });  
		}else{
			
			Toast.makeText(Reservation_List.this,getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_LONG).show();
		} 
	}


	private class GetReservationList extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;
			try{
				responseBody = "";
				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_TOP;

				String strURL = getResources().getString(R.string.RESERVATION_LIST_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&top_id=" + URLEncoder.encode(top_id, "UTF-8");

				HTTP_POST_TOP = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);
				//Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	
				bookJSONArray = new JSONArray(responseBody);
				response="SUCCESS";
				//Log.d(LOG_TAG,"responseBody="+ responseBody);	



			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetReservationList-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				ListView listView = (ListView) findViewById(R.id.listbooking);
				adapter_bookingDetail=new bookingDetail_Adapter ();
				adapter_bookingDetail.clear();
				adapter_bookingDetail.notifyDataSetChanged();
				listView.setAdapter(null);

				if (result.equals("SUCCESS")){

					if (bookJSONArray.length()>0){

						Log.d(LOG_TAG,"bookJSONArray lenght :"+ bookJSONArray.length());
						for (int i = 0; i < bookJSONArray.length(); i++) {

							current_bookingDetail=new Collection_BookingDetail();
							current_bookingDetail.setbooking_no(bookJSONArray.getJSONObject(i).getString("booking_no").toString());
							current_bookingDetail.setbranch_id(bookJSONArray.getJSONObject(i).getString("branch_id").toString());
							current_bookingDetail.setbranch_name(bookJSONArray.getJSONObject(i).getString("branch_name").toString());
							current_bookingDetail.setproduct_id(bookJSONArray.getJSONObject(i).getString("product_id").toString());
							current_bookingDetail.setproduct_name(bookJSONArray.getJSONObject(i).getString("product_name").toString());
							current_bookingDetail.setid(bookJSONArray.getJSONObject(i).getString("id").toString());
							current_bookingDetail.setcdate(bookJSONArray.getJSONObject(i).getString("cdate").toString());
							current_bookingDetail.setname(bookJSONArray.getJSONObject(i).getString("customer_name").toString());
							current_bookingDetail.setstatus(bookJSONArray.getJSONObject(i).getString("status").toString());
							current_bookingDetail.setstatus_desc(bookJSONArray.getJSONObject(i).getString("status_desc").toString());
							current_bookingDetail.setdate_start(bookJSONArray.getJSONObject(i).getString("date_start").toString());
							current_bookingDetail.settime_start(bookJSONArray.getJSONObject(i).getString("time_start").toString());
							current_bookingDetail.setprice_list(bookJSONArray.getJSONObject(i).getString("price_list").toString());
							current_bookingDetail.setprice_total(bookJSONArray.getJSONObject(i).getString("price_total").toString());
							current_bookingDetail.setqtt_list(bookJSONArray.getJSONObject(i).getString("qtt_list").toString());
							adapter_bookingDetail.add(current_bookingDetail);

						}
						listView.setAdapter(adapter_bookingDetail);			
						listView.setOnItemClickListener(onListClickbook);	

					}else{
						//Toast.makeText(Reservation_List.this,getResources().getString(R.string.rsv_nodata), Toast.LENGTH_LONG).show();
					}

				}else{
					Toast.makeText(Reservation_List.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}


				Reservation_List.progressDialog.dismiss();

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "GetReservationList-onPostExecute-Error:" + t.getMessage(), t);
				Reservation_List.progressDialog.dismiss();
			}
		}

	}



	class bookingDetail_Adapter extends ArrayAdapter<Collection_BookingDetail> {
		bookingDetail_Adapter() {
			super(Reservation_List.this, R.layout.reservation_row, model_bookingDetail);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View row=convertView;
			NotificationHolderhome holder=null;

			if (row==null) {													
				LayoutInflater inflater=getLayoutInflater();

				row=inflater.inflate(R.layout.reservation_row, parent, false);
				holder=new NotificationHolderhome(row);
				row.setTag(holder);
			} else {
				holder=(NotificationHolderhome)row.getTag();
			}

			Log.d(LOG_TAG,"Booking No :"+ model_bookingDetail.get(position).getbooking_no());
			holder.populateFromhome(model_bookingDetail.get(position));

			return(row);
		}
	}

	class NotificationHolderhome {
		@SuppressWarnings("unused")
		private View row=null;
		private TextView book_title=null;
		private TextView book_date=null;
		private TextView book_place=null;
		private TextView book_status=null;

		NotificationHolderhome(View row) {
			this.row=row;	
			book_title=(TextView) row.findViewById(R.id.texttitle);
			book_date=(TextView) row.findViewById(R.id.textdate);
			book_place=(TextView) row.findViewById(R.id.textplace);
			book_status=(TextView) row.findViewById(R.id.textstatus);
		}

		void populateFromhome(Collection_BookingDetail r) {

			
			String time_start,bookingdate;
			if(r.getdate_start().length() > 0){
				if(r.gettime_start().equals("")){ 
					time_start = "00:00:00";
				}else{ 
					time_start = r.gettime_start(); 
				}
				String date = r.getdate_start()+" "+time_start;
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
				Date testDate = null;
				try {
					testDate = sdf.parse(date);
				}catch(Exception ex){
					ex.printStackTrace();
				}
				Log.d(LOG_TAG, "testDate : " + testDate);
				SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd, yyyy hh:mm a");
				bookingdate = formatter.format(testDate);
				
			}else{
				
				SimpleDateFormat sdfDate = new SimpleDateFormat("EEE MMM dd, yyyy hh:mm a");
			    Date now = new Date();
			    bookingdate = sdfDate.format(now);
			}
			
			String bookingtitle = "#" + r.getbooking_no();
			Log.d(LOG_TAG,"bookingtitle :"+ bookingtitle);
			book_title.setText(bookingtitle);

			Log.d(LOG_TAG,"bookingdate :"+ bookingdate);
			book_date.setText(bookingdate);

			String bookingplace = "";

			if (!r.getbranch_id().equals("0")) {
				bookingplace = r.getbranch_name();
				Log.d(LOG_TAG,"bookingplace :"+ bookingplace);
				book_place.setText(bookingplace);
			} else if (!r.getproduct_id().equals("0")) {
				bookingplace = r.getproduct_name();
				Log.d(LOG_TAG,"product_name :"+ bookingplace);
				book_place.setText(bookingplace);
			} else {
				book_place.setVisibility(View.GONE);
			}
			book_place.setVisibility(View.GONE);
			String status = r.getstatus();
			String bookingstatus = r.getstatus_desc();
			Log.d(LOG_TAG,"bookingstatus :"+ bookingstatus);
			book_status.setText(bookingstatus);
			Log.d(LOG_TAG, " status : " + status);
			if (status.equals("2")) {
				book_status.setBackgroundColor(Color.parseColor("#20B57F"));
			} else {
				book_status.setBackgroundColor(Color.parseColor("#FF9C00"));
			}

		}

	}



	private AdapterView.OnItemClickListener onListClickbook=new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent,
				View view, int position,
				long id) {
			current_bookingDetail=model_bookingDetail.get(position);	
			//Log.d(LOG_TAG,"onListClick table name="+current_bookinghDetail.getname());

			String branch_id="0";
			if (!current_bookingDetail.getbranch_id().equals("0")) {
				branch_id = current_bookingDetail.getbranch_id();
			} else if (!current_bookingDetail.getproduct_id().equals("0")) {
				branch_id = current_bookingDetail.getproduct_id();
			}
			
			String bookingdate,time_start;
			if(current_bookingDetail.getdate_start().length() > 0){
				if(current_bookingDetail.gettime_start().equals("")){ 
					time_start = "00:00:00";
				}else{ 
					time_start = current_bookingDetail.gettime_start(); 
				}
				String date = current_bookingDetail.getdate_start()+" "+time_start;
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
				Date testDate = null;
				try {
					testDate = sdf.parse(date);
				}catch(Exception ex){
					ex.printStackTrace();
				}
				Log.d(LOG_TAG, "testDate : " + testDate);
				SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd, yyyy hh:mm a");
				bookingdate = formatter.format(testDate);
				
			}else{
				
				SimpleDateFormat sdfDate = new SimpleDateFormat("EEE MMM dd, yyyy hh:mm a");
			    Date now = new Date();
			    bookingdate = sdfDate.format(now);
			}

			Log.d(LOG_TAG, "TQ MESSAGE onCLICK : " + tq_message);
			Log.d(LOG_TAG, "STATUS onCLICK : " + current_bookingDetail.getstatus());
			
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
			Intent i=new Intent(Reservation_List.this, Reservation_Detail.class);
			i.putExtra("product_id", current_bookingDetail.getproduct_id());
			i.putExtra("bookno", current_bookingDetail.getbooking_no());
			i.putExtra("custname", current_bookingDetail.getname());
			i.putExtra("datebook", bookingdate);
			i.putExtra("bookid", current_bookingDetail.getid());
			i.putExtra("customer_id", customerID);
			i.putExtra("branch_id", branch_id);
			i.putExtra("status", current_bookingDetail.getstatus());
			i.putExtra("status_desc", current_bookingDetail.getstatus_desc());
			i.putExtra("tq_message", tq_message);
			i.putExtra("display_price", display_price);
			i.putExtra("display_total", display_total);
			i.putExtra("display_qtt", display_qtt);
			i.putExtra("price_list", current_bookingDetail.getprice_list());
			i.putExtra("price_total", current_bookingDetail.getprice_total());
			i.putExtra("qtt_list", current_bookingDetail.getqtt_list());
			i.putExtra("multiple_product", multiple_product);
			
			startActivity(i);

		}
	};

	private View.OnClickListener onClickReservation=new View.OnClickListener() {
		public void onClick(View v) {
			try{
				//finish();
				Log.d(LOG_TAG, "TOP_ID : " + top_id);
				Intent i=new Intent(Reservation_List.this, Reservation_New.class);
				i.putExtra("top_id", top_id);
				startActivity(i);
				overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onClickReservation-Error:" + t.getMessage(), t); 

			}
		}

	};

	private View.OnClickListener onClickListing=new View.OnClickListener() {

		public void onClick(View v) {
			try{
				network_status = isNetworkAvailable();
				if(network_status == true){
					GetReservationList Task = new GetReservationList();
					Task.execute(new String[] { });  
				}
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onClickList-Error:" + t.getMessage(), t); 

			}
		}
	};

	private class getReservationSettings extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Reservation_List.this, "", getResources().getString(R.string.please_wait));
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;

			try{
				/*
				Date d = new Date();
				currentdate = (new SimpleDateFormat("yyyy-MM-dd hh")).format(d);
				Log.d(LOG_TAG,"currentdate: "+currentdate);

				Cursor c_time4=Helper_BookSet.getByLastUpdate(currentdate);
				if (c_time4.moveToLast()){
					Log.d(LOG_TAG,"Get TQ Message");
					tq_message = Helper_BookSet.gettq_message(c_time4).toString();
					display_price = Helper_BookSet.getdisplay_price(c_time4).toString();
					display_qtt = Helper_BookSet.getdisplay_qtt(c_time4).toString();
					display_total = Helper_BookSet.getdisplay_total(c_time4).toString();
					response="DONE";

				} else {
					Log.d(LOG_TAG,"Get TQ Message FROM Cloud");

					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

					HttpPost HTTP_POST_RSVSETTING;
					String strURL = getResources().getString(R.string.RESERVATION_SETTING_API)
							+ "db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
							+ "&top_id=" + URLEncoder.encode(top_id, "UTF-8"); 

					HTTP_POST_RSVSETTING = new HttpPost(strURL);
					Log.d(LOG_TAG,"strURL="+ strURL);

					ResponseHandler<String> responseHandler_RSVSETTING=new BasicResponseHandler();
					responseBody=httpclient.execute(HTTP_POST_RSVSETTING, responseHandler_RSVSETTING);	

					Log.d(LOG_TAG,responseBody);
					Log.d(LOG_TAG,"Insert into DB");

					booksetJSONArray = new JSONArray(responseBody);
					Log.d(LOG_TAG,"booksetJSONArray lenght :"+ booksetJSONArray.length());

					response="SUCCESS";
				}
				c_time4.close();
				*/
				
				Log.d(LOG_TAG,"### getReservationSettings ###");

				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_RSVSETTING;
				String strURL = getResources().getString(R.string.RESERVATION_SETTING_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&top_id=" + URLEncoder.encode(top_id, "UTF-8"); 

				HTTP_POST_RSVSETTING = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);

				ResponseHandler<String> responseHandler_RSVSETTING=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_RSVSETTING, responseHandler_RSVSETTING);	

				Log.d(LOG_TAG,responseBody);
				Log.d(LOG_TAG,"Insert into DB");

				booksetJSONArray = new JSONArray(responseBody);
				Log.d(LOG_TAG,"booksetJSONArray lenght :"+ booksetJSONArray.length());

				response="SUCCESS";


			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "getReservationSettings-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){

					if ( booksetJSONArray.length() > 0){

						Helper_BookSet.Delete();

						for (int n = 0; n < booksetJSONArray.length(); n++) {
							tq_message= booksetJSONArray.getJSONObject(n).getString("tq_message").toString();
							String bookset_id= booksetJSONArray.getJSONObject(n).getString("id").toString();	
							String max_guest=booksetJSONArray.getJSONObject(n).getString("max_guesstotal").toString();
							String v_product= booksetJSONArray.getJSONObject(n).getString("v_product").toString();	
							String v_branch=booksetJSONArray.getJSONObject(n).getString("v_branch").toString();
							String v_datestart= booksetJSONArray.getJSONObject(n).getString("v_datestart").toString();	
							String v_timestart= booksetJSONArray.getJSONObject(n).getString("v_timestart").toString();
							String v_dateend= booksetJSONArray.getJSONObject(n).getString("v_dateend").toString();
							String v_timeend= booksetJSONArray.getJSONObject(n).getString("v_timeend").toString();
							String v_guesstotal= booksetJSONArray.getJSONObject(n).getString("v_guesstotal").toString();
							String v_notes= booksetJSONArray.getJSONObject(n).getString("v_notes").toString();
							String avai_date= booksetJSONArray.getJSONObject(n).getString("avai_date").toString();
							String avai_time= booksetJSONArray.getJSONObject(n).getString("avai_time").toString();
							String avai_time_end= booksetJSONArray.getJSONObject(n).getString("avai_time_end").toString();
							String avai_time_start= booksetJSONArray.getJSONObject(n).getString("avai_time_start").toString();
							String book_image_name= booksetJSONArray.getJSONObject(n).getString("book_image_name").toString();
							String book_image= booksetJSONArray.getJSONObject(n).getString("book_image").toString();
							// Reservation v2 : 23/5/2016 12:03 pm
							String top_id= booksetJSONArray.getJSONObject(n).getString("top_id").toString();
							String booking_title= booksetJSONArray.getJSONObject(n).getString("booking_title").toString();
							multiple_product= booksetJSONArray.getJSONObject(n).getString("multiple_product").toString();
							// Reservation v2.1 : 26/5/2016 10:03 am
							display_price= booksetJSONArray.getJSONObject(n).getString("display_price").toString();
							display_total= booksetJSONArray.getJSONObject(n).getString("display_total").toString();
							String product_title= booksetJSONArray.getJSONObject(n).getString("product_title").toString();
							display_qtt= booksetJSONArray.getJSONObject(n).getString("display_qtt").toString();
							String product_err= booksetJSONArray.getJSONObject(n).getString("product_err").toString();
							
							
							Log.d(LOG_TAG,"Insert into DB-start"); 
							Helper_BookSet.insert(bookset_id,max_guest,v_product,v_branch,
									v_datestart,v_timestart,v_dateend,v_timeend,v_guesstotal,v_notes,
									tq_message,book_image_name,book_image,
									avai_date,avai_time,avai_time_start,avai_time_end,currentdate,top_id,booking_title,multiple_product,
									display_price,display_total,product_title,display_qtt,product_err);
						}

						GetReservationList Task = new GetReservationList();
						Task.execute(new String[] { }); 
						
					}

				} else if (!result.equals("DONE")){
					Cursor c_time4=Helper_BookSet.getAll();
					if (c_time4.moveToLast()){
						Log.d(LOG_TAG,"Get TQ Message");
						tq_message = Helper_BookSet.gettq_message(c_time4).toString();
					}
					c_time4.close();
				}

				 

			} catch (Throwable t) {
				Log.e(LOG_TAG, "getReservationSettings-Error:" + t.getMessage(), t);
			}
		}

	}

	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onResume() {
		super.onResume();

		network_status = isNetworkAvailable();
		if(network_status == true){
			GetReservationList Task = new GetReservationList();
			Task.execute(new String[] { });  
		}
	}


	public void onBackPressed() {
		adapter_bookingDetail.clear();
		adapter_bookingDetail.notifyDataSetChanged();
		super.onBackPressed(); // allows standard use of backbutton for page 1
		this.finish();
		overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
	}

	public void onDestroy() {
		adapter_bookingDetail.clear();
		adapter_bookingDetail.notifyDataSetChanged();
		this.finish();
		super.onStop();
		super.onDestroy();		
	}

}
