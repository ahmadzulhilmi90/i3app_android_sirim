package com.m3tech.background;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.m3tech.appv3.SlotAction;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class PageBackground {

	private static final String LOG_TAG = "HEADER";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();
	SharedPreferences settings;
	String colorcode,background;
	SlotAction slotAction = null;
	
	public PageBackground(Context c,ImageView page){
		Log.d(LOG_TAG, LOG_TAG);
		
		slotAction = new SlotAction(c);
		
		settings = c.getSharedPreferences(PREFS_NAME, 0);
		colorcode = settings.getString("colorcode", "");
		background = settings.getString("background", "");
		
		/**** Page Background ****/
		if (background.length()>10){
		
			String bgUri = slotAction.getImageURI(background);
			String draw = "drawable://";
			if (bgUri.toLowerCase().contains(draw.toLowerCase())) {
				bgUri = bgUri.replace("drawable://", "");
				Drawable d = c.getResources().getDrawable(Integer.parseInt(bgUri));
				page.setBackground(d);
			} else {
				
				imageLoader.displayImage(bgUri, page, displayOptions, new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {		
					}
					@Override
					public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
						
					}
					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					}
					@Override
					public void onLoadingCancelled(String imageUri, View view) {
					}

				});
			}
			
		}else{
			if (background.length()>5) {
			page.setBackgroundColor(Color.parseColor("#FFFFFF"));
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public static DisplayImageOptions getDisplayImageOptions() {
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}
}
