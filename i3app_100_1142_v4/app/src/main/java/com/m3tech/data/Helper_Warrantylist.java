package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Warrantylist extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "warrantylist.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Warrantylist(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE warrantylist (_id INTEGER PRIMARY KEY AUTOINCREMENT, warrantylist_id TEXT, user_id TEXT, customer_id TEXT, " +
				"unregister_id TEXT, title TEXT, dealer TEXT, serial_number TEXT, model_number TEXT, purchase_date TEXT,warranty_period TEXT," +
				"icon TEXT,img_product TEXT,img_receipt TEXT,img_warranty TEXT,comments TEXT,date_inserted TEXT,date_updated TEXT," +
				"status TEXT,brand TEXT,category TEXT,lastupdate TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM warrantylist",
				
						null));
	}
	
	 // Search product

    public Cursor getSearchByName(String key) {
        String[] args = { "%"+key+"%","%"+key+"%","%"+key+"%" }; //

        return (getReadableDatabase()
                .rawQuery(
                        "SELECT * FROM warrantylist WHERE title LIKE ? OR dealer LIKE ? OR comments LIKE ?",
                        args));
    }
	
	
	public Cursor getByLastUpdate(String lastupdate) {
		String[] args={lastupdate};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM warrantylist WHERE lastupdate=?",
											args));
	}
	
	public void insert(String warrantylist_id, String user_id, String customer_id, String unregister_id, String title, String dealer, 
			String serial_number, String model_number, String purchase_date, String warranty_period, String icon,String img_product,
			String img_receipt,String img_warranty,String comments,String date_inserted,String date_updated,String status,
			String brand,String category, String lastupdate){
		ContentValues cv = new ContentValues();
		cv.put("warrantylist_id", warrantylist_id);
		cv.put("user_id", user_id);
		cv.put("customer_id",customer_id);
		cv.put("unregister_id",unregister_id);
		cv.put("title", title);
		cv.put("dealer", dealer);
		cv.put("serial_number", serial_number);
		cv.put("model_number", model_number);
		cv.put("purchase_date", purchase_date);
		cv.put("warranty_period", warranty_period);
		cv.put("icon", icon);
		cv.put("img_product", img_product);
		cv.put("img_receipt", img_receipt);
		cv.put("img_warranty", img_warranty);
		cv.put("comments", comments);
		cv.put("date_inserted", date_inserted);
		cv.put("date_updated", date_updated);
		cv.put("status", status);
		cv.put("brand", brand);
		cv.put("category", category);
		cv.put("lastupdate", lastupdate);
		
		getWritableDatabase().insert("warrantylist", "_id", cv);
	}

	public Cursor getLastUpdate() {
		String[] args={};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM warrantylist ORDER BY date_updated DESC LIMIT 1",
											args));
	}
	

	public void Delete() {
		getWritableDatabase().delete("warrantylist", null,null);
	}
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getwarrantylist_id(Cursor c){
		return(c.getString(1));
	}
	
	public String getuser_id(Cursor c){
		return(c.getString(2));
	}
	
	public String getcustomer_id(Cursor c){
		return(c.getString(3));
	}
	
	public String getunregister_id(Cursor c){
		return(c.getString(4));
	}
	
	public String gettitle(Cursor c){
		return(c.getString(5));
	}
	
	public String getdealer(Cursor c){
		return(c.getString(6));
	}
	public String getserial_number(Cursor c){
		return(c.getString(7));
	}
	public String getmodel_number(Cursor c){
		return(c.getString(8));
	}
	public String getpurchase_date(Cursor c){
		return(c.getString(9));
	}
	public String getwarranty_period(Cursor c){
		return(c.getString(10));
	}
	public String geticon(Cursor c){
		return(c.getString(11));
	}
	public String getimg_product(Cursor c){
		return(c.getString(12));
	}
	public String getimg_receipt(Cursor c){
		return(c.getString(13));
	}
	public String getimg_warranty(Cursor c){
		return(c.getString(14));
	}
	public String getcomments(Cursor c){
		return(c.getString(15));
	}
	public String getdate_inserted(Cursor c){
		return(c.getString(16));
	}
	public String getdate_updated(Cursor c){
		return(c.getString(17));
	}
	public String getstatus(Cursor c){
		return(c.getString(18));
	}
	public String getbrand(Cursor c){
		return(c.getString(19));
	}
	public String getcategory(Cursor c){
		return(c.getString(20));
	}
	public String getlastupdate(Cursor c){
		return(c.getString(21));
	}
	
}
