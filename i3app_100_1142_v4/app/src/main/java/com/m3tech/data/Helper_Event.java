package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Event extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "event.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Event(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE event (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"row_id TEXT," +
				"event_name TEXT," +
				"event_description TEXT," +
				"venue TEXT," +
				"date_start TEXT," +
				"date_end TEXT," +
				"time_start TEXT," +
				"time_end TEXT," +
				"pax_limit TEXT," +
				"latitude TEXT," +
				"longitude TEXT," +
				"url TEXT," +
				"cutoff TEXT," +
				"notification TEXT," +
				"status TEXT," +
				"cdate TEXT," +
				"mdate TEXT," +
				"user_id TEXT," +
				"top_id TEXT," +
				"schedule_date TEXT," +
				"page_id TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM event",null));
	}
	
	public void insert(String row_id,String event_name,String event_description,String venue,String date_start,String date_end,
			String time_start,String time_end,String pax_limit,String latitude,String longitude,String url, String cutoff,
			String notification,String status,String cdate,String mdate,String user_id,String top_id,String schedule_date,String page_id){
		ContentValues cv = new ContentValues();
		cv.put("row_id", row_id);
		cv.put("event_name", event_name);
		cv.put("event_description", event_description);
		cv.put("venue", venue);
		cv.put("date_start", date_start);
		cv.put("date_end", date_end);
		cv.put("time_start", time_start);
		cv.put("time_end", time_end);
		cv.put("pax_limit", pax_limit);
		cv.put("latitude", latitude);
		cv.put("longitude", longitude);
		cv.put("url", url);
		cv.put("cutoff", cutoff);
		cv.put("notification", notification);
		cv.put("status", status);
		cv.put("cdate", cdate);
		cv.put("mdate", mdate);
		cv.put("user_id", user_id);
		cv.put("top_id", top_id);
		cv.put("schedule_date", schedule_date);
		cv.put("page_id", page_id);
		
		getWritableDatabase().insert("event", "_id", cv);
	}
	
	public void Delete() {
		getWritableDatabase().delete("event", null,null);
	}

	public String getId(Cursor c) {
		return (c.getString(0));
	}

	public String getrow_id(Cursor c) {
		return (c.getString(1));
	}
	public String getevent_name(Cursor c) {
		return (c.getString(2));
	}
	public String getevent_description(Cursor c) {
		return (c.getString(3));
	}
	public String getvenue(Cursor c) {
		return (c.getString(4));
	}
	public String getdate_start(Cursor c) {
		return (c.getString(5));
	}
	public String getdate_end(Cursor c) {
		return (c.getString(6));
	}
	public String gettime_start(Cursor c) {
		return (c.getString(7));
	}
	public String gettime_end(Cursor c) {
		return (c.getString(8));
	}
	public String getpax_limit(Cursor c) {
		return (c.getString(9));
	}
	public String getlatitude(Cursor c) {
		return (c.getString(10));
	}
	public String getlongitude(Cursor c) {
		return (c.getString(11));
	}
	public String geturl(Cursor c) {
		return (c.getString(12));
	}
	public String getcutoff(Cursor c) {
		return (c.getString(13));
	}
	public String getnotification(Cursor c) {
		return (c.getString(14));
	}
	public String getstatus(Cursor c) {
		return (c.getString(15));
	}
	public String getcdate(Cursor c) {
		return (c.getString(16));
	}
	public String getmdate(Cursor c) {
		return (c.getString(17));
	}
	public String getuser_id(Cursor c) {
		return (c.getString(18));
	}
	public String gettop_id(Cursor c) {
		return (c.getString(19));
	}
	public String getschedule_date(Cursor c) {
		return (c.getString(20));
	}
	public String getpage_id(Cursor c) {
		return (c.getString(21));
	}
}
