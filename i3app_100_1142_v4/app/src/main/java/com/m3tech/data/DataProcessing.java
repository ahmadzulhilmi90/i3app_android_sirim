package com.m3tech.data;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.JSONArray;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.m3tech.collection.Collection_PointSettings;
import com.m3tech.app_100_1474.R;


public class DataProcessing {

	private static final String LOG_TAG = "Data Processing";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	public static final String COMMON_FILE = "MyCommonFile"; 	

	SharedPreferences settings;
	SharedPreferences commonfile;
	
	// Declare Helper
	Helper_Themepage Helper_Themepage = null;
	Helper_Appsetup Helper_Appsetup = null;
	Helper_Themecolor Helper_Themecolor = null;
	Helper_Themeobject Helper_Themeobject = null;
	Helper_Themeobjectplugin Helper_Themeobjectplugin = null;
	Helper_Content Helper_Content = null;
	Helper_Contentcategory Helper_Contentcategory = null;
	Helper_Loyalty Helper_Loyalty = null;
	Helper_Ibeacondevice Helper_Ibeacondevice = null;
	Helper_Ibeaconcontent Helper_Ibeaconcontent = null;
	Helper_Ibeaconxref Helper_Ibeaconxref =null;

	Context context;

	public DataProcessing(Context ctx) {
		context = ctx;
		settings = context.getSharedPreferences(PREFS_NAME, 0);
		commonfile = context.getSharedPreferences(COMMON_FILE, 0);

		// call helper
		Helper_Themepage = new Helper_Themepage(context);	
		Helper_Appsetup = new Helper_Appsetup(context);
		Helper_Themecolor = new Helper_Themecolor(context);
		Helper_Themeobject = new Helper_Themeobject(context);
		Helper_Themeobjectplugin = new Helper_Themeobjectplugin(context);
		Helper_Content = new Helper_Content(context);
		Helper_Contentcategory = new Helper_Contentcategory(context);
		Helper_Loyalty = new Helper_Loyalty(context);
		Helper_Ibeacondevice = new Helper_Ibeacondevice(context);
		Helper_Ibeaconcontent = new Helper_Ibeaconcontent(context);
		Helper_Ibeaconxref = new Helper_Ibeaconxref(context);
	}

	public void CreateContent_Appsetup(String strCheckDate, String strAPI_APPSETUP) {
		JSONArray appsetupJSONArray;
		
		try{
			appsetupJSONArray = new JSONArray(strAPI_APPSETUP);
			Log.d(LOG_TAG,"CreateContent_Appsetup"); 

			if (appsetupJSONArray.length()>0) {
				Helper_Appsetup.Delete();			

				for (int n = 0; n < appsetupJSONArray.length(); n++) {
					String app_id= appsetupJSONArray.getJSONObject(n).getString("id").toString();	
					String app_name=appsetupJSONArray.getJSONObject(n).getString("name").toString();
					String app_icon_name=appsetupJSONArray.getJSONObject(n).getString("icon_name").toString();
					String app_icon_pic=appsetupJSONArray.getJSONObject(n).getString("icon_pic").toString();
					String app_currency = appsetupJSONArray.getJSONObject(n).getString("currency").toString();
					String app_splash_page=appsetupJSONArray.getJSONObject(n).getString("splash_page").toString();
					String app_user_id=appsetupJSONArray.getJSONObject(n).getString("user_id").toString();
					String app_themecolor=appsetupJSONArray.getJSONObject(n).getString("themecolor").toString();
					String app_tnc=appsetupJSONArray.getJSONObject(n).getString("termscons_url").toString();
					String app_policy=appsetupJSONArray.getJSONObject(n).getString("policy_url").toString();
					String app_custtitle=appsetupJSONArray.getJSONObject(n).getString("android_link").toString();
					String app_background=appsetupJSONArray.getJSONObject(n).getString("background").toString();
					String date_updated=appsetupJSONArray.getJSONObject(n).getString("date_updated").toString();
					String app_loyaltytitle=appsetupJSONArray.getJSONObject(n).getString("titlebar_img").toString();
					String app_loyaltystatus=appsetupJSONArray.getJSONObject(n).getString("show_title").toString();
					String app_branchtitle="";
					String app_product_title="";
					String app_profile_title="";
					
					Log.d(LOG_TAG,"Insert into DB : " + n); 
					Helper_Appsetup.insert(app_id,app_name,app_icon_name,app_icon_pic,app_background,
							app_splash_page,app_user_id,app_themecolor,strCheckDate,
							app_icon_pic,app_splash_page,app_currency,
							app_product_title,app_profile_title,app_tnc,
							app_policy,app_loyaltytitle,app_loyaltystatus,
							app_branchtitle,app_custtitle,app_background,date_updated);

				}		
			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "CreateContent_Appsetup-Error:" + t.getMessage(), t); 
		}
	}

	public void CreateContent_Themepage(String strCheckDate, String strAPI_THEMEPAGE) {
		JSONArray themepageJSONArray;

		try{
			themepageJSONArray = new JSONArray(strAPI_THEMEPAGE);
			Log.d(LOG_TAG,"CreateContent_Themepage"); 

			if (themepageJSONArray.length()>0) {
				Helper_Themepage.Delete();

				for (int n = 0; n < themepageJSONArray.length(); n++) {
					String themepage_id= themepageJSONArray.getJSONObject(n).getString("id").toString();	
					String theme_code=themepageJSONArray.getJSONObject(n).getString("theme_code").toString();
					String name=themepageJSONArray.getJSONObject(n).getString("name").toString();
					String prev_page=themepageJSONArray.getJSONObject(n).getString("prev_page").toString();
					String remark=themepageJSONArray.getJSONObject(n).getString("remark").toString();	
					String date_updated=themepageJSONArray.getJSONObject(n).getString("date_updated").toString();
					String user_id=themepageJSONArray.getJSONObject(n).getString("user_id").toString();

					Log.d(LOG_TAG,"Insert into DB : " + n); 
					Helper_Themepage.insert(themepage_id,theme_code,name,prev_page,
							user_id,remark,date_updated,strCheckDate);
				}


			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "CreateContent_Themepage-Error:" + t.getMessage(), t); 
		}
	}

	public void CreateContent_Themeobject(String strCheckDate, String strAPI_THEMEOBJECT) {
		JSONArray themeobjectJSONArray;
		try{
			themeobjectJSONArray=new JSONArray(strAPI_THEMEOBJECT);
			Log.d(LOG_TAG,"CreateContent_Themeobject"); 

			if (themeobjectJSONArray.length()>0) {
				Helper_Themeobject.Delete();

				for (int n = 0; n < themeobjectJSONArray.length(); n++) {
					String themeobject_id= themeobjectJSONArray.getJSONObject(n).getString("id").toString();	
					String theme_code=themeobjectJSONArray.getJSONObject(n).getString("theme_code").toString();
					String slot_code=themeobjectJSONArray.getJSONObject(n).getString("slot_code").toString();
					String page_id=themeobjectJSONArray.getJSONObject(n).getString("page_id").toString();
					String object_type=themeobjectJSONArray.getJSONObject(n).getString("object_type").toString();
					String object_value= themeobjectJSONArray.getJSONObject(n).getString("object_value").toString().replaceAll("\\r?\\n", "<br />");
					String action_type=themeobjectJSONArray.getJSONObject(n).getString("action_type").toString();
					String action_value=themeobjectJSONArray.getJSONObject(n).getString("action_value").toString();
					String user_id=themeobjectJSONArray.getJSONObject(n).getString("user_id").toString();
					String remark=themeobjectJSONArray.getJSONObject(n).getString("remark").toString();
					String date_updated=themeobjectJSONArray.getJSONObject(n).getString("date_updated").toString();
					String attribute=themeobjectJSONArray.getJSONObject(n).getString("attribute").toString();

					Log.d(LOG_TAG, "themeobject_id : " + themeobject_id + " | action_value :: "+action_value + " | slot_code :: " + slot_code);
					Log.d(LOG_TAG,"Insert into DB : " + n); 
					Helper_Themeobject.insert(themeobject_id,theme_code,slot_code,page_id,
							object_type,object_value,action_type,action_value,
							user_id,remark,date_updated,strCheckDate,attribute);
				}

			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "CreateContent_Themeobject-Error:" + t.getMessage(), t); 
		}
	}

	public void CreateContent_Themecolor(String strCheckDate, String strAPI_THEMECOLOR) {
		JSONArray themecolorJSONArray;
		try{
			themecolorJSONArray=new JSONArray(strAPI_THEMECOLOR);
			Log.d(LOG_TAG,"CreateContent_Themecolor"); 

			if (themecolorJSONArray.length()>0) {				
				Helper_Themecolor.Delete();

				for (int n = 0; n < themecolorJSONArray.length(); n++) {
					String theme_id= themecolorJSONArray.getJSONObject(n).getString("id").toString();	
					String theme_name=themecolorJSONArray.getJSONObject(n).getString("name").toString();
					String theme_color=themecolorJSONArray.getJSONObject(n).getString("color").toString();
					String theme_color1=themecolorJSONArray.getJSONObject(n).getString("color1").toString();
					String theme_color2=themecolorJSONArray.getJSONObject(n).getString("color2").toString();
					String theme_color3=themecolorJSONArray.getJSONObject(n).getString("color3").toString();
					String theme_color4=themecolorJSONArray.getJSONObject(n).getString("color4").toString();
					String date_updated=themecolorJSONArray.getJSONObject(n).getString("date_updated").toString();					    			

					Log.d(LOG_TAG,"Insert into DB : " + n); 
					Helper_Themecolor.insert(theme_id,theme_name,theme_color,theme_color1,
							theme_color2,theme_color3,theme_color4,strCheckDate,date_updated);
				}

			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "CreateContent_Themecolor-Error:" + t.getMessage(), t); 
		}
	}

	public void CreateContent_ThemeObjectPlugin(String strCheckDate, String strAPI_THEMEOBJECTPLUGIN) {
		JSONArray themeobjectpluginJSONArray;
		try{
			themeobjectpluginJSONArray=new JSONArray(strAPI_THEMEOBJECTPLUGIN);
			Log.d(LOG_TAG,"CreateContent_ThemeObjectPlugin"); 

			if (themeobjectpluginJSONArray.length()>0) {
				Helper_Themeobjectplugin.Delete();

				SharedPreferences.Editor editor = settings.edit();

				for (int n = 0; n < themeobjectpluginJSONArray.length(); n++) {
					String themeobjectplugin_id= themeobjectpluginJSONArray.getJSONObject(n).getString("id").toString();	
					String page_id=themeobjectpluginJSONArray.getJSONObject(n).getString("page_id").toString();
					String object_id=themeobjectpluginJSONArray.getJSONObject(n).getString("object_id").toString();
					String plugin_type=themeobjectpluginJSONArray.getJSONObject(n).getString("plugin_type").toString();
					String plugin_value1=themeobjectpluginJSONArray.getJSONObject(n).getString("plugin_value1").toString();
					String plugin_value2=themeobjectpluginJSONArray.getJSONObject(n).getString("plugin_value2").toString();
					String plugin_value3=themeobjectpluginJSONArray.getJSONObject(n).getString("plugin_value3").toString();
					String remark=themeobjectpluginJSONArray.getJSONObject(n).getString("remark").toString();
					String date_updated=themeobjectpluginJSONArray.getJSONObject(n).getString("date_updated").toString();					    			

					Log.d(LOG_TAG,"Insert into DB : " + n); 
					Helper_Themeobjectplugin.insert(themeobjectplugin_id,page_id,object_id,
							plugin_type,plugin_value1,plugin_value2,plugin_value3,remark,date_updated,strCheckDate);

					//Get Plugin Name						
					if (themeobjectplugin_id.equals("14")) {
						editor.remove("LoginTitle");
						editor.putString("LoginTitle",plugin_value1); 

					} else if (themeobjectplugin_id.equals("15")) {
						editor.remove("LoyaltyTitle");
						editor.putString("LoyaltyTitle",plugin_value1); 

					} else if (themeobjectplugin_id.equals("16")) {
						editor.remove("iBeaconTitle");
						editor.putString("iBeaconTitle",plugin_value1); 

					} else if (themeobjectplugin_id.equals("17")) {
						editor.remove("WarrantyTitle");
						editor.putString("WarrantyTitle",plugin_value1); 

					} else if (themeobjectplugin_id.equals("24")) {
						editor.remove("PointsTitle");
						editor.putString("PointsTitle",plugin_value1); 

					}

				}

				editor.commit();				

			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "CreateContent_ThemeObjectPlugin-Error:" + t.getMessage(), t); 
		}
	}

	public void CreateContent_Content(String strCheckDate, String strAPI_CONTENT) {
		JSONArray contentJSONArray;
		try{
			contentJSONArray=new JSONArray(strAPI_CONTENT);
			Log.d(LOG_TAG,"CreateContent_Content"); 

			if (contentJSONArray.length()>0) {				
				Helper_Content.Delete();

				for (int n = 0; n < contentJSONArray.length(); n++) {
					String content_id= contentJSONArray.getJSONObject(n).getString("id").toString();	
					String title=contentJSONArray.getJSONObject(n).getString("title").toString();
					String category_id=contentJSONArray.getJSONObject(n).getString("category_id").toString();
					String image=contentJSONArray.getJSONObject(n).getString("image").toString();
					String thumbnail=contentJSONArray.getJSONObject(n).getString("thumbnail").toString();
					String call_title=contentJSONArray.getJSONObject(n).getString("call_title").toString();
					String telephone=contentJSONArray.getJSONObject(n).getString("telephone").toString();
					String email_title=contentJSONArray.getJSONObject(n).getString("email_title").toString();
					String email=contentJSONArray.getJSONObject(n).getString("email").toString();
					String details=contentJSONArray.getJSONObject(n).getString("details").toString().replaceAll("\\r?\\n", "<br />");
					String link_title=contentJSONArray.getJSONObject(n).getString("link_title").toString();
					String link_url=contentJSONArray.getJSONObject(n).getString("link_url").toString();
					String map_title=contentJSONArray.getJSONObject(n).getString("map_title").toString();
					String map_lat=contentJSONArray.getJSONObject(n).getString("map_lat").toString();
					String map_lon=contentJSONArray.getJSONObject(n).getString("map_lon").toString();
					String video_title=contentJSONArray.getJSONObject(n).getString("video_title").toString();
					String video_url=contentJSONArray.getJSONObject(n).getString("video_url").toString();
					String fb_title=contentJSONArray.getJSONObject(n).getString("fb_title").toString();
					String fb_url=contentJSONArray.getJSONObject(n).getString("fb_url").toString();
					String user_id=contentJSONArray.getJSONObject(n).getString("user_id").toString();
					String date_updated=contentJSONArray.getJSONObject(n).getString("date_updated").toString();	
					String orderno =contentJSONArray.getJSONObject(n).getString("orderno").toString();
					String price_title =contentJSONArray.getJSONObject(n).getString("price_title").toString();
					String price =contentJSONArray.getJSONObject(n).getString("price").toString();
					String var1_title =contentJSONArray.getJSONObject(n).getString("var1_title").toString();
					String var1 =contentJSONArray.getJSONObject(n).getString("var1").toString();
					String var2_title =contentJSONArray.getJSONObject(n).getString("var2_title").toString();
					String var2 =contentJSONArray.getJSONObject(n).getString("var2").toString();
					String var3_title =contentJSONArray.getJSONObject(n).getString("var3_title").toString();
					String var3 =contentJSONArray.getJSONObject(n).getString("var3").toString();
					String var4_title =contentJSONArray.getJSONObject(n).getString("var4_title").toString();
					String var4 =contentJSONArray.getJSONObject(n).getString("var4").toString();
					String var5_title =contentJSONArray.getJSONObject(n).getString("var5_title").toString();
					String var5=contentJSONArray.getJSONObject(n).getString("var5").toString();

					Log.d(LOG_TAG,"Insert into DB : " + n); 
					Helper_Content.insert(content_id,title,category_id,image,thumbnail,call_title,telephone,
							email_title,email,details,link_title,link_url,map_title,map_lat,map_lon,
							video_title,video_url,fb_title,fb_url,orderno,price_title,price, 
							var1_title,var1,var2_title,var2,var3_title,var3, var4_title, 
							var4,var5_title,var5,user_id,date_updated,strCheckDate);
				}

			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "CreateContent_Content-Error:" + t.getMessage(), t); 
		}
	}


	public void CreateContent_Contentcategory(String strCheckDate, String strAPI_CONTENTCATEGORY) {
		JSONArray contentcatJSONArray;
		try{
			Log.d(LOG_TAG,"CreateContent_Contentcategory"); 
			contentcatJSONArray=new JSONArray(strAPI_CONTENTCATEGORY);

			if (contentcatJSONArray.length()>0) {				
				Helper_Contentcategory.Delete();

				for (int n = 0; n < contentcatJSONArray.length(); n++) {
					String contentcat_id= contentcatJSONArray.getJSONObject(n).getString("id").toString();	
					String name=contentcatJSONArray.getJSONObject(n).getString("name").toString();
					String plugin_id=contentcatJSONArray.getJSONObject(n).getString("plugin_id").toString();
					String list_template=contentcatJSONArray.getJSONObject(n).getString("list_template").toString();
					String user_id=contentcatJSONArray.getJSONObject(n).getString("user_id").toString();
					String is_hidden=contentcatJSONArray.getJSONObject(n).getString("is_hidden").toString();
					String top_id=contentcatJSONArray.getJSONObject(n).getString("top_id").toString();
					String date_updated=contentcatJSONArray.getJSONObject(n).getString("date_updated").toString();
					String allow_favorite=contentcatJSONArray.getJSONObject(n).getString("allow_favorite").toString();
					String allow_comments=contentcatJSONArray.getJSONObject(n).getString("allow_comments").toString();
					String parent_id=contentcatJSONArray.getJSONObject(n).getString("parent_id").toString();

					Log.d(LOG_TAG,"Insert into DB : " + n); 
					Helper_Contentcategory.insert(contentcat_id,name,plugin_id,list_template,user_id,
							is_hidden,top_id,date_updated,strCheckDate,allow_favorite,allow_comments,parent_id);
				}
			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "CreateContent_Contentcategory-Error:" + t.getMessage(), t); 
		}
	}

	public void CreateContent_Loyalty(String strCheckDate, String strAPI_LOYALTY) {
		JSONArray LoyaltyJSONArray;

		try{
			Log.d(LOG_TAG,"CreateContent_Loyalty"); 
			LoyaltyJSONArray=new JSONArray(strAPI_LOYALTY);

			if (LoyaltyJSONArray.length()>0) {				
				Helper_Loyalty.Delete();

				for (int n = 0; n < LoyaltyJSONArray.length(); n++) {
					String loyalt_id= LoyaltyJSONArray.getJSONObject(n).getString("id").toString();	
					String title=LoyaltyJSONArray.getJSONObject(n).getString("title").toString();
					String user_id= LoyaltyJSONArray.getJSONObject(n).getString("user_id").toString();	
					String short_description=LoyaltyJSONArray.getJSONObject(n).getString("short_description").toString();
					String secrect_code= LoyaltyJSONArray.getJSONObject(n).getString("secret_code").toString();	
					String start_date= LoyaltyJSONArray.getJSONObject(n).getString("start_date").toString();
					String end_date= LoyaltyJSONArray.getJSONObject(n).getString("end_date").toString();
					String total_stamp= LoyaltyJSONArray.getJSONObject(n).getString("total_stamp").toString();
					String status= LoyaltyJSONArray.getJSONObject(n).getString("status").toString();
					String stamp_icon_type= LoyaltyJSONArray.getJSONObject(n).getString("stamp_icon_type").toString();
					String icon= LoyaltyJSONArray.getJSONObject(n).getString("icon").toString();
					String bg_img= LoyaltyJSONArray.getJSONObject(n).getString("bg_img").toString();
					String stamp_icon_url= LoyaltyJSONArray.getJSONObject(n).getString("stamp_icon").toString();
					String stamp_icon_bg_url= LoyaltyJSONArray.getJSONObject(n).getString("stamp_icon_bg").toString();
					String date_updated= LoyaltyJSONArray.getJSONObject(n).getString("date_updated").toString();


					Log.d(LOG_TAG,"Insert into DB : " + n); 
					Helper_Loyalty.insert(loyalt_id,title,user_id,short_description,secrect_code,
							start_date,end_date, total_stamp,icon,bg_img,status,stamp_icon_type,
							stamp_icon_url,stamp_icon_bg_url,date_updated,strCheckDate);
				}

			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "CreateContent_Loyalty-Error:" + t.getMessage(), t); 
		}
	}

	public void CreateContent_IbeaconDevice(String strCheckDate, String strAPI_IBEACONDEVICE) {
		JSONArray iBeconDeviceJSONArray;
		try{
			Log.d(LOG_TAG,"CreateContent_IbeaconDevice"); 
			iBeconDeviceJSONArray=new JSONArray(strAPI_IBEACONDEVICE);

			if (iBeconDeviceJSONArray.length()>0) {				
				Helper_Ibeacondevice.Delete();

				for (int n = 0; n < iBeconDeviceJSONArray.length(); n++) {
					String beacondeviceid= iBeconDeviceJSONArray.getJSONObject(n).getString("id").toString();	
					String uuid =iBeconDeviceJSONArray.getJSONObject(n).getString("uuid").toString();
					String major = iBeconDeviceJSONArray.getJSONObject(n).getString("major").toString();	
					String minor =iBeconDeviceJSONArray.getJSONObject(n).getString("minor").toString();
					String beaconname= iBeconDeviceJSONArray.getJSONObject(n).getString("name").toString();	
					String error_msg = iBeconDeviceJSONArray.getJSONObject(n).getString("error_msg").toString();
					String user_id= iBeconDeviceJSONArray.getJSONObject(n).getString("user_id").toString();
					String status= iBeconDeviceJSONArray.getJSONObject(n).getString("status").toString();
					String date_updated= iBeconDeviceJSONArray.getJSONObject(n).getString("date_updated").toString();

					Log.d(LOG_TAG,"Insert into DB : " + n); 
					Helper_Ibeacondevice.insert(beacondeviceid,uuid,major,minor,beaconname,
							error_msg,user_id,status,date_updated,strCheckDate);
				}

			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "CreateContent_IbeaconDevice-Error:" + t.getMessage(), t); 
		}
	}


	public void CreateContent_IbeaconContent(String strCheckDate, String strAPI_IBEACONCONTENT) {
		JSONArray beaconJSONArray;
		try{
			Log.d(LOG_TAG,"CreateContent_IbeaconContent"); 
			beaconJSONArray=new JSONArray(strAPI_IBEACONCONTENT);

			if (beaconJSONArray.length()>0) {				
				Helper_Ibeaconcontent.Delete();

				for (int n = 0; n < beaconJSONArray.length(); n++) {
					String beaconcontentid= beaconJSONArray.getJSONObject(n).getString("id").toString();	
					String device_id =beaconJSONArray.getJSONObject(n).getString("device_id").toString();
					String welcome_msg = beaconJSONArray.getJSONObject(n).getString("welcome_msg").toString();	
					String welcome_img =beaconJSONArray.getJSONObject(n).getString("welcome_img").toString();
					String page_id= beaconJSONArray.getJSONObject(n).getString("page_id").toString();	
					String start_date = beaconJSONArray.getJSONObject(n).getString("start_date").toString();
					String end_date = beaconJSONArray.getJSONObject(n).getString("end_date").toString();
					String user_id= beaconJSONArray.getJSONObject(n).getString("user_id").toString();
					String status= beaconJSONArray.getJSONObject(n).getString("distance_range").toString();
					String date_updated= beaconJSONArray.getJSONObject(n).getString("date_updated").toString();
					String limitperday= beaconJSONArray.getJSONObject(n).getString("limitperday").toString();


					Log.d(LOG_TAG,"Insert into DB : " + n); 
					Helper_Ibeaconcontent.insert(beaconcontentid, device_id, welcome_msg, welcome_img,
							page_id,start_date,end_date,user_id,status,
							date_updated,strCheckDate,limitperday);
				}

			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "CreateContent_IbeaconContent-Error:" + t.getMessage(), t); 
		}
	}

	public void CreateContent_IbeaconXRef(String strCheckDate, String strAPI_IBEACONXREF) {
		JSONArray beaconJSONArray;
		try{
			Log.d(LOG_TAG,"CreateContent_IbeaconXRef"); 
			beaconJSONArray=new JSONArray(strAPI_IBEACONXREF);

			if (beaconJSONArray.length()>0) {				
				Helper_Ibeaconxref.Delete();

				for (int n = 0; n < beaconJSONArray.length(); n++) {
					String beaconcontentid= beaconJSONArray.getJSONObject(n).getString("id").toString();	
					String device_id =beaconJSONArray.getJSONObject(n).getString("device_id").toString();
					String content_id = beaconJSONArray.getJSONObject(n).getString("content_id").toString();
					String user_id= beaconJSONArray.getJSONObject(n).getString("user_id").toString();
					String date_updated= beaconJSONArray.getJSONObject(n).getString("date_updated").toString();

					Log.d(LOG_TAG,"Insert into DB : " + n); 
					Helper_Ibeaconxref.insert(beaconcontentid,device_id,content_id,user_id,date_updated,strCheckDate);
				}
			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "CreateContent_IbeaconXRef-Error:" + t.getMessage(), t); 
		}
	}

	public void CreateContent_PointsSettings(String strCheckDate, String strAPI_PointsSettings) {
		try{
			Log.d(LOG_TAG,"CreateContent_Points"); 
			SharedPreferences.Editor editor = commonfile.edit();
			editor.remove("PointsSettings");
			editor.putString("PointsSettings",strAPI_PointsSettings); 
			editor.commit();

		} catch (Throwable t) { 
			Log.e(LOG_TAG, "CreateContent_Points-Error:" + t.getMessage(), t); 
		}
	}

	public Collection_PointSettings GetPointsSettings (String strCode) {

		Collection_PointSettings selected_settings = new Collection_PointSettings();
		
		try{
			String strSetting = commonfile.getString("PointsSettings", "[]");
			Log.d(LOG_TAG, "Points Settings: " + strSetting); 
			JSONArray settingJSONArray = new JSONArray(strSetting);

			if (settingJSONArray.length()>0) {

				for (int n = 0; n < settingJSONArray.length(); n++) {
					String setting_id = settingJSONArray.getJSONObject(n).getString("id").toString();	
					String title = settingJSONArray.getJSONObject(n).getString("title").toString();	
					String plugin_code = settingJSONArray.getJSONObject(n).getString("plugin_code").toString();	
					String plugin_id = settingJSONArray.getJSONObject(n).getString("plugin_id").toString();
					String details = settingJSONArray.getJSONObject(n).getString("details").toString();	
					String image_url = settingJSONArray.getJSONObject(n).getString("image_url").toString();	
					String point_value = settingJSONArray.getJSONObject(n).getString("point_value").toString();	
					String thankyou_msg = settingJSONArray.getJSONObject(n).getString("thankyou_msg").toString();	
					String waiting_msg = settingJSONArray.getJSONObject(n).getString("waiting_msg").toString();	
					String waiting_interval = settingJSONArray.getJSONObject(n).getString("waiting_interval").toString();	
					
					if (plugin_code.equals(strCode)) {
						selected_settings.set_setting_id(setting_id);
						selected_settings.set_title(title);
						selected_settings.set_plugin_code(plugin_code);
						selected_settings.set_plugin_id(plugin_id);
						selected_settings.set_details(details);
						selected_settings.set_image_url(image_url);
						selected_settings.set_point_value(point_value);
						selected_settings.set_thankyou_msg(thankyou_msg);
						selected_settings.set_waiting_msg(waiting_msg);
						selected_settings.set_waiting_interval(waiting_interval);
					}

				}		
			}

			
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "GetPointsSettings-Error:" + t.getMessage(), t); 
		}
		return selected_settings;
	}
	
	public String GetAPIOfflineData (String strCheckDate) {
		String response="FAILED";
		
		try{
			Helper_Appsetup.Delete();
			Helper_Themepage.Delete();
			Helper_Themecolor.Delete();
			Helper_Themeobject.Delete();
			Helper_Themeobjectplugin.Delete();
			Helper_Ibeacondevice.Delete();
			Helper_Ibeaconcontent.Delete();
			Helper_Ibeaconxref.Delete();
			Helper_Loyalty.Delete();
	
			String strAPI_APPSETUP=ReadFromText(context.getResources().openRawResource(R.raw.appsetup));
			CreateContent_Appsetup (strCheckDate,strAPI_APPSETUP);
	
			String strAPI_THEMEPAGE=ReadFromText(context.getResources().openRawResource(R.raw.themepage));
			CreateContent_Themepage (strCheckDate,strAPI_THEMEPAGE);
	
			String strAPI_THEMECOLOR=ReadFromText(context.getResources().openRawResource(R.raw.themecolor));
			CreateContent_Themecolor (strCheckDate,strAPI_THEMECOLOR);
	
			String strAPI_THEMEOBJECT=ReadFromText(context.getResources().openRawResource(R.raw.themeobject));
			CreateContent_Themeobject (strCheckDate,strAPI_THEMEOBJECT);
	
			String strAPI_THEMEOBJECTPLUGIN=ReadFromText(context.getResources().openRawResource(R.raw.themeobjectplugin));
			CreateContent_ThemeObjectPlugin (strCheckDate,strAPI_THEMEOBJECTPLUGIN);
	
			String strAPI_CONTENT=ReadFromText(context.getResources().openRawResource(R.raw.content));
			CreateContent_Content (strCheckDate,strAPI_CONTENT);
	
			String strAPI_CONTENTCATEGORY=ReadFromText(context.getResources().openRawResource(R.raw.contentcategory));
			CreateContent_Contentcategory (strCheckDate,strAPI_CONTENTCATEGORY);
	
			String strAPI_IBEACONDEVICE=ReadFromText(context.getResources().openRawResource(R.raw.ibeacon));
			CreateContent_IbeaconDevice (strCheckDate,strAPI_IBEACONDEVICE);	
			response="SUCCESS";
			
		} catch (Throwable t) { 
			response="FAILED";
			Log.e(LOG_TAG, "GetAPIOfflineData-Error:" + t.getMessage(), t); 
		}
		return response;
	}

	public String ReadFromText (InputStream textfile) {

		InputStream is = textfile;
		StringBuilder text = new StringBuilder();

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;

			while ((line = br.readLine()) != null) {

				text.append(line);
				text.append('\n');
			}
		}
		catch (IOException e) {
			//You'll need to add proper error handling here
		}

		return text.toString();

	}


	public void close() {		
		Helper_Themepage.close();		
		Helper_Appsetup.close();	
		Helper_Themecolor.close();	
		Helper_Themeobject.close();	
		Helper_Themeobjectplugin.close();	
		Helper_Content.close();	
		Helper_Contentcategory.close();	
		Helper_Loyalty.close();	
		Helper_Ibeacondevice.close();	
		Helper_Ibeaconcontent.close();	
		Helper_Ibeaconxref.close();	
		
	}

}

