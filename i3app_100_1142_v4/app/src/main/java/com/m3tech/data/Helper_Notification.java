package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Helper_Notification extends SQLiteOpenHelper {

	private static final String LOG_TAG = "HELPER NOTIFICATION";
	private static final String DATABASE_NAME = "notification.db";
	//private static final int SCHEMA_VERSION = 1;  // INITIAL
	private static final int SCHEMA_VERSION = 2; 	// 7 SEPT 2015

	public Helper_Notification(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE notification (_id INTEGER PRIMARY KEY AUTOINCREMENT, notification_id TEXT, " +
				"user_id TEXT, push_id TEXT, token_id TEXT, customer_id TEXT, process_date TEXT, " +
				"push_msg TEXT, lastupdate TEXT, action_type TEXT, action_value TEXT, content_id TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    Log.d (LOG_TAG, "Current Version: " + Integer.toString(db.getVersion()));

	    if (oldVersion < 2) {
	    	db.execSQL("ALTER TABLE notification ADD Column action_type TEXT");
	    	db.execSQL("ALTER TABLE notification ADD Column action_value TEXT");
	    	db.execSQL("ALTER TABLE notification ADD Column content_id TEXT");
	        db.setVersion(SCHEMA_VERSION);
	        Log.d (LOG_TAG, "Upgrade Version: " + Integer.toString(db.getVersion()));

	    }

	}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM notification",
						null));
	}
	
	
	public void insert(String notification_id, String user_id, String push_id, String token_id, 
			String customer_id, String process_date, String push_msg, String lastupdate,
			String action_type, String action_value, String content_id){
		ContentValues cv = new ContentValues();
		cv.put("notification_id", notification_id);
		cv.put("user_id", user_id);
		cv.put("push_id",push_id);
		cv.put("token_id",token_id);
		cv.put("customer_id", customer_id);
		cv.put("process_date", process_date);
		cv.put("push_msg", push_msg);
		cv.put("lastupdate", lastupdate);
		cv.put("action_type", action_type);
		cv.put("action_value", action_value);
		cv.put("content_id", content_id);
		
		getWritableDatabase().insert("notification", "_id", cv);
	}
	
	public void Delete() {
		getWritableDatabase().delete("notification", null,null);
	}
	
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getnotification_id(Cursor c){
		return(c.getString(1));
	}
	
	public String getuser_id(Cursor c){
		return(c.getString(2));
	}
	
	public String getpush_id(Cursor c){
		return(c.getString(3));
	}
	
	public String gettoken_id(Cursor c){
		return(c.getString(4));
	}
	
	public String getcustomer_id(Cursor c){
		return(c.getString(5));
	}
	
	public String getprocess_date(Cursor c){
		return(c.getString(6));
	}
	
	public String getpush_msg(Cursor c){
		return(c.getString(7));
	}

	public String getlastupdate(Cursor c){
		return(c.getString(8));
	}

	public String getaction_type(Cursor c){
		return(c.getString(9));
	}

	public String getaction_value(Cursor c){
		return(c.getString(10));
	}

	public String getcontent_id(Cursor c){
		return(c.getString(11));
	}

}
