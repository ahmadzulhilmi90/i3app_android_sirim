package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Content extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "content.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Content(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE content (_id INTEGER PRIMARY KEY AUTOINCREMENT, content_id TEXT, title TEXT, category_id TEXT, " +
				"image TEXT, thumbnail TEXT, call_title TEXT, telephone TEXT, email_title TEXT, email TEXT, details TEXT, " +
				"link_title TEXT, link_url TEXT, map_title TEXT, map_lat TEXT, map_lon TEXT, video_title TEXT, video_url TEXT, "+
				"fb_title TEXT, fb_url TEXT, orderno TEXT, price_title TEXT,price TEXT,var1_title TEXT,var1 TEXT,var2_title TEXT,var2 TEXT,var3_title TEXT," +
				"var3 TEXT,var4_title TEXT,var4 TEXT,var5_title TEXT,var5 TEXT, user_id TEXT, date_updated TEXT, lastupdate TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM content",

						null));
	}



	public Cursor getByLastUpdate(String lastupdate) {
		String[] args={lastupdate};

		return(getReadableDatabase()
				.rawQuery("SELECT * FROM content WHERE lastupdate=?",
						args));
	}

	public Cursor getLastUpdate() {
		String[] args={};

		return(getReadableDatabase()
				.rawQuery("SELECT * FROM content ORDER BY date_updated DESC LIMIT 1",
						args));
	}

	public Cursor getByCategoryId(String category_id) {
		String[] args = category_id.split(","); 

		return(getReadableDatabase()
				.rawQuery("SELECT * FROM content WHERE category_id IN (" + makePlaceholders(args.length) + ")",
						args));
	}

	public Cursor getByCategoryId_Latest(String category_id) {
		String[] args = category_id.split(","); 

		return(getReadableDatabase()
				.rawQuery("SELECT * FROM content WHERE category_id IN (" + makePlaceholders(args.length) + ")" +
						" ORDER BY _id LIMIT 1",
						args));
	}

	public Cursor getByTimeline(String category_id) {
		String[] args = category_id.split(","); 

		return(getReadableDatabase()
				.rawQuery("SELECT * FROM content WHERE category_id IN (" + makePlaceholders(args.length) + ") ORDER BY date_updated DESC",
						args));
	}


	public Cursor getContentGallery(String content_id) {
		String[] args = { content_id }; 
		
		return(getReadableDatabase()
				.rawQuery("SELECT * FROM content WHERE var1 = ?",
						args));
	}
	
	public Cursor getContentCategoryID(String category_id) {
		String[] args = { category_id }; 
		
		return(getReadableDatabase()
				.rawQuery("SELECT * FROM content WHERE category_id = ? ORDER BY orderno ASC",
						args));
	}


	public Cursor getByCategoryIdPOI(String category_id, String map_lat, String map_lon) {
		String[] args = { category_id, map_lat, map_lon }; 

		
		return(getReadableDatabase()
				.rawQuery("SELECT * FROM content WHERE category_id = ? ORDER BY abs(map_lat - (?)) + abs( map_lon - (?)) LIMIT 30",
						args));
		
	}

	public Cursor getByCatAndKeyPOI(String category_id, String key, String map_lat, String map_lon) {
		String[] args = { category_id, "%"+key+"%", "%"+key+"%", "%"+key+"%", "%"+key+"%", "%"+key+"%", "%"+key+"%", map_lat, map_lon };

		return(getReadableDatabase()
				.rawQuery("SELECT * FROM content "
						+ "WHERE category_id=? AND (title like ? OR var1 like ? OR var2 like ? "
						+ "OR var3 like ? OR var4 like ? OR var5 like ?)  "
						+ "ORDER BY abs(map_lat - (?)) + abs( map_lon - (?)) LIMIT 50",
						args));
	}


	public Cursor getContentOrderByName(String category_id) {
		String[] args = category_id.split(","); 

		return(getReadableDatabase()
				.rawQuery("SELECT * FROM content WHERE category_id IN (" + makePlaceholders(args.length) + ") ORDER BY title",
						args));
	}

	public Cursor getBySearchKey(String key) {
		String[] args = { "%"+key+"%","%"+key+"%","%"+key+"%","%"+key+"%","%"+key+"%","%"+key+"%" };

		return(getReadableDatabase()
				.rawQuery("SELECT * FROM content "
						+ "WHERE title like ? OR var1 like ? OR var2 like ? "
						+ "OR var3 like ? OR var4 like ? OR var5 like ?",
						args));
	}

	public Cursor getByContID(String content_id) {
		String[] args={content_id};

		return(getReadableDatabase()
				.rawQuery("SELECT * FROM content WHERE content_id=?",
						args));
	}

	String makePlaceholders(int len) {
		if (len < 1) {
			// It will lead to an invalid query anyway ..
			throw new RuntimeException("No placeholders");
		} else {
			StringBuilder sb = new StringBuilder(len * 2 - 1);
			sb.append("?");
			for (int i = 1; i < len; i++) {
				sb.append(",?");
			}
			return sb.toString();
		}
	}

	public void insert(String content_id, String title, String category_id, String image, String thumbnail, String call_title,String telephone, String email_title,
			String email,String details,String link_title,String link_url,String map_title,String map_lat,String map_lon,String video_title,String video_url,
			String fb_title,String fb_url, String orderno, String price_title,String price,String var1_title,String var1,String var2_title,String var2,String var3_title,
			String var3, String var4_title ,String var4 ,String var5_title, String var5,String user_id, String date_updated, String lastupdate){
		ContentValues cv = new ContentValues();
		cv.put("content_id", content_id);
		cv.put("title", title);
		cv.put("category_id",category_id);
		cv.put("image",image);
		cv.put("thumbnail", thumbnail);
		cv.put("call_title", call_title);
		cv.put("telephone", telephone);
		cv.put("email_title", email_title);
		cv.put("email", email);
		cv.put("details", details);
		cv.put("link_title", link_title);
		cv.put("link_url", link_url);
		cv.put("map_title", map_title);
		cv.put("map_lat", map_lat);
		cv.put("map_lon", map_lon);
		cv.put("video_title", video_title);
		cv.put("video_url", video_url);
		cv.put("fb_title", fb_title);
		cv.put("fb_url", fb_url);
		cv.put("orderno", orderno);
		cv.put("price_title", price_title);
		cv.put("price", price);
		cv.put("var1_title", var1_title);
		cv.put("var1",var1 );
		cv.put("var2_title", var2_title);
		cv.put("var2", var2);
		cv.put("var3_title", var3_title);
		cv.put("var3", var3);
		cv.put("var4_title", var4_title);
		cv.put("var4", var4);
		cv.put("var5_title", var5_title);
		cv.put("var5", var5);
		cv.put("user_id", user_id);
		cv.put("date_updated", date_updated);
		cv.put("lastupdate", lastupdate);


		getWritableDatabase().insert("content", "_id", cv);
	}

	public void Delete() {
		getWritableDatabase().delete("content", null,null);
	}
	public String getId(Cursor c){
		return(c.getString(0));
	}

	public String getcontent_id(Cursor c){
		return(c.getString(1));
	}

	public String gettitle(Cursor c){
		return(c.getString(2));
	}

	public String getcategory_id(Cursor c){
		return(c.getString(3));
	}

	public String getimage(Cursor c){
		return(c.getString(4));
	}

	public String getthumbnail(Cursor c){
		return(c.getString(5));
	}

	public String getcall_title(Cursor c){
		return(c.getString(6));
	}

	public String gettelephone(Cursor c){
		return(c.getString(7));
	}

	public String getemail_title(Cursor c){
		return(c.getString(8));
	}


	public String getemail(Cursor c){
		return(c.getString(9));
	}

	public String getdetails(Cursor c){
		return(c.getString(10));
	}

	public String getlink_title(Cursor c){
		return(c.getString(11));
	}

	public String getlink_url(Cursor c){
		return(c.getString(12));
	}

	public String getmap_title(Cursor c){
		return(c.getString(13));
	}

	public String getmap_lat(Cursor c){
		return(c.getString(14));
	}

	public String getmap_lon(Cursor c){
		return(c.getString(15));
	}

	public String getvideo_title(Cursor c){
		return(c.getString(16));
	}

	public String getvideo_url(Cursor c){
		return(c.getString(17));
	}

	public String getfb_title(Cursor c){
		return(c.getString(18));
	}

	public String getfb_url(Cursor c){
		return(c.getString(19));
	}

	public String getorderno(Cursor c){
		return(c.getString(20));
	}

	public String getprice_title(Cursor c){
		return(c.getString(21));
	}

	public String getprice(Cursor c){
		return(c.getString(22));
	}

	public String getvar1_title(Cursor c){
		return(c.getString(23));
	}

	public String getvar1(Cursor c){
		return(c.getString(24));
	}

	public String getvar2_title(Cursor c){
		return(c.getString(25));
	}

	public String getvar2(Cursor c){
		return(c.getString(26));
	}

	public String getvar3_title(Cursor c){
		return(c.getString(27));
	}

	public String getvar3(Cursor c){
		return(c.getString(28));
	}

	public String getvar4_title(Cursor c){
		return(c.getString(29));
	}

	public String getvar4(Cursor c){
		return(c.getString(30));
	}

	public String getvar5_title(Cursor c){
		return(c.getString(31));
	}

	public String getvar5(Cursor c){
		return(c.getString(32));
	}

	public String getuser_id(Cursor c){
		return(c.getString(33));
	}

	public String getdate_updated(Cursor c){
		return(c.getString(34));
	}

	public String getlastupdate(Cursor c){
		return(c.getString(35));
	}

}
