package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Ibeaconxref extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "ibeaconxref.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Ibeaconxref(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE ibeaconxref (_id INTEGER PRIMARY KEY AUTOINCREMENT, ibeaconxref_id TEXT, device_id TEXT, content_id TEXT, " +
				"user_id TEXT, date_updated TEXT, lastupdate TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM ibeaconxref",
						null));
	}
	
	
	
	public Cursor getByLastUpdate(String lastupdate) {
		String[] args={lastupdate};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM ibeaconxref WHERE lastupdate=?",
											args));
	}
	
	
	public Cursor getBydevideID(String device_id) {
		String[] args={device_id};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM ibeaconxref WHERE device_id=?",
											args));
	}
	
	
	public void insert(String ibeaconxref_id, String device_id, String content_id, String user_id, String date_updated, String lastupdate){
		ContentValues cv = new ContentValues();
		cv.put("ibeaconxref_id", ibeaconxref_id);
		cv.put("device_id", device_id);
		cv.put("content_id",content_id);
		cv.put("user_id",user_id);
		cv.put("date_updated", date_updated);
		cv.put("lastupdate", lastupdate);
	
		
		getWritableDatabase().insert("ibeaconxref", "_id", cv);
	}

	public Cursor getLastUpdate() {
		String[] args={};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM ibeaconxref ORDER BY date_updated DESC LIMIT 1",
											args));
	}
	

	public void Delete() {
		getWritableDatabase().delete("ibeaconxref", null,null);
	}
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getibeaconxref_id(Cursor c){
		return(c.getString(1));
	}
	
	public String getdevice_id(Cursor c){
		return(c.getString(2));
	}
	
	public String getcontent_id(Cursor c){
		return(c.getString(3));
	}
	
	public String getuser_id(Cursor c){
		return(c.getString(4));
	}
	
	public String getdate_updated(Cursor c){
		return(c.getString(5));
	}

	public String getlastupdate(Cursor c){
		return(c.getString(6));
	}
	
	
}
