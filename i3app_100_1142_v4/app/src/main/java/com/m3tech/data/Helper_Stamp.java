package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Stamp extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "stamp.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Stamp(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE stamp (_id INTEGER PRIMARY KEY AUTOINCREMENT, userid TEXT, custid TEXT, loyaltyid TEXT,secretcode TEXT, repcode TEXT, task TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM stamp",
						null));
	}
	
	
	
	public void insert(String userid, String custid, String loyaltyid, String secretcode, String repcode, String task){
		ContentValues cv = new ContentValues();
		cv.put("userid", userid);
		cv.put("custid", custid);
		cv.put("loyaltyid",loyaltyid);
		cv.put("secretcode", secretcode);
		cv.put("repcode", repcode);
		cv.put("task", task);
		getWritableDatabase().insert("stamp", "_id", cv);
	}
	
	public void Delete() {
		getWritableDatabase().delete("stamp", null,null);
	}
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getuserid(Cursor c){
		return(c.getString(1));
	}
	
	public String getcustid(Cursor c){
		return(c.getString(2));
	}
	
	public String getloyaltyid(Cursor c){
		return(c.getString(3));
	}
	
	public String getsecretcode(Cursor c){
		return(c.getString(4));
	}
	
	public String getrepcode(Cursor c){
		return(c.getString(5));
	}
	
	public String gettask(Cursor c){
		return(c.getString(6));
	}
	
}
