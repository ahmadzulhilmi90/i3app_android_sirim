package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Ibeacondevice extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "ibeacondevice.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Ibeacondevice(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE ibeacondevice (_id INTEGER PRIMARY KEY AUTOINCREMENT, beacondevice_id TEXT, uuid TEXT, major TEXT, " +
				"minor TEXT, name TEXT, error_msg TEXT, user_id TEXT, status TEXT, date_updated TEXT, lastupdate TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM ibeacondevice",
				
						null));
	}
	
	public Cursor getByDetails(String uuid, String major, String minor) {
		String[] args={uuid,major,minor};
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM ibeacondevice WHERE uuid=? AND major=? AND minor=? ",
						args));
	}
	
	
	public Cursor getByLastUpdate(String lastupdate) {
		String[] args={lastupdate};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM ibeacondevice WHERE lastupdate=?",
											args));
	}
	
	
	public Cursor getLastUpdate() {
		String[] args={};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM ibeacondevice ORDER BY date_updated DESC LIMIT 1",
											args));
	}
	
	public void insert(String beacondevice_id, String uuid, String major, String minor, String name, String error_msg,String user_id, String status,
			String date_updated, String lastupdate){
		ContentValues cv = new ContentValues();
		cv.put("beacondevice_id", beacondevice_id);
		cv.put("uuid", uuid);
		cv.put("major",major);
		cv.put("minor",minor);
		cv.put("name", name);
		cv.put("error_msg", error_msg);
		cv.put("user_id", user_id);
		cv.put("status", status);
		cv.put("date_updated", date_updated);
		cv.put("lastupdate", lastupdate);
		
		
		getWritableDatabase().insert("ibeacondevice", "_id", cv);
	}
	
	public void Delete() {
		getWritableDatabase().delete("ibeacondevice", null,null);
	}
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getbeacondevice_id(Cursor c){
		return(c.getString(1));
	}
	
	public String getuuid(Cursor c){
		return(c.getString(2));
	}
	
	public String getmajor(Cursor c){
		return(c.getString(3));
	}
	
	public String getminor(Cursor c){
		return(c.getString(4));
	}
	
	public String getname(Cursor c){
		return(c.getString(5));
	}
	
	public String geterror_msg(Cursor c){
		return(c.getString(6));
	}
	
	public String getuser_id(Cursor c){
		return(c.getString(7));
	}
		
	public String getstatus(Cursor c){
		return(c.getString(8));
	}
	
	public String getdate_updated(Cursor c){
		return(c.getString(9));
	}
	
	public String getlastupdate(Cursor c){
		return(c.getString(10));
	}
	
	
}
