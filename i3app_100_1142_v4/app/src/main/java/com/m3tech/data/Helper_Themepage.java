package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Themepage extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "themepage.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Themepage(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE themepage (_id INTEGER PRIMARY KEY AUTOINCREMENT, themepage_id TEXT, theme_code TEXT, name TEXT, " +
				"prev_page TEXT, user_id TEXT, remark TEXT, date_updated TEXT, lastupdate TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM themepage",
				//.rawQuery("SELECT _id, appsetup_id, name, icon_name, icon_pic, splash_page, user_id, themecolor, lastupdate FROM appsetup",
						null));
	}
	
	
	
	public Cursor getByLastUpdate(String lastupdate) {
		String[] args={lastupdate};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themepage WHERE lastupdate=?",
											args));
	}
	
	public Cursor getByPrevpage(String prev_page) {
		String[] args={prev_page};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themepage WHERE prev_page=?",
											args));
	}
	
	public Cursor getByActionvalue(String actionvalue) {
		String[] args={actionvalue};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themepage WHERE themepage_id=?",
											args));
	}

	public Cursor getLastUpdate() {
		String[] args={};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themepage ORDER BY date_updated DESC LIMIT 1",
											args));
	}
	

	public void insert(String themepage_id, String theme_code, String name, String prev_page, String user_id, String remark, 
			String date_updated, String lastupdate){
		ContentValues cv = new ContentValues();
		cv.put("themepage_id", themepage_id);
		cv.put("theme_code", theme_code);
		cv.put("name",name);
		cv.put("prev_page",prev_page);
		cv.put("user_id", user_id);
		cv.put("remark", remark);
		cv.put("date_updated", date_updated);
		cv.put("lastupdate", lastupdate);
	
		
		getWritableDatabase().insert("themepage", "_id", cv);
	}
	
	public void Delete() {
		getWritableDatabase().delete("themepage", null,null);
	}
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getthemepage_id(Cursor c){
		return(c.getString(1));
	}
	
	public String gettheme_code(Cursor c){
		return(c.getString(2));
	}
	
	public String getname(Cursor c){
		return(c.getString(3));
	}
	
	public String getprev_page(Cursor c){
		return(c.getString(4));
	}
	
	public String getuser_id(Cursor c){
		return(c.getString(5));
	}
	
	public String getremark(Cursor c){
		return(c.getString(6));
	}
	public String getdate_updated(Cursor c){
		return(c.getString(7));
	}
	public String getlastupdate(Cursor c){
		return(c.getString(8));
	}
	
	
}
