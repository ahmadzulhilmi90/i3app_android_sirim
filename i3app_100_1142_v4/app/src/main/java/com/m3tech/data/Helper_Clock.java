package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Clock extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "clock.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Clock(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE clock (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"type TEXT," +
				"data TEXT," +
				"log TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM clock",null));
	}
	
	public void insert(String type,String data,String log){
		ContentValues cv = new ContentValues();
		cv.put("type", type);
		cv.put("data", data);
		cv.put("log", log);
		
		getWritableDatabase().insert("clock", "_id", cv);
	}
	
	public Cursor getByType(String type) {
		String[] args = { type }; 
		
		return(getReadableDatabase()
				.rawQuery("SELECT * FROM clock WHERE type = ? LIMIT 1",
						args));
	}
	
	public Cursor getByLog(String type,String log) {
		String[] args = { type,log }; 
		
		return(getReadableDatabase()
				.rawQuery("SELECT * FROM clock WHERE type = ? AND log=? LIMIT 1",
						args));
	}
	
	public void DeleteTypeIn() {
		getWritableDatabase().delete("clock","type=in", null);
	}
	
	public void DeleteTypeOut() {
		getWritableDatabase().delete("clock","type=out", null);
	}
	
	public void Delete() {
		getWritableDatabase().delete("clock", null,null);
	}

	public String getId(Cursor c) {
		return (c.getString(0));
	}

	public String gettype(Cursor c) {
		return (c.getString(1));
	}
	public String getdata(Cursor c) {
		return (c.getString(2));
	}
	public String getlog(Cursor c) {
		return (c.getString(3));
	}

}
