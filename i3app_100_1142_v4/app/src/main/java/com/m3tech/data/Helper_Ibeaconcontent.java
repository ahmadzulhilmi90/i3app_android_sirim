package com.m3tech.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Ibeaconcontent extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "ibeaconcontent.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Ibeaconcontent(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE ibeaconcontent (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ "beaconcontent_id TEXT, device_id TEXT, welcome_msg TEXT, "
				+ "welcome_img TEXT, page_id TEXT, start_date TEXT, end_date TEXT, "
				+ "user_id TEXT, status TEXT, date_updated TEXT, "
				+ "lastupdate TEXT, limitperday TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM ibeaconcontent",				
						null));
	}
	
	
	public Cursor getLastUpdate() {
		String[] args={};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM ibeaconcontent ORDER BY date_updated DESC LIMIT 1",
											args));
	}
	
	
	public Cursor getByLastUpdate(String lastupdate) {
		String[] args={lastupdate};

		return(getReadableDatabase()
				.rawQuery("SELECT * FROM ibeaconcontent WHERE lastupdate=?",
									args));
	}
	
	public Cursor getfromDeviceID(String device_id) {
		String[] args={device_id};

		return(getReadableDatabase()
				.rawQuery("SELECT * FROM ibeaconcontent WHERE device_id=?",
									args));
	}
	
	public Cursor getfromContentID(String content_id) {
		String[] args={content_id};

		return(getReadableDatabase()
				.rawQuery("SELECT * FROM ibeaconcontent WHERE beaconcontent_id=?",
									args));
	}

	public Cursor getfromContentIDThisMoment(String content_id) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm");
		String todayDate = df.format(cal.getTime());
		
		String[] args={content_id,todayDate};
		
		//select * from time where date('now') > date(start_date) and date('now') < date(end_date);
		return(getReadableDatabase()
				.rawQuery("SELECT * FROM ibeaconcontent WHERE beaconcontent_id=? AND Datetime(?) BETWEEN Datetime(start_date) AND Datetime(end_date)",
									args));
	}
	
	public void insert(String beaconcontent_id, String device_id, 
			String welcome_msg, String welcome_img, String page_id, 
			String start_date, String end_date,String user_id, String status,
			String date_updated, String lastupdate, 
			String limitperday) {
		
		ContentValues cv = new ContentValues();
		cv.put("beaconcontent_id", beaconcontent_id);
		cv.put("device_id", device_id);
		cv.put("welcome_msg",welcome_msg);
		cv.put("welcome_img",welcome_img);
		cv.put("page_id", page_id);
		cv.put("start_date", start_date);
		cv.put("end_date", end_date);
		cv.put("user_id", user_id);
		cv.put("status", status);
		cv.put("date_updated", date_updated);
		cv.put("lastupdate", lastupdate);
		cv.put("limitperday", limitperday); 
		
		getWritableDatabase().insert("ibeaconcontent", "_id", cv);
	}
	
	public void Delete() {
		getWritableDatabase().delete("ibeaconcontent", null,null);
	}
	
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getbeaconcontent_id(Cursor c){
		return(c.getString(1));
	}
	
	public String getdevice_id(Cursor c){
		return(c.getString(2));
	}
	
	public String getwelcome_msg(Cursor c){
		return(c.getString(3));
	}
	
	public String getwelcome_img(Cursor c){
		return(c.getString(4));
	}
	
	public String getpage_id(Cursor c){
		return(c.getString(5));
	}
	
	public String getstart_date(Cursor c){
		return(c.getString(6));
	}
	
	public String getend_date(Cursor c){
		return(c.getString(7));
	}
	
	public String getuser_id(Cursor c){
		return(c.getString(8));
	}
		
	public String getstatus(Cursor c){
		return(c.getString(9));
	}
	
	public String getdate_updated(Cursor c){
		return(c.getString(10));
	}
	
	public String getlastupdate(Cursor c){
		return(c.getString(11));
	}

	public String getlimitperday(Cursor c){
		return(c.getString(12));
	}
	
}
