package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Favorite extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "favorite.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Favorite(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE favorite (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"content_id TEXT," +
				"status TEXT," +
				"total TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM favorite",null));
	}
	
	public void insert(String content_id,String status,String total){
		ContentValues cv = new ContentValues();
		cv.put("content_id", content_id);
		cv.put("status", status);
		cv.put("total", total);
		
		getWritableDatabase().insert("favorite", "_id", cv);
	}
	
	public Cursor getTotalByContentID(String content_id) {
		String[] args = { content_id }; 
		
		return(getReadableDatabase()
				.rawQuery("SELECT * FROM favorite WHERE content_id = ? AND status = 0 LIMIT 1",
						args));
	}
	
	public Cursor getListFavorite(String content_id) {
		String[] args = { content_id }; 
		
		return(getReadableDatabase()
				.rawQuery("SELECT * FROM favorite WHERE content_id = ? AND status = 1 LIMIT 1",
						args));
	}
	
	public void DeleteListFavorite() {
		getWritableDatabase().delete("favorite","status=1", null);
	}
	
	public void DeleteAllTotal() {
		getWritableDatabase().delete("favorite","status=0", null);
	}
	
	public void Delete() {
		getWritableDatabase().delete("favorite", null,null);
	}

	public String getId(Cursor c) {
		return (c.getString(0));
	}

	public String getcontent_id(Cursor c) {
		return (c.getString(1));
	}
	public String getstatus(Cursor c) {
		return (c.getString(2));
	}

	public String gettotal(Cursor c) {
		return (c.getString(3));
	}

}
