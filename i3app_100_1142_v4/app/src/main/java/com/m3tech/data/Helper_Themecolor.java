package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Themecolor extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "themecolor.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Themecolor(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE themecolor (_id INTEGER PRIMARY KEY AUTOINCREMENT, color_id TEXT, name TEXT, color TEXT, " +
				"color1 TEXT, color2 TEXT, color3 TEXT, color4 TEXT,lastupdate TEXT,date_updated TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM themecolor",
						null));
	}
	
	public Cursor getLastUpdate() {
		String[] args={};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themecolor ORDER BY date_updated DESC LIMIT 1",
											args));
	}
	
	
	public Cursor getByLastUpdate(String lastupdate) {
		String[] args={lastupdate};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themecolor WHERE lastupdate=?",
											args));
	}
	
	public Cursor getbycolorname(String name) {
		String[] args={name};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themecolor WHERE name=?",
											args));
	}
	
	
	public void insert(String color_id, String name, String color, String color1, String color2, String color3, 
			String color4, String lastupdate, String date_updated){
		ContentValues cv = new ContentValues();
		cv.put("color_id", color_id);
		cv.put("name", name);
		cv.put("color",color);
		cv.put("color1",color1);
		cv.put("color2", color2);
		cv.put("color3", color3);
		cv.put("color4", color4);
		cv.put("lastupdate", lastupdate);
		cv.put("date_updated", date_updated);
	
		
		getWritableDatabase().insert("themecolor", "_id", cv);
	}
	
	public void Delete() {
		getWritableDatabase().delete("themecolor", null,null);
	}
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getcolor_id(Cursor c){
		return(c.getString(1));
	}
	
	public String getname(Cursor c){
		return(c.getString(2));
	}
	
	public String getcolor(Cursor c){
		return(c.getString(3));
	}
	
	public String getcolor1(Cursor c){
		return(c.getString(4));
	}
	
	public String getcolor2(Cursor c){
		return(c.getString(5));
	}
	
	public String getcolor3(Cursor c){
		return(c.getString(6));
	}
	public String getcolor4(Cursor c){
		return(c.getString(7));
	}
	public String getlastupdate(Cursor c){
		return(c.getString(8));
	}
	
	public String getdate_updated(Cursor c){
		return(c.getString(9));
	}
	
}
