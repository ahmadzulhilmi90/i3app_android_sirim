package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Notes extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "notes.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Notes(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE notes (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"note TEXT," +
				"cid TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM notes LIMIT 1",null));
	}
	
	public void insert(String note,String cid){
		ContentValues cv = new ContentValues();
		cv.put("note", note);
		cv.put("cid", cid);
		
		getWritableDatabase().insert("notes", "_id", cv);
	}
	
	public Cursor getByCid(String cid) {
		String[] args = { cid }; 
		return(getReadableDatabase()
				.rawQuery("SELECT * FROM notes WHERE cid=?",args));
	}
	
	public void Delete() {
		getWritableDatabase().delete("notes", null,null);
	}

	public String getId(Cursor c) {
		return (c.getString(0));
	}

	public String getnote(Cursor c) {
		return (c.getString(1));
	}
	public String getcid(Cursor c) {
		return (c.getString(2));
	}

}
