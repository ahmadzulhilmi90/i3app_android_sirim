package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Contentcategory extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "contentcategory.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Contentcategory(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE contentcategory (_id INTEGER PRIMARY KEY AUTOINCREMENT, contentcat_id TEXT, name TEXT, plugin_id TEXT, " +
				"list_template TEXT, user_id TEXT, is_hidden TEXT, top_id TEXT, date_updated TEXT,lastupdate TEXT,allow_favorite TEXT,allow_comments TEXT,parent_id TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM contentcategory",
				
						null));
	}
	
	public Cursor getLastUpdate() {
		String[] args={};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM contentcategory ORDER BY date_updated DESC LIMIT 1",
											args));
	}
	
	
	
	public Cursor getByLastUpdate(String lastupdate) {
		String[] args={lastupdate};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM contentcategory WHERE lastupdate=?",
											args));
	}
	
	
	public Cursor getByTopId(String top_id) {
		String[] args={top_id};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM contentcategory WHERE top_id=?",
											args));
	}

	public Cursor getByCatId(String cat_id) {
		String[] args={cat_id};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM contentcategory WHERE contentcat_id=?",
											args));
	}
	
	public Cursor getByParentId(String parent_id) {
		String[] args={parent_id};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM contentcategory WHERE parent_id=?",
											args));
	}

	public Cursor getByCatIdList(String cat_id_list) {
		String[] args = cat_id_list.split(",");

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM contentcategory " +
									"WHERE contentcat_id IN (" + makePlaceholders(args.length) + ") " +
									"ORDER BY name",
											args));
	}

	public void insert(String contentcat_id, String name, String plugin_id, String list_template, 
			String user_id, String is_hidden,String top_id, String date_updated, String lastupdate,String allow_favorite,String allow_comments,String parent_id) {
		ContentValues cv = new ContentValues();
		cv.put("contentcat_id", contentcat_id);
		cv.put("name", name);
		cv.put("plugin_id",plugin_id);
		cv.put("list_template",list_template);
		cv.put("user_id", user_id);
		cv.put("is_hidden", is_hidden);
		cv.put("top_id", top_id);
		cv.put("date_updated", date_updated);
		cv.put("lastupdate", lastupdate);
		cv.put("allow_favorite", allow_favorite);
		cv.put("allow_comments", allow_comments);
		cv.put("parent_id", parent_id);
		
		
		getWritableDatabase().insert("contentcategory", "_id", cv);
	}
	
	public void Delete() {
		getWritableDatabase().delete("contentcategory", null,null);
	}
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getcontentcat_id(Cursor c){
		return(c.getString(1));
	}
	
	public String getname(Cursor c){
		return(c.getString(2));
	}
	
	public String getplugin_id(Cursor c){
		return(c.getString(3));
	}
	
	public String getlist_template(Cursor c){
		return(c.getString(4));
	}
	
	public String getuser_id(Cursor c){
		return(c.getString(5));
	}
	
	public String getis_hidden(Cursor c){
		return(c.getString(6));
	}
	
	public String gettop_id(Cursor c){
		return(c.getString(7));
	}
	
	public String getdate_updated(Cursor c){
		return(c.getString(8));
	}
	
	public String getlastupdate(Cursor c){
		return(c.getString(9));
	}
	
	public String getallow_favorite(Cursor c){
		return(c.getString(10));
	}
	
	public String getallow_comments(Cursor c){
		return(c.getString(11));
	}
	
	public String getparent_id(Cursor c){
		return(c.getString(12));
	}
	

	String makePlaceholders(int len) {
	    if (len < 1) {
	        // It will lead to an invalid query anyway ..
	        throw new RuntimeException("No placeholders");
	    } else {
	        StringBuilder sb = new StringBuilder(len * 2 - 1);
	        sb.append("?");
	        for (int i = 1; i < len; i++) {
	            sb.append(",?");
	        }
	        return sb.toString();
	    }
	}
}
