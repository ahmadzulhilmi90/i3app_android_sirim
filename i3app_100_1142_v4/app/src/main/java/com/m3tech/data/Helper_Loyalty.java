package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Loyalty extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "loyalty.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Loyalty(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE loyalty (_id INTEGER PRIMARY KEY AUTOINCREMENT, loyalt_id TEXT, title TEXT, user_id TEXT, "
				+ "short_description TEXT, secrect_code TEXT, start_date TEXT, end_date TEXT, total_stamp TEXT, icon TEXT, bg_img TEXT, "
				+ "status TEXT, stamp_icon_type TEXT, stamp_icon_url TEXT, stamp_icon_bg_url TEXT, date_updated TEXT, last_updated TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM loyalty",
				//.rawQuery("SELECT _id, appsetup_id, name, icon_name, icon_pic, splash_page, user_id, themecolor, lastupdate FROM appsetup",
						null));
	}
	
	
	
	public Cursor getByLastUpdate(String lastupdate) {
		String[] args={lastupdate};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM loyalty WHERE last_updated=?",
											args));
	}
	
	
	public Cursor getByuserid(String userid) {
		String[] args={userid};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM loyalty WHERE user_id=?",
											args));
	}
	
	public void insert(String loyalt_id, String title, String user_id, String short_description, String secrect_code, 
			String start_date, String end_date, String total_stamp, String icon, String bg_img, String status, String stamp_icon_type, 
			String stamp_icon_url, String stamp_icon_bg_url,String date_updated,String last_updated){
		ContentValues cv = new ContentValues();
		cv.put("loyalt_id", loyalt_id);
		cv.put("title", title);
		cv.put("user_id", user_id);
		cv.put("short_description",short_description);
		cv.put("secrect_code",secrect_code);
		cv.put("start_date", start_date);
		cv.put("end_date", end_date);
		cv.put("total_stamp", total_stamp);
		cv.put("icon", icon);
		cv.put("bg_img", bg_img);
		cv.put("status", status);
		cv.put("stamp_icon_type", stamp_icon_type);
		cv.put("stamp_icon_url", stamp_icon_url);
		cv.put("stamp_icon_bg_url", stamp_icon_bg_url);
		cv.put("date_updated", last_updated);
		cv.put("last_updated", last_updated);
		
		getWritableDatabase().insert("loyalty", "_id", cv);
	}

	public Cursor getLastUpdate() {
		String[] args={};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM loyalty ORDER BY date_updated DESC LIMIT 1",
											args));
	}

	public void Delete() {
		getWritableDatabase().delete("loyalty", null,null);
	}
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getloyalt_id(Cursor c){
		return(c.getString(1));
	}
	
	public String gettitle(Cursor c){
		return(c.getString(2));
	}
	
	public String getuser_id(Cursor c){
		return(c.getString(3));
	}
	
	public String getshort_description(Cursor c){
		return(c.getString(4));
	}
	
	public String getsecrect_code(Cursor c){
		return(c.getString(5));
	}
	
	public String getstart_date(Cursor c){
		return(c.getString(6));
	}
	
	public String getend_date(Cursor c){
		return(c.getString(7));
	}
	public String gettotal_stamp(Cursor c){
		return(c.getString(8));
	}
	public String geticon(Cursor c){
		return(c.getString(9));
	}
	public String getbg_img(Cursor c){
		return(c.getString(10));
	}
	public String getstatus(Cursor c){
		return(c.getString(11));
	}
	public String getstamp_icon_type(Cursor c){
		return(c.getString(12));
	}
	public String getstamp_icon_url(Cursor c){
		return(c.getString(13));
	}
	public String getstamp_icon_bg_url(Cursor c){
		return(c.getString(14));
	}
	public String getdate_updated(Cursor c){
		return(c.getString(15));
	}
	public String getlast_updated(Cursor c){
		return(c.getString(16));
	}
	
	
}
