package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Themeobjectplugin extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "themeobjectplugin.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Themeobjectplugin(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE themeobjectplugin (_id INTEGER PRIMARY KEY AUTOINCREMENT, themeobjectplugin_id TEXT, page_id TEXT, object_id TEXT, " +
				"plugin_type TEXT, plugin_value1 TEXT, plugin_value2 TEXT, plugin_value3 TEXT,remark TEXT,date_updated TEXT,lastupdate TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM themeobjectplugin",
				
						null));
	}
	
	public Cursor getLastUpdate() {
		String[] args={};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themeobjectplugin ORDER BY date_updated DESC LIMIT 1",
											args));
	}
	
	
	public Cursor getByLastUpdate(String lastupdate) {
		String[] args={lastupdate};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themeobjectplugin WHERE lastupdate=?",
											args));
	}
	
	
	public Cursor getByActvalue(String themeobjectplugin_id) {
		String[] args={themeobjectplugin_id};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themeobjectplugin WHERE themeobjectplugin_id=?",
											args));
	}

	public Cursor getByPluginType(String plugin_type) {
		String[] args={plugin_type};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themeobjectplugin WHERE plugin_type=?",
											args));
	}
	
	public void insert(String themeobjectplugin_id, String page_id, String object_id, String plugin_type, String plugin_value1, String plugin_value2, String plugin_value3, 
			  String remark, String date_updated,String lastupdate){
		ContentValues cv = new ContentValues();
		cv.put("themeobjectplugin_id", themeobjectplugin_id);
		cv.put("page_id", page_id);
		cv.put("object_id",object_id);
		cv.put("plugin_type",plugin_type);
		cv.put("plugin_value1",plugin_value1);
		cv.put("plugin_value2", plugin_value2);
		cv.put("plugin_value3", plugin_value3);
		cv.put("remark", remark);
		cv.put("date_updated", date_updated);
		cv.put("lastupdate", lastupdate);
		
		
		getWritableDatabase().insert("themeobjectplugin", "_id", cv);
	}
	
	public void Delete() {
		getWritableDatabase().delete("themeobjectplugin", null,null);
	}
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getthemeobjectplugin_id(Cursor c){
		return(c.getString(1));
	}
	
	public String getpage_id(Cursor c){
		return(c.getString(2));
	}
	
	public String getobject_id(Cursor c){
		return(c.getString(3));
	}
	
	public String getplugin_type(Cursor c){
		return(c.getString(4));
	}
	
	public String getplugin_value1(Cursor c){
		return(c.getString(5));
	}
	
	public String getplugin_value2(Cursor c){
		return(c.getString(6));
	}
	
	public String getplugin_value3(Cursor c){
		return(c.getString(7));
	}
	
	public String getremark(Cursor c){
		return(c.getString(8));
	}
	public String getdate_updated(Cursor c){
		return(c.getString(9));
	}
	public String getlastupdate(Cursor c){
		return(c.getString(10));
	}
	
	
}
