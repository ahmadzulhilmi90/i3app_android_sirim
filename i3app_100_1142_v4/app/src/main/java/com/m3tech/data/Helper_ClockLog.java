package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_ClockLog extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "clocklog.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_ClockLog(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE clocklog (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"type TEXT," +
				"data TEXT," +
				"log TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM clocklog",null));
	}
	
	public void insert(String type,String data,String log){
		ContentValues cv = new ContentValues();
		cv.put("type", type);
		cv.put("data", data);
		cv.put("log", log);
		
		getWritableDatabase().insert("clocklog", "_id", cv);
	}
	
	public Cursor getByType(String type) {
		String[] args = { type }; 
		
		return(getReadableDatabase()
				.rawQuery("SELECT * FROM clocklog WHERE type = ? LIMIT 1",
						args));
	}
	
	public Cursor getByLog(String type,String log) {
		String[] args = { type,log }; 
		
		return(getReadableDatabase()
				.rawQuery("SELECT * FROM clocklog WHERE type = ? AND log=? LIMIT 1",
						args));
	}
	
	public void DeleteAllExceptToday(String log){
		getWritableDatabase().delete("clocklog","log != " + log, null);
	}
	
	public void DeleteTypeIn() {
		getWritableDatabase().delete("clocklog","type=in", null);
	}
	
	public void DeleteTypeOut() {
		getWritableDatabase().delete("clocklog","type=out", null);
	}
	
	public void Delete() {
		getWritableDatabase().delete("clocklog", null,null);
	}

	public String getId(Cursor c) {
		return (c.getString(0));
	}

	public String gettype(Cursor c) {
		return (c.getString(1));
	}
	public String getdata(Cursor c) {
		return (c.getString(2));
	}
	public String getlog(Cursor c) {
		return (c.getString(3));
	}

}
