package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Comment extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "comment.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Comment(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE comment (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"content_id TEXT," +
				"total TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM comment",null));
	}
	
	public void insert(String content_id,String total){
		ContentValues cv = new ContentValues();
		cv.put("content_id", content_id);
		cv.put("total", total);
		
		getWritableDatabase().insert("comment", "_id", cv);
	}
	
	public Cursor getTotalByContentID(String content_id) {
		String[] args = { content_id }; 
		
		return(getReadableDatabase()
				.rawQuery("SELECT * FROM comment WHERE content_id = ? LIMIT 1",
						args));
	}
	
	public void Delete() {
		getWritableDatabase().delete("comment", null,null);
	}

	public String getId(Cursor c) {
		return (c.getString(0));
	}

	public String getcontent_id(Cursor c) {
		return (c.getString(1));
	}
	public String gettotal(Cursor c) {
		return (c.getString(2));
	}

}
