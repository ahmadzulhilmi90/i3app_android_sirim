package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Themeobject extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "themeobject.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Themeobject(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE themeobject (_id INTEGER PRIMARY KEY AUTOINCREMENT, themeobject_id TEXT, theme_code TEXT, slot_code TEXT, " +
				"page_id TEXT, object_type TEXT, object_value TEXT, action_type TEXT, action_value TEXT, user_id TEXT,remark TEXT," +
				"date_updated TEXT,lastupdate TEXT, attribute TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM themeobject",
				
						null));
	}
	
	public Cursor getLastUpdate() {
		String[] args={};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themeobject ORDER BY date_updated DESC LIMIT 1",
											args));
	}
	
	
	public Cursor getByLastUpdate(String lastupdate) {
		String[] args={lastupdate};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themeobject WHERE lastupdate=?",
											args));
	}
	
	public Cursor getByslotcodepageid(String slot_code, String page_id) {
		String[] args={slot_code,page_id};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themeobject WHERE slot_code=? AND page_id=?",
											args));
	}
	
	public Cursor getByPageLogin(String slot_code, String page_id) {
		String[] args={slot_code,page_id};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themeobject WHERE object_value=1 AND slot_code=? AND page_id=?",
											args));
	}
	
	
	public Cursor getTabData(String slot_code, String page_id) {
		String[] args={slot_code,page_id};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themeobject WHERE slot_code !=? AND page_id=? ORDER BY slot_code",
											args));
	}
	
	public Cursor getThemecode(String theme_code, String page_id) {
		String[] args={theme_code,page_id};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themeobject WHERE theme_code =? AND page_id=?",
											args));
	}
	
	public Cursor getThemecode4(String theme_code) {
		String[] args={theme_code};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themeobject WHERE theme_code =?",
											args));
	}
	
	public Cursor getByMenuVer(String user_id) {
		String[] args={"MENU-VER","textImage",user_id};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themeobject WHERE theme_code=? AND object_type=? AND user_id=? ORDER BY slot_code",
											args));
	}
	
	public Cursor getBypageid(String page_id) {
		String[] args={page_id,"BG","MENU"};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themeobject WHERE page_id=? AND slot_code<>? AND slot_code<>? ORDER BY slot_code",
											args));
	}

	public Cursor getMenu() {
		String[] args={"MENU"};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM themeobject WHERE theme_code=? ORDER BY slot_code",
											args));
	}

	public void insert(String themeobject_id, String theme_code, String slot_code, String page_id, String object_type, String object_value, 
			String action_type, String action_value, String user_id, String remark, String date_updated,String lastupdate,String attribute){
		ContentValues cv = new ContentValues();
		cv.put("themeobject_id", themeobject_id);
		cv.put("theme_code", theme_code);
		cv.put("slot_code",slot_code);
		cv.put("page_id",page_id);
		cv.put("object_type", object_type);
		cv.put("object_value", object_value);
		cv.put("action_type", action_type);
		cv.put("action_value", action_value);
		cv.put("user_id", user_id);
		cv.put("remark", remark);
		cv.put("date_updated", date_updated);
		cv.put("lastupdate", lastupdate);
		cv.put("attribute", attribute);
		
		
		getWritableDatabase().insert("themeobject", "_id", cv);
	}
	
	public void Delete() {
		getWritableDatabase().delete("themeobject", null,null);
	}
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getthemeobject_id(Cursor c){
		return(c.getString(1));
	}
	
	public String gettheme_code(Cursor c){
		return(c.getString(2));
	}
	
	public String getslot_code(Cursor c){
		return(c.getString(3));
	}
	
	public String getpage_id(Cursor c){
		return(c.getString(4));
	}
	
	public String getobject_type(Cursor c){
		return(c.getString(5));
	}
	
	public String getobject_value(Cursor c){
		return(c.getString(6));
	}
	public String getaction_type(Cursor c){
		return(c.getString(7));
	}
	public String getaction_value(Cursor c){
		return(c.getString(8));
	}
	public String getuser_id(Cursor c){
		return(c.getString(9));
	}
	public String getremark(Cursor c){
		return(c.getString(10));
	}
	public String getdate_updated(Cursor c){
		return(c.getString(11));
	}
	public String getlastupdate(Cursor c){
		return(c.getString(12));
	}
	public String getattribute(Cursor c){
		return(c.getString(13));
	}
	
}
