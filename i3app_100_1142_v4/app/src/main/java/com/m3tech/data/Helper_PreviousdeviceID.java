package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_PreviousdeviceID extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "prev_device.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_PreviousdeviceID(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE ibeacon_counter (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
													+ "deviceid TEXT, viewdate TEXT, counter TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM ibeacon_counter",
						null));
	}
	
	public Cursor getbyDeviceID(String deviceid) {
		String[] args={deviceid};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM ibeacon_counter WHERE deviceid=?",
											args));
	}
	
	
	public void Insert (String deviceid, String viewdate, String counter) {
		ContentValues cv = new ContentValues();
		cv.put("deviceid", deviceid);
		cv.put("viewdate", viewdate);
		cv.put("counter", counter);
		
		getWritableDatabase().insert("ibeacon_counter", "_id", cv);
	}

	public void Update(String deviceid, String viewdate,String counter) {

		ContentValues cv=new ContentValues();
		String[] args={deviceid};

		cv.put("viewdate", viewdate);
		cv.put("counter", counter);

		getWritableDatabase().update("ibeacon_counter", cv, "deviceid=?",args);
	}	
	
	public void Delete() {
		getWritableDatabase().delete("ibeacon_counter", null,null);
	}
	
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getdeviceid(Cursor c){
		return(c.getString(1));
	}

	public String getviewdate(Cursor c){
		return(c.getString(2));
	}

	public String getcounter(Cursor c){
		return(c.getString(3));
	}

	
}
