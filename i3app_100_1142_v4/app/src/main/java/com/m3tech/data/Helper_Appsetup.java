package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_Appsetup extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "appsetup.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_Appsetup(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE appsetup (_id INTEGER PRIMARY KEY AUTOINCREMENT, appsetup_id TEXT, name TEXT, icon_name TEXT, " +
				"icon_pic TEXT, background TEXT,splash_page TEXT, user_id TEXT, themecolor TEXT, lastupdate TEXT, icon_pic_name TEXT,splash_page_name TEXT," +
				"currency TEXT,product_title TEXT,profile_title TEXT,booking_title TEXT,booking_status TEXT,loyalty_title TEXT,loyalty_status TEXT," +
				"branch_title TEXT,customer_title TEXT,template TEXT,date_updated TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM appsetup",null));
	}
	
	
	
	public Cursor getByLastUpdate(String lastupdate) {
		String[] args={lastupdate};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM appsetup WHERE lastupdate=?",
											args));
	}

	public Cursor getLastUpdate() {
		String[] args={};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM appsetup ORDER BY date_updated DESC LIMIT 1",
											args));
	}

	public void insert(String appsetup_id, String name, String icon_name, String icon_pic, String background,String splash_page, String user_id, 
			String themecolor, String lastupdate, String icon_pic_name, String splash_page_name, String currency,String product_title,
			String profile_title,String booking_title,String booking_status,String loyalty_title,String loyalty_status,String branch_title,
			String customer_title, String template, String date_updated){
		ContentValues cv = new ContentValues();
		cv.put("appsetup_id", appsetup_id);
		cv.put("name", name);
		cv.put("icon_name",icon_name);
		cv.put("icon_pic",icon_pic);
		cv.put("background",background);
		cv.put("splash_page", splash_page);
		cv.put("user_id", user_id);
		cv.put("themecolor", themecolor);
		cv.put("lastupdate", lastupdate);
		cv.put("icon_pic_name", icon_pic_name);
		cv.put("splash_page_name", splash_page_name);
		cv.put("currency", currency);
		cv.put("product_title", product_title);
		cv.put("profile_title", profile_title);
		cv.put("booking_title", booking_title);
		cv.put("booking_status", booking_status);
		cv.put("loyalty_title", loyalty_title);
		cv.put("loyalty_status", loyalty_status);
		cv.put("branch_title", branch_title);
		cv.put("customer_title", customer_title);
		cv.put("template", template);
		cv.put("date_updated", date_updated);
		
		getWritableDatabase().insert("appsetup", "_id", cv);
	}
	
	public void Delete() {
		getWritableDatabase().delete("appsetup", null,null);
	}
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getappsetup_id(Cursor c){
		return(c.getString(1));
	}
	
	public String getname(Cursor c){
		return(c.getString(2));
	}
	
	public String geticon_name(Cursor c){
		return(c.getString(3));
	}
	
	public String geticon_pic(Cursor c){
		return(c.getString(4));
	}
	
	public String getbackground(Cursor c){
		return(c.getString(5));
	}
	
	public String getsplash_page(Cursor c){
		return(c.getString(6));
	}
	
	public String getuser_id(Cursor c){
		return(c.getString(7));
	}
	public String getthemecolor(Cursor c){
		return(c.getString(8));
	}
	public String getlastupdate(Cursor c){
		return(c.getString(9));
	}
	public String geticon_pic_name(Cursor c){
		return(c.getString(10));
	}
	public String getsplash_page_name(Cursor c){
		return(c.getString(11));
	}
	public String getcurrency(Cursor c){
		return(c.getString(12));
	}
	public String getproduct_title(Cursor c){
		return(c.getString(13));
	}
	public String getprofile_title(Cursor c){
		return(c.getString(14));
	}
	public String getapp_tnc(Cursor c){
		//change from booking_title to app_tnc
		return(c.getString(15));
	}
	public String getapp_policy(Cursor c){
		//change from booking_status to app_policy
		return(c.getString(16));
	}
	public String getloyalty_title(Cursor c){
		return(c.getString(17));
	}
	public String getloyalty_status(Cursor c){
		return(c.getString(18));
	}
	public String getbranch_title(Cursor c){
		return(c.getString(19));
	}
	public String getcustomer_title(Cursor c){
		return(c.getString(20));
	}
	public String gettemplate(Cursor c){
		return(c.getString(21));
	}
	public String getdate_updated(Cursor c){
		return(c.getString(22));
	}
	
}
