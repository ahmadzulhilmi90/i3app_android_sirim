package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Helper_BookSet extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "bookset.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_BookSet(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE bookset (_id INTEGER PRIMARY KEY AUTOINCREMENT, bookset_id TEXT, max_guesstotal TEXT, v_product TEXT, " +
				"v_branch TEXT, v_datestart TEXT, v_timestart TEXT, v_dateend TEXT, v_timeend TEXT, v_guesstotal TEXT,v_notes TEXT," +
				"tq_message TEXT,book_image_name TEXT,book_image TEXT,avai_date TEXT,avai_time TEXT,avai_time_start TEXT,avai_time_end TEXT,lastupdate TEXT,top_id TEXT,booking_title TEXT,multiple_product TEXT,display_price TEXT,display_total TEXT,product_title TEXT,display_qtt TEXT,product_err TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM bookset",
				//.rawQuery("SELECT _id, appsetup_id, name, icon_name, icon_pic, splash_page, user_id, themecolor, lastupdate FROM appsetup",
						null));
	}
	
	public Cursor getAllByTopID(String top_id) {
		String[] args={top_id};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM bookset WHERE top_id=?",
											args));
	}
	
	
	public Cursor getByLastUpdate(String lastupdate) {
		String[] args={lastupdate};

		return(getReadableDatabase()
						.rawQuery("SELECT * FROM bookset WHERE lastupdate=?",
											args));
	}
	
	
	
	public void insert(String bookset_id, String max_guesstotal, String v_product, String v_branch, String v_datestart, String v_timestart, 
			String v_dateend, String v_timeend, String v_guesstotal, String v_notes, String tq_message,String book_image_name,
			String book_image,String avai_date,String avai_time,String avai_time_start,String avai_time_end,String lastupdate,String top_id,String booking_title,String multiple_product,String display_price,String display_total,String product_title,String display_qtt,String product_err){
		ContentValues cv = new ContentValues();
		cv.put("bookset_id", bookset_id);
		cv.put("max_guesstotal", max_guesstotal);
		cv.put("v_product",v_product);
		cv.put("v_branch",v_branch);
		cv.put("v_datestart", v_datestart);
		cv.put("v_timestart", v_timestart);
		cv.put("v_dateend", v_dateend);
		cv.put("v_timeend", v_timeend);
		cv.put("v_guesstotal", v_guesstotal);
		cv.put("v_notes", v_notes);
		cv.put("tq_message", tq_message);
		cv.put("book_image_name", book_image_name);
		cv.put("book_image", book_image);
		cv.put("avai_date", avai_date);
		cv.put("avai_time", avai_time);
		cv.put("avai_time_start", avai_time_start);
		cv.put("avai_time_end", avai_time_end);
		cv.put("lastupdate", lastupdate);
		cv.put("top_id", top_id);
		cv.put("booking_title", booking_title);
		cv.put("multiple_product", multiple_product);
		cv.put("display_price", display_price);
		cv.put("display_total", display_total);
		cv.put("product_title", product_title);
		cv.put("display_qtt", display_qtt);
		cv.put("product_err", product_err);
		getWritableDatabase().insert("bookset", "_id", cv);
	}
	
	public void Delete() {
		getWritableDatabase().delete("bookset", null,null);
	}
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getbookset_id(Cursor c){
		return(c.getString(1));
	}
	
	public String getmax_guesstotal(Cursor c){
		return(c.getString(2));
	}
	
	public String getv_product(Cursor c){
		return(c.getString(3));
	}
	
	public String getv_branch(Cursor c){
		return(c.getString(4));
	}
	
	public String getv_datestart(Cursor c){
		return(c.getString(5));
	}
	
	public String getv_timestart(Cursor c){
		return(c.getString(6));
	}
	public String getv_dateend(Cursor c){
		return(c.getString(7));
	}
	public String getv_timeend(Cursor c){
		return(c.getString(8));
	}
	public String getv_guesstotal(Cursor c){
		return(c.getString(9));
	}
	public String getv_notes(Cursor c){
		return(c.getString(10));
	}
	public String gettq_message(Cursor c){
		return(c.getString(11));
	}
	public String getbook_image_name(Cursor c){
		return(c.getString(12));
	}
	public String getbook_image(Cursor c){
		return(c.getString(13));
	}
	public String getavai_date(Cursor c){
		return(c.getString(14));
	}
	
	public String getavai_time(Cursor c){
		return(c.getString(15));
	}
	
	public String getavai_time_start(Cursor c){
		return(c.getString(16));
	}
	
	public String getavai_time_end(Cursor c){
		return(c.getString(17));
	}
	
	public String getlastupdate(Cursor c){
		return(c.getString(18));
	}
	
	public String gettop_id(Cursor c){
		return(c.getString(19));
	}
	
	public String getbooking_title(Cursor c){
		return(c.getString(20));
	}
	
	public String getmultiple_product(Cursor c){
		return(c.getString(21));
	}
	
	public String getdisplay_price(Cursor c){
		return(c.getString(22));
	}
	
	public String getdisplay_total(Cursor c){
		return(c.getString(23));
	}
	
	public String getproduct_title(Cursor c){
		return(c.getString(24));
	}
	
	public String getdisplay_qtt(Cursor c){
		return(c.getString(25));
	}
	
	public String getproduct_err(Cursor c){
		return(c.getString(26));
	}
}
