package com.m3tech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Helper_BookAdded extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "bookadded.db";
	private static final int SCHEMA_VERSION = 1;

	public Helper_BookAdded(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE bookadded (_id INTEGER PRIMARY KEY AUTOINCREMENT, content_id TEXT,title TEXT,quantity TEXT,total_price TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public Cursor getAll() {
		return (getReadableDatabase()
				.rawQuery("SELECT * FROM bookadded",null));
	}
	
	public Cursor getByContentID(String content_id) {
		String[] args={content_id};
		return(getReadableDatabase().rawQuery("SELECT * FROM bookadded WHERE content_id=?",args));
	}
	
	public void DeleteById(String content_id) {
		getWritableDatabase().delete("bookadded","content_id=" + content_id, null);
	}
	
	public void UpdateData(String content_id,String quantity,String total_price) {
		ContentValues cv=new ContentValues();
		String[] args={String.valueOf(content_id)};
		cv.put("quantity", quantity);
		cv.put("total_price", total_price);
		Log.d("DB", "Update DB BookedAdded");
		getWritableDatabase().update("bookadded", cv, "content_id=?",args);
	}
	
	public void insert(String content_id,String title,String quantity,String total_price){
		ContentValues cv = new ContentValues();
		cv.put("content_id", content_id);
		cv.put("title", title);
		cv.put("quantity", quantity);
		cv.put("total_price", total_price);
		getWritableDatabase().insert("bookadded", "_id", cv);
	}
	
	public void Delete() {
		getWritableDatabase().delete("bookadded", null,null);
	}
	public String getId(Cursor c){
		return(c.getString(0));
	}
	
	public String getcontent_id(Cursor c){
		return(c.getString(1));
	}
	
	public String gettitle(Cursor c){
		return(c.getString(2));
	}
	
	public String getquantity(Cursor c){
		return(c.getString(3));
	}
	
	public String getprice(Cursor c){
		return(c.getString(4));
	}
	
}
