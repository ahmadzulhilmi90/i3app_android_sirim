package com.m3tech.appv3;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.text.Html;
import android.util.Log;

import com.m3tech.app_100_1474.R;
import com.m3tech.clock.Plugin_Clock_T1;
import com.m3tech.collection.Collection_themeobject;
import com.m3tech.content.Plugin_Content_CatT1;
import com.m3tech.content.Plugin_Content_CatT2;
import com.m3tech.content.Plugin_Content_CatT3;
import com.m3tech.content.Plugin_Content_CatT5;
import com.m3tech.content.Plugin_Content_CatT7;
import com.m3tech.content.Plugin_Content_Detail;
import com.m3tech.content.Plugin_Content_Gallery;
import com.m3tech.content.Plugin_Content_T2;
import com.m3tech.content.Plugin_Content_T3;
import com.m3tech.content.Plugin_Content_T5;
import com.m3tech.content.Plugin_Content_T6;
import com.m3tech.content.Plugin_Content_T9;
import com.m3tech.customer.Customer_Inbox;
import com.m3tech.customer.Customer_MainPage;
import com.m3tech.customer.Customer_Point;
import com.m3tech.data.Helper_Content;
import com.m3tech.data.Helper_Contentcategory;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.data.Helper_Themeobjectplugin;
import com.m3tech.data.Helper_Themepage;
import com.m3tech.event.Event_Detail;
import com.m3tech.event.Event_List;
import com.m3tech.favorite.FavoriteList;
import com.m3tech.loyalty.Loyalty;
import com.m3tech.notes.Notes;
import com.m3tech.reservation.Reservation_List;
import com.m3tech.sirim.SirimLabel;
import com.m3tech.theme.Theme_Std;
import com.m3tech.warranty.Warranty_List;
import com.m3tech.web.WebBrowser;
import com.m3tech.weblink.WebLink;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

public class SlotAction {

	private static final String LOG_TAG = "Slot Action";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	SharedPreferences settings;
	Helper_Themeobject Helper_Themeobject = null;
	Helper_Themepage Helper_Themepage = null;
	Helper_Themeobjectplugin Helper_Themeobjectplugin =null;
	Helper_Contentcategory Helper_Contentcategory= null;
	Helper_Content Helper_Content=null;
	Cursor c, d,f,h;
	String theme_code,app_title,home_themecode,home_pagename,home_pageid,colorcode,colorcode1,fromhome;
	String customerID,app_user,app_db, udid;
	Context context;

	public SlotAction(Context ctx) {
		context = ctx;
		settings = context.getSharedPreferences(PREFS_NAME, 0);

		home_themecode = settings.getString("home_themecode", "");
		home_pagename = settings.getString("home_pagename", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		colorcode1 = settings.getString("colorcode1", "");		
		home_pageid = settings.getString("home_pageid", "");

		app_user = context.getResources().getString(R.string.app_user);
		app_db = context.getResources().getString(R.string.app_db);
		customerID = settings.getString("customerID", "");
		udid = settings.getString("udid", "");		

		Helper_Themeobject = new Helper_Themeobject(context);
		Helper_Themepage = new Helper_Themepage(context);
		Helper_Themeobjectplugin = new Helper_Themeobjectplugin(context);
		Helper_Contentcategory = new Helper_Contentcategory(context);
		Helper_Content = new Helper_Content(context);
	}


	public static boolean assetExists(AssetManager assets, String name) {
		try {
			// using File to extract path / filename
			// alternatively use name.lastIndexOf("/") to extract the path
			File f = new File(name);
			String parent = f.getParent();
			if (parent == null) parent = "";
			String fileName = f.getName();
			// now use path to list all files
			String[] assetList = assets.list(parent);
			if (assetList != null && assetList.length > 0) {
				for (String item : assetList) {
					if (fileName.equals(item))
						return true;
				}
			}
		} catch (IOException e) {
			// Log.w(TAG, e); // enable to log errors
		}
		return false;
	}

	public String getImageURI(String imgUrl) {
		String response, imageUri;
		String[] values;
		String[] values2;

		try {
			//Log.d(LOG_TAG+" getImageURI", "imgUrl="+imgUrl);

			values = imgUrl.split("/");
			String filename = values[values.length-1];

			filename = "a"+filename.replace("-", "_");
			
			values2 = filename.split("\\.");
			String resName = "";
			
			if (values2.length>0) {
				resName = values2[0];				
			} else {
				resName = filename;
			}

			int resId = context.getResources().getIdentifier(resName, "drawable", context.getPackageName());
			
			if(resId>0){ 
				imageUri = "drawable://"+resId; // from drawable
			} else {
				imageUri = imgUrl;
			}

			Log.d(LOG_TAG+" getImageURI", "imageUri="+imageUri);
			response = imageUri;

		} catch (Throwable t) { 
			response="";
			Log.e(LOG_TAG, "getImageURI-Error:" + t.getMessage(), t); 
		}

		return response;
	}

	public String getCheckSum() {
		String response = "";
		String newtext = "";

		try {
			/* 
			 * ALGORITHME
			 	1. Get all basic value to be sent, i.e, db, userid, udid, cuid
					- i.e: db=183, userid=183, udid=ABC-12345-678910, cuid=3
			  	2. Combine all value in sequence (save as string).
					- So, the combined value will be: 183183ABC-12345-6789103
				3. Remove character "-"
					New value: value will be: 183183ABC123456789103
				4. Get the combined value length, and add to the end of the combined value string
					- The combined value character length in the sample is 21
					- So, the new text will be: 183183ABC12345678910321
				5. Reverse the string: 12301987654321CBA381381
					- please make sure each character is case sensitive
				6. Get the left 15 characters of the String: 12301987654321C
			 */

			//1. Get all basic value to be sent, i.e, db, userid, udid, cuid
			//2. Combine all value in sequence (save as string).			
			newtext = app_db + app_user + udid + customerID; 
			
			//3. Remove character "-"
			newtext = newtext.replace("-", "");
			
			//4. Get the combined value length, and add to the end of the combined value string
			newtext = newtext + String.valueOf(newtext.length());
			
			//5. Reverse the string
			String rvs = new StringBuilder(newtext).reverse().toString();
			
			if (rvs.length()>15) {
				newtext = rvs.substring(0, 14); 
			} else {
				newtext = rvs;
			}
			
			response = newtext;

		} catch (Throwable t) { 
			response="";
			Log.e(LOG_TAG, "getCheckSum-Error:" + t.getMessage(), t); 
		}

		Log.w(LOG_TAG, "getCheckSum: " + response); 
		return response;
	}

	public String getWords(String sentence, int count) {
		String response = "";

		try {
			String[] str = sentence.split("\\s+"); 
			
			if (str.length>count) {
				//TODO
				for (int n = 0; n < count; n++) {
					response = response + str[n] + " ";
				}
				response = response + "...";
			} else {			
				response = sentence;
			}

		} catch (Throwable t) { 
			response="";
			Log.e(LOG_TAG, "getWords-Error:" + t.getMessage(), t); 
		}

		Log.d(LOG_TAG, "getWords: " + response); 
		return response;
	}
	
	public String getStrDistance(String ALat, String ALon, String BLat, String BLon) {
		String strdistance = "";
		String response = "";
		
		double pointLat = Double.parseDouble(ALat);
		double pointLon = Double.parseDouble(ALon);

		double refLat = Double.parseDouble(BLat);
		double refLon = Double.parseDouble(BLon);

		Location locationA = new Location("point A");
		locationA.setLatitude(pointLat);
		locationA.setLongitude(pointLon);

		Location locationB = new Location("point B");
		locationB.setLatitude(refLat);
		locationB.setLongitude(refLon);

		float distance = locationA.distanceTo(locationB);
		distance = distance / 1000;

		if (distance > 9) {
			DecimalFormat formatter = new DecimalFormat("#,###");
			strdistance = formatter.format(distance) + " km";
		} else if (distance > 1) {
			strdistance = String.format("%.1f", distance)
					+ " km";
		} else if (distance >= 0) {
			strdistance = String.format("%.2f", distance);
			float f = Float.parseFloat(strdistance) * 1000;
			strdistance = String.format("%.0f", f) + " m";
		}
		
		response = strdistance;
		return response;
	}

	public void CreateNextPage (Collection_themeobject objectlist) {
		String action_value="", plugin_type="", plugin_value1="", plugin_value2="", plugin_value3="";
		action_value = objectlist.getaction_value();

		Log.d(LOG_TAG+" CreateNextPage", objectlist.getaction_type());
		Log.d(LOG_TAG, "action_value="+action_value);
		
		if(objectlist.getaction_type().equals("page")) { 

			d = Helper_Themepage.getByActionvalue(action_value);  
			if (d.moveToLast() != false){
				theme_code = Helper_Themepage.gettheme_code(d).toString();
			} else {
				theme_code = "0";
			}
			d.close();
			
			CreateThemePage(theme_code,action_value);

		} else if (objectlist.getaction_type().equals("plugin")){

			f = Helper_Themeobjectplugin.getByActvalue(action_value); //get object plugin data base on action value
			if (f.moveToLast() != false){

				plugin_type = Helper_Themeobjectplugin.getplugin_type(f).toString();
				plugin_value1 = Helper_Themeobjectplugin.getplugin_value1(f).toString();
				plugin_value2 = Helper_Themeobjectplugin.getplugin_value2(f).toString();
				plugin_value3 = Helper_Themeobjectplugin.getplugin_value3(f).toString();
				Log.d(LOG_TAG, "plugin_type="+plugin_type);
				Log.d(LOG_TAG, "plugin_value1="+plugin_value1);
				Log.d(LOG_TAG, "plugin_value2="+plugin_value2);
				Log.d(LOG_TAG, "plugin_value3="+plugin_value3);

			}
			f.close();

			CreatePluginPage(action_value,plugin_type,plugin_value1,plugin_value2,plugin_value3);
		}			
	}

	public void CreateThemePage(String tc, String pid) {

		String noback="no";
		String pagetitle="";
		
		SharedPreferences.Editor editor = settings.edit();
		editor.remove("current_pageid");
		editor.putString("current_pageid", pid);
		editor.commit();

		Log.d(LOG_TAG,"current_pageid: "+pid);								
		
		if (tc.equals("no-back-button")) {
			noback = "yes";
			tc="";
		}
		
		if (tc.length()==0) {
			tc = settings.getString("home_themecode", "");
			pid = settings.getString("home_pageid", "");
		}

		
		Cursor c = Helper_Themepage.getByActionvalue(pid);
		if (c.moveToLast() != false) {
			tc = Helper_Themepage.gettheme_code(c).toString();
			pagetitle = Helper_Themepage.getname(c).toString();
		}
		c.close();
		

		Log.d(LOG_TAG+" CreateThemePage","Theme "+tc+": Page "+pid);
		
		Intent i=new Intent(context.getApplicationContext(), Theme_Std.class);
		i.putExtra("themecode", tc);
		i.putExtra("pagetitle", pagetitle);
		i.putExtra("themepageid", pid);
		i.putExtra("noback", noback);
		context.startActivity(i);
		

	};
	
	public void CreateNextThemePage(String pid,String value) { //value == ex: event id
		
		String tc = "";
		String noback="no";
		String pagetitle="";
		
		SharedPreferences.Editor editor = settings.edit();
		editor.remove("current_pageid");
		editor.putString("current_pageid", pid);
		editor.remove("current_valueid");
		editor.putString("current_valueid", value);
		editor.commit();

		Log.d(LOG_TAG,"current_pageid: "+pid);
		Log.d(LOG_TAG,"current_valueid: "+value);	
		
		Cursor c = Helper_Themepage.getByActionvalue(pid);
		if (c.moveToLast() != false) {
			tc = Helper_Themepage.gettheme_code(c).toString();
			pagetitle = Helper_Themepage.getname(c).toString();
		}
		c.close();
		

		Log.d(LOG_TAG+" CreateThemePage","Theme "+tc+": Page "+pid);
		
		Intent i=new Intent(context.getApplicationContext(), Theme_Std.class);
		i.putExtra("themecode", tc);
		i.putExtra("pagetitle", pagetitle);
		i.putExtra("themepageid", pid);
		i.putExtra("noback", noback);
		context.startActivity(i);
		

	};


	public void CreatePluginPage(String actvalue, String ptype, String p1, String p2, String p3) {
		String listtemplate="", cont_cat_name="", cont_category_id="", contentid="", plugin_id="", allow_comments = "", allow_favorite="";
		Log.d(LOG_TAG+" CreatePluginPage",ptype+"|"+actvalue+"|"+p1+"|"+p2+"|"+p3);

		SharedPreferences.Editor editor = settings.edit();
		editor.remove("current_pageid");
		editor.putString("current_pageid", "0");
		editor.commit();


		//GET customerID again, in case user already logout
		customerID = settings.getString("customerID", "");

		try {

			if(ptype.equals("sirim_label")){

				Intent i = new Intent(context, SirimLabel.class);
				i.putExtra("pagetitle",p1);
				context.startActivity(i);

			}else if(ptype.equals("event_reservation")){
				
				editor.remove("eventPageTitle");
				editor.putString("eventPageTitle", p1);
				editor.remove("afterLogin");
				editor.commit();
				
				String valueid = settings.getString("current_valueid", "");
				Log.d(LOG_TAG, "event_reservation id : "+ valueid);
				
				Intent i = new Intent(context, Event_Detail.class);
				i.putExtra("event_id",valueid);
				context.startActivity(i);

			}else if(ptype.equals("event_invite")){
				editor.remove("eventPageTitle");
				editor.putString("eventPageTitle", p1);
				editor.remove("afterLogin");
				editor.putString("afterLogin", "Event");
				editor.putString("topid_event", actvalue);
				editor.commit();

				if(customerID.equals("")){

					final AlertDialog.Builder alert = new AlertDialog.Builder(context);

					alert.setTitle(context.getResources().getString(R.string.login_toproceed));

					alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							Intent i=new Intent(context.getApplicationContext(), Customer_MainPage.class);
							context.startActivity(i);
						}

					});
					alert.show();

				}else{
					Intent i=new Intent(context, Event_List.class);
					i.putExtra("topid_event",actvalue);
					context.startActivity(i);
				}

			}else if(ptype.equals("call")){

				/*Uri uri= Uri.fromParts("tel",p1, null);
				Intent callIntent = new Intent(Intent.ACTION_CALL,uri);
				callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(callIntent);*/

			} else if(ptype.equals("email")){

				Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
				emailIntent.setType("plain/text");
				emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{p1});
				emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,app_title);
				emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(""));

				context.startActivity(Intent.createChooser(emailIntent, "Send email..."));

			} else if(ptype.equals("web")){

				if (!p1.startsWith("http://") && !p1.startsWith("https://")){
					p1 = "http://" + p1;
				} 
				
				Intent i=new Intent(context, WebBrowser.class);
				i.putExtra("pagetitle", "");
				i.putExtra("url", p1);
				i.putExtra("openweb", p2);
				context.startActivity(i);

			} else if(ptype.equals("html5")){

				if (!p2.startsWith("http://") && !p2.startsWith("https://")){
					p2 = "http://" + p2;
				}

				Intent i=new Intent(context, WebBrowser.class);
				i.putExtra("pagetitle", p1);
				i.putExtra("url", p2);
				i.putExtra("openweb", "no");
				context.startActivity(i);

			}else if(ptype.equals("html5c")){

				if(customerID.length()>0){
					
					if (!p2.startsWith("http://") && !p2.startsWith("https://")){
						p2 = "http://" + p2;
					}
					
					Intent i=new Intent(context, WebLink.class);
					i.putExtra("pagetitle", p1);
					i.putExtra("url", p2);
					i.putExtra("openweb", "no");
					context.startActivity(i);
					
				}else{
					
					editor.remove("customerPageTitle");
					editor.putString("customerPageTitle", p1);
					editor.remove("afterLogin");
					editor.putString("afterLogin", "html5c");
					editor.remove("html5c_url");
					editor.putString("html5c_url", p2);
					editor.commit();

					Intent i=new Intent(context, Customer_MainPage.class);
					context.startActivity(i);
					
				}
				
			}else if(ptype.equals("notes")){
				Log.d(LOG_TAG, "Plugin Notes Click!...");
				
				if(customerID.length()>0){
					
					Intent i=new Intent(context, Notes.class);
					i.putExtra("pagetitle", p1);
					context.startActivity(i);
					
				}else{
					
					editor.remove("customerPageTitle");
					editor.putString("customerPageTitle", p1);
					editor.remove("afterLogin");
					editor.putString("afterLogin", "notes");
					editor.commit();

					Intent i=new Intent(context, Customer_MainPage.class);
					context.startActivity(i);
					
				}
				
				
			}
			
			else if(ptype.equals("clock")){

				if(customerID.length()>0){
					
					Intent i=new Intent(context, Plugin_Clock_T1.class);
					i.putExtra("pagetitle", p1);
					context.startActivity(i);
					
				}else{
					
					editor.remove("customerPageTitle");
					editor.putString("customerPageTitle", p1);
					editor.remove("afterLogin");
					editor.putString("afterLogin", "clock");
					editor.commit();

					Intent i=new Intent(context, Customer_MainPage.class);
					context.startActivity(i);
					
				}
				
			}else if(ptype.equals("gps")){

				String uri = "geo:"+ p1 + "," + p2;
				context.startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri)));
			
			} else if(ptype.equals("sms")){
				/*Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( "sms:" + p1 ) ); 
				intent.putExtra( "sms_body", p2 ); 
				context.startActivity(intent);*/

			} else if(ptype.equals("productcat")){
				
				Log.d(LOG_TAG, "P1  = "+p1);
				Log.d(LOG_TAG, "P2  = "+p2);
				Log.d(LOG_TAG, "P3  = "+p3);
				
				String prod_cat_name = p1;
				String parent_name = p2;
				String child_name = p3;
				
				Intent i=new Intent(context, Plugin_Content_CatT1.class);
				i.putExtra("prod_cat_name", prod_cat_name);
				i.putExtra("parent_name", parent_name);
				i.putExtra("child_name", child_name);
				i.putExtra("top_id", actvalue);
				context.startActivity(i);
				
			}else if(ptype.equals("contentcat")){

				listtemplate = p2;
				cont_cat_name = p1;
				cont_category_id = p3;
				Log.d(LOG_TAG, "listtemplate="+listtemplate);

				if(listtemplate.equals("3") || listtemplate.equals("4") || listtemplate.equals("8")){

					Intent i=new Intent(context, Plugin_Content_CatT3.class);
					i.putExtra("cont_cat_name", cont_cat_name);
					i.putExtra("cont_category_id", cont_category_id);
					i.putExtra("list_template", listtemplate);
					i.putExtra("top_id", actvalue);
					context.startActivity(i);

				}else if(listtemplate.equals("5")){
					
					Intent i=new Intent(context, Plugin_Content_CatT5.class);
					i.putExtra("cont_cat_name", cont_cat_name);
					i.putExtra("cont_category_id", cont_category_id);
					i.putExtra("list_template", listtemplate);
					i.putExtra("top_id", actvalue);
					context.startActivity(i);
				}
				else if(listtemplate.equals("7")){
					
					Intent i=new Intent(context, Plugin_Content_CatT7.class);
					i.putExtra("cont_cat_name", cont_cat_name);
					i.putExtra("cont_category_id", cont_category_id);
					i.putExtra("list_template", listtemplate);
					i.putExtra("top_id", actvalue);
					context.startActivity(i);
			
				} else {

					Intent i=new Intent(context, Plugin_Content_CatT2.class);
					i.putExtra("cont_cat_name", cont_cat_name);
					i.putExtra("cont_category_id", cont_category_id);
					i.putExtra("list_template", listtemplate);
					i.putExtra("top_id", actvalue);
					context.startActivity(i);

				}
				
			} else if(ptype.equals("content")){

				f = Helper_Contentcategory.getByTopId(actvalue);
				if (f.moveToLast() != false){

					listtemplate = Helper_Contentcategory.getlist_template(f).toString();
					cont_cat_name = Helper_Contentcategory.getname(f).toString();
					cont_category_id = Helper_Contentcategory.getcontentcat_id(f).toString();
					plugin_id = Helper_Contentcategory.getplugin_id(f).toString();
					allow_comments = Helper_Contentcategory.getallow_comments(f).toString();
					allow_favorite = Helper_Contentcategory.getallow_favorite(f).toString();
					Log.d(LOG_TAG, "listtemplate="+listtemplate);
				}
				f.close();

				if(listtemplate.equals("1")){ //**** plugin content template 1 skip template list direct to content detail

					h = Helper_Content.getByCategoryId(cont_category_id);
					if (h.moveToLast() != false){			 				 		
						contentid = Helper_Content.getcontent_id(h).toString();
						Log.d(LOG_TAG, "contentid="+contentid);			 				 		 
					}
					h.close();

					Intent i=new Intent(context, Plugin_Content_Detail.class);
					i.putExtra("contentcategorytitle", cont_cat_name);
					i.putExtra("contentid", contentid);
					i.putExtra("list_template", listtemplate);
					i.putExtra("plugin_id", plugin_id);
					i.putExtra("allow_comments", allow_comments);
					i.putExtra("allow_favorite", allow_favorite);
					i.putExtra("top_id", actvalue.toString());
					i.putExtra("from", "content-home");
					context.startActivity(i);

				} else if(listtemplate.equals("2")) {

					Intent i=new Intent(context, Plugin_Content_T2.class);
					i.putExtra("cont_cat_name", cont_cat_name);
					i.putExtra("cont_category_id", cont_category_id);
					i.putExtra("list_template", listtemplate);
					i.putExtra("plugin_id", plugin_id);
					i.putExtra("allow_comments", allow_comments);
					i.putExtra("allow_favorite", allow_favorite);
					context.startActivity(i);

				}else if(listtemplate.equals("3") || listtemplate.equals("4") || listtemplate.equals("8")){

					Intent i=new Intent(context, Plugin_Content_T3.class);
					i.putExtra("cont_cat_name", cont_cat_name);
					i.putExtra("cont_category_id", cont_category_id);
					i.putExtra("list_template", listtemplate);
					i.putExtra("plugin_id", plugin_id);
					i.putExtra("allow_comments", allow_comments);
					i.putExtra("allow_favorite", allow_favorite);
					context.startActivity(i);

				}else if(listtemplate.equals("6")){

					Intent i=new Intent(context, Plugin_Content_T6.class);
					i.putExtra("cont_cat_name", cont_cat_name);
					i.putExtra("cont_category_id", cont_category_id);
					i.putExtra("list_template", listtemplate);
					i.putExtra("plugin_id", plugin_id);
					i.putExtra("allow_comments", allow_comments);
					i.putExtra("allow_favorite", allow_favorite);
					context.startActivity(i);

				}else if(listtemplate.equals("9")){

					Intent i=new Intent(context, Plugin_Content_T9.class);
					i.putExtra("cont_cat_name", cont_cat_name);
					i.putExtra("cont_category_id", cont_category_id);
					i.putExtra("list_template", listtemplate);
					i.putExtra("plugin_id", plugin_id);
					i.putExtra("allow_comments", allow_comments);
					i.putExtra("allow_favorite", allow_favorite);
					context.startActivity(i);

				} else {
					//Plugin_Content_T5 Currently for 5 & 7
					Intent i=new Intent(context, Plugin_Content_T5.class);
					i.putExtra("cont_cat_name", cont_cat_name);
					i.putExtra("cont_category_id", cont_category_id);
					i.putExtra("list_template", listtemplate);					
					i.putExtra("plugin_id", plugin_id);
					i.putExtra("allow_comments", allow_comments);
					i.putExtra("allow_favorite", allow_favorite);
					context.startActivity(i);

				}

			} else if(ptype.equals("search")){

				Intent i=new Intent(context, Plugin_Content_T5.class);
				i.putExtra("cont_cat_name", p1);
				i.putExtra("cont_category_id", "search");
				i.putExtra("list_template", "5");					
				i.putExtra("plugin_id", plugin_id);
				context.startActivity(i);

			} else if(ptype.equals("customer")){

				editor.remove("customerPageTitle");
				editor.putString("customerPageTitle", p1);
				editor.remove("afterLogin");
				editor.putString("afterLogin", "None");
				editor.commit();

				Intent i=new Intent(context, Customer_MainPage.class);
				context.startActivity(i);

			} else if(ptype.equals("loyalty")){

				editor.remove("loyaltyPageTitle");
				editor.putString("loyaltyPageTitle", p1);
				editor.remove("afterLogin");
				editor.putString("afterLogin", "Loyalty");
				editor.commit();

				Intent i=new Intent(context, Loyalty.class);
				context.startActivity(i);

			}else if(ptype.equals("favorite")){

				editor.putString("favoritePageTitle", p1);
				editor.remove("afterLogin");
				editor.putString("afterLogin", "favorite");
				editor.commit();

				Intent i=new Intent(context, FavoriteList.class);
				i.putExtra("top_id",actvalue);
				i.putExtra("favoritePageTitle",p1);
				context.startActivity(i);

			}else if(ptype.equals("warranty")){
				editor.remove("warrantyPageTitle");
				editor.putString("warrantyPageTitle", p1);
				editor.remove("afterLogin");
				editor.putString("afterLogin", "Warranty");
				editor.commit();

				if(customerID.equals("")){

					final AlertDialog.Builder alert = new AlertDialog.Builder(context);

					alert.setTitle(context.getResources().getString(R.string.login_toproceed));

					alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							Intent i=new Intent(context.getApplicationContext(), Customer_MainPage.class);
							context.startActivity(i);
						}

					});
					alert.show();

				}else{
					Intent i=new Intent(context, Warranty_List.class);
					context.startActivity(i);
				}

			} else if(ptype.equals("inbox")){
				editor.remove("InboxTitle");
				editor.putString("InboxTitle", p1);
				editor.remove("InboxTop");
				editor.putString("InboxTop", actvalue);
				editor.remove("afterLogin");
				editor.putString("afterLogin", "Inbox");
				editor.commit();

				if(customerID.equals("")){

					final AlertDialog.Builder alert = new AlertDialog.Builder(context);

					alert.setTitle(context.getResources().getString(R.string.login_toproceed));

					alert.setPositiveButton(context.getResources().getString(R.string.login), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							Intent i=new Intent(context.getApplicationContext(), Customer_MainPage.class);
							context.startActivity(i);
						}

					});
					alert.show();

				}else{
					Intent i=new Intent(context, Customer_Inbox.class);
					i.putExtra("noback", "no");
					context.startActivity(i);
				}

			} else if(ptype.equals("booking")){
				
				editor.remove("rsvPageTitle");
				editor.putString("rsvPageTitle", p1);
				editor.remove("afterLogin");
				editor.putString("afterLogin", "Reservation");
				editor.putString("topid_reservation", actvalue);
				editor.commit();

				if(customerID.equals("")){

					final AlertDialog.Builder alert = new AlertDialog.Builder(context);

					alert.setTitle(context.getResources().getString(R.string.login_toproceed));

					alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							Intent i=new Intent(context.getApplicationContext(), Customer_MainPage.class);
							context.startActivity(i);
						}

					});
					alert.show();

				}else{
					Intent i=new Intent(context, Reservation_List.class);
					i.putExtra("topid_reservation",actvalue);
					context.startActivity(i);
				}
				
			} else if(ptype.equals("points")){

				if(customerID.equals("")){

					final AlertDialog.Builder alert = new AlertDialog.Builder(context);

					alert.setTitle(context.getResources().getString(R.string.login_toproceed));

					alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							Intent i=new Intent(context.getApplicationContext(), Customer_MainPage.class);
							context.startActivity(i);
						}

					});
					alert.show();

				}else{
					Intent i=new Intent(context, Customer_Point.class);
					i.putExtra("noback", "no");
					i.putExtra("point_title", p1);
					i.putExtra("customer_point_title", p2);
					i.putExtra("level_title", p3);
					context.startActivity(i);
				}
				
			}

			
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "CreatePluginPage-Error:" + t.getMessage(), t); 
		}

	}

	public void CreateNotificationPage (String action_type, String action_value, String content_id) {
		String plugin_type="", plugin_value1="", plugin_value2="", plugin_value3="";

		Log.d(LOG_TAG+" CreateNotificationPage", action_type);
		Log.d(LOG_TAG, "action_value="+action_value);
		
		if(action_type.equals("page")) { 

			d = Helper_Themepage.getByActionvalue(action_value);  
			if (d.moveToLast() != false){
				theme_code = Helper_Themepage.gettheme_code(d).toString();
			} else {
				theme_code = "0";
			}
			d.close();
			
			CreateThemePage(theme_code,action_value);

		} else if (action_type.equals("plugin")){

			f = Helper_Themeobjectplugin.getByActvalue(action_value); //get object plugin data base on action value
			if (f.moveToLast() != false){

				plugin_type = Helper_Themeobjectplugin.getplugin_type(f).toString();
				plugin_value1 = Helper_Themeobjectplugin.getplugin_value1(f).toString();
				plugin_value2 = Helper_Themeobjectplugin.getplugin_value2(f).toString();
				plugin_value3 = Helper_Themeobjectplugin.getplugin_value3(f).toString();
				Log.d(LOG_TAG, "plugin_type="+plugin_type);
				Log.d(LOG_TAG, "plugin_value1="+plugin_value1);
				Log.d(LOG_TAG, "plugin_value2="+plugin_value2);
				Log.d(LOG_TAG, "plugin_value3="+plugin_value3);

			}
			f.close();
			
			int cid = Integer.parseInt(content_id);
			
			if (plugin_type.equals("content") && cid>0) {
				//VIEW CONTENT PAGE DIRECTLY
				String plugin_id="", cont_cat_name="", list_template="";
				String title="", details="", cat_id="";
				
				c = Helper_Content.getByContID(content_id);
				Log.d(LOG_TAG, "c.getCount()=" + c.getCount());

				if (c.moveToLast() != false) {
					title = Helper_Content.gettitle(c).toString();
					details = Helper_Content.getdetails(c).toString();
					cat_id = Helper_Content.getcategory_id(c).toString();					
				}

				c.close();
				
				if (cat_id.length()>0) {
				
					d = Helper_Contentcategory.getByCatId(cat_id);
					Log.d(LOG_TAG, "d.getCount()=" + d.getCount());

					if (d.moveToLast() != false) {
						plugin_id = Helper_Contentcategory.getplugin_id(d).toString();
						cont_cat_name = Helper_Contentcategory.getname(d).toString();
						list_template = Helper_Contentcategory.getlist_template(d).toString();
					}
				
					d.close();
				}				
				
				if (plugin_id.equals("20") || plugin_id.equals("21")) {
					Intent i=new Intent(context, Plugin_Content_Gallery.class);
					i.putExtra("contentcategorytitle", cont_cat_name);
					i.putExtra("contentid", content_id);
					i.putExtra("list_template", list_template);
					i.putExtra("plugin_id", plugin_id);
					i.putExtra("contentdetails", details);
					i.putExtra("contenttitle", title);

					context.startActivity(i);
					
				} else if(plugin_id.length()>0) {			
					Intent i=new Intent(context, Plugin_Content_Detail.class);
					i.putExtra("contentcategorytitle", cont_cat_name);
					i.putExtra("contentid", content_id);
					i.putExtra("list_template", list_template);
					i.putExtra("plugin_id", plugin_id);
					i.putExtra("top_id", action_value);
					i.putExtra("from", "content&cid>0");
					context.startActivity(i);
				}
				
			} else {
				CreatePluginPage(action_value,plugin_type,plugin_value1,plugin_value2,plugin_value3);
			}
		}			
	}

	public void close() {
		Helper_Themeobject.close();	
		Helper_Themepage.close();	
		Helper_Themeobjectplugin.close();	
		Helper_Contentcategory.close();	
		Helper_Content.close();	
	}

}

