package com.m3tech.appv3;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import com.m3tech.app_100_1474.R;

public class Main extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		//JDK
		//file:///Applications/Android%20Studio.app/Contents/jre/jdk/Contents/Home/
		///Library/Java/JavaVirtualMachines/jdk1.8.0_121.jdk
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}