package com.m3tech.appv3;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.provider.Settings.Secure;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.BeaconManager.MonitoringListener;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;
import com.m3tech.app_100_1474.R;
import com.m3tech.collection.Collection_Ibeaconcontent;
import com.m3tech.collection.Collection_Ibeacondevice;
import com.m3tech.customer.Customer_Inbox;
import com.m3tech.data.DataProcessing;
import com.m3tech.data.Helper_Appsetup;
import com.m3tech.data.Helper_Content;
import com.m3tech.data.Helper_Contentcategory;
import com.m3tech.data.Helper_Ibeaconcontent;
import com.m3tech.data.Helper_Ibeacondevice;
import com.m3tech.data.Helper_Ibeaconxref;
import com.m3tech.data.Helper_Loyalty;
import com.m3tech.data.Helper_PreviousdeviceID;
import com.m3tech.data.Helper_Themecolor;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.data.Helper_Themeobjectplugin;
import com.m3tech.data.Helper_Themepage;
import com.m3tech.ibeacon.BeaconView;
import com.m3tech.tools.BackgroundService;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


@SuppressWarnings("deprecation")
public class Splash extends Activity {

	private static final String LOG_TAG = "SPLASH";
	public static final String PREFS_NAME = "MyPrefsFile"; 

	Boolean blnAPI_APPSETUP=false;
	String strAPI_APPSETUP;
	Boolean blnAPI_THEMEPAGE=false;
	String strAPI_THEMEPAGE;
	Boolean blnAPI_THEMEOBJECT=false;
	String strAPI_THEMEOBJECT;
	Boolean blnAPI_THEMECOLOR=false;
	String strAPI_THEMECOLOR;
	Boolean blnAPI_THEMEOBJECTPLUGIN=false;
	String strAPI_THEMEOBJECTPLUGIN;
	Boolean blnAPI_CONTENT=false;
	String strAPI_CONTENT;
	Boolean blnAPI_CONTENTCATEGORY=false;
	String strAPI_CONTENTCATEGORY;
	Boolean blnAPI_LOYALTY = false;
	String strAPI_LOYALTY;
	Boolean blnAPI_IBEACONDEVICE = false;
	String strAPI_IBEACONDEVICE;
	Boolean blnAPI_IBEACONCONTENT = false;
	String strAPI_IBEACONCONTENT;
	Boolean blnAPI_IBEACONXREF = false;
	String strAPI_IBEACONXREF;
	Boolean blnAPI_CHECKDATE = false;
	String strAPI_CHECKDATE;
	

	Boolean beaconIsGood = false;
	protected boolean _active = true;
	protected int _splashTime = 5000;
	static Boolean network_status=false;
	Boolean getAPI = false;
	Boolean firsttimer = false;
	Boolean dataHasBeenLoaded = false;
	Boolean isLatestData = false;
	boolean reCreateFirstPage = false;
	ImageView imglogo;
	LinearLayout layout_progress,layout_login;
	
	String ntf_action_type="", ntf_action_value="", ntf_content_id="";

	private static final int REQUEST_ENABLE_BT = 1234;
	// Cache Image

	//private static HashMap<String, Bitmap> cache = new HashMap<String, Bitmap>();

	// Declare Helper
	Helper_Themepage Helper_Themepage = null;
	Helper_Appsetup Helper_Appsetup = null;
	Helper_Themecolor Helper_Themecolor = null;
	Helper_Themeobject Helper_Themeobject = null;
	Helper_Themeobjectplugin Helper_Themeobjectplugin = null;
	Helper_Content Helper_Content = null;
	Helper_Contentcategory Helper_Contentcategory = null;
	Helper_Loyalty Helper_Loyalty = null;
	Helper_Ibeacondevice Helper_Ibeacondevice = null;
	Helper_Ibeaconcontent Helper_Ibeaconcontent = null;
	Helper_Ibeaconxref Helper_Ibeaconxref =null;
	Helper_PreviousdeviceID Helper_PreviousdeviceID =null;

	static ProgressDialog progressDialog;

	SharedPreferences settings;

	String app_user, app_db, currentdate, udid, customerID, latest_app_user;
	String customerName,deviceID,contentID;
	String scanDate = "0";
	Cursor z,h,j,p,q;
	String wel_msg, wel_img, beaconpage_id, beaconstart_date, beaconend_date, beaconstatus;
	String app_ibeacon, app_loyalty, app_points;

	// private IBeaconManager iBeaconManager = IBeaconManager.getInstanceForApplication(this);

	List<Collection_Ibeacondevice> model_ibeacondevice=new ArrayList<Collection_Ibeacondevice>();
	Collection_Ibeacondevice current_ibeacondevice=null;	

	List<Collection_Ibeaconcontent> model_ibeaconcontent=new ArrayList<Collection_Ibeaconcontent>();
	Collection_Ibeaconcontent current_ibeaconcontent=null;	

	SlotAction slotAction = null;
	DataProcessing dataProcessing = null;

	Handler handler = new Handler();
	Runnable refresh;
	
	String home_pageid="";
	String home_pagename="";
	String app_title="";
	String themecolor="";
	String colorcode="";
	String colorcode1="";
	String app_background="";
	String theme_code="";
	String app_tnc="";
	String titlebar_img="";
	String show_title="";
	String background = "";

	/** Called when the activity is first created. */
	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		Window window = getWindow();
		window.setFormat(PixelFormat.RGBA_8888);
	}

	private BeaconManager beaconManager;
	private NotificationManager notificationManager;
	List<Beacon> listOfBeacons=new ArrayList<Beacon>();
	int NOTIFICATION_ID = 123;
	@SuppressWarnings("unused")
	private static final String ESTIMOTE_BEACON_PROXIMITY_UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
	private static final Region ALL_ESTIMOTE_BEACONS_REGION = new Region("rid", null, null, null);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.e(LOG_TAG,"ENTERING SPLASH");
		
		
		String packagename = getApplicationContext().getPackageName();
		Log.e("My Key", "Package Name: "+packagename);

		try {
			PackageInfo info = getPackageManager().getPackageInfo(packagename, PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				String sign=Base64.encodeToString(md.digest(), Base64.DEFAULT);
				Log.e("My Key", "MY KEY HASH: "+sign);
				//Toast.makeText(getApplicationContext(),sign,     Toast.LENGTH_LONG).show();
			}
		} catch (NameNotFoundException e) {
		} catch (NoSuchAlgorithmException e) {
		}


		setContentView(R.layout.splash);

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
		.threadPriority(Thread.NORM_PRIORITY)
		.denyCacheImageMultipleSizesInMemory()
		.discCacheFileNameGenerator(new Md5FileNameGenerator())
		.tasksProcessingOrder(QueueProcessingType.FIFO)
		//.writeDebugLogs() 
		.build();

		ImageLoader.getInstance().init(config);

		int screen_height = getWindowManager().getDefaultDisplay().getHeight();
		int screen_width = getWindowManager().getDefaultDisplay().getWidth();
		Log.d(LOG_TAG,"screen_height="+ screen_height);
		Log.d(LOG_TAG,"screen_width="+ screen_width);

		// call helper
		Helper_Themepage = new Helper_Themepage(this);	
		Helper_Appsetup = new Helper_Appsetup(this);
		Helper_Themecolor = new Helper_Themecolor(this);
		Helper_Themeobject = new Helper_Themeobject(this);
		Helper_Themeobjectplugin = new Helper_Themeobjectplugin(this);
		Helper_Content = new Helper_Content(this);
		Helper_Contentcategory = new Helper_Contentcategory(this);
		Helper_Loyalty = new Helper_Loyalty(this);
		Helper_Ibeacondevice = new Helper_Ibeacondevice(this);
		Helper_Ibeaconcontent = new Helper_Ibeaconcontent(this);
		Helper_Ibeaconxref = new Helper_Ibeaconxref(this);
		Helper_PreviousdeviceID = new Helper_PreviousdeviceID(this);
		
		slotAction = new SlotAction(this);
		dataProcessing = new DataProcessing(this);

		app_db = getResources().getString(R.string.app_db);
		app_user = getResources().getString(R.string.app_user);
		NOTIFICATION_ID = Integer.parseInt(app_user);
		app_ibeacon = getResources().getString(R.string.app_ibeacon);
		app_loyalty = getResources().getString(R.string.app_loyalty);
		app_points = getResources().getString(R.string.app_points);

		settings = this.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();

		customerName = settings.getString("customerName", "");
		customerID = settings.getString("customerID", "");
		latest_app_user = settings.getString("app_user", "");
		udid = Secure.getString(this.getContentResolver(),Secure.ANDROID_ID); //device id
		editor.putString("udid", udid);
		editor.commit();

		Log.d(LOG_TAG,"app_db = " + app_db);		
		Log.d(LOG_TAG,"app_user = " + app_user);		
		Log.d(LOG_TAG,"Customer = " + customerID + " : "+ customerName);		
		Log.d(LOG_TAG,"UDID = " + udid);		

		// Configure BeaconManager.
		beaconManager = new BeaconManager(this);

		// Default values are 5s of scanning and 25s of waiting time to save CPU cycles.
		// In order for this demo to be more responsive and immediate we lower down those values.
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		beaconManager.setBackgroundScanPeriod(TimeUnit.SECONDS.toMillis(10), TimeUnit.SECONDS.toMillis(20));	 
		printKeyHash();
		SplashStart Task = new SplashStart();
		Task.execute(new String[] { });
		
		startService(new Intent(this, BackgroundService.class));

	}

	private void printKeyHash() {
	    // Add code to print out the key hash
	    try {
	        PackageInfo info = getPackageManager().getPackageInfo("com.th.m3tech.piyanas", PackageManager.GET_SIGNATURES);
	        for (Signature signature : info.signatures) {
	            MessageDigest md = MessageDigest.getInstance("SHA");
	            md.update(signature.toByteArray());
	            Log.d(LOG_TAG, "key : " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
	        }
	    } catch (NameNotFoundException e) {
	        Log.e("KeyHash:", e.toString());
	    } catch (NoSuchAlgorithmException e) {
	        Log.e("KeyHash:", e.toString());
	    }
	}
	
	private class SplashStart extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			Log.d(LOG_TAG,"SplashStart start...");
			super.onPreExecute();
			//progressDialog = ProgressDialog.show(getActivity(), "", getResources().getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;
			try{
				if(app_user.equals(latest_app_user)){	    	 
					Log.d(LOG_TAG, "Not a first time");	

				} else {
					Log.e(LOG_TAG, "First timer");	
					SharedPreferences.Editor editor = settings.edit();
					editor.remove("app_user");
					editor.putString("app_user", app_user);
					editor.commit();
					firsttimer = true;
				}

				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "SplashStart-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}
		@SuppressWarnings("unused")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				Log.d(LOG_TAG,"SplashStart end...");
				if (result.equals("SUCCESS")) {
					if 	(firsttimer) {

						GetAPIOFFLINEData Task = new GetAPIOFFLINEData();
						Task.execute(new String[] { });
						
					} else {

						Intent j = getIntent();
						String ib_pid = j.getStringExtra("ib_pid");
						String ib_cid = j.getStringExtra("ib_cid");
						String action_type = j.getStringExtra("action_type");
						String action_value = j.getStringExtra("action_value");
						String content_id = j.getStringExtra("content_id");

						if (ib_pid==null || ib_pid=="") {
							if (action_type==null) {
								action_type="";
							}
							
							if (action_type.equals("inbox")) {
								Log.e(LOG_TAG, "Load Inbox");	

								BeaconStart();
								Intent i = new Intent(Splash.this, Customer_Inbox.class);
								i.putExtra("noback", "yes");

								startActivity(i);

							} else {
								if (action_type.length()>0) {
																		
									ntf_action_type = action_type;
									if (action_value!=null) { ntf_action_value = action_value; }
									if (content_id!=null) { ntf_content_id = content_id; }
	
									SharedPreferences.Editor editor = settings.edit();
									editor.remove("ntf_action_type");
									editor.remove("ntf_action_value");
									editor.remove("ntf_content_id");
	
									editor.putString("ntf_action_type", ntf_action_type);
									editor.putString("ntf_action_value", ntf_action_value);
									editor.putString("ntf_content_id",ntf_content_id);
									editor.commit();
								}

								SplashProcess();
							}
							
						} else {
							Log.e(LOG_TAG, "Load beacon details: "+ib_pid);	

							BeaconStart();
							slotAction.CreateThemePage("no-back-button",ib_pid); //tell slot action to remove back button

							Intent intent = new Intent(getApplicationContext(), BeaconView.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

							startActivity(intent);

						}

					}
				}
			}

			catch (Throwable t) {
				Log.e(LOG_TAG, "SplashStart-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}   


	public void SplashProcess(){

		BeaconAsk();

		// thread for displaying the SplashScreen
		Thread SplashProcessTread = new Thread() {
			@Override
			public void run() {
				try {
					int waited = 0;
					while((_active || (waited < _splashTime))) {
						sleep(100);
						waited += 100;
					}

				} catch(InterruptedException e) {
					// do nothing
					CreateFirstPage Task = new CreateFirstPage();
					Task.execute(new String[] { });

				} finally {

					Date d = new Date();
					currentdate = (new SimpleDateFormat("yyyy-MM-dd kk:mm")).format(d);
					Log.d(LOG_TAG,"Offline currentdate: "+currentdate);

					//terus create dulu.. baru update kemudian..
					Log.d(LOG_TAG,"LOOP at SplashProcess");

					CreateFirstPage Task = new CreateFirstPage();
					Task.execute(new String[] { });

				}
			}
		};
		SplashProcessTread.start();

	}


	private class CreateFirstPage extends AsyncTask<String, Void, String> {


		@Override
		protected void onPreExecute() {
			Log.d(LOG_TAG,"CreateFirstPage start...");
			super.onPreExecute();
			//progressDialog = ProgressDialog.show(getActivity(), "", getResources().getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;

			try{
				Log.d(LOG_TAG,"Get First Page");
				Cursor c = Helper_Themepage.getByPrevpage("0");
				if (c.moveToLast() != false) {
					theme_code = Helper_Themepage.gettheme_code(c).toString();
					home_pageid = Helper_Themepage.getthemepage_id(c).toString();
					home_pagename = Helper_Themepage.getname(c).toString();
					Log.d(LOG_TAG,"theme_code="+theme_code);
				}
				c.close();

				Cursor a = Helper_Appsetup.getAll();
				if (a.moveToLast() != false){
					app_title = Helper_Appsetup.geticon_name(a).toString();
					themecolor = Helper_Appsetup.getthemecolor(a).toString();
					background = Helper_Appsetup.getbackground(a).toString();
					app_background = Helper_Appsetup.gettemplate(a).toString();
					app_tnc = Helper_Appsetup.getapp_tnc(a).toString();
					titlebar_img = Helper_Appsetup.getloyalty_title(a).toString();
					show_title = Helper_Appsetup.getloyalty_status(a).toString();
					Log.d(LOG_TAG, "app_title="+app_title);
					Log.d(LOG_TAG, "themecolor="+themecolor);
				}
				a.close();

				Cursor g = Helper_Themecolor.getbycolorname(themecolor);
				if (g.moveToLast() != false){
					colorcode = Helper_Themecolor.getcolor(g).toString();
					colorcode1 = Helper_Themecolor.getcolor1(g).toString();
					Log.d(LOG_TAG, "colorcode="+colorcode);
				}
				g.close();


				SharedPreferences.Editor editor = settings.edit();
				editor.remove("home_themecode");
				editor.remove("home_pageid");
				editor.remove("home_pagename");
				editor.remove("app_title");
				editor.remove("colorcode");
				editor.remove("colorcode1");
				editor.remove("app_background");
				editor.remove("app_tnc");
				editor.remove("titlebar_img");
				editor.remove("show_title");
				editor.remove("background");

				editor.putString("home_themecode", theme_code);
				editor.putString("home_pageid", home_pageid);
				editor.putString("home_pagename",home_pagename);
				editor.putString("app_title",app_title);
				editor.putString("colorcode",colorcode);
				editor.putString("colorcode1",colorcode1);
				editor.putString("app_background",app_background);
				editor.putString("app_tnc",app_tnc);
				editor.putString("titlebar_img",titlebar_img);
				editor.putString("show_title",show_title);
				editor.putString("background",background);
				editor.commit();

				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "CreateFirstPage-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")) {
					if (theme_code.length()>0) {

						if (!isLatestData) {
							//slotAction.CreateThemePage(theme_code,home_pageid);
							
							//TODO FOR IBEACON TESTING (WITHOUT IBEACON) ONLY
							//Intent intent = new Intent(getApplicationContext(), BeaconView.class);
							//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							//startActivity(intent);
							
							
							UpdateDefaultData();
						} else {
							if (reCreateFirstPage) {
								Log.d(LOG_TAG,"reCreateFirstPage TRUE");								
								String current_pageid = settings.getString("current_pageid", "");
								Log.d(LOG_TAG,"current_pageid: "+current_pageid);								
								
								if (ntf_action_type.length()>0) {
									slotAction.CreateNotificationPage (ntf_action_type, ntf_action_value, ntf_content_id);									
								} else {
									slotAction.CreateThemePage(theme_code,home_pageid);									
								}

								BeaconStart();

							}
						}

						BeaconStart();
					}
				}
				Log.d(LOG_TAG,"CreateFirstPage end...");

			}

			catch (Throwable t) {
				Log.e(LOG_TAG, "CreateFirstPage-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}   


	public void UpdateDefaultData(){

		// thread for displaying the SplashScreen
		Thread updateTread = new Thread() {
			@Override
			public void run() {

				try {
					int waited = 0;
					int maxDelay = 200;

					while(!isLatestData && waited < maxDelay) {
						sleep(100);
						waited += 100;
					}

				} catch(InterruptedException e) {
					// do nothing

				} finally {
					Log.d(LOG_TAG,"LOOP at UpdateDefaultData");
					Building_Handler.sendEmptyMessage(0);

				}
			}
		};
		updateTread.start();

	}

	private Handler Building_Handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			try{

				Date d = new Date();
				currentdate = (new SimpleDateFormat("yyyy-MM-dd kk:mm")).format(d);

				Log.d(LOG_TAG,"Check existing log date: "+currentdate);

				Cursor c_time1=Helper_Appsetup.getByLastUpdate(currentdate);
				if (c_time1.moveToLast() != false){
					Log.d(LOG_TAG,"API_APPSETUP Database-UPDATED");

				}else{
					blnAPI_APPSETUP=true;
					getAPI = true;
					Log.d(LOG_TAG,"API_APPSETUP Database-OLD");
				}
				c_time1.close();

				Cursor c_time4=Helper_Content.getByLastUpdate(currentdate);
				if (c_time4.moveToLast() != false){
					Log.d(LOG_TAG,"API_CONTENT Database-UPDATED");

				}else{
					blnAPI_CONTENT=true;
					getAPI = true;
					Log.d(LOG_TAG,"API_CONTENT Database-OLD");
				}
				c_time4.close();

				Cursor c_time6=Helper_Contentcategory.getByLastUpdate(currentdate);
				if (c_time6.moveToLast() != false){
					Log.d(LOG_TAG,"API_CONTENTCATEGORY Database-UPDATED");

				}else{
					blnAPI_CONTENTCATEGORY=true;
					getAPI = true;
					Log.d(LOG_TAG,"API_CONTENTCATEGORY Database-OLD");
				}
				c_time6.close();
				
				if (app_ibeacon.equalsIgnoreCase("YES")) {
					Cursor c_time9=Helper_Ibeaconcontent.getByLastUpdate(currentdate);
					if (c_time9.moveToLast() != false){
						Log.d(LOG_TAG,"IBEACON CONTENT Database-UPDATED");
		
					}else{
						blnAPI_IBEACONCONTENT=true;
						getAPI = true;
						Log.d(LOG_TAG,"IBEACON CONTENT Database-OLD");
					}
					c_time9.close();
		
		
					Cursor c_time8=Helper_Ibeacondevice.getByLastUpdate(currentdate);
					if (c_time8.moveToLast() != false){
						Log.d(LOG_TAG,"IBEACON DEVICE Database-UPDATED");
		
					}else{
						blnAPI_IBEACONDEVICE=true;
						getAPI = true;
						Log.d(LOG_TAG,"IBEACON DEVICE Database-OLD");
					}
					c_time8.close();
		
		
					Cursor c_time10=Helper_Ibeaconxref.getByLastUpdate(currentdate);
					if (c_time10.moveToLast() != false){
						Log.d(LOG_TAG,"IBEACON XREF Database-UPDATED");
		
					}else{
						blnAPI_IBEACONXREF=true;
						getAPI = true;
						Log.d(LOG_TAG,"IBEACON XREF Database-OLD");
		
					}
					c_time10.close();				
				}

				if (app_loyalty.equalsIgnoreCase("YES")) {
					Cursor c_time7=Helper_Loyalty.getByLastUpdate(currentdate);
					if (c_time7.moveToLast() != false){
						Log.d(LOG_TAG,"LOYALTY_LIST Database-UPDATED");
	
					}else{
						blnAPI_LOYALTY=true;
						getAPI = true;
						Log.d(LOG_TAG,"LOYALTY Database-OLD");
					}
					c_time7.close();
				}

				Cursor c_time5=Helper_Themecolor.getByLastUpdate(currentdate);
				if (c_time5.moveToLast() != false){
					Log.d(LOG_TAG,"API_THEMECOLOR Database-UPDATED");

				}else{
					blnAPI_THEMECOLOR=true;
					Log.d(LOG_TAG,"API_THEMECOLOR Database-OLD");
					getAPI = true;
				}
				c_time5.close();

				Cursor c_time2=Helper_Themeobject.getByLastUpdate(currentdate);
				if (c_time2.moveToLast() != false){
					Log.d(LOG_TAG,"API_THEMEOBJECT Database-UPDATED");

				}else{
					blnAPI_THEMEOBJECT=true;
					Log.d(LOG_TAG,"API_THEMEOBJECT Database-OLD");
					getAPI = true;
				}
				c_time2.close();


				Cursor c_time3=Helper_Themeobjectplugin.getByLastUpdate(currentdate);
				if (c_time3.moveToLast() != false){
					Log.d(LOG_TAG,"API_THEMEOBJECTPLUGIN Database-UPDATED");

				}else{
					blnAPI_THEMEOBJECTPLUGIN=true;
					Log.d(LOG_TAG,"API_THEMEOBJECTPLUGIN Database-OLD");
					getAPI = true;
				}
				c_time3.close();


				Cursor c_time=Helper_Themepage.getByLastUpdate(currentdate);
				if (c_time.moveToLast() != false){
					Log.d(LOG_TAG,"API_THEMEPAGE Database-UPDATED");

				}else{
					blnAPI_THEMEPAGE=true;
					Log.d(LOG_TAG,"API_THEMEPAGE Database-OLD");
					getAPI = true;
				}
				c_time.close(); 

				if(getAPI){
					Log.d(LOG_TAG,"LOOP at Building_Handler");

					GetAPILatestDate Task = new GetAPILatestDate();
					Task.execute(new String[] { currentdate });
				}


			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "Process_Handler-Error:" + t.getMessage(), t);

			}
		}
	}; 


	private class GetAPILatestDate extends AsyncTask<String, Void, String> {
		@SuppressWarnings("unused")
		String strCheckDate;
		@Override
		protected void onPreExecute() {
			Log.d(LOG_TAG,"GetAPILatestDate start...");
			super.onPreExecute();
			//progressDialog = ProgressDialog.show(getActivity(), "", getResources().getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;
			try{
				network_status = isNetworkAvailable();

				if(network_status == true){

					strCheckDate = arg0[0];
					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

					String stdPara = "db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
							+ "&cs="+URLEncoder.encode(slotAction.getCheckSum(), "UTF-8");

					//Check if got any new message

					HttpPost HTTP_POST_TOP;
					String strURL;

					if(!customerID.equals("null") && !customerID.isEmpty() && !customerID.equals("")){
						
						strURL = getResources().getString(R.string.INBOX_LIST_API) + stdPara;

						HTTP_POST_TOP = new HttpPost(strURL);
						Log.d(LOG_TAG,"strURL="+ strURL);
						//Execute HTTP Post Request
						ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
						String responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	

						JSONArray msgJSONArray = new JSONArray(responseBody);

						if (msgJSONArray.length()>0){
							int i = msgJSONArray.length()-1;
							String msgid = msgJSONArray.getJSONObject(i).getString("id").toString();

							settings = getSharedPreferences(PREFS_NAME, 0);
							String latest_msgid = settings.getString("latest_msgid", "");
							if(latest_msgid ==null || latest_msgid.equals("") || !latest_msgid.equals(msgid)){						
								PostNewMessageAvailable("You got new message!");
							}

							SharedPreferences.Editor editor = settings.edit();
							editor.remove("latest_msgid");
							editor.putString("latest_msgid", msgid);
							editor.commit();
						}
					}


					if(blnAPI_APPSETUP==true){  
						Log.d(LOG_TAG,"Retrieve from Check Date API...");
						strURL = getResources().getString(R.string.CHECKDATE_API) + stdPara;
						Log.d(LOG_TAG,"strURL="+ strURL);

						HttpPost HTTP_POST_CHECKDATE;
						HTTP_POST_CHECKDATE = new HttpPost(strURL);

						//Execute HTTP Post Request
						ResponseHandler<String> responseHandler_CHECKDATE=new BasicResponseHandler();
						strAPI_CHECKDATE=httpclient.execute(HTTP_POST_CHECKDATE, responseHandler_CHECKDATE);	


						JSONArray checkdateJSONArray = new JSONArray(strAPI_CHECKDATE);

						if (checkdateJSONArray.length()>0) {
							String app_setup="",contents="",content_category="",ibeacon_content="",ibeacon_device="",ibeacon_xref="",
									loyalty="",theme_color="", theme_object="",theme_object_plugin="",theme_page="";

							for (int n = 0; n < checkdateJSONArray.length(); n++) {
								app_setup= checkdateJSONArray.getJSONObject(n).getString("app_setup").toString();	
								contents=checkdateJSONArray.getJSONObject(n).getString("contents").toString();
								content_category=checkdateJSONArray.getJSONObject(n).getString("content_category").toString();
								ibeacon_content=checkdateJSONArray.getJSONObject(n).getString("ibeacon_content").toString();
								ibeacon_device=checkdateJSONArray.getJSONObject(n).getString("ibeacon_device").toString();
								ibeacon_xref=checkdateJSONArray.getJSONObject(n).getString("ibeacon_xref").toString();
								loyalty=checkdateJSONArray.getJSONObject(n).getString("loyalty").toString();
								theme_color=checkdateJSONArray.getJSONObject(n).getString("theme_color").toString();
								theme_object=checkdateJSONArray.getJSONObject(n).getString("theme_object").toString();
								theme_object_plugin=checkdateJSONArray.getJSONObject(n).getString("theme_object_plugin").toString();
								theme_page=checkdateJSONArray.getJSONObject(n).getString("theme_page").toString();

							}

							if(blnAPI_APPSETUP){ 
								String du="";
								Cursor c=Helper_Appsetup.getLastUpdate();
								if (c.moveToLast() != false) {
									du = Helper_Appsetup.getdate_updated(c).toString();
								}
								c.close();

								if (du.equals(app_setup)) {
									blnAPI_APPSETUP=false;
								}
							}


							if(blnAPI_THEMEPAGE){  
								String du="";
								Cursor c=Helper_Themepage.getLastUpdate();
								if (c.moveToLast() != false) {
									du = Helper_Themepage.getdate_updated(c).toString();
								}
								c.close();

								if (du.equals(theme_page)) {
									blnAPI_THEMEPAGE=false;
								}
							} 


							if(blnAPI_THEMECOLOR){  
								String du="";
								Cursor c=Helper_Themecolor.getLastUpdate();
								if (c.moveToLast() != false) {
									du = Helper_Themecolor.getdate_updated(c).toString();
								}
								c.close();
								
								if (du.equals(theme_color)) {									
									blnAPI_THEMECOLOR=false; 									
								}
							} 


							if(blnAPI_THEMEOBJECT){  
								String du="";
								Cursor c=Helper_Themeobject.getLastUpdate();
								if (c.moveToLast() != false) {
									du = Helper_Themeobject.getdate_updated(c).toString();
								}
								c.close();

								if (du.equals(theme_object)) {
									blnAPI_THEMEOBJECT=false;
								}
							} 


							if(blnAPI_THEMEOBJECTPLUGIN){  
								String du="";
								Cursor c=Helper_Themeobjectplugin.getLastUpdate();
								if (c.moveToLast() != false) {
									du = Helper_Themeobjectplugin.getdate_updated(c).toString();
								}
								c.close();

								if (du.equals(theme_object_plugin)) {
									blnAPI_THEMEOBJECTPLUGIN=false;
								}
							} 


							if(blnAPI_CONTENT){  
								String du="";
								Cursor c=Helper_Content.getLastUpdate();
								if (c.moveToLast() != false) {
									du = Helper_Content.getdate_updated(c).toString();
								}
								c.close();

								if (du.equals(contents)) {
									blnAPI_CONTENT=false;
								}
							} 


							if(blnAPI_CONTENTCATEGORY){  
								String du="";
								Cursor c=Helper_Contentcategory.getLastUpdate();
								if (c.moveToLast() != false) {
									du = Helper_Contentcategory.getdate_updated(c).toString();
								}
								c.close();

								if (du.equals(content_category)) {
									blnAPI_CONTENTCATEGORY=false;
								}
							} 

							if(blnAPI_LOYALTY && app_loyalty.equalsIgnoreCase("YES")){  
								String du="";
								Cursor c=Helper_Loyalty.getLastUpdate();
								if (c.moveToLast() != false) {
									du = Helper_Loyalty.getdate_updated(c).toString();
								}
								c.close();

								if (du.equals(loyalty)) {
									blnAPI_LOYALTY=false;
								}
							} 

							if (app_ibeacon.equalsIgnoreCase("YES")) {
								if(blnAPI_IBEACONDEVICE){  
									String du="";
									Cursor c=Helper_Ibeacondevice.getLastUpdate();
									if (c.moveToLast() != false) {
										du = Helper_Ibeacondevice.getdate_updated(c).toString();
									}
									c.close();
	
									if (du.equals(ibeacon_device)) {
										blnAPI_IBEACONDEVICE=false;
									}
								} 
	
	
								if(blnAPI_IBEACONCONTENT){  
									String du="";
									Cursor c=Helper_Ibeaconcontent.getLastUpdate();
									if (c.moveToLast() != false) {
										du = Helper_Ibeaconcontent.getdate_updated(c).toString();
									}
									c.close();
	
									if (du.equals(ibeacon_content)) {
										blnAPI_IBEACONCONTENT=false;
									}
								} 
	
								if(blnAPI_IBEACONXREF){  
									String du="";
									Cursor c=Helper_Ibeaconxref.getLastUpdate();
									if (c.moveToLast() != false) {
										du = Helper_Ibeaconxref.getdate_updated(c).toString();
									}
									c.close();
	
									if (du.equals(ibeacon_xref)) {
										blnAPI_IBEACONXREF=false;
									}
								} 
							}

						}

					}
					response="SUCCESS";
				} else {
					response="NETWORK ERROR";					
				}

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetAPILatestDate-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				Log.d(LOG_TAG,"GetAPILatestDate: "+result);
				if (result.equals("SUCCESS")) {
					GetAPIData Task = new GetAPIData();
					Task.execute(new String[] { currentdate });
				}else{
					slotAction.CreateThemePage(theme_code,home_pageid);

				}
			}

			catch (Throwable t) {
				Log.e(LOG_TAG, "GetAPILatestDate-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}   

	private class GetAPIData extends AsyncTask<String, Void, String> {
		String strCheckDate;
		@Override
		protected void onPreExecute() {
			Log.d(LOG_TAG,"GetAPIData start...");
			super.onPreExecute();
			//progressDialog = ProgressDialog.show(getActivity(), "", getResources().getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;
			try{
				strCheckDate = arg0[0];
				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
				network_status = isNetworkAvailable();

				if(network_status == true){

					String stdPara = "db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
							+ "&cs="+URLEncoder.encode(slotAction.getCheckSum(), "UTF-8");

					if(blnAPI_APPSETUP==true){  
						Log.d(LOG_TAG,"Retrieve from APPSSETUP_API...");
						String strURL = getResources().getString(R.string.APPSSETUP_API) + stdPara;
						Log.d(LOG_TAG,"strURL="+ strURL);

						HttpPost HTTP_POST_APPSETUP;
						HTTP_POST_APPSETUP = new HttpPost(strURL);

						//Execute HTTP Post Request
						ResponseHandler<String> responseHandler_APPSETUP=new BasicResponseHandler();
						strAPI_APPSETUP=httpclient.execute(HTTP_POST_APPSETUP, responseHandler_APPSETUP);	

						dataProcessing.CreateContent_Appsetup (strCheckDate,strAPI_APPSETUP);
						reCreateFirstPage=true;
					}


					if(blnAPI_THEMEPAGE==true){  
						Log.d(LOG_TAG,"Retrieve from THEMEPAGE_API...");
						String strURL = getResources().getString(R.string.THEMEPAGE_API) + stdPara;
						Log.d(LOG_TAG,"strURL="+ strURL);

						HttpPost HTTP_POST_THEMEPAGE;
						HTTP_POST_THEMEPAGE = new HttpPost(strURL);

						ResponseHandler<String> responseHandler_THEMEPAGE=new BasicResponseHandler();
						strAPI_THEMEPAGE=httpclient.execute(HTTP_POST_THEMEPAGE, responseHandler_THEMEPAGE);	

						dataProcessing.CreateContent_Themepage (strCheckDate, strAPI_THEMEPAGE);
						reCreateFirstPage=true;

					} 


					if(blnAPI_THEMECOLOR==true){  
						Log.d(LOG_TAG,"Retrieve from THEMEPAGE_API...");
						String strURL = getResources().getString(R.string.THEME_COLOR_API) + stdPara;
						Log.d(LOG_TAG,"strURL="+ strURL);

						HttpPost HTTP_POST_THEMECOLOR;
						HTTP_POST_THEMECOLOR = new HttpPost(strURL);

						//Execute HTTP Post Request
						ResponseHandler<String> responseHandler_THEMECOLOR=new BasicResponseHandler();
						strAPI_THEMECOLOR=httpclient.execute(HTTP_POST_THEMECOLOR, responseHandler_THEMECOLOR);	

						dataProcessing.CreateContent_Themecolor (strCheckDate,strAPI_THEMECOLOR);

						if(blnAPI_APPSETUP) {
							reCreateFirstPage=true; //true only if App Setup updated
						}

					} 


					if(blnAPI_THEMEOBJECT==true){  
						Log.d(LOG_TAG,"Retrieve from THEMEOBJECT API...");
						String strURL = getResources().getString(R.string.THEMEOBJECT_API) + stdPara;
						Log.d(LOG_TAG,"strURL="+ strURL);

						HttpPost HTTP_POST_THEMEOBJECT;
						HTTP_POST_THEMEOBJECT = new HttpPost(strURL);

						//Execute HTTP Post Request
						ResponseHandler<String> responseHandler_THEMEOBJECT=new BasicResponseHandler();
						strAPI_THEMEOBJECT=httpclient.execute(HTTP_POST_THEMEOBJECT, responseHandler_THEMEOBJECT);	

						dataProcessing.CreateContent_Themeobject (strCheckDate,strAPI_THEMEOBJECT);
						reCreateFirstPage=true;
					} 


					if(blnAPI_THEMEOBJECTPLUGIN==true){  
						Log.d(LOG_TAG,"Retrieve from THEMEOBJECTPLUGIN API...");
						String strURL = getResources().getString(R.string.THEMEOBJECTPLUGIN_API) + stdPara;
						Log.d(LOG_TAG,"strURL="+ strURL);

						HttpPost HTTP_POST_THEMEOBJECTPLUGIN;
						HTTP_POST_THEMEOBJECTPLUGIN = new HttpPost(strURL);

						//Execute HTTP Post Request
						ResponseHandler<String> responseHandler_THEMEOBJECTPLUGIN=new BasicResponseHandler();
						strAPI_THEMEOBJECTPLUGIN=httpclient.execute(HTTP_POST_THEMEOBJECTPLUGIN, responseHandler_THEMEOBJECTPLUGIN);	

						dataProcessing.CreateContent_ThemeObjectPlugin (strCheckDate,strAPI_THEMEOBJECTPLUGIN);
						reCreateFirstPage=true;
					} 


					if(blnAPI_CONTENT==true){  
						Log.d(LOG_TAG,"Retrieve from CONTENT API...");
						String strURL = getResources().getString(R.string.CONTENT_API) + stdPara;
						Log.d(LOG_TAG,"strURL="+ strURL);

						HttpPost HTTP_POST_CONTENT;
						HTTP_POST_CONTENT = new HttpPost(strURL);

						ResponseHandler<String> responseHandler_CONTENT=new BasicResponseHandler();
						strAPI_CONTENT=httpclient.execute(HTTP_POST_CONTENT, responseHandler_CONTENT);	

						dataProcessing.CreateContent_Content (strCheckDate,strAPI_CONTENT);

					} 


					if(blnAPI_CONTENTCATEGORY==true){  
						Log.d(LOG_TAG,"Retrieve from CONTENTCATEGORY API...");
						String strURL = getResources().getString(R.string.CONTENTCAT_API) + stdPara;
						Log.d(LOG_TAG,"strURL="+ strURL);

						HttpPost HTTP_POST_CONTENTCATEGORY;
						HTTP_POST_CONTENTCATEGORY = new HttpPost(strURL);

						ResponseHandler<String> responseHandler_CONTENTCAT=new BasicResponseHandler();
						strAPI_CONTENTCATEGORY=httpclient.execute(HTTP_POST_CONTENTCATEGORY, responseHandler_CONTENTCAT);	

						dataProcessing.CreateContent_Contentcategory (strCheckDate,strAPI_CONTENTCATEGORY);
						reCreateFirstPage=true;

					} 

					if(blnAPI_LOYALTY==true){  
						Log.d(LOG_TAG,"Retrieve from LOYALTY API...");
						String strURL = getResources().getString(R.string.LOYALTY_API) + stdPara;
						Log.d(LOG_TAG,"strURL="+ strURL);

						HttpPost HTTP_POST_LOYALTY;
						HTTP_POST_LOYALTY = new HttpPost(strURL);

						ResponseHandler<String> responseHandler_LOYALTY=new BasicResponseHandler();
						strAPI_LOYALTY=httpclient.execute(HTTP_POST_LOYALTY, responseHandler_LOYALTY);	

						dataProcessing.CreateContent_Loyalty (strCheckDate,strAPI_LOYALTY);
					} 

					if(blnAPI_IBEACONDEVICE==true){  
						Log.d(LOG_TAG,"Retrieve from IBEACONDEVICE API...");
						String strURL = getResources().getString(R.string.IBEACONDEVICE_API) + stdPara;
						Log.d(LOG_TAG,"strURL="+ strURL);

						HttpPost HTTP_POST_IBEACONDEVICE;
						HTTP_POST_IBEACONDEVICE = new HttpPost(strURL);

						ResponseHandler<String> responseHandler_IBEACONDEVICE=new BasicResponseHandler();
						strAPI_IBEACONDEVICE=httpclient.execute(HTTP_POST_IBEACONDEVICE, responseHandler_IBEACONDEVICE);	

						dataProcessing.CreateContent_IbeaconDevice (strCheckDate,strAPI_IBEACONDEVICE);
					} 


					if(blnAPI_IBEACONCONTENT==true){  
						Log.d(LOG_TAG,"Retrieve from IBEACONCONTENT API...");
						String strURL = getResources().getString(R.string.IBEACONCONTENT_API) + stdPara;
						Log.d(LOG_TAG,"strURL="+ strURL);

						HttpPost HTTP_POST_IBEACONCONTENT;
						HTTP_POST_IBEACONCONTENT = new HttpPost(strURL);

						ResponseHandler<String> responseHandler_IBEACONCONTENT=new BasicResponseHandler();
						strAPI_IBEACONCONTENT=httpclient.execute(HTTP_POST_IBEACONCONTENT, responseHandler_IBEACONCONTENT);	

						dataProcessing.CreateContent_IbeaconContent (strCheckDate,strAPI_IBEACONCONTENT);
					} 

					if(blnAPI_IBEACONXREF==true){  
						Log.d(LOG_TAG,"Retrieve from IBEACONXREF API...");
						String strURL = getResources().getString(R.string.IBEACONXREF_API) + stdPara;
						Log.d(LOG_TAG,"strURL="+ strURL);

						HttpPost HTTP_POST_IBEACONXREF;
						HTTP_POST_IBEACONXREF = new HttpPost(strURL);

						ResponseHandler<String> responseHandler_IBEACONXREF=new BasicResponseHandler();
						strAPI_IBEACONXREF=httpclient.execute(HTTP_POST_IBEACONXREF, responseHandler_IBEACONXREF);	

						dataProcessing.CreateContent_IbeaconXRef (strCheckDate,strAPI_IBEACONXREF);

					} 

					if (app_points.equalsIgnoreCase("YES")) {  
						Log.d(LOG_TAG,"Retrieve from POINTS API...");
						String strURL = getResources().getString(R.string.POINT_SETTING_API) + stdPara;
						Log.d(LOG_TAG,"strURL="+ strURL);

						HttpPost HTTP_POST_POINTS;
						HTTP_POST_POINTS = new HttpPost(strURL);

						ResponseHandler<String> responseHandler_POINTS=new BasicResponseHandler();
						String strAPI_POINTS=httpclient.execute(HTTP_POST_POINTS, responseHandler_POINTS);	

						dataProcessing.CreateContent_PointsSettings (strCheckDate,strAPI_POINTS);
					} 
				}

				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetAPIData-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				Log.d(LOG_TAG,"GetAPIData end...");
				if (result.equals("SUCCESS")) {
					firsttimer = false;
					isLatestData = true;
					reCreateFirstPage = true;
					if (reCreateFirstPage) {
						Log.d(LOG_TAG,"reCreateFirstPage TRUE");

						CreateFirstPage Task = new CreateFirstPage();
						Task.execute(new String[] { });

					} else if (blnAPI_IBEACONDEVICE) {
						BeaconStart();
					}
				}
			}

			catch (Throwable t) {
				Log.e(LOG_TAG, "GetAPIData-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}   


	private class GetAPIOFFLINEData extends AsyncTask<String, Void, String> {
		String strCheckDate = "0";

		@Override
		protected void onPreExecute() {
			Log.d(LOG_TAG,"GetAPIOFFLINEData start...");
			super.onPreExecute();
			//progressDialog = ProgressDialog.show(getActivity(), "", getResources().getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;
			try{
				response=dataProcessing.GetAPIOfflineData(strCheckDate);

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetAPIOFFLINEData-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				Log.d(LOG_TAG,"GetAPIOFFLINEData end...");
				if (result.equals("SUCCESS")) {
					SplashProcess();
				}
			}

			catch (Throwable t) {
				Log.e(LOG_TAG, "GetAPIOFFLINEData-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}   


	private void BeaconAsk() {

		if (app_ibeacon.equalsIgnoreCase("YES")) {
			Log.d("Beacon", "Beacon Ask"); 
			z = Helper_Ibeacondevice.getAll();
	
			if(z.getCount()>0){
				// Check if device supports Bluetooth Low Energy.
				if (!beaconManager.hasBluetooth()) {
					Log.d("Beacon", "Device does not have Bluetooth Low Energy"); 
					Toast.makeText(this, "Device does not have Bluetooth Low Energy", Toast.LENGTH_LONG).show();
					_active = false;
					return;
				}
	
				// If Bluetooth is not enabled, let user enable it.
				if (beaconManager.isBluetoothEnabled()) {
					Log.d("Beacon", "Bluetooth enabled"); 
					beaconIsGood = true;
					_active = false;
					return;
				} else {
					Log.d("Beacon", "Wait for response"); 
					//Toast.makeText(this, "Bluetooth not enabled", Toast.LENGTH_LONG).show();
					Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
					enableBtIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300); //ADDON
					startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
				}
	
			} else {
				Log.d("Beacon", "iBeacon Device List DB is Empty"); 
				_active = false;
			}
			
		} else {
			Log.d("Beacon", "iBeacon features not enabled."); 
			_active = false;
		}
	}

	private void BeaconStart() {

		try{
			if (beaconIsGood) {
				Log.d("Beacon", "Beacon Start"); 
				z = Helper_Ibeacondevice.getAll();

				if(z.getCount()>0){

					// Check if device supports Bluetooth Low Energy.
					if (beaconManager.isBluetoothEnabled()) {
						Getbeaconid Task2 = new Getbeaconid();
						Task2.execute();
					}

				}
			}
		} catch (Throwable t) { 
			Log.e("Beacon", "BeaconStart-Error:" + t.getMessage(), t); 
		}
	}


	public class Getbeaconid extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			Log.d("Beacon", "doInBackground"); 
			String response = null;
			try{

				z = Helper_Ibeacondevice.getAll();
				Log.d(LOG_TAG, "z=" + z.getCount());

				for(z.moveToFirst(); !z.isAfterLast(); z.moveToNext()){
					current_ibeacondevice=new Collection_Ibeacondevice();

					current_ibeacondevice.setid(Helper_Ibeacondevice.getbeacondevice_id(z).toString());
					current_ibeacondevice.setuuid(Helper_Ibeacondevice.getuuid(z).toString());
					current_ibeacondevice.setmajor(Helper_Ibeacondevice.getmajor(z).toString());
					current_ibeacondevice.setminor(Helper_Ibeacondevice.getminor(z).toString());
					current_ibeacondevice.setname(Helper_Ibeacondevice.getname(z).toString());
					current_ibeacondevice.seterror_msg(Helper_Ibeacondevice.geterror_msg(z).toString());
					current_ibeacondevice.setstatus(Helper_Ibeacondevice.getstatus(z).toString());

					model_ibeacondevice.add(current_ibeacondevice);
				}
				z.close();


				Log.d("Beacon", "Connect to Estimote Beacon Service"); 
				connectToService();

				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e("Beacon", "Getbeaconid-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

			}
			catch (Throwable t) {
				Log.e("Beacon", "Getbeaconid-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}


	@Override
	protected void onStart() {
		super.onStart();
		Log.e(LOG_TAG, "onStart"); 

	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_ENABLE_BT) {
			if (resultCode == Activity.RESULT_OK) {
				beaconIsGood = true;
			} else {
				Toast.makeText(this, "Bluetooth not enabled", Toast.LENGTH_LONG).show();
			}
			Log.d("Beacon", "User response to REQUEST_ENABLE_BT"); 
			_active = false;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}



	private void connectToService() {
		beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
			@Override
			public void onServiceReady() {
				try {

					beaconManager.setRangingListener(new BeaconManager.RangingListener() {
						@Override
						public void onBeaconsDiscovered(Region region, final List<Beacon> beacons) {

							listOfBeacons = filterBeacons(beacons);
							//Log.d("Beacon", "RangingListener");
							//Log.d("Beacon", "listOfBeacons.size = "+listOfBeacons.size());

							if(listOfBeacons.size() > 0){
								BeaconProcesses();
							}

						}
					});

					beaconManager.setMonitoringListener(new MonitoringListener() {
						@SuppressWarnings("unused")
						public void onEnteredRegion(Region region) {
							Log.d("Beacon", "MonitoringListener");
							Log.d("Beacon", "listOfBeacons.size = "+listOfBeacons.size());

							if(listOfBeacons.size() > 0){
								BeaconProcesses();
							}

						}

						@Override
						public void onExitedRegion(Region region) {

						}

						@Override
						public void onEnteredRegion(Region arg0,
								List<Beacon> arg1) {
							
						}
					});

					beaconManager.startMonitoring(ALL_ESTIMOTE_BEACONS_REGION);  
					beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);


				} catch (RemoteException e) {
					Toast.makeText(Splash.this, "Cannot start ranging, something terrible happened",
							Toast.LENGTH_LONG).show();
					Log.e(LOG_TAG, "Cannot start ranging", e);
				}
			}
		});

	}


	private List<Beacon> filterBeacons(List<Beacon> beacons) {
		List<Beacon> filteredBeacons = new ArrayList<Beacon>(beacons.size());
		for (Beacon beacon : beacons) {
			//Log.d("Beacon", "UUID: "+beacon.getProximityUUID());

			for(int l = 0;l<model_ibeacondevice.toArray().length;l++){	
				if (beacon.getProximityUUID().equalsIgnoreCase(model_ibeacondevice.get(l).getuuid())) {
					filteredBeacons.add(beacon);
				}
			}
		}
		return filteredBeacons;
	}

	private void BeaconProcesses() {

		String default_viewdate = "00-00-0000 00:00";
		String viewdate = default_viewdate;
		Integer counter=0;
		Integer limitperday=1;
		Boolean goodtogo=false;
		String ibPageID="";
		String strrange="";

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy kk:mm");
		String todayDate = df.format(cal.getTime());

		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();

		if (scanDate.equals(todayDate)) {
			//Log.d("Beacon", "wait.. " + todayDate);			
		} else {
			Log.d("Beacon", "Now: " + todayDate);	

			for(int ib=0; ib<listOfBeacons.size(); ib++){
				Beacon currentBeacon = listOfBeacons.get(ib);
				String ib_uuid = currentBeacon.getProximityUUID();
				String  major = Integer.toString(currentBeacon.getMajor());
				String  minor = Integer.toString(currentBeacon.getMinor());

				Log.i("Beacon", "[" + ib + "] " + ib_uuid);
				Log.i("Beacon", "[" + ib + "] " + major+"/"+minor);

				String beaconrange = String.valueOf(Utils.proximityFromAccuracy(Utils.computeAccuracy(currentBeacon)));
				Log.d("Beacon", "RANGE - DEVICE : " + beaconrange);	
				
				q = Helper_Ibeacondevice.getByDetails(ib_uuid.toUpperCase() , major, minor);

				if (q.moveToLast() != false){
					deviceID = Helper_Ibeacondevice.getbeacondevice_id(q).toString();

					String ibErrMsg = "";
					settings = getSharedPreferences(PREFS_NAME, 0);
					ibErrMsg  = settings.getString("ibErrMsg", "");

					if (ibErrMsg.length()<5) {
						editor = settings.edit();		
						ibErrMsg = Helper_Ibeacondevice.geterror_msg(q).toString();
						editor.remove("ibErrMsg");
						editor.putString("ibErrMsg", ibErrMsg);
						editor.commit();
					}

					j = Helper_Ibeaconxref.getBydevideID(deviceID);

					for(j.moveToFirst(); !j.isAfterLast(); j.moveToNext()) {
						contentID = Helper_Ibeaconxref.getcontent_id(j).toString();

						Log.d("Beacon", "   contentID = "+contentID);

						if(!contentID.equals("")){

							h = Helper_Ibeaconcontent.getfromContentIDThisMoment(contentID);

							if (h.moveToLast() != false){
								goodtogo = true;
								wel_msg = Helper_Ibeaconcontent.getwelcome_msg(h).toString();
								contentID = Helper_Ibeaconcontent.getbeaconcontent_id(h).toString();
								ibPageID = Helper_Ibeaconcontent.getpage_id(h).toString();
								strrange = Helper_Ibeaconcontent.getstatus(h).toString();
								limitperday = Integer.parseInt(Helper_Ibeaconcontent.getlimitperday(h).toString());
								Log.d("Beacon", "   RANGE - CONTENT: " + strrange);	

								if (strrange.equalsIgnoreCase("NEAR")) {
									if (!beaconrange.equalsIgnoreCase("NEAR") && !beaconrange.equalsIgnoreCase("IMMEDIATE")) {
										Log.d("Beacon", "   This content only for NEAR");
										goodtogo = false;
									}
								}

								else if (strrange.equalsIgnoreCase("FAR")) {
									if (!beaconrange.equalsIgnoreCase("FAR") && !beaconrange.equalsIgnoreCase("UNKNOWN")) {
										Log.d("Beacon", "   This content only for FAR");
										goodtogo = false;
									}
								}							

							} else {
								Log.d("Beacon", "   OUTSIDE DATE RANGE");	
								goodtogo = false;
							}

							h.close();

							if (goodtogo) {
								goodtogo = false; //FALSE UNTIL PROVEN TRUE

								p = Helper_PreviousdeviceID.getbyDeviceID(contentID);

								if (p.moveToLast() != false){
									viewdate = Helper_PreviousdeviceID.getviewdate(p).toString();
									counter = Integer.parseInt(Helper_PreviousdeviceID.getcounter(p).toString());
								} else {
									viewdate = default_viewdate;
								}
								p.close();

								Log.d("Beacon", "   counter = "+counter+"/" + limitperday);

								String subdate = todayDate.substring(0,10);
								String subviewdate = viewdate.substring(0,10);

								String subtime = todayDate.substring(0,15);
								String subtimeview = viewdate.substring(0,15);

								if(subtimeview.equals(subtime)){
									goodtogo = false;
									Log.d("Beacon", "   wait: "+subtime);

								} else if(viewdate.equals(default_viewdate)){
									Helper_PreviousdeviceID.Insert(contentID,todayDate,"1");
									goodtogo = true;

								} else if(subviewdate.equals(subdate)){	

									if(counter<limitperday){
										Integer newcounter = counter+1;
										Helper_PreviousdeviceID.Update(contentID, todayDate, newcounter.toString());
										goodtogo = true;														
									} else {
										goodtogo = false;
										Log.d("Beacon", "   Above limit");
									}

								} else {
									Helper_PreviousdeviceID.Update(contentID, todayDate, "1");
									goodtogo = true;
								}

							}

							if(goodtogo){
								settings = getSharedPreferences(PREFS_NAME, 0);
								editor = settings.edit();
								editor.remove("ibPageID");
								editor.remove("ibContentID");
								editor.putString("ibPageID", ibPageID);
								editor.putString("ibContentID", contentID);
								editor.commit();

								ib=listOfBeacons.size();

								Log.d("Beacon", "   Send notifi msg Beacon discover beacon");
								Log.d("Beacon", "   wel_msg = " + wel_msg);

								postNotification(wel_msg);

								Intent intent = new Intent(getApplicationContext(), BeaconView.class);
								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

								startActivity(intent);
							}

						}						
					}
					
					j.close();
				} else {
					Log.d("Beacon", "   NO DEVICE DATA FOUND");	
					goodtogo = false;
				}
				
				q.close();

			}
		}
		scanDate = todayDate;
	}



	@SuppressLint("NewApi")
	private void PostNewMessageAvailable(String msg) {
		Intent notifyIntent = null;

		notifyIntent = new Intent(Splash.this, Splash.class);

		notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		notifyIntent.putExtra("action_type", "inbox");

		PendingIntent pendingIntent = PendingIntent.getActivities(
				Splash.this,
				0,
				new Intent[]{notifyIntent},
				PendingIntent.FLAG_UPDATE_CURRENT);

		Notification notification = new Notification.Builder(Splash.this)
		.setSmallIcon(R.drawable.icon90)
		.setContentTitle("Hi "+ customerName)
		.setContentText(msg)
		.setAutoCancel(true)
		.setContentIntent(pendingIntent)
		.build();
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.defaults |= Notification.DEFAULT_LIGHTS;
		notificationManager.notify(NOTIFICATION_ID, notification);
	}


	@SuppressLint("NewApi")
	private void postNotification(String msg) {
		String ib_pid="",ib_cid="";
		Intent notifyIntent = null;

		settings = this.getSharedPreferences(PREFS_NAME, 0);
		ib_pid = settings.getString("ibPageID", "");
		ib_cid = settings.getString("ibContentID", "");

		if (ib_pid==null || ib_pid.length()==0) {
			ib_pid = settings.getString("home_pageid", "");
		}

		if (ib_pid.length()>0) {
			notifyIntent = new Intent(Splash.this, Splash.class);

			notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			notifyIntent.putExtra("ib_pid", ib_pid);
			notifyIntent.putExtra("ib_cid", ib_cid);

			PendingIntent pendingIntent = PendingIntent.getActivities(
					Splash.this,
					0,
					new Intent[]{notifyIntent},
					PendingIntent.FLAG_UPDATE_CURRENT);

			Notification notification = new Notification.Builder(Splash.this)
			.setSmallIcon(R.drawable.icon90)
			.setContentTitle("Welcome "+ customerName)
			.setContentText(msg)
			.setAutoCancel(true)
			.setContentIntent(pendingIntent)
			.build();
			notification.defaults |= Notification.DEFAULT_SOUND;
			notification.defaults |= Notification.DEFAULT_LIGHTS;
			notificationManager.notify(NOTIFICATION_ID, notification);
		}

	}

	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			Log.d(LOG_TAG, "User touch down."); 
			_active = false;
		}
		return true;
	}

	public void onBackPressed() {

		super.onBackPressed(); // allows standard use of backbutton for page 1
		this.finish();
		overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
	}



	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
			int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}


	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();


		beaconManager.disconnect();
		Helper_Themepage.close();
		Helper_Appsetup.close();
		Helper_Themecolor.close();
		Helper_Themeobject.close();
		Helper_Themeobjectplugin.close();
		Helper_Content.close();
		Helper_Contentcategory.close();
		Helper_Ibeaconcontent.close();
		Helper_Ibeacondevice.close();
		Helper_Ibeaconxref.close();
		Helper_Loyalty.close();
		
		slotAction.close();
		dataProcessing.close();
		notificationManager.cancel(NOTIFICATION_ID);
	}


}
