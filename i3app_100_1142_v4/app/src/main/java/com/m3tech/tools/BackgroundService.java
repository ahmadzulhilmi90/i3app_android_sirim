package com.m3tech.tools;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.appv3.Splash;
import com.m3tech.data.Helper_Notification;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.util.Log;

public class BackgroundService extends Service{

	private static final String LOG_TAG = "BACKGROUND SERVICE";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	public static final String COMMON_FILE = "MyCommonFile"; 	

	int handledelay = 60; //in seconds
	String udid, token_id, app_db, app_user, app_name, app_version;
	SharedPreferences settings;

	SlotAction slotAction = null;
	Helper_Notification Helper_Notification =null;
	int NOTIFICATION_ID = 123;
	private NotificationManager notificationManager;

	@Override
	public void onCreate() {
		Log.d(LOG_TAG,"OnCreate");
		super.onCreate();

		slotAction = new SlotAction(this);

		app_db = getResources().getString(R.string.app_db);
		app_user = getResources().getString(R.string.app_user);
		
		NOTIFICATION_ID = Integer.parseInt(app_user);

		app_name = getResources().getString(R.string.app_name);
		app_version = getResources().getString(R.string.app_version);

		try {
			app_version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (Throwable t) {
			Log.e(LOG_TAG, "app_version-Error:" + t.getMessage(), t); 
		}
		
		udid = Secure.getString(this.getContentResolver(),Secure.ANDROID_ID);
		token_id = udid + "@" + app_user + "." + app_db + ".sma";

		Helper_Notification = new Helper_Notification(this);
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

	}

	@Override
	public IBinder onBind(Intent arg0) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Let it continue running until it is stopped.
		Log.d(LOG_TAG, "Service started");
		StartTheService(); 

		settings = this.getSharedPreferences(PREFS_NAME, 0);

		String pn_status = getResources().getString(R.string.push_notification);
		
		//TODO Check User Settings as well
		if (pn_status.equalsIgnoreCase("YES")) {
			GetPushNotification PNTask = new GetPushNotification();
			PNTask.execute(new String[] { });

		}
		
		return START_STICKY;

	}


	private class GetPushNotification extends AsyncTask<String, Void, String> {
		String customerID = "";
		String tokenRegistered = "";
		String tokenCustomer = "";
		String appVersionRegistered = "";
		
		@Override
		protected void onPreExecute() {
			Log.d(LOG_TAG, "PushNotification Running every " + handledelay + " second..");			
			customerID = settings.getString("customerID", "");
			tokenRegistered = settings.getString("tokenRegistered", "");
			tokenCustomer = settings.getString("tokenCustomer", "");
			appVersionRegistered = settings.getString("appVersion", "");
			
			Log.d(LOG_TAG, "udid = " + udid);
			Log.d(LOG_TAG, "token_id = " + token_id);
			Log.d(LOG_TAG, "customerID = " + customerID);
			Log.d(LOG_TAG, "tokenCustomer = " + tokenCustomer);
			Log.d(LOG_TAG, "tokenRegistered = " + tokenRegistered);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;
			try{
				boolean network_status = isNetworkAvailable();

				if(network_status == true){

					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

					String stdPara = "db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
							+ "&cs="+URLEncoder.encode(slotAction.getCheckSum(), "UTF-8");

					//Check if got any new message

					HttpPost HTTP_POST_TOP;
					String strURL;

					if(tokenRegistered.equals("null") || tokenRegistered.isEmpty() 
							|| tokenRegistered.equals("") || !app_version.equals(appVersionRegistered)
							|| !customerID.equals(tokenCustomer)) {
						
						//REGISTER TOKEN FIRST
						strURL = getResources().getString(R.string.PN_REGISTER_API) + stdPara
								+ "&token_id=" + URLEncoder.encode(token_id, "UTF-8")
								+ "&appname=" + URLEncoder.encode(app_name, "UTF-8")
								+ "&appversion=" + URLEncoder.encode(app_version, "UTF-8");

						HTTP_POST_TOP = new HttpPost(strURL);
						Log.d(LOG_TAG,"strURL="+ strURL);
						//Execute HTTP Post Request
						ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
						String responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	

						JSONArray msgJSONArray = new JSONArray(responseBody);

						if (msgJSONArray.length()>0){
							int i = msgJSONArray.length()-1;
							String status = msgJSONArray.getJSONObject(i).getString("status").toString();

							if (status.equals("1") || status.equals("2")) {
								SharedPreferences.Editor editor = settings.edit();
								editor.remove("tokenRegistered");
								editor.remove("appVersion");
								editor.remove("tokenCustomer");
								editor.putString("tokenRegistered", token_id);
								editor.putString("appVersion", app_version);
								editor.putString("tokenCustomer", customerID);
								editor.commit();
							}
						}
						
						response="REGISTER";
						
					} else {
						//GET ALL PUSH NOTIFICATION
						//http://203.175.162.60/api/pn_message.php?db=200&userid=116&udid=&cuid=1&token_id=abc
						strURL = getResources().getString(R.string.PN_MESSAGE_API) + stdPara
								+ "&token_id=" + URLEncoder.encode(token_id, "UTF-8");

						HTTP_POST_TOP = new HttpPost(strURL);
						Log.d(LOG_TAG,"strURL="+ strURL);
						//Execute HTTP Post Request
						ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
						String responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	
						
						response=responseBody;
					}

					
				} else {
					response="FAILED";					
				}

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetPushNotification-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				Log.d(LOG_TAG,"GetPushNotification: "+result);
								
				if (result.equals("REGISTER")) {
					//Nothing to do at this moment - might be added later
				} else if (result.equals("FAILED")) {
					//Nothing to do at this moment - might be added later
				} else {

					Date d = new Date();
					String currentdate = (new SimpleDateFormat("yyyy-MM-dd kk:mm")).format(d);
					
					JSONArray msgJSONArray = new JSONArray(result);

					if (msgJSONArray.length()>0) {

						for (int n = 0; n < msgJSONArray.length(); n++) {
							String notification_id= msgJSONArray.getJSONObject(n).getString("id").toString();	
							String user_id=msgJSONArray.getJSONObject(n).getString("user_id").toString();
							String push_id=msgJSONArray.getJSONObject(n).getString("push_id").toString();
							String token_id=msgJSONArray.getJSONObject(n).getString("token_id").toString();
							String customer_id = msgJSONArray.getJSONObject(n).getString("customer_id").toString();
							String process_date=msgJSONArray.getJSONObject(n).getString("process_date").toString();
							String push_msg=msgJSONArray.getJSONObject(n).getString("push_msg").toString();
							String action_type=msgJSONArray.getJSONObject(n).getString("action_type").toString();
							String action_value=msgJSONArray.getJSONObject(n).getString("action_value").toString();
							String content_id=msgJSONArray.getJSONObject(n).getString("content_id").toString();
							
							String lastupdate = currentdate;
							Log.d(LOG_TAG,"Insert into DB : " + n); 
							Helper_Notification.insert(notification_id, user_id, push_id, token_id, 
									customer_id, process_date, push_msg, lastupdate, action_type, action_value, content_id);
							
							PostNotification (push_msg, action_type, action_value, content_id);

						}		
					}
				}
			}

			catch (Throwable t) {
				Log.e(LOG_TAG, "GetPushNotification-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}   

	@SuppressLint("NewApi")
	private void PostNotification(String msg, String action_type, String action_value, String content_id) {
				
		Intent notifyIntent = null;

		notifyIntent = new Intent(this, Splash.class);

		notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		notifyIntent.putExtra("action_type", action_type);
		notifyIntent.putExtra("action_value", action_value);
		notifyIntent.putExtra("content_id", content_id);

		PendingIntent pendingIntent = PendingIntent.getActivities(
				this,
				0,
				new Intent[]{notifyIntent},
				PendingIntent.FLAG_UPDATE_CURRENT);
				
		//TODO ADD ACTION TYPE
		Notification notification = new Notification.Builder(this)
										.setSmallIcon(R.drawable.icon90)
										.setContentTitle(app_name)
										.setContentText(msg)
										.setAutoCancel(true)
										.setContentIntent(pendingIntent)
										.setStyle(new Notification.BigTextStyle()
								         	.bigText(msg))
										.build();
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.defaults |= Notification.DEFAULT_LIGHTS;
		notificationManager.notify(NOTIFICATION_ID, notification);
		
	}

	private void StartTheService() {	
		Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
		restartServiceIntent.setPackage(getPackageName());

		PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
		AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
		alarmService.set(
				AlarmManager.ELAPSED_REALTIME,
				SystemClock.elapsedRealtime() + (handledelay*1000),
				restartServicePendingIntent);
	
	}

	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(LOG_TAG, "Service destroyed");
		notificationManager.cancel(NOTIFICATION_ID);
	}

	@Override
	public void onTaskRemoved(Intent rootIntent){
		StartTheService();
		super.onTaskRemoved(rootIntent);
	}

}