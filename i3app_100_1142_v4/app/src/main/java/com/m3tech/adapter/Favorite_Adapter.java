package com.m3tech.adapter;


import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.m3tech.app_100_1474.R;
import com.m3tech.collection.Collection_Favorite;
import com.m3tech.data.Helper_Content;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

public class Favorite_Adapter extends ArrayAdapter<Collection_Favorite> {

	private static final String LOG_TAG = "FAVORITE ADAPTER";
	protected Helper_Content Helper_Content = null;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	protected DisplayImageOptions displayOptions = getDisplayImageOptions();
	
    public Favorite_Adapter(Context context, int resource, List<Collection_Favorite> items) {
        super(context, resource, items);
        Helper_Content = new Helper_Content(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.plugin_favorite_list_row, null);
            
        }
        
        Collection_Favorite f = getItem(position);
        
		if (f != null) {
			
			TextView text=(TextView) v.findViewById(R.id.text);
			TextView price=(TextView) v.findViewById(R.id.price);
			TextView var1=(TextView) v.findViewById(R.id.var1);
			ImageView image = (ImageView) v.findViewById(R.id.image_url);
			final ProgressBar spinner = (ProgressBar) v.findViewById(R.id.progress);
			price.setVisibility(View.GONE);
			
			Cursor c = Helper_Content.getByContID(f.getcontent_id());
			if(c.getCount() > 0){
				
				if(c.moveToLast() != false){
					do{
						
						String name = Helper_Content.gettitle(c).toString();
						String image_url = Helper_Content.getimage(c).toString();
						String desc = Helper_Content.getdetails(c).toString();
						Log.d(LOG_TAG, name);
						
						/*** Setting Row Data ***/
						
						text.setText(name);
						var1.setText(desc);
						
						imageLoader.displayImage(image_url, image, displayOptions, new ImageLoadingListener() {
							@Override
							public void onLoadingStarted(String imageUri, View view) {					
								spinner.setVisibility(View.VISIBLE);
							}
							@Override
							public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
								spinner.setVisibility(View.GONE);
							}
							@Override
							public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
								spinner.setVisibility(View.GONE);
							}
							@Override
							public void onLoadingCancelled(String imageUri, View view) {
								spinner.setVisibility(View.GONE);
							}

						});
						
					}while(c.moveToNext());
				}
				
			}else{
				
			}
			c.close();
			Helper_Content.close();
        }

        return v;
    }

	@SuppressWarnings("deprecation")
	public static DisplayImageOptions getDisplayImageOptions() {
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}

}
