package com.m3tech.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.m3tech.app_100_1474.R;
import com.m3tech.collection.Collection_themeobject;
import com.m3tech.data.Helper_Content;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

public class SlideMenu_Adapter extends ArrayAdapter<Collection_themeobject> {

	private static final String LOG_TAG = "SLIDE MENU ADAPTER";
	protected Helper_Content Helper_Content = null;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	protected DisplayImageOptions displayOptions = getDisplayImageOptions();
	protected Context mycontext;
	
    public SlideMenu_Adapter(Context context, int resource, List<Collection_themeobject> items) {
        super(context, resource, items);
        mycontext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.plugin_content_slidemenu_row, null);
            
        }
        
        Collection_themeobject r = getItem(position);
        
		if (r != null) {
			
			TextView text=(TextView) v.findViewById(R.id.text);
			RelativeLayout layout_text=(RelativeLayout) v.findViewById(R.id.layout_text);
			RelativeLayout layout_image = (RelativeLayout)v.findViewById(R.id.layout_image);
			ImageView imageview = (ImageView) v.findViewById(R.id.image_url);
			LinearLayout linear_row = (LinearLayout)v.findViewById(R.id.linear_row);
			final ProgressBar spinner = (ProgressBar) v.findViewById(R.id.progress);
			
			
			String object_type = r.getobject_type().toString();
			String title = r.getobject_value().toString();
			String attribute = r.getattribute().toString();
			String bgcolor = null,textcolor = null,textsize = null,textalign = null,image_url = null;
			
			if(object_type.equals("textImage")){
				
				/*** Setting attribute ***/
				if(attribute.length()>0){
					String[] parts = attribute.split("\\|",-1);
					
					bgcolor = parts[0];
					textcolor = parts[1];
					textsize = parts[2];
					textalign = parts[3];
					image_url = parts[4];
					
					
					if(image_url.equals("") || image_url.length() < 1 || image_url.equals(null)){
						image_url = "null";
					}
					
					if(bgcolor.equals("") || bgcolor.length() < 1 || bgcolor.equals(null)){
						bgcolor = "000000";
					}
					
					Log.d(LOG_TAG,"Attribute : bgcolor:"+bgcolor+" textcolor:"+textcolor+" textsize:"+textsize+" textalign:"+textalign+" imageurl:"+image_url);
				}
				
				/*** Settings TextView ***/
				
				if(title.length()>0){
					
					if(textcolor.length()>0){
						textcolor = "#"+textcolor;
						text.setTextColor(Color.parseColor(textcolor));
					}
					if(textsize.length()>0){
						int fontsize_i = Integer.parseInt(textsize)+4;
						text.setTextSize((float)fontsize_i);
					}
					if(textalign.length()>0){
						if(textalign.equals("center")){
							text.setGravity(Gravity.CENTER);
						}else if(textalign.equals("left")){
							text.setGravity(Gravity.LEFT | Gravity.CENTER);
						}else if(textalign.equals("right")){
							text.setGravity(Gravity.RIGHT | Gravity.CENTER);
						}else{
							text.setGravity(Gravity.CENTER);
						}
					}
					if(bgcolor.length()>0){
						bgcolor = "#"+bgcolor;
						linear_row.setBackgroundColor(Color.parseColor(bgcolor));
					}
					text.setText(title);
					imageview.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
					
				}else{
					layout_text.setVisibility(View.GONE);
					imageview.setScaleType(ImageView.ScaleType.FIT_XY);
				}
				/*** End Settings TextView ***/
				
				if(image_url.length()>0 && !image_url.equals("null")){
					
					imageLoader.displayImage(image_url, imageview, displayOptions, new ImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {					
							spinner.setVisibility(View.VISIBLE);
						}
						@Override
						public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
							spinner.setVisibility(View.GONE);
						}
						@Override
						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
							spinner.setVisibility(View.GONE);
						}
						@Override
						public void onLoadingCancelled(String imageUri, View view) {
							spinner.setVisibility(View.GONE);
						}

					});
				}else{
					layout_image.setVisibility(View.GONE);
				}
				
			}else{
				linear_row.setVisibility(View.GONE);
			}
        }

        return v;
    }

	@SuppressWarnings("deprecation")
	public static DisplayImageOptions getDisplayImageOptions() {
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}
	
	public static float pxFromDp(String dp, Context mContext) {
        return Float.parseFloat(dp) * mContext.getResources().getDisplayMetrics().density;
    }

}
