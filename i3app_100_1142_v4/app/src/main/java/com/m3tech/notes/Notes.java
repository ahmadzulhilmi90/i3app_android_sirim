package com.m3tech.notes;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.widget.Notes_Widget;

public class Notes extends Notes_Widget{

	private static final String LOG_TAG = "NOTES";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(LOG_TAG,"Class "+LOG_TAG);
	
		//1. Get Data
		GetNotesList run = new GetNotesList();
		run.execute();
		
		btn_save_note.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(onError()){
					Toast.makeText(context, "Please enter Note.", Toast.LENGTH_SHORT).show();
				}else{
					Helper_Notes.Delete();
					//1. Save Note to Helper_Notes
					String note = edit_note.getText().toString();
					Helper_Notes.insert(note, cuid);
					Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
					finish();
				}
			}
		});
		
	}
	
	public boolean onError(){
		int len = edit_note.getText().length();
		if(len > 0){
			return false;
		}return true;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Log.d(LOG_TAG, "onResume()");
		GetNotesList run = new GetNotesList();
		run.execute();
	}
	
	/**** start function get GetNotesList list ****/
	private class GetNotesList extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			try{
				response="SUCCESS";
			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetContentList-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				
				if (result.equals("SUCCESS")){
						
					Cursor c = Helper_Notes.getAll();
					
					if(c.getCount() > 0){
						if(c.moveToLast() != false){
							do{
								String note = Helper_Notes.getnote(c).toString();
								if(note.length()>0){
									edit_note.setText(note);
								}
							}while(c.moveToNext());
						}
					}c.close();	
					
				}else{
					Toast.makeText(Notes.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}
			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "Getcontentlist-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}
	

}
