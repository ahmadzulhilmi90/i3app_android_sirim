package com.m3tech.header;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.slidemenu.SlideMenu;
import com.m3tech.theme.Theme_Std;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class AppHeader {

	private static final String LOG_TAG = "APP HEADER";
	public static final String PREFS_NAME = "MyPrefsFile";
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();
	ImageView appstitle,img_back,img_home;
	TextView texttitlebar,texthome;

	Cursor cb;
	ImageView topheader2,topheader;
	LinearLayout tabback,tabhome;
	String titlebar_img,show_title;
	String app_user,app_title, prev_pageid, home_themecode, home_pageid, colorcode, colorcode1;

	Context mycontext;
	Theme_Std myactivity;
	View layout;
	SharedPreferences settings;

	SlotAction slotAction = null;
	Helper_Themeobject Helper_Themeobject = null;

	@SuppressLint("NewApi")

	public AppHeader(Theme_Std activity,Context context, View lay, String pageTitle, String prev_page, String noback) {
		// TODO Auto-generated constructor stub
		myactivity = activity;
		mycontext = context;
		layout = lay;
		slotAction = new SlotAction(mycontext);
		Helper_Themeobject = new Helper_Themeobject(mycontext);

		Log.e(LOG_TAG,"ENTERING .. "+LOG_TAG);

		settings = context.getSharedPreferences(PREFS_NAME, 0);
		home_themecode = settings.getString("home_themecode", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		colorcode1 = settings.getString("colorcode1", "");		
		home_pageid = settings.getString("home_pageid", "");
		prev_pageid = settings.getString("current_pageid", "");
		titlebar_img = settings.getString("titlebar_img", "");
		show_title = settings.getString("show_title", "");
		app_user = mycontext.getResources().getString(R.string.app_user);
		
		Log.d(LOG_TAG, "show_title :"+ show_title);
		Log.d(LOG_TAG, "colorcode :"+ colorcode);
		Log.d(LOG_TAG, "app_title :"+ app_title);
		
		if (pageTitle.length()==0) {
			pageTitle = app_title;
		}
		
		/***** text view declaration ******/
		texttitlebar = (TextView) layout.findViewById(R.id.texttitlebar);
		texttitlebar.setText(pageTitle);
		
		

		/**** home & back button *****/
		topheader = (ImageView) layout.findViewById(R.id.tabheader_);	
		//topheader2 = (ImageView) layout.findViewById(R.id.tabheader2_);		
		tabback = (LinearLayout) layout.findViewById(R.id.tabback);
		tabhome = (LinearLayout) layout.findViewById(R.id.tabhome);
		
		img_back = (ImageView) layout.findViewById(R.id.back);
		img_home = (ImageView) layout.findViewById(R.id.home);

		/**** Setting color back and home button ***/
		if(colorcode.length()>0){
			onGetSettingColorButtonBackAndHome(colorcode);
		}
		
		Cursor c = Helper_Themeobject.getByMenuVer(app_user);
		Log.d(LOG_TAG, "c=" + c.getCount());
		c.close();
		
		if(c.getCount()>0){
			tabhome.setVisibility(View.VISIBLE);
		}else{
			
			if(prev_page.equals("0")){
				tabback.setVisibility(View.INVISIBLE);
				tabhome.setVisibility(View.INVISIBLE);

			}else{		
				
				if (noback.equals("yes")) {
					tabback.setVisibility(View.INVISIBLE);
				} else {
					//tabback.setVisibility(View.VISIBLE);
					tabhome.setVisibility(View.VISIBLE);
					
				}
				
			}
			
		}
		
		if(prev_page.equals("0")){
			tabback.setVisibility(View.INVISIBLE);

		}else{		
			
			if (noback.equals("yes")) {
				tabback.setVisibility(View.INVISIBLE);
			} else {
				tabback.setVisibility(View.VISIBLE);
			}
			
		}
		
		
		tabback.setOnClickListener(onClickbackbutton);
		//tabhome.setVisibility(View.VISIBLE);
		tabhome.setOnClickListener(onclickhomebutton);
		//topheader.setBackground(null);
		Log.d(LOG_TAG, "titlebarimg length : "+titlebar_img.length());
		//Log.d(LOG_TAG, "topheader!=null : "+topheader!=null);
		
		//Toast.makeText(context, "AppHeader : show_title = "+show_title + " | pageTitle =" + pageTitle, Toast.LENGTH_LONG).show();
		
		if (show_title.equals("0") ) {
			texttitlebar.setVisibility(View.INVISIBLE);
		}else{
			texttitlebar.setVisibility(View.VISIBLE);
		}
		
		
		if (titlebar_img.length()>10 && topheader!=null){
			
			topheader.setVisibility(View.VISIBLE);
			
			String bgUri = slotAction.getImageURI(titlebar_img);
			Log.d(LOG_TAG, "bgURI : " + bgUri);
			String draw = "drawable://";
			if (bgUri.toLowerCase().contains(draw.toLowerCase())) {
				bgUri = bgUri.replace("drawable://", "");
				Drawable d = mycontext.getResources().getDrawable(Integer.parseInt(bgUri));
				topheader.setBackground(d);
				Log.d(LOG_TAG, "1");
			} else {
				Log.d(LOG_TAG, "2");
				imageLoader.displayImage(bgUri, topheader, displayOptions, new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {		
					}
					@Override
					public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
						topheader.setBackgroundColor(Color.parseColor(colorcode));
					}
					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					}
					@Override
					public void onLoadingCancelled(String imageUri, View view) {
					}

				});
			}
		}else {
			Log.d(LOG_TAG, "3");
				
			 if (colorcode.length()>5) {
				topheader.setBackgroundColor(Color.parseColor(colorcode));
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public static DisplayImageOptions getDisplayImageOptions() {
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}
	
	public void onGetSettingColorButtonBackAndHome(String colorcode){
		String[] parts = colorcode.split("#");
		String part1 = parts[0];
		String part2 = parts[1];
		
		Log.d(LOG_TAG, "part 1 : "+ part1);
		Log.d(LOG_TAG, "part 2 : "+ part2);
		
		StringBuilder tmp = new StringBuilder();

		for (int i = 0; i < part2.length(); i++) {
		    tmp.append(part2.charAt(i));
		    if (i % 2 == 1) tmp.append(" ");
		}
		Log.d(LOG_TAG, "Text : "+ tmp.toString());
		String color_split = tmp.toString();
		String[] x = color_split.split("\\s+");
		String rr = x[0];
		String gg = x[1];
		String bb = x[2];
		
		int RR = hex2decimal(rr.toString());
		int GG = hex2decimal(gg.toString());
		int BB = hex2decimal(bb.toString());
		
		if(RR>180 && GG>180 && BB>180){
			Log.d(LOG_TAG, "ICON : BLACK");
			texttitlebar.setTextColor(Color.BLACK);
			img_back.setBackgroundResource(R.drawable.arrow_back_black);
			img_home.setBackgroundResource(R.drawable.home_black);
		}else{
			Log.d(LOG_TAG, "ICON : WHITE");
			texttitlebar.setTextColor(Color.WHITE);
			img_back.setBackgroundResource(R.drawable.arrow_back_white);
			img_home.setBackgroundResource(R.drawable.home_white);
		}
		
		Log.d(LOG_TAG, "RR : "+ String.valueOf(RR));
		Log.d(LOG_TAG, "GG : "+ String.valueOf(GG));
		Log.d(LOG_TAG, "BB : "+ String.valueOf(BB));
	}
	
	public static int hex2decimal(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16*val + d;
        }
        return val;
    }
	
	/***  home button onclick ****/
	private View.OnClickListener onclickhomebutton=new View.OnClickListener() {
		public void onClick(View v) {
			try{
				
				Log.d(LOG_TAG, "Click MENU-VER");
				Cursor c = Helper_Themeobject.getByMenuVer(app_user);
				Log.d(LOG_TAG, "c=" + c.getCount());
				c.close();
				
				if(c.getCount()>0){
					Intent to = new Intent(mycontext,SlideMenu.class);
					((Activity)mycontext).startActivity(to);
					((Activity)mycontext).overridePendingTransition(R.anim.slide_in_right,
							R.anim.slide_out_left);
				}else{
					
					Log.d(LOG_TAG,"home");
					((Activity)mycontext).finish();

					if(!prev_pageid.equals(home_pageid)){ 
						slotAction.CreateThemePage(home_themecode,home_pageid);
					}
					
				}

			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onHomeButton-Error:" + t.getMessage(), t); 

			}
		}

	};

	/***  back button onclick ****/
	private View.OnClickListener onClickbackbutton=new View.OnClickListener() {
		public void onClick(View v) {
			try{
				
				((Activity)mycontext).finish();

			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onBackButton-Error:" + t.getMessage(), t); 

			}
		}

	};

	/***  END SAME FOR EACH THEME PAGE ****************************************************/

	public void onDestroy() {
		slotAction.close();

	}
}

