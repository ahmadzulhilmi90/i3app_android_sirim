package com.m3tech.header;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.data.Helper_BookAdded;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.slidemenu.SlideMenu;
import com.m3tech.warranty.Warranty_List;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class Header{

	private static final String LOG_TAG = "HEADER";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();
	SharedPreferences settings;
	String titlebar_img,show_title,colorcode,app_user,background;
	SlotAction slotAction = null;
	Helper_Themeobject Helper_Themeobject = null;
	Helper_BookAdded Helper_BookAdded = null;
	TextView texttitlebar;
	Context context;
	ImageView img_back,img_home;
	
	public Header(final Context c,final ImageView tabheader,LinearLayout tabback,LinearLayout tabhome,ImageView back,ImageView home,TextView texttitlebar_,String pageTitle,final String value){
		
		Log.d(LOG_TAG,"ENTERING .. "+LOG_TAG);
		context = c;
		settings = c.getSharedPreferences(PREFS_NAME, 0);
		titlebar_img = settings.getString("titlebar_img", "");
		show_title = settings.getString("show_title", "");
		colorcode = settings.getString("colorcode", "");
		background = settings.getString("background", "");
		app_user = c.getResources().getString(R.string.app_user);
		
		slotAction = new SlotAction(c);
		Helper_Themeobject = new Helper_Themeobject(c);
		Helper_BookAdded = new Helper_BookAdded(c);
		
		texttitlebar = texttitlebar_;
		img_back = back;
		img_home = home;
		
		/**** Setting Variable ****/
		texttitlebar.setText(pageTitle.toString());
		tabhome.setVisibility(View.VISIBLE);
		
		/**** Setting Value ****/
		if(value.equals("yes")){
			tabback.setVisibility(View.GONE);
		}else{
			tabback.setVisibility(View.VISIBLE);
		}
		Log.d(LOG_TAG, "show_title :"+ show_title);
		
		//Toast.makeText(context, "Header : show_title = "+show_title + " | pageTitle =" + pageTitle, Toast.LENGTH_LONG).show();
		
		if (show_title.equals("0") ) {
			texttitlebar.setVisibility(View.INVISIBLE);
		}else{
			texttitlebar.setVisibility(View.VISIBLE);
		}
		
		/**** Setting color back and home button ***/
		if(colorcode.length()>0){
			onGetSettingColorButtonBackAndHome(colorcode);
		}
		
		/*
		String afterLogin="";
		afterLogin = settings.getString("afterLogin", "");
		
		if(afterLogin.equals("Theme_Std")){
			tabback.setVisibility(View.INVISIBLE);
			tabhome.setVisibility(View.INVISIBLE);
		}*/
		
		
		/**** Header Background ****/
		if (titlebar_img.length()>10){
			
			
			String bgUri = slotAction.getImageURI(titlebar_img);
			String draw = "drawable://";
			if (bgUri.toLowerCase().contains(draw.toLowerCase())) {
				bgUri = bgUri.replace("drawable://", "");
				Drawable d = c.getResources().getDrawable(Integer.parseInt(bgUri));
				tabheader.setBackground(d);
			} else {
				
				imageLoader.displayImage(bgUri, tabheader, displayOptions, new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {		
					}
					@Override
					public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
						tabheader.setBackgroundColor(Color.parseColor(colorcode));
					}
					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					}
					@Override
					public void onLoadingCancelled(String imageUri, View view) {
					}

				});
			}
			
		} else {
			if (colorcode.length()>5) {
				tabheader.setBackgroundColor(Color.parseColor(colorcode));
			}
		}
		
		/*** onClick Home Button ***/
		
		tabhome.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Cursor cur = Helper_Themeobject.getByMenuVer(app_user);
				Log.d(LOG_TAG, "c=" + cur.getCount());
				cur.close();
				
				if(cur.getCount()>0){
					Intent to = new Intent(c,SlideMenu.class);
					((Activity)c).startActivity(to);
					((Activity)c).overridePendingTransition(R.anim.slide_in_right,
							R.anim.slide_out_left);
				}else{
					((Activity)c).finish();
					String home_themecode="";
					String home_pageid="";				
					home_themecode = settings.getString("home_themecode", "");
					home_pageid = settings.getString("home_pageid", "");
					Log.d(LOG_TAG,"home_themecode="+ home_themecode);
					slotAction.CreateThemePage(home_themecode,home_pageid);
				}
			}
		});
		
		/*** onClick Back Button ***/
		
		tabback.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((Activity)c).finish();
				String afterLogin="";
				afterLogin = settings.getString("afterLogin", "");
				Log.d(LOG_TAG, "afterLogin :: " + afterLogin);
				if(afterLogin.equals("Theme_Std")){
					//Intent i = new Intent(c, Customer_MainPage.class);
					//c.startActivity(i);
					/*Intent startMain = new Intent(Intent.ACTION_MAIN);
					startMain.addCategory(Intent.CATEGORY_HOME);
					startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					c.startActivity(startMain);*/
					//System.exit(0);
					((Activity)c).finish();
					((Activity)c).overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					
				}else{
					
				
					if (value.equals("normal")) {
						
						String home_themecode="";
						String home_pageid="";				
						home_themecode = settings.getString("home_themecode", "");
						home_pageid = settings.getString("home_pageid", "");
						Log.d(LOG_TAG,"home_themecode="+ home_themecode);
	
						slotAction.CreateThemePage(home_themecode,home_pageid);
					}else if(value.equals("warranty")){
						Intent i=new Intent(c, Warranty_List.class);
						((Activity)c).startActivity(i);
					}else if(value.equals("reservation")){
						Helper_BookAdded.Delete();
						((Activity)c).overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
					}else{
						((Activity)c).finish();
						((Activity)c).overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_left);
					}
				}
			}
		});
		
	}
	
	public void onGetSettingColorButtonBackAndHome(String colorcode){
		String[] parts = colorcode.split("#");
		String part1 = parts[0];
		String part2 = parts[1];
		
		Log.d(LOG_TAG, "part 1 : "+ part1);
		Log.d(LOG_TAG, "part 2 : "+ part2);
		
		StringBuilder tmp = new StringBuilder();

		for (int i = 0; i < part2.length(); i++) {
		    tmp.append(part2.charAt(i));
		    if (i % 2 == 1) tmp.append(" ");
		}
		Log.d(LOG_TAG, "Text : "+ tmp.toString());
		
		String color_split = tmp.toString();
		String[] x = color_split.split("\\s+");
		String rr = x[0];
		String gg = x[1];
		String bb = x[2];
		
		int RR = hex2decimal(rr.toString());
		int GG = hex2decimal(gg.toString());
		int BB = hex2decimal(bb.toString());
		
		if(RR>180 && GG>180 && BB>180){
			Log.d(LOG_TAG, "ICON : BLACK");
			texttitlebar.setTextColor(Color.BLACK);
			img_back.setBackgroundResource(R.drawable.arrow_back_black);
			img_home.setBackgroundResource(R.drawable.home_black);
		}else{
			Log.d(LOG_TAG, "ICON : WHITE");
			texttitlebar.setTextColor(Color.WHITE);
			img_back.setBackgroundResource(R.drawable.arrow_back_white);
			img_home.setBackgroundResource(R.drawable.home_white);
		}
		
		Log.d(LOG_TAG, "RR : "+ String.valueOf(RR));
		Log.d(LOG_TAG, "GG : "+ String.valueOf(GG));
		Log.d(LOG_TAG, "BB : "+ String.valueOf(BB));
	}
	
	public static int hex2decimal(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16*val + d;
        }
        return val;
    }
	
	
	@SuppressWarnings("deprecation")
	public static DisplayImageOptions getDisplayImageOptions() {
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}
	
}
