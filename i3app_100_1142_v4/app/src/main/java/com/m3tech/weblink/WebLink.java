package com.m3tech.weblink;

import java.net.URLEncoder;
import java.util.Random;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.header.Header;
import com.m3tech.widget.WebLink_Widget;

public class WebLink extends WebLink_Widget {
	private static final String LOG_TAG = "WEBLINK";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING WEBLINK");

		Intent i = getIntent();
		url = i.getStringExtra("url");
		openweb = i.getStringExtra("openweb");
		pagetitle = i.getStringExtra("pagetitle");
		Log.i(LOG_TAG, "pagetitle: " + pagetitle);
		Log.i(LOG_TAG, "openweb: " + openweb);
		
		cusid = settings.getString("session_id", "");
		
		if (pagetitle.length()==0) {
			pagetitle = app_title;
		}

		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome,img_back,img_home, texttitlebar, pagetitle,"");
		
		tabback.setOnClickListener(onClickbackbutton);
			
		if (openweb.equals("yes")) {
			btnopen.setVisibility(View.VISIBLE);
		} else {
			btnopen.setVisibility(View.GONE);			
		}
		
		//**** WebLinkProcess ****//
		if(cusid.length()>0){
			Session_Task session = new Session_Task();
			session.execute();
		}else{
			onGetSession();
		}

	}
	
	public void onGetSession(){
		
		final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random rnd = new Random();
		int len = 16;
		StringBuilder sb = new StringBuilder( len );
		
		for( int i = 0; i < len; i++ ) 
		   sb.append(AB.charAt( rnd.nextInt(AB.length()) ) 
		);
		SharedPreferences.Editor editor = settings.edit();
		editor.remove("session_id");
		editor.putString("session_id",sb.toString());
		editor.commit();
		
		//**** WebLinkProcess ****//
		Session_Task session = new Session_Task();
		session.execute();
		
	}
	
	private class Session_Task extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(WebLink.this, "", getResources()
					.getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			
			String status = "",responseBody;
			JSONArray dataJSONArray;
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {
	
					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	
	
					String session = settings.getString("session_id","");
					
					HttpPost HTTP_POST_TOP;
					String strURL = context.getResources().getString(R.string.CUST_SESSION)
							+ "db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&cuid=" + URLEncoder.encode(cuid, "UTF-8")
							+ "&cusid=" + URLEncoder.encode(session, "UTF-8");
	
	
					HTTP_POST_TOP = new HttpPost(strURL);
					Log.d(LOG_TAG,"strURL="+ strURL);
					//Execute HTTP Post Request
					ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
					responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	
	
					dataJSONArray = new JSONArray(responseBody);
					Log.d(LOG_TAG,"responseBody="+ responseBody);	
					
					if(dataJSONArray.length() > 0){
						for(int i = 0;i < dataJSONArray.length();i++ ) {
						     
							JSONObject jsonObj = dataJSONArray.getJSONObject(i);
							status = jsonObj.getString("status");
						 }
					}
					
				}else{
					status = "0";
					Toast.makeText(WebLink.this,getResources().getString(R.string.please_retry_again),Toast.LENGTH_LONG).show();
				}
				
			} catch (Throwable t) {
				status = "-1";
				Log.e(LOG_TAG, "doInBackground-Error:" + t.getMessage(), t);
			}
			return status;
		}
	
		@Override
		protected void onPostExecute(String status) {
			super.onPostExecute(status);
			Log.d(LOG_TAG, "status: "+status);
			progressDialog.dismiss();
			if (status.equals("1")) {
				runWebLink();
			} else {
				Toast.makeText(WebLink.this,getResources().getString(R.string.please_retry_again),Toast.LENGTH_LONG).show();
				onDialogBox();
			}
		}

	}
	
	public void runWebLink(){
		
		if (url != null) {
			progressbar.setVisibility(View.VISIBLE);
			spinner.setVisibility(View.VISIBLE);
			
			//**** Check '?' ****//
			int total = 0;
			for(int a=0; a<url.length(); a++){
				if(url.charAt(a) == '?'){
					total +=1;
				}
			}
			Log.d(LOG_TAG, "total = "+total);
			String session = settings.getString("session_id","");
			
			if(total < 1){
				url = url+"?f=1&cuid="+cuid+"&cusid="+session+"&db="+app_db+"&userid="+app_user;
			}else{
				url = url+"&f=1&cuid="+cuid+"&cusid="+session+"&db="+app_db+"&userid="+app_user;
			}
			
			Log.i(LOG_TAG, "Loading url: " + url);
			//final ProgressDialog pd = ProgressDialog.show(this, "", "Loading...", true);
			

			WebSettings webViewSettings = webView.getSettings();
			webViewSettings.setAppCacheEnabled(true);

			webViewSettings.setJavaScriptEnabled(true);
			webViewSettings.setSupportZoom(true);  
			//webViewSettings.setBuiltInZoomControls(true);
			webViewSettings.setSaveFormData(true);
			webViewSettings.setUseWideViewPort(true);
			webViewSettings.setLoadWithOverviewMode(true);

			webView.canGoBack();
			webView.loadUrl(url);

			webView.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {
        			Log.i(LOG_TAG, "setWebChromeClient - onProgressChanged: " + progress);

                	progressbar.setProgress(progress);
                    if (progress == 100) {
                    	progressbar.setVisibility(View.GONE);
                    	spinner.setVisibility(View.GONE);
                    	progressbar.setProgress(0);
                    	if (!openweb.equals("yes")) {
                			tabbottom.setVisibility(View.GONE);			
                		}
                    } else {
                    	progressbar.setVisibility(View.VISIBLE);
                    	spinner.setVisibility(View.VISIBLE);
                    }
                }
            });


			// prevent opening new window
			webView.setWebViewClient(new WebViewClient() {
				@Override
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					if (url.toLowerCase().startsWith("tel:")) { 
		                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url)); 
		                startActivity(intent); 
		                return true;	
					} 
					
					else if (url.toLowerCase().startsWith("mailto:")) { 
						String email = url.toLowerCase();
						email = url.replace("mailto:", "");
						
						Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
						emailIntent.setType("plain/text");
						emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] { email });
						emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,app_title);
						emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(""));

						startActivity(Intent.createChooser(emailIntent, "Send email..."));
						return true;
					} 

					return false;
				}

				@Override
				public void onPageFinished(WebView view, String url) {
					isPageLoadedComplete = true;
					progressbar.setVisibility(View.GONE);
					spinner.setVisibility(View.GONE);
					super.onPageFinished(view, url);
				}

				@Override
				public void onReceivedError(WebView view, int errorCod,String description, String failingUrl) {
					progressbar.setVisibility(View.GONE);
					spinner.setVisibility(View.GONE);
					Toast.makeText(getApplicationContext(), 
							getResources().getString(R.string.MSG_NO_INTERNET), 
							Toast.LENGTH_LONG).show();
				}
				
			});
			
			btnopen.setOnClickListener(onclickopenbutton);

		} else {
			progressbar.setVisibility(View.GONE);
			spinner.setVisibility(View.GONE);
		}
	}
	

	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_DOWN){
            switch(keyCode)
            {
            case KeyEvent.KEYCODE_BACK:
                if(webView.canGoBack()){
                    webView.goBack();
                }else{
                    finish();
                }
                return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }
	
	private View.OnClickListener onclickopenbutton=new View.OnClickListener() {
		public void onClick(View v) {
			try{
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			    startActivity(intent);


			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onclickopenbutton-Error:" + t.getMessage(), t); 

			}
		}

	};


	/***  back button onclick ****/
	private View.OnClickListener onClickbackbutton=new View.OnClickListener() {
		public void onClick(View v) {
			try{
				if (webView != null) {
					ViewGroup parent = (ViewGroup) webView.getParent();
					if (parent != null) {
						parent.removeView(webView);
					}
					webView.removeAllViews();
					webView.destroy();
				}
				finish();
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onBackButton-Error:" + t.getMessage(), t); 

			}
		}

	};
	
	public boolean isNetworkAvailable() {

		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}
	
	public void onDialogBox(){
		alert = new AlertDialog.Builder(this);

		alert.setTitle(getResources().getString(R.string.please_retry_again));
		alert.setIcon(R.drawable.icon90);

		alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				onGetSession();
			}

		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.cancel();
				finish();
			}
		});
		alert.show();  
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
        finish();
 	}

	public void onDestroy() {
		if (webView != null) {
			ViewGroup parent = (ViewGroup) webView.getParent();
			if (parent != null) {
				parent.removeView(webView);
			}
			webView.removeAllViews();
			webView.destroy();
		}
		this.finish();
		super.onStop();
		super.onDestroy();	
	}


}
