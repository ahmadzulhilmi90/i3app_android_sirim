package com.m3tech.ibeacon;


import java.net.URLEncoder;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.collection.Collection_PointSettings;
import com.m3tech.data.DataProcessing;
import com.m3tech.data.Helper_Ibeaconcontent;
import com.m3tech.data.Helper_Ibeacondevice;
import com.m3tech.data.Helper_Themeobjectplugin;
import com.m3tech.data.Helper_Themepage;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;


@SuppressWarnings("deprecation")
public class BeaconView extends Activity {

	private static final String LOG_TAG = "Beacon";
	public static final String PREFS_NAME = "MyPrefsFile";
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();
	Helper_Themepage Helper_Themepage = null;
	Helper_Ibeacondevice Helper_Ibeacondevice = null;
	Helper_Ibeaconcontent Helper_Ibeaconcontent = null;
	Helper_Themeobjectplugin Helper_Themeobjectplugin = null;
	SlotAction slotAction = null;
	DataProcessing dataProcessing = null;
	
	String error_msg = null;
	String ibContentID,ibErrMsg;
	String wel_msg,wel_img,beaconpage_id,beaconstart_date,beaconend_date,beaconstatus,deviceID;
	String app_user,app_db,udid,customerID;

	static ProgressDialog progressDialog;
	Cursor c,z,h,f;
	ImageView beaconimage;
	SharedPreferences settings;
	RelativeLayout layoutImg;
	LayoutParams paramsImg;
	
	Handler handler = new Handler();
	Runnable refresh;

	ProgressBar spinner;
	/** Called when the activity is first created. */
	@Override

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.beaconview);

		Helper_Themepage = new Helper_Themepage(this);
		Helper_Ibeacondevice = new Helper_Ibeacondevice(this);
		Helper_Ibeaconcontent = new Helper_Ibeaconcontent(this);
		Helper_Themeobjectplugin = new Helper_Themeobjectplugin(this);
		slotAction = new SlotAction(this);
		dataProcessing = new DataProcessing(this);

		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		customerID = settings.getString("customerID", "");

		ibContentID  = settings.getString("ibContentID", "");
		ibErrMsg  = settings.getString("ibErrMsg", "");

		Log.d(LOG_TAG, "ibContentID="+ibContentID);

		GetBeaconList Task = new GetBeaconList();
		Task.execute(); 

		spinner = (ProgressBar) findViewById(R.id.progressbar);

		int screen_width = getWindowManager().getDefaultDisplay().getWidth();
		Log.d(LOG_TAG,"screen_width="+ screen_width);

		layoutImg = (RelativeLayout)findViewById(R.id.layoutImg);
		paramsImg = layoutImg.getLayoutParams();
		paramsImg.height = screen_width;
		paramsImg.width = screen_width;
						
		beaconimage = (ImageView)findViewById(R.id.beaconimage);
		beaconimage.setOnClickListener(onClickimage);
		Button btnSkip=(Button)findViewById(R.id.btnClose);
		btnSkip.setOnClickListener(onSkip);
	}


	public class GetBeaconList extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//progressDialog = ProgressDialog.show(Beaconimage.this, "", getResources().getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response = null;
			try {
				h = Helper_Ibeaconcontent.getfromContentID(ibContentID);

				if (h.moveToLast() != false) {
					wel_msg = Helper_Ibeaconcontent.getwelcome_msg(h).toString();
					wel_img = Helper_Ibeaconcontent.getwelcome_img(h).toString();
					beaconpage_id = Helper_Ibeaconcontent.getpage_id(h).toString();
					beaconstart_date = Helper_Ibeaconcontent.getstart_date(h).toString();
					beaconend_date = Helper_Ibeaconcontent.getend_date(h).toString();
					beaconstatus = Helper_Ibeaconcontent.getstatus(h).toString();
					deviceID = Helper_Ibeaconcontent.getdevice_id(h).toString();
				}

				h.close();

				if (wel_img.length()>10) {
					Log.w(LOG_TAG, "IMAGE SUCCESS"); 
					Log.d(LOG_TAG, "wel_img:" + wel_img); 
					response="SUCCESS";
				} else {
					response="FAILED";
				}

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "Getbeaconlist-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				
				if (result.equals("SUCCESS")){
					imageLoader.displayImage(wel_img,  beaconimage, displayOptions, new ImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {
							spinner.setVisibility(View.VISIBLE);
						}
						@Override
						public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
							spinner.setVisibility(View.GONE);

						}
						@Override
						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
							spinner.setVisibility(View.GONE);
							beaconimage.setVisibility(View.VISIBLE);
						}
						@Override
						public void onLoadingCancelled(String imageUri, View view) {
							spinner.setVisibility(View.GONE);
						}

					});

				} else {
					spinner.setVisibility(View.GONE);
					beaconimage.setVisibility(View.GONE);
					TextView beaconText = (TextView)findViewById(R.id.textbeacon);
					beaconText.setVisibility(View.VISIBLE);
					beaconText.setText(ibErrMsg);
				}
			}
			
			catch (Throwable t) {
				Log.e(LOG_TAG, "Getbeaconlist-onPostExecute-Error:" + t.getMessage(), t);
				//progressDialog.dismiss();
			}
		}

	}


	private View.OnClickListener onSkip=new View.OnClickListener() {
		public void onClick(View v) {
			try{
				finish();
				//startActivity( new Intent(Popup.this, Main.class ) );
				overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onSkip-Error:" + t.getMessage(), t); 
			}
		}
	};


	private View.OnClickListener onClickimage=new View.OnClickListener() {
		public void onClick(View v) {
			try{
				String themepageid="",theme_code="";
				Log.d(LOG_TAG, "beaconpage_id:"+beaconpage_id); 

				//GET NEXT PAGE
				c = Helper_Themepage.getByActionvalue(beaconpage_id);
				if (c.moveToLast() != false) {
					themepageid = Helper_Themepage.getthemepage_id(c).toString();
					theme_code = Helper_Themepage.gettheme_code(c).toString();
				}
				c.close();

				EarningPoints Task = new EarningPoints();
				Task.execute(new String[] { });
				
				if (themepageid.length()>0) {
					Log.d(LOG_TAG, "theme_code:"+theme_code); 
					Log.d(LOG_TAG, "themepageid:"+themepageid); 
					slotAction.CreateThemePage(theme_code,themepageid);
				}
				
				finish();												
				
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onClickimage-Error:" + t.getMessage(), t); 

			}
		}

	};

	private class EarningPoints extends AsyncTask<String, Void, String> {

		String tqmsg = "";
		String waitmsg = "";
		
		@Override
		protected void onPreExecute() {
			Log.d(LOG_TAG,"EarningPoints start...");
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			String status = "";
			try{

				String plugin_code = "ibeacon_checkin";
				Collection_PointSettings selected_settings = new Collection_PointSettings();
				selected_settings = dataProcessing.GetPointsSettings(plugin_code);
				
				if(selected_settings.toString().length()>1 && customerID.length()>0) {
				
					String plugin_id = selected_settings.get_plugin_id();
					String setting_id = selected_settings.get_setting_id();
					tqmsg = selected_settings.get_thankyou_msg();
					waitmsg = selected_settings.get_waiting_msg();
					
					Boolean Network_status = isNetworkAvailable();
					if (Network_status == true) {
	
						HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
						HttpClient httpclient = new DefaultHttpClient();
						httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
						
						String strURL = getResources().getString(R.string.POINT_EARNING_API)
								+ "db=" + URLEncoder.encode(app_db, "UTF-8")
								+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
								+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
								+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
								+ "&plugin_id="+URLEncoder.encode(plugin_id, "UTF-8")
								+ "&setting_id=" + URLEncoder.encode(setting_id, "UTF-8")
								+ "&data1=" + URLEncoder.encode(plugin_code, "UTF-8")
								+ "&data2=" + URLEncoder.encode(deviceID, "UTF-8");
	
						//name,mobile,gender,birthday,language,country,state
	
						HttpPost httppost = new HttpPost(strURL);
						Log.d(LOG_TAG,"strURL:"+ strURL);
						String responseBody = null;
						ResponseHandler<String> responseHandler = new BasicResponseHandler();
						responseBody = httpclient
								.execute(httppost, responseHandler);
	
						Log.d(LOG_TAG, "responseBody:" + responseBody);
	
						JSONArray StatusJSONArray = new JSONArray(responseBody);
						Log.d(LOG_TAG, "StatusJSONArray.length():"
								+ StatusJSONArray.length());
						if (StatusJSONArray.length() > 0) {
							status = StatusJSONArray.getJSONObject(0).getString("status").toString();
						}
	
					} else {
						status = "2";
						Log.d(LOG_TAG, "EarningPoints: No network");
					}
				}
			} catch (Throwable t) { 
				status = "2";
				Log.e(LOG_TAG, "EarningPoints-doInBackground-Error: " + t.getMessage(), t); 
			}
			return status;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				Log.d(LOG_TAG,"EarningPoints end...");
				if (result.equals("1")) {
					if (tqmsg.length()>0) {
						
						Toast toast = Toast.makeText(BeaconView.this, tqmsg, Toast.LENGTH_LONG);
						LinearLayout toastLayout = (LinearLayout) toast.getView();
						TextView toastTV = (TextView) toastLayout.getChildAt(0);
						toastTV.setTextSize(26);
						toast.show();
						
					}
				} else if (result.equals("0")) {
					if (waitmsg.length()>0) {
						Toast.makeText(BeaconView.this, waitmsg, Toast.LENGTH_LONG).show();
					}
				}
			}

			catch (Throwable t) {
				Log.e(LOG_TAG, "GetAPIOFFLINEData-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}   
	
	public static DisplayImageOptions getDisplayImageOptions() {
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)

		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}


	public boolean isNetworkAvailable() {    
		ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}


	public void onDestroy() {
		super.onStop();
		super.onDestroy();	
		slotAction.close();
		finish();
	}   

}
