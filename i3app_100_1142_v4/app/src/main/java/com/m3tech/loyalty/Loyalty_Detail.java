package com.m3tech.loyalty;


import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.collection.Collection_Loyalty;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

@SuppressWarnings("deprecation")
public class Loyalty_Detail extends Activity {

	private static final String LOG_TAG = "LOYALTY Detail";
	public static final String PREFS_NAME = "MyPrefsFile";
	static Boolean network_status=false;
	private SharedPreferences settings;
	String pathFOLDER,home_pagename,themecolor,btn_menucolor,tabbar,customerID,loyaltyid,title,short_desc,totalstamp,secrectcode,
	totalgotStamp,representcode,secret_code,stamp_icon_name,stamp_icon_bg_name,app_background;
	String responseBody = null;
	GridView gridView;
	EditText Editsearch;
	Cursor a,g;
	Button name,latest;
	Boolean Network_status;
	InputMethodManager imm;
	int stampicon,stampiconbg;
	TextView texttitlebar,texthome;
	EditText editrepresentcode,editsecretcode;
	String stamp_icon_nameextRemoved,stamp_icon_bg_nameextRemoved,stamp_icon_url,stamp_icon_bg_url,loyaltytitle,
	app_title,colorcode,home_themecode,app_user,app_db,udid,colorcode1;
	LinearLayout layouttitleheader,container;
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();
	Header Header = null;
	ImageView tabheader;

	//Progress
	static ProgressDialog progressDialog;

	//JSON
	JSONArray loyaltyJSONArray;

	List<Collection_Loyalty> model_loyalty=new ArrayList<Collection_Loyalty>();
	//loyalty_Adapter adapter_loyalty=null;
	Collection_Loyalty current_loyalty=null;

	SlotAction slotAction = null;
	Helper_Themeobject Helper_Themeobject = null;
	Context context = this;

	private String[] statusID;
	ArrayList<String> status = new ArrayList<String>();
	Dialog dialog1;
	Button btnstamp,btnredeem;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING LOYALTY DETAIL ");
		setContentView(R.layout.loyalty_detail);

		slotAction = new SlotAction(this);
		Helper_Themeobject = new Helper_Themeobject(this);
		Network_status = isNetworkAvailable();

		settings = this.getSharedPreferences(PREFS_NAME, 0);
		stamp_icon_url = settings.getString("stamp_icon_url", "");
		stamp_icon_bg_url = settings.getString("stamp_icon_bg_url", "");
		home_themecode = settings.getString("home_themecode", "");
		title= settings.getString("loyaltytitle", "");
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		customerID = settings.getString("customerID", "");
		home_pagename = settings.getString("home_pagename", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		colorcode1 = settings.getString("colorcode1", "");

		app_background = settings.getString("loyaltybg_img", "");

		String pageTitle = settings.getString("loyaltyPageTitle", "");
		if(pageTitle ==null||pageTitle.equals("")){
			pageTitle = app_title;
		}

		totalgotStamp = settings.getString("totalgotStamp", "");
		short_desc = settings.getString("shortdesc", "");
		secrectcode = settings.getString("secrectcode", "");
		loyaltyid = settings.getString("loyaltyid", "");
		totalstamp = settings.getString("totalstamp", "");
		representcode = "";
		
		Log.d(LOG_TAG, "secrectcode=" + secrectcode);
		Log.d(LOG_TAG,"stamp_icon_url="+ stamp_icon_url);
		Log.d(LOG_TAG,"stamp_icon_bg_url="+ stamp_icon_bg_url);


		/*** layout linear declaration *****/
		container = (LinearLayout) findViewById(R.id.container);

		tabheader = (ImageView) findViewById(R.id.tabheader);

		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);

		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, pageTitle,"");

		TextView texttitlebar=(TextView) findViewById(R.id.title2);
		texttitlebar.setText(title);
		texttitlebar.setTextColor(Color.parseColor(colorcode1));

		TextView shortdesc=(TextView) findViewById(R.id.shortdesc);
		shortdesc.setText(short_desc);

		//find background image for container
		if (app_background.toString().length()>5) {
			LoadBackground taskBg = new LoadBackground();
			taskBg.execute();
		}

		getLoyalty Task = new getLoyalty();
		Task.execute();

		btnstamp = (Button) findViewById(R.id.btnstamp);    
		btnstamp.setOnClickListener(onBtnStamp);

		btnredeem = (Button) findViewById(R.id.btnredeem);    
		btnredeem.setOnClickListener(onBtnRedeem);

	}


	private class getLoyalty extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (status.isEmpty()) {
				Toast.makeText(Loyalty_Detail.this,getResources().getString(R.string.please_wait), Toast.LENGTH_LONG).show();
			}
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;

			try{

				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(
						ConnRoutePNames.DEFAULT_PROXY, proxy);

				String strURL = getResources().getString(R.string.LOYALTY_STAMP_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&loyalty_id=" + URLEncoder.encode(loyaltyid, "UTF-8")
						+ "&rep_code=" + URLEncoder.encode(representcode, "UTF-8")
						+ "&task=" + URLEncoder.encode("get", "UTF-8");

				HttpPost httppost = new HttpPost(strURL);
				Log.d(LOG_TAG, "strURL" + strURL);
				// Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP = new BasicResponseHandler();
				responseBody = httpclient.execute(httppost, responseHandler_TOP);

				response="SUCCESS";
				//LoyaltyDetail.progressDialog.dismiss();

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "Getloyalty-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){

					loyaltyJSONArray = new JSONArray(responseBody);
					Log.d(LOG_TAG, "loyaltyJSONArray=" + loyaltyJSONArray);
					totalgotStamp = loyaltyJSONArray.getJSONObject(0).getString("total_gotStamped").toString();

					Log.d(LOG_TAG, "totalgotStamp=" + totalgotStamp);

					if(totalgotStamp.equals(totalstamp)){
						btnstamp.setVisibility(View.GONE);
						btnredeem.setVisibility(View.VISIBLE);

					}else{
						btnstamp.setVisibility(View.VISIBLE);
						btnredeem.setVisibility(View.GONE);
					}


					SharedPreferences.Editor editor = settings.edit();
					editor.putString("totalgotStamp",totalgotStamp);
					editor.commit();

					status.clear();
					for (int x=1;x<=Integer.parseInt(totalstamp);x++) {

						if(x <= Integer.parseInt(totalgotStamp)){
							status.add("1");

						}else{
							status.add("0");
						}
					}

					Log.d(LOG_TAG, "status=" + status);
					statusID = (String[]) status.toArray(new String[status.size()]);

					Log.d(LOG_TAG, "statusID=" + statusID);

					gridView = (GridView) findViewById(R.id.gridview);

					MyAdapter adapter = new MyAdapter(Loyalty_Detail.this, R.layout.loyalty_detail_row, statusID);

					gridView.setAdapter(adapter);


				}else{
					Toast.makeText(Loyalty_Detail.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}

				
			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "onloyalty-Error:" + t.getMessage(), t);
				Toast.makeText(Loyalty_Detail.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
			}
		}

	}

	/****  function LoadBackground  *****/
	public class LoadBackground extends AsyncTask<String, Void, String> {
		String bgimg=app_background;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			try{
				response="SUCCESS";				
			} catch (Throwable t) { 
				response="FAILED";
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				if (result.equals("SUCCESS") && bgimg.length()>10){
					String bgUri = slotAction.getImageURI(bgimg);	
					Log.d(LOG_TAG, "Background: " + bgUri);						

					String draw = "drawable://";
					if (bgUri.toLowerCase().contains(draw.toLowerCase())) {
					     bgUri = bgUri.replace("drawable://", "");
					     Drawable d = getResources().getDrawable(Integer.parseInt(bgUri));
					     container.setBackground(d);
					} else {
					     imageLoader.loadImage(bgUri, new SimpleImageLoadingListener() {
					          @Override
					          public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					               super.onLoadingComplete(imageUri, view, loadedImage);
					               container.setBackground(new BitmapDrawable(loadedImage));
					          }
					     });
					}
				}
			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "LoadBackground-onPostExecute-Error:" + t.getMessage(), t);

			}
		}
	}

	public class MyAdapter extends ArrayAdapter<String> {

		String[] objects;
		Context context;

		public MyAdapter(Context context, int textViewResourceId, String[] objects) {
			super(context, textViewResourceId, objects);
			this.context = context;
			this.objects = objects;


		}


		@Override
		public View getView(int position, android.view.View convertView, android.view.ViewGroup parent) {
			ImageView product_image = null;
			String url2=statusID[position];

			try{

				if (convertView == null) {
					LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					product_image = (ImageView)inflater.inflate(R.layout.loyalty_detail_row,parent,false);
				} else {
					product_image = (ImageView) convertView;
				}
				product_image.setOnClickListener(null);
				
				if (url2.equals("0")){


					imageLoader.displayImage(stamp_icon_bg_url, product_image, displayOptions, new ImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {
							//progress.setVisibility(View.VISIBLE);
						}
						@Override
						public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
							// progress.setVisibility(View.GONE);

						}
						@Override
						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
							//progress.setVisibility(View.GONE);
						}
						@Override
						public void onLoadingCancelled(String imageUri, View view) {
							//progress.setVisibility(View.GONE);
						}

					});


				}else{

					imageLoader.displayImage(stamp_icon_url, product_image, displayOptions, new ImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {
							//progress.setVisibility(View.VISIBLE);
						}
						@Override
						public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
							// progress.setVisibility(View.GONE);

						}
						@Override
						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
							//progress.setVisibility(View.GONE);
						}
						@Override
						public void onLoadingCancelled(String imageUri, View view) {
							//progress.setVisibility(View.GONE);
						}

					});


				}


			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onRegister-Error:" + t.getMessage(), t); 

			}
			return product_image;
		}
	}


	private  View.OnClickListener onBtnStamp=new View.OnClickListener() {
		public void onClick(View v) {
			try{


				//if(Network_status == true){

				dialog1 = new Dialog(Loyalty_Detail.this);
				dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog1.setContentView(R.layout.loyalty_custom_alert);

				Button yes = (Button) dialog1.findViewById(R.id.button1);
				Button redeem = (Button) dialog1.findViewById(R.id.button3);
				yes.setVisibility(View.VISIBLE);
				redeem.setVisibility(View.GONE);
				Button no = (Button) dialog1.findViewById(R.id.button2);

				editsecretcode = (EditText) dialog1.findViewById(R.id.editsecretcode);
				//editsecretcode.setText(secrectcode);
				editrepresentcode = (EditText) dialog1.findViewById(R.id.editrepresentcode);

				yes.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v) 
					{

						imm = (InputMethodManager)getSystemService(
								Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(editsecretcode.getWindowToken(), 0);
						imm.hideSoftInputFromWindow(editrepresentcode.getWindowToken(), 0);

						try{

							secret_code = editsecretcode.getText().toString();
							representcode = editrepresentcode.getText().toString();

							if(!secret_code.equals(secrectcode)){

								Toast.makeText(Loyalty_Detail.this, "Invalid secret code.", Toast.LENGTH_SHORT).show();

							}else{
								if(representcode.equals("")){

									Toast.makeText(Loyalty_Detail.this, "Please key in your representative code.", Toast.LENGTH_SHORT).show();

								}else{

									GetStamp Task = new GetStamp();
									Task.execute();

								}	
							}

						} catch (Throwable t) { 
							Log.e(LOG_TAG, "onStamp-Error:" + t.getMessage(), t); 

						}

					}
				});
				no.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) 
					{

						InputMethodManager imm = (InputMethodManager)getSystemService(
								Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(editrepresentcode.getWindowToken(), 0);

						dialog1.dismiss();  

					}
				});
				dialog1.show();

			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onRegister-Error:" + t.getMessage(), t); 

			}
		}

	};

	private  View.OnClickListener onBtnRedeem=new View.OnClickListener() {
		public void onClick(View v) {
			try{


				//if(Network_status == true){

				dialog1 = new Dialog(Loyalty_Detail.this);
				dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog1.setContentView(R.layout.loyalty_custom_alert);

				Button yes = (Button) dialog1.findViewById(R.id.button1);
				Button no = (Button) dialog1.findViewById(R.id.button2);
				Button redeem = (Button) dialog1.findViewById(R.id.button3);
				yes.setVisibility(View.GONE);
				redeem.setVisibility(View.VISIBLE);
				final EditText editsecretcode = (EditText) dialog1.findViewById(R.id.editsecretcode);
				// editsecretcode.setText(secrectcode);
				final EditText editrepresentcode = (EditText) dialog1.findViewById(R.id.editrepresentcode);

				redeem.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v) 
					{

						imm = (InputMethodManager)getSystemService(
								Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(editsecretcode.getWindowToken(), 0);
						imm.hideSoftInputFromWindow(editrepresentcode.getWindowToken(), 0);

						try{


							secret_code = editsecretcode.getText().toString();
							representcode = editrepresentcode.getText().toString();

							if(!secret_code.equals(secrectcode)){

								Toast.makeText(Loyalty_Detail.this, "Invalid secret code.", Toast.LENGTH_SHORT).show();

							}else{
								if(representcode.equals("")){

									Toast.makeText(Loyalty_Detail.this, "Please key in your representative code.", Toast.LENGTH_SHORT).show();

								}else{

									GetRedeem Task = new GetRedeem();
									Task.execute();


								}	

							}

						} catch (Throwable t) { 
							Log.e(LOG_TAG, "onStamp-Error:" + t.getMessage(), t); 

						}

					}
				});
				no.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v) 
					{

						InputMethodManager imm = (InputMethodManager)getSystemService(
								Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(editrepresentcode.getWindowToken(), 0);

						dialog1.dismiss();  

					}
				});
				dialog1.show();

				overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onRegister-Error:" + t.getMessage(), t); 

			}
		}

	};



	private class GetStamp extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Loyalty_Detail.this, "", getResources().getString(R.string.please_wait));
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;


			try{

				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

				String strURL = getResources().getString(R.string.LOYALTY_STAMP_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&loyalty_id=" + URLEncoder.encode(loyaltyid, "UTF-8")
						+ "&rep_code=" + URLEncoder.encode(representcode, "UTF-8")
						+ "&task=" + URLEncoder.encode("add", "UTF-8");


				HttpPost httppost = new HttpPost(strURL);
				Log.d(LOG_TAG, "strURL:" + strURL);
				// Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP = new BasicResponseHandler();
				responseBody = httpclient
						.execute(httppost, responseHandler_TOP);

				response="SUCCESS";
				Loyalty_Detail.progressDialog.dismiss();

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "Getloyalty-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				Loyalty_Detail.progressDialog.dismiss();	
				dialog1.dismiss();

				if (result.equals("SUCCESS")){

					loyaltyJSONArray = new JSONArray(responseBody);

					Log.d(LOG_TAG, "loyaltyJSONArray=" + loyaltyJSONArray);

					String status = loyaltyJSONArray.getJSONObject(0)
							.getString("status").toString();

					Log.d(LOG_TAG, "status=" + status);

					if (status.equals("1")) {
						
						Toast.makeText(Loyalty_Detail.this, getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();
						
						getLoyalty Task = new getLoyalty();
						Task.execute();
					} else {
						Toast.makeText(Loyalty_Detail.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_SHORT).show();
					}


				}else{
					Toast.makeText(Loyalty_Detail.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_SHORT).show();
				}




			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "onloyalty-Error:" + t.getMessage(), t);

			}
		}

	}




	private class GetRedeem extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Loyalty_Detail.this, "", getResources().getString(R.string.please_wait));
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;

			try{

				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				String strURL = getResources().getString(R.string.LOYALTY_STAMP_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&loyalty_id=" + URLEncoder.encode(loyaltyid, "UTF-8")
						+ "&rep_code=" + URLEncoder.encode(representcode, "UTF-8")
						+ "&task=" + URLEncoder.encode("redeem", "UTF-8");

				HttpPost httppost = new HttpPost(strURL);

				Log.d(LOG_TAG,"strURL:"+ strURL);
				//Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
				responseBody=httpclient.execute(httppost, responseHandler_TOP);	
				Log.d(LOG_TAG, "responseBody=" + responseBody);

				response="SUCCESS";


			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetRedeem-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				Loyalty_Detail.progressDialog.dismiss();  
				dialog1.dismiss();

				if (result.equals("SUCCESS")){

					loyaltyJSONArray = new JSONArray(responseBody);

					Log.d(LOG_TAG, "loyaltyJSONArray=" + loyaltyJSONArray);

					String status = loyaltyJSONArray.getJSONObject(0).getString("status").toString();

					Log.d(LOG_TAG, "status=" + status);

					if(status.equals("1")){
						
						Toast.makeText(Loyalty_Detail.this, getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();

						getLoyalty Task = new getLoyalty();
						Task.execute();
					} else {
						Toast.makeText(Loyalty_Detail.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_SHORT).show();
					}


				}else{
					Toast.makeText(Loyalty_Detail.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_SHORT).show();
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "GetRedeem-Error:" + t.getMessage(), t);

			}
		}

	}

	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;


	}

	private static DisplayImageOptions getDisplayImageOptions() {
		// TODO Auto-generated method stub
		return new DisplayImageOptions.Builder()

		// .showImageForEmptyUri(R.drawable.ic_empty)
		//.showImageOnFail(R.drawable.ic_empty)


		.cacheInMemory(true)
		.cacheOnDisc(true)


		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


	public void onBackPressed() {

		super.onBackPressed(); // allows standard use of backbutton for page 1
		this.finish();
		//Intent i=new Intent(LoyaltyDetail.this, Loyalty.class);
		// startActivity(i);

		overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
	}

	public void onDestroy() {

		this.finish();
		super.onStop();
		super.onDestroy();		
	}

}
