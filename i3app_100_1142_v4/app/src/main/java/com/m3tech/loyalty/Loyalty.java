package com.m3tech.loyalty;


import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.collection.Collection_Loyalty;
import com.m3tech.customer.Customer_MainPage;
import com.m3tech.data.Helper_Loyalty;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class Loyalty extends Activity {

	private static final String LOG_TAG = "LOYALTY";
	public static final String PREFS_NAME = "MyPrefsFile";
	static Boolean network_status=false;

	//Cache Image
	//private static File cacheDir;

	//private static HashMap<String, Bitmap> cache=new HashMap<String, Bitmap>();

	private SharedPreferences settings;
	String colorcode,app_user,app_db,udid,home_pagename,currency,themecolor,btn_menucolor,app_title,customerID,loyaltytitle,template;
	Helper_Loyalty Helper_Loyalty =null;
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();
	LinearLayout container;
	TextView loyalty_name,texttitlebar,texthome;
	EditText Editsearch;
	Cursor a,b,c,d,e,g,f,h;
	Button name,latest;
	ImageView back,tabheader;

	//Progress
	static ProgressDialog progressDialog;

	//JSON
	JSONArray companyprofileJSONArray;

	List<Collection_Loyalty> model_loyalty=new ArrayList<Collection_Loyalty>();
	loyalty_Adapter adapter_loyalty=null;
	Collection_Loyalty current_loyalty=null;

	SlotAction slotAction = null;
	Helper_Themeobject Helper_Themeobject = null;
	Context context = this;
	Header Header = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING LOYALTY LIST ");
		setContentView(R.layout.loyalty);

		slotAction = new SlotAction(this);
		Helper_Loyalty=new Helper_Loyalty(this);
		Helper_Themeobject = new Helper_Themeobject(this);

		settings = getSharedPreferences(PREFS_NAME, 0);
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		customerID = settings.getString("customerID", "");
		home_pagename = settings.getString("home_pagename", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");

		String pageTitle = settings.getString("loyaltyPageTitle", "");
		if(pageTitle ==null||pageTitle.equals("")){
			pageTitle = app_title;
		}


		/*** layout linear declaration *****/

		tabheader = (ImageView) findViewById(R.id.tabheader);

		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);

		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, pageTitle,"");


		Getproductcat Task = new Getproductcat();
		Task.execute(new String[] {app_user });

	}


	private class Getproductcat extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Loyalty.this, "", getResources().getString(R.string.please_wait));
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			app_user = arg0[0];
			Log.d(LOG_TAG,"app_user="+ app_user);	

			try{

				adapter_loyalty=new loyalty_Adapter();
				adapter_loyalty.clear();

				c=Helper_Loyalty.getByuserid(app_user);

				if (c.moveToLast() != false ){
					c.moveToFirst();
					do {

						current_loyalty=new Collection_Loyalty();

						current_loyalty.setid(Helper_Loyalty.getloyalt_id(c).toString());
						current_loyalty.settitle(Helper_Loyalty.gettitle(c).toString());
						current_loyalty.setshort_description(Helper_Loyalty.getshort_description(c).toString());
						current_loyalty.seticon(Helper_Loyalty.geticon(c).toString());
						current_loyalty.setbg_img(Helper_Loyalty.getbg_img(c).toString());
						current_loyalty.setuser_id(Helper_Loyalty.getuser_id(c).toString());
						current_loyalty.settotal_stamp(Helper_Loyalty.gettotal_stamp(c).toString());
						current_loyalty.setsecret_code(Helper_Loyalty.getsecrect_code(c).toString());
						current_loyalty.setstamp_icon_type(Helper_Loyalty.getstamp_icon_type(c).toString());
						current_loyalty.setstamp_icon_url(Helper_Loyalty.getstamp_icon_url(c).toString());
						current_loyalty.setstamp_icon_bg_url(Helper_Loyalty.getstamp_icon_bg_url(c).toString());

						adapter_loyalty.add(current_loyalty);

					}while (c.moveToNext());
				}

				response="SUCCESS";
				Loyalty.progressDialog.dismiss();

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "Getloyalty-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				ListView listView = (ListView) findViewById(R.id.listcategory);

				if (result.equals("SUCCESS")){

					if (c.getCount()>0){

						listView.setAdapter(adapter_loyalty);			
						listView.setOnItemClickListener(onListClickhome);	

					}else{
						Toast.makeText(Loyalty.this,getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG).show();
					}

				}else{
					Toast.makeText(Loyalty.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}

				Loyalty.progressDialog.dismiss();

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "onloyalty-Error:" + t.getMessage(), t);
				Loyalty.progressDialog.dismiss();
			}
		}

	}



	class loyalty_Adapter extends ArrayAdapter<Collection_Loyalty> {
		loyalty_Adapter() {
			super(Loyalty.this, R.layout.loyalty_row, model_loyalty);
		}

		public View getView(int position, View convertView,
				ViewGroup parent) {
			View row=convertView;
			NotificationHolderhome holder=null;

			if (row==null) {													
				LayoutInflater inflater=getLayoutInflater();

				row=inflater.inflate(R.layout.loyalty_row, parent, false);
				holder=new NotificationHolderhome(row);
				row.setTag(holder);
			}
			else {
				holder=(NotificationHolderhome)row.getTag();
			}

			holder.populateFromhome(model_loyalty.get(position));

			return(row);
		}
	}


	class NotificationHolderhome {
		@SuppressWarnings("unused")
		private View row=null;
		private TextView title=null;
		private TextView shortdesc=null;
		private ImageView product_image =null;
		private LinearLayout spinner = null;
		NotificationHolderhome(View row) {

			this.row=row;	

			title=(TextView) row.findViewById(R.id.title);
			shortdesc=(TextView) row.findViewById(R.id.shortdesc);
			product_image = (ImageView) row.findViewById(R.id.product_image);
			spinner = (LinearLayout) row.findViewById(R.id.progressbar);


		}

		void populateFromhome(Collection_Loyalty r) {

			title.setText(r.gettitle());
			shortdesc.setText(r.getshort_description());


			imageLoader.displayImage(r.geticon(), product_image, displayOptions, new ImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {
					spinner.setVisibility(View.VISIBLE);
				}
				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					spinner.setVisibility(View.GONE);

				}
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					spinner.setVisibility(View.GONE);
				}
				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					spinner.setVisibility(View.GONE);
				}

			});
		}
	}



	private AdapterView.OnItemClickListener onListClickhome=new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent,
				View view, int position,
				long id) {
			current_loyalty=model_loyalty.get(position);	
			Log.d(LOG_TAG,"onListClick table name="+current_loyalty.gettitle());


			if(customerID.equals("")){

				final AlertDialog.Builder alert = new AlertDialog.Builder(Loyalty.this);

				alert.setTitle("Please login to your account");

				alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {

						SharedPreferences.Editor editor = settings.edit();
						editor.remove("afterLogin");
						editor.putString("afterLogin", "Loyalty");
						editor.commit();

						overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
						Intent i=new Intent(Loyalty.this, Customer_MainPage.class);
						startActivity(i);
					}

				});
				alert.show();

			}else{

				overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
				Intent i=new Intent(Loyalty.this, Loyalty_Detail.class);

				startActivity(i);

				settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();    
				editor.putString("stamp_icon_url", current_loyalty.getstamp_icon_url());
				Log.d(LOG_TAG,"stamp_icon_url="+current_loyalty.getstamp_icon_url());
				editor.putString("stamp_icon_bg_url", current_loyalty.getstamp_icon_bg_url());	
				Log.d(LOG_TAG,"stamp_icon_bg_url="+current_loyalty.getstamp_icon_bg_url());
				editor.putString("loyaltytitle", current_loyalty.gettitle());
				editor.putString("loyaltyid", current_loyalty.getid());
				editor.putString("shortdesc", current_loyalty.getshort_description());
				editor.putString("totalstamp", current_loyalty.gettotal_stamp());
				editor.putString("secrectcode", current_loyalty.getsecret_code());
				editor.putString("loyaltybg_img", current_loyalty.getbg_img());
				editor.commit();

			}
		}
	};



	@SuppressWarnings("deprecation")
	public static DisplayImageOptions getDisplayImageOptions()
	{
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}

	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;


	}



	public static int getResourceId(Context context, String name, String resourceType) {   //this will returns you Drawable with that String Variable.
		return context.getResources().getIdentifier(name, resourceType, context.getPackageName());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


	public void onBackPressed() {

		super.onBackPressed(); // allows standard use of backbutton for page 1
		this.finish();


		overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
	}

	public void onDestroy() {

		this.finish();
		super.onStop();
		super.onDestroy();	
		Helper_Loyalty.close();

	}

}
