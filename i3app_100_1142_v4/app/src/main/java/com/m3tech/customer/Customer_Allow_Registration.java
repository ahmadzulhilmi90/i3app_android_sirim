package com.m3tech.customer;

import java.io.IOException;
import java.net.URLEncoder;

import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;

public class Customer_Allow_Registration {

	Context context;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static final String LOG_TAG = "CUST_ALLOW_REGISTRATION";
	private String app_user,app_db,responseBody,id,allow_registration;
	JSONArray dataJSONArray;
	Boolean network_status;
	
	public Customer_Allow_Registration(Context mycontext){
		context = mycontext;
		app_user = context.getResources().getString(R.string.app_user);
		app_db = context.getResources().getString(R.string.app_db);
		
		/*** Call onGetCustAllowRegistration ***/
		
		network_status = isNetworkAvailable();
		if(!network_status){
			Toast.makeText(context,context.getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_LONG).show();
			allow_registration = "0";
		} else{
			try {
				onGetCustAllowRegistration();
				
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	/*** Call onGetCustAllowRegistration ***/
	
	public void onGetCustAllowRegistration() throws NotFoundException, ClientProtocolException, IOException, JSONException{
		
			String response;
			HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
			HttpClient httpclient = new DefaultHttpClient();
			httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

			HttpPost HTTP_POST_TOP;
			String strURL = context.getResources().getString(R.string.CUST_ALLOW_REGISTRATION_API)
					+ "db=" + URLEncoder.encode(app_db, "UTF-8")
					+ "&userid=" + URLEncoder.encode(app_user, "UTF-8");


			HTTP_POST_TOP = new HttpPost(strURL);
			Log.d(LOG_TAG,"strURL="+ strURL);
			//Execute HTTP Post Request
			ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
			responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	
			dataJSONArray = new JSONArray(responseBody);
			
			if(dataJSONArray.length() > 0){
				for(int i = 0;i < dataJSONArray.length();i++ ) {
				     JSONObject jsonObj = dataJSONArray.getJSONObject(i);
		
				     //now get id & value
				      id = jsonObj.getString("id");
				      allow_registration = jsonObj.getString("allow_registration");
				      Log.d(LOG_TAG, "RESPONSE : "+ id +"|"+allow_registration);
				 }
			}
			response="SUCCESS";
			Log.d(LOG_TAG, "response : "+response);
	}
	
	public String onResultAllowRegistration(){
		Log.d(LOG_TAG, "Cust Allow Registration : " + allow_registration);
		if(allow_registration.length()>0 && !allow_registration.equals("null")){
			return allow_registration;
		}else{
			return "0";
		}
	}
	
	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;
	}
	
}
