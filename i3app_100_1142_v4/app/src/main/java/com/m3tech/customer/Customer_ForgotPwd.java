package com.m3tech.customer;

import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.background.PageBackground;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;

public class Customer_ForgotPwd extends Activity {

	private static final String LOG_TAG = "CUSTOMER FORGOT PASSWORD";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	SharedPreferences settings;
	String home_pagename, app_title,themecolor,colorcode,app_user,app_db,udid;
	ImageView tabheader;
	TextView texttitlebar,texthome;
	ImageView page_background,back;
	Button btnsubmit;
	EditText editTextemail;
	static ProgressDialog progressDialog;
	Helper_Themeobject Helper_Themeobject = null;
	SlotAction slotAction = null;
	Context context = this;
	Header Header = null;
	PageBackground PageBackground = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING "+LOG_TAG);
		setContentView(R.layout.customer_forgotpwd);

		slotAction = new SlotAction(this);
		Helper_Themeobject = new Helper_Themeobject(this);
		settings = this.getSharedPreferences(PREFS_NAME, 0);
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		home_pagename = settings.getString("home_pagename", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");

		/*** layout linear declaration *****/

		tabheader = (ImageView) findViewById(R.id.tabheader);
		tabheader.setBackgroundColor(Color.parseColor(colorcode));

		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		String title_header = getResources().getString(R.string.forgot_pass);

		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, title_header,"");

		/*** Setting Page Background ***/
		page_background = (ImageView) findViewById(R.id.page_background);
		PageBackground = new PageBackground(context,page_background);
		
		btnsubmit = (Button) findViewById(R.id.btnsubmit);
		btnsubmit.setOnClickListener(onClickSubmit);

		editTextemail = (EditText) findViewById(R.id.editTextemail);

	}


	//click button forgot pass
	private View.OnClickListener onClickSubmit = new View.OnClickListener() {
		public void onClick(View v) {
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) { // check network

					if (editTextemail.getText().toString().trim().equals("")) { // check
						// email

						editTextemail.setError("Email is required!");

					} else {
						editTextemail.setError(null);
						if (isEmailValid(editTextemail.getText().toString()) == false) { // check

							editTextemail.setError("Please enter valid format email address!");

						} else {
							editTextemail.setError(null);
							InputMethodManager imm = (InputMethodManager)getSystemService(
									Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(editTextemail.getWindowToken(), 0);

							Forgot_Task Login = new Forgot_Task();
							Login.execute();


						}
					}

				} else {
					Toast.makeText(Customer_ForgotPwd.this,
							getResources().getString(R.string.MSG_NO_INTERNET),
							Toast.LENGTH_LONG).show();
				}
			} catch (Throwable t) {
				Log.e(LOG_TAG, "onLogin-Error:" + t.getMessage(), t);

			}
		}
	};



	private class Forgot_Task extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Customer_ForgotPwd.this, "", getResources()
					.getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String status = null;
			String status_desc = null;
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {

					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

					String strURL = getResources().getString(R.string.FORGOT_API)
							+ "db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&email=" + URLEncoder.encode(editTextemail.getText().toString(), "UTF-8");


					HttpPost httppost = new HttpPost(strURL);
					Log.d(LOG_TAG,"strURL:"+ strURL);
					String responseBody = null;
					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					responseBody = httpclient
							.execute(httppost, responseHandler);

					Log.d(LOG_TAG, "responseBody:" + responseBody);

					JSONArray StatusJSONArray = new JSONArray(responseBody);
					Log.d(LOG_TAG, "StatusJSONArray.length():"
							+ StatusJSONArray.length());
					if (StatusJSONArray.length() > 0) {
						status = StatusJSONArray.getJSONObject(0)
								.getString("status").toString();
						status_desc = StatusJSONArray.getJSONObject(0)
								.getString("desc").toString();

					}

				} else {
					status = "2";
					status_desc = getResources().getString(
							R.string.MSG_NO_INTERNET);
				}
			} catch (Throwable t) {
				status = "-1";
				status_desc = getResources().getString(
						R.string.please_retry_again);
				Log.e(LOG_TAG, "doInBackground-Error:" + t.getMessage(), t);
			}
			return status + "~" + status_desc;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			AlertDialog.Builder alert;
			final String[] arr_result = TextUtils.split(result, "~");
			
			String toastmsg = "";
			String resultmsg = "";
			
			Customer_ForgotPwd.progressDialog.dismiss();
			
			if (arr_result[0].equals("1")) {
				toastmsg = getResources().getString(R.string.forgot_success);
				resultmsg = getResources().getString(R.string.success);
			} else if (arr_result[0].equals("2")) {
				toastmsg = getResources().getString(R.string.MSG_NO_INTERNET);
				resultmsg = getResources().getString(R.string.error);
			} else if (arr_result[0].equals("0")) {
				toastmsg = getResources().getString(R.string.forgot_failed);
				resultmsg = getResources().getString(R.string.error);
			} else {				
				toastmsg = getResources().getString(R.string.error_msg);
				resultmsg = getResources().getString(R.string.error);			
			} 

			alert = new AlertDialog.Builder(Customer_ForgotPwd.this);

			alert.setTitle(resultmsg);
			alert.setIcon(R.drawable.icon90);
			alert.setMessage(toastmsg);

			alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					if (arr_result[0].equals("1")) {
						finish();
					} else {
						dialog.cancel();
					}
					
				}

			});
			
			alert.show();   
		}

	}



	// FUNCTION NETWORK

	public boolean isNetworkAvailable() {

		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}


	/**
	 * method is used for checking valid email id format.
	 * 
	 * @param email
	 * @return boolean true for valid false for invalid
	 */
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public void onBackPressed() {

		String afterLogin="";
		afterLogin = settings.getString("afterLogin", "");
		if(afterLogin.equals("Theme_Std")){
			finish();
			Intent i = new Intent(Customer_ForgotPwd.this, Customer_MainPage.class);
			startActivity(i);
			//overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		}else{
			super.onBackPressed(); 
			this.finish();
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
		}
	}


	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();
	}
}
