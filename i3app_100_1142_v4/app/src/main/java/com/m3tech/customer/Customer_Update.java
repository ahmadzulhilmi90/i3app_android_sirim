package com.m3tech.customer;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.background.PageBackground;
import com.m3tech.collection.Collection_List;
import com.m3tech.data.DataProcessing;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;

public class Customer_Update extends Activity {

	private static final String LOG_TAG = "CUSTOMER UPDATE";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	SharedPreferences settings;
	String home_pagename, app_title,themecolor,colorcode,app_user,app_db,udid,customerID;
	TextView texttitlebar,texthome;
	ImageView page_background,back,tabheader;
	Button btnsubmit;
	EditText editText_name, editText_mobile;
	String language_name="", language_code="", gender_name="", gender_code="", country_name="", country_code="", state_name="", state_code="";
	static ProgressDialog progressDialog;
	JSONArray custJSONArray;
	JSONArray languageJSONArray, genderJSONArray, countryJSONArray, stateJSONArray;
	
	String strLanguage_List = null, strGender_List = null, strCountry_List = null, strState_List = null;
	int posLang=0, posGender=0, posCountry=0, posState=0;
	String codeLang="", codeGender="", codeCountry="MY", codeState="";	

	List<Collection_List> model_gender = new ArrayList<Collection_List>();
	List<Collection_List> model_language = new ArrayList<Collection_List>();
	List<Collection_List> model_country = new ArrayList<Collection_List>();
	List<Collection_List> model_state = new ArrayList<Collection_List>();

	ArrayAdapter<Collection_List> adapter_gender = null;
	ArrayAdapter<Collection_List> adapter_language = null;
	ArrayAdapter<Collection_List> adapter_country = null;
	ArrayAdapter<Collection_List> adapter_state = null;
	
	SlotAction slotAction = null;
	DataProcessing dataProcessing = null;
	ProgressBar progressbar; 
	ScrollView main_update; 
	Helper_Themeobject Helper_Themeobject = null;
	Context context = this;
	Header Header = null;
	PageBackground PageBackground = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING "+LOG_TAG);
		setContentView(R.layout.customer_update);

		slotAction = new SlotAction(this);
		dataProcessing = new DataProcessing(this);
		Helper_Themeobject = new Helper_Themeobject(this);
		
		settings = this.getSharedPreferences(PREFS_NAME, 0);
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		home_pagename = settings.getString("home_pagename", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		customerID = settings.getString("customerID", "");

		/*** layout linear declaration *****/
		tabheader = (ImageView) findViewById(R.id.tabheader);

		progressbar = (ProgressBar) findViewById(R.id.progressbar);
		main_update = (ScrollView) findViewById(R.id.main_update);
		
		
		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		String title_header = getResources().getString(R.string.updatedetails);
		
		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, title_header,"");
		
		/*** Setting Page Background ***/
		page_background = (ImageView) findViewById(R.id.page_background);
		PageBackground = new PageBackground(context,page_background);

		btnsubmit = (Button) findViewById(R.id.btnsubmit);
		btnsubmit.setOnClickListener(onClickSubmit);

		editText_name = (EditText) findViewById(R.id.editText_name);
		editText_mobile = (EditText) findViewById(R.id.editText_mobile);

		Boolean Network_status = isNetworkAvailable();
		if (Network_status == true) { // check network
			getCustomerDetails Task = new getCustomerDetails();
			Task.execute();

			GetAPIOFFLINEData Task2 = new GetAPIOFFLINEData();
			Task2.execute();
		} else {
			Toast.makeText(Customer_Update.this,
					getResources().getString(R.string.MSG_NO_INTERNET),
					Toast.LENGTH_LONG).show();
			onBackPressed();
		}

	}

	private class GetAPIOFFLINEData extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			Log.d(LOG_TAG,"GetAPIOFFLINEData start...");
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;
			try{

				strLanguage_List = dataProcessing.ReadFromText(getApplication().getResources().openRawResource(R.raw.app_language));
				strGender_List = dataProcessing.ReadFromText(getApplication().getResources().openRawResource(R.raw.app_gender));
				strCountry_List = dataProcessing.ReadFromText(getApplication().getResources().openRawResource(R.raw.app_countries));
				strState_List = dataProcessing.ReadFromText(getApplication().getResources().openRawResource(R.raw.app_states));

				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Toast.makeText(Customer_Update.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				Log.e(LOG_TAG, "GetAPIOFFLINEData-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				Log.d(LOG_TAG,"GetAPIOFFLINEData end...");
				if (result.equals("SUCCESS")) {
					AssignLanguageList (strLanguage_List);
					AssignGenderList (strGender_List);
					AssignCountryList (strCountry_List);
					
					progressbar.setVisibility(View.GONE);
					main_update.setVisibility(View.VISIBLE);
				}
			}

			catch (Throwable t) {
				Toast.makeText(Customer_Update.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				Log.e(LOG_TAG, "GetAPIOFFLINEData-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}   

	private class getCustomerDetails extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Toast.makeText(Customer_Update.this,getResources().getString(R.string.please_wait), Toast.LENGTH_LONG).show();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;

			try{
				
				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_RSVSETTING;
				String strURL = getResources().getString(R.string.CUST_DETAILS_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8");

				HTTP_POST_RSVSETTING = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);

				ResponseHandler<String> responseHandler_RSVSETTING=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_RSVSETTING, responseHandler_RSVSETTING);	
				Log.d(LOG_TAG,responseBody);

				custJSONArray = new JSONArray(responseBody);
				Log.d(LOG_TAG,"custJSONArray lenght :"+ custJSONArray.length());

				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "getCustomerDetails-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){

					if ( custJSONArray.length() > 0){
						if (custJSONArray.getJSONObject(0).getString("name").toString()!=null) {
							editText_name.setText(custJSONArray.getJSONObject(0).getString("name").toString());
						}
						
						if (custJSONArray.getJSONObject(0).getString("mobile").toString()!=null && 
								!custJSONArray.getJSONObject(0).getString("mobile").toString().isEmpty()) {
							editText_mobile.setText(custJSONArray.getJSONObject(0).getString("mobile").toString());
						}

						if (custJSONArray.getJSONObject(0).getString("country").toString()!=null && 
								!custJSONArray.getJSONObject(0).getString("country").toString().isEmpty()) {
							codeCountry = custJSONArray.getJSONObject(0).getString("country").toString();
						}

						codeLang = custJSONArray.getJSONObject(0).getString("language").toString();
						codeGender = custJSONArray.getJSONObject(0).getString("gender").toString();						
						codeState = custJSONArray.getJSONObject(0).getString("state").toString();
					}else{
						Toast.makeText(Customer_Update.this,getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG).show();
					}

				}else{
					Toast.makeText(Customer_Update.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "getCustomerDetails-Error:" + t.getMessage(), t);
			}
		}

	}

	public void AssignLanguageList (String strList) {
		try{
			Log.d(LOG_TAG,"AssignLanguage"); 
			Spinner spinLanguage = (Spinner) findViewById(R.id.list_language);

			adapter_language = new ArrayAdapter<Collection_List>(this, android.R.layout.simple_spinner_item, model_language);
			adapter_language.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			languageJSONArray = new JSONArray(strList);

			if (languageJSONArray.length()>0) {

				for (int n = 0; n < languageJSONArray.length(); n++) {
					String title = languageJSONArray.getJSONObject(n).getString("language_name").toString();	
					String code1 =languageJSONArray.getJSONObject(n).getString("language_code").toString();

					if (code1.equals(codeLang)) {
						posLang = n;
					}
					Collection_List current_language = new Collection_List();
					current_language.set_title(title);
					current_language.set_code1(code1);
					adapter_language.add(current_language);
				}		
				
				spinLanguage.setAdapter(adapter_language);
				spinLanguage.setSelection(posLang);
				spinLanguage.setOnItemSelectedListener(new language_Selected());

			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "AssignLanguageList-Error:" + t.getMessage(), t); 
		}
		
	}
	
	public class language_Selected implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

			try {
				if (pos > -1) {
					language_name = model_language.get(pos).get_title(); 
					language_code = model_language.get(pos).get_code1();
					Log.d(LOG_TAG, "Language: " + language_code + " " + language_name);

				} else {
					Toast.makeText(parent.getContext(),"Please Choose Language",Toast.LENGTH_LONG).show();
				}

			} catch (Throwable t) {
				Log.e(LOG_TAG, "AssignLanguageList-doInBackground-Error :" + t.getMessage(), t);
			}

		}

		@SuppressWarnings("rawtypes")
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public void AssignGenderList (String strList) {
		try{
			Log.d(LOG_TAG,"AssignGenderList"); 
			Spinner spinGender = (Spinner) findViewById(R.id.list_gender);

			adapter_gender = new ArrayAdapter<Collection_List>(this, android.R.layout.simple_spinner_item, model_gender);
			adapter_gender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			genderJSONArray = new JSONArray(strList);

			if (genderJSONArray.length()>0) {

				for (int n = 0; n < genderJSONArray.length(); n++) {
					String title = genderJSONArray.getJSONObject(n).getString("gender_name").toString();	
					String code1 =genderJSONArray.getJSONObject(n).getString("gender_code").toString();
					if (code1.equals(codeGender)) {
						posGender = n;
					}

					Collection_List current_gender = new Collection_List();
					current_gender.set_title(title);
					current_gender.set_code1(code1);
					adapter_gender.add(current_gender);
				}		
				
				spinGender.setAdapter(adapter_gender);
				spinGender.setSelection(posGender);
				spinGender.setOnItemSelectedListener(new gender_Selected());

			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "AssignGenderList-Error:" + t.getMessage(), t); 
		}
		
	}
	
	public class gender_Selected implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

			try {
				if (pos > -1) {
					gender_name = model_gender.get(pos).get_title(); 
					gender_code = model_gender.get(pos).get_code1();
					Log.d(LOG_TAG, "Gender: " + gender_code + " " + gender_name);

				} else {
					Toast.makeText(parent.getContext(),"Please Choose Gender",Toast.LENGTH_LONG).show();
				}

			} catch (Throwable t) {
				Log.e(LOG_TAG, "AssignGenderList-doInBackground-Error :" + t.getMessage(), t);
			}

		}

		@SuppressWarnings("rawtypes")
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public void AssignCountryList (String strList) {
		try{
			Log.d(LOG_TAG,"AssignCountryList"); 
			Spinner spinCountry = (Spinner) findViewById(R.id.list_country);

			adapter_country = new ArrayAdapter<Collection_List>(this, android.R.layout.simple_spinner_item, model_country);
			adapter_country.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			countryJSONArray = new JSONArray(strList);

			if (countryJSONArray.length()>0) {

				for (int n = 0; n < countryJSONArray.length(); n++) {
					String title = countryJSONArray.getJSONObject(n).getString("country_name").toString();	
					String code1 =countryJSONArray.getJSONObject(n).getString("country_code").toString();
					if (code1.equals(codeCountry)) {
						posCountry = n;
					}

					Collection_List current_country = new Collection_List();
					current_country.set_title(title);
					current_country.set_code1(code1);
					adapter_country.add(current_country);
				}		
				
				spinCountry.setAdapter(adapter_country);
				spinCountry.setSelection(posCountry);
				spinCountry.setOnItemSelectedListener(new country_Selected());

			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "AssignCountryList-Error:" + t.getMessage(), t); 
		}
		
	}
	
	public class country_Selected implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

			try {
				if (pos > -1) {
					country_name = model_country.get(pos).get_title(); 
					country_code = model_country.get(pos).get_code1();
					Log.d(LOG_TAG, "Country: " + country_code + " " + country_name);
					
					codeCountry = country_code;
					AssignStateList (strState_List);

				} else {
					Toast.makeText(parent.getContext(),"Please Choose Country",Toast.LENGTH_LONG).show();
				}

			} catch (Throwable t) {
				Log.e(LOG_TAG, "AssignCountryList-doInBackground-Error :" + t.getMessage(), t);
			}

		}

		@SuppressWarnings("rawtypes")
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}


	public void AssignStateList (String strList) {
		try{
			Log.d(LOG_TAG,"AssignStateList"); 
			Spinner spinState = (Spinner) findViewById(R.id.list_state);
			
			model_state = new ArrayList<Collection_List>();
			adapter_state = new ArrayAdapter<Collection_List>(this, android.R.layout.simple_spinner_item, model_state);
			adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			stateJSONArray = new JSONArray(strList);

			if (stateJSONArray.length()>0) {
				posState = 0;
				int newindex = 0;
				
				for (int n = 0; n < stateJSONArray.length(); n++) {
					String code2 =stateJSONArray.getJSONObject(n).getString("country_code").toString();
					
					if (code2.equals(codeCountry)) {
						String title = stateJSONArray.getJSONObject(n).getString("state_name").toString();	
						String code1 =stateJSONArray.getJSONObject(n).getString("state_code").toString();
						
						if (code1.equals(codeState)) {
							posState = newindex;
						}
	
						Collection_List current_state = new Collection_List();
						current_state.set_title(title);
						current_state.set_code1(code1);
						adapter_state.add(current_state);
						
						newindex++;
					}
				}		
				
				adapter_state.notifyDataSetChanged();
				spinState.setAdapter(adapter_state);
				spinState.setSelection(posState);
				spinState.setOnItemSelectedListener(new state_Selected());

			}
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "AssignStateList-Error:" + t.getMessage(), t); 
		}
		
	}
	
	public class state_Selected implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

			try {
				if (pos > -1) {
					state_name = model_state.get(pos).get_title(); 
					state_code = model_state.get(pos).get_code1();
					Log.d(LOG_TAG, "State: " + pos + " " + state_code + " " + state_name);

				} else {
					Toast.makeText(parent.getContext(),"Please Choose State",Toast.LENGTH_LONG).show();
				}

			} catch (Throwable t) {
				Log.e(LOG_TAG, "AssignStateList-doInBackground-Error :" + t.getMessage(), t);
			}

		}

		@SuppressWarnings("rawtypes")
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}


	//click button submit
	private View.OnClickListener onClickSubmit = new View.OnClickListener() {
		public void onClick(View v) {
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) { // check network

					editText_name.setError(null);
					editText_mobile.setError(null);

					if (editText_name.getText().toString().length()<2) { // check
						editText_name.setError(getResources().getString(R.string.err_name));

					} else if (editText_mobile.getText().toString().length()<6) { // check
						editText_mobile.setError(getResources().getString(R.string.err_telephone));

					} else {
						InputMethodManager imm = (InputMethodManager)getSystemService(
								Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(editText_mobile.getWindowToken(), 0);

						UpdateDetails_Task changepwd = new UpdateDetails_Task();
						changepwd.execute();
					}

				} else {
					Toast.makeText(Customer_Update.this,
							getResources().getString(R.string.MSG_NO_INTERNET),
							Toast.LENGTH_LONG).show();
				}
			} catch (Throwable t) {
				Log.e(LOG_TAG, "onClickSubmit-Error:" + t.getMessage(), t);

			}
		}
	};



	private class UpdateDetails_Task extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Customer_Update.this, "", getResources()
					.getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String status = null;
			String status_desc = null;
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {

					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
					
					String strURL = getResources().getString(R.string.CUST_UPDATE_API)
							+ "db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
							+ "&cs="+URLEncoder.encode(slotAction.getCheckSum(), "UTF-8")
							+ "&name=" + URLEncoder.encode(editText_name.getText().toString(), "UTF-8")
							+ "&mobile=" + URLEncoder.encode(editText_mobile.getText().toString(), "UTF-8")
							+ "&gender=" + URLEncoder.encode(gender_code, "UTF-8")
							+ "&language=" + URLEncoder.encode(language_code, "UTF-8")
							+ "&country=" + URLEncoder.encode(country_code, "UTF-8")
							+ "&state=" + URLEncoder.encode(state_code, "UTF-8");

					//name,mobile,gender,birthday,language,country,state

					HttpPost httppost = new HttpPost(strURL);
					Log.d(LOG_TAG,"strURL:"+ strURL);
					String responseBody = null;
					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					responseBody = httpclient
							.execute(httppost, responseHandler);

					Log.d(LOG_TAG, "responseBody:" + responseBody);

					JSONArray StatusJSONArray = new JSONArray(responseBody);
					Log.d(LOG_TAG, "StatusJSONArray.length():"
							+ StatusJSONArray.length());
					if (StatusJSONArray.length() > 0) {
						status = StatusJSONArray.getJSONObject(0).getString("status").toString();
						status_desc = StatusJSONArray.getJSONObject(0).getString("desc").toString();

					}

				} else {
					status = "1";
					status_desc = getResources().getString(R.string.MSG_NO_INTERNET);
				}
			} catch (Throwable t) {
				status = "-1";
				status_desc = getResources().getString(R.string.please_retry_again);
				Log.e(LOG_TAG, "doInBackground-Error:" + t.getMessage(), t);
			}
			Customer_Update.progressDialog.dismiss();			
			return status + "~" + status_desc;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			String[] arr_result = TextUtils.split(result, "~");

			if (arr_result[0].equals("1")) {
				Toast.makeText(Customer_Update.this, arr_result[1], Toast.LENGTH_LONG).show();
				onBackPressed();

			} else {

				Toast.makeText(Customer_Update.this, arr_result[1], Toast.LENGTH_LONG).show();

			}
			
		}

	}

	// FUNCTION NETWORK

	public boolean isNetworkAvailable() {

		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}


	/**
	 * method is used for checking valid email id format.
	 * 
	 * @param email
	 * @return boolean true for valid false for invalid
	 */
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public void onBackPressed() {

		super.onBackPressed(); 
		this.finish();

	}


	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();
		
		slotAction.close();
		dataProcessing.close();

	}
}
