package com.m3tech.customer;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.background.PageBackground;
import com.m3tech.clock.Plugin_Clock_T1;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.data.Helper_Themeobjectplugin;
import com.m3tech.event.Event_List;
import com.m3tech.header.Header;
import com.m3tech.loyalty.Loyalty;
import com.m3tech.reservation.Reservation_List;
import com.m3tech.theme.Theme_Std;
import com.m3tech.warranty.Warranty_List;
import com.m3tech.weblink.WebLink;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import java.net.URLEncoder;
import java.security.MessageDigest;

/*
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.ProfilePictureView;*/


public class Customer_MainPage extends Activity {

	private static final String LOG_TAG = "CUSTOMER LOGIN PAGE";
	public static final String PREFS_NAME = "MyPrefsFile";
	Context context = this;
	Helper_Themeobject Helper_Themeobject = null;
	LinearLayout layout_login, layout_logoutfb,layout_profile;
	static LinearLayout layout_signin,layout_fblogin,layout_fbimage;
	String home_pagename, themecolor,colorcode, btn_menucolor, tabbar,fbuserID, session_state,bookingStatus, app_user,app_db, udid,
	customerPhone, customerEmail,customerName, customerGender, customerLocale, customerTimezone,customerBirthday,
	data,id_user,status,status_desc,customerID,app_title,loyaltytitle,pageTitle="";
	TextView text_signinwith, text_signin,texttitlebar,texthome,btnchange,btnupdate,btnpoint;
	ImageView page_background,back,msglinepoint,tabheader;
	JSONArray dataJSONArray;
	public static final String[] PERMISSIONS = new String[] {"email","public_profile","user_friends"};

	/*private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};*/
	private SharedPreferences settings;
	//private ProfilePictureView profilePictureView;
	private TextView userNameView;

	Helper_Themeobjectplugin Helper_Themeobjectplugin =null;
	Cursor f;
	String point_title="", customer_point_title="", level_title="";

	SlotAction slotAction = null;
	Header Header = null;
	PageBackground PageBackground = null;
	Customer_Allow_Registration customer_allow_registration = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING "+LOG_TAG);
		setContentView(R.layout.customer_main_page);

		Helper_Themeobjectplugin = new Helper_Themeobjectplugin(this);

		slotAction = new SlotAction(this);
		Helper_Themeobject = new Helper_Themeobject(this);
		customer_allow_registration = new Customer_Allow_Registration(this);
		settings = getSharedPreferences(PREFS_NAME, 0);
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		customerID = settings.getString("customerID", "");
		udid = settings.getString("udid", "");
		home_pagename = settings.getString("home_pagename", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		pageTitle = settings.getString("customerPageTitle", "");

		session_state = settings.getString("session_state", "");
		fbuserID = settings.getString("fbuserID", "");
		customerName = settings.getString("customerName", "");

		Log.d(LOG_TAG, "app_user=" + app_user);
		Log.d(LOG_TAG, "session_state=" + session_state);
		Log.d(LOG_TAG, "fbuserID=" + fbuserID);
		Log.d(LOG_TAG, "customerID=" + customerID);
		Log.d(LOG_TAG, "customerName=" + customerName);

		if(pageTitle ==null||pageTitle.equals("")){
			pageTitle = app_title;
		}

		layout_profile = (LinearLayout) findViewById(R.id.layout_profile);
		layout_fblogin = (LinearLayout) findViewById(R.id.layout_fblogin);
		layout_signin = (LinearLayout) findViewById(R.id.layout_signin);
		layout_signin.setOnClickListener(onClickSignMyapp);
		layout_login= (LinearLayout) findViewById(R.id.layout_login);
		text_signin = (TextView) findViewById(R.id.text_signin);
		text_signinwith = (TextView) findViewById(R.id.text_signinwith);
		layout_fbimage = (LinearLayout) findViewById(R.id.layout_fbimage);
		page_background = (ImageView) findViewById(R.id.page_background);

		// Find the user's profile picture custom view
		//profilePictureView = (ProfilePictureView) findViewById(R.id.selection_profile_pic);
		//profilePictureView.setCropped(true);

		/*** layout linear declaration *****/
		tabheader=(ImageView)findViewById(R.id.tabheader);

		/*** text view *******/
		texttitlebar=(TextView)findViewById(R.id.texttitlebar);

		/**** home & back button *****/
		LinearLayout tabback=(LinearLayout)findViewById(R.id.tabback);
		LinearLayout tabhome=(LinearLayout)findViewById(R.id.tabhome);
		ImageView img_back=(ImageView)findViewById(R.id.back);
		ImageView img_home=(ImageView)findViewById(R.id.home);

		/*** Setting Header ***/
		Header=new Header(context,tabheader,tabback,tabhome,img_back,img_home,texttitlebar,pageTitle,"");

		/*** Setting Page Background ***/
		PageBackground=new PageBackground(context,page_background);

		TextView btnlogout=(TextView)findViewById(R.id.btnlogout);
		btnlogout.setOnClickListener(onBtnLogout);

		TextView btninbox=(TextView)findViewById(R.id.btninbox);
		btninbox.setOnClickListener(onBtnInbox);

		btnchange=(TextView)findViewById(R.id.btnchange);
		btnchange.setOnClickListener(onBtnChange);

		btnupdate=(TextView)findViewById(R.id.btnupdate);
		btnupdate.setOnClickListener(onBtnUpdate);

		btnpoint=(TextView)findViewById(R.id.btnpoint);
		btnpoint.setOnClickListener(onBtnPoint);

		msglinepoint=(ImageView)findViewById(R.id.msglinepoint);

		GetPointsDetails Task=new GetPointsDetails();
		Task.execute(new String[]{});

		viewLoginLayout();

		/*uiHelper=new UiLifecycleHelper(this,callback);
		uiHelper.onCreate(savedInstanceState);
		LoginButton fb_authButton=(LoginButton)findViewById(R.id.fb_authButton);
		fb_authButton.setReadPermissions(Arrays.asList(PERMISSIONS));

		if(customer_allow_registration.onResultAllowRegistration().equals("0")){
		fb_authButton.setVisibility(View.INVISIBLE);
		finish();
		Intent i=new Intent(Customer_MainPage.this,Customer_Login.

class);
			startActivity(i);
			
		}else{
			fb_authButton.setVisibility(View.VISIBLE);
		}*/

	}

	private void viewLoginLayout () {
		// Find the user's name view
		userNameView = (TextView) findViewById(R.id.txtUserName);
		customerID = settings.getString("customerID", "");
		customerName = settings.getString("customerName", "");

		if (customerID.length()>0) {
			
			String afterLogin="";
			afterLogin = settings.getString("afterLogin", "");
			if(afterLogin.equals("html5c")){
				
				String p1 = settings.getString("customerPageTitle", "");
				String p2 = settings.getString("html5c_url", "");
				
				if (!p2.startsWith("http://") && !p2.startsWith("https://")){
					p2 = "http://" + p2;
				}
					
				Intent i=new Intent(context, WebLink.class);
				i.putExtra("pagetitle", p1);
				i.putExtra("url", p2);
				i.putExtra("openweb", "no");
				context.startActivity(i);
				finish();
			}else if(afterLogin.equals("Theme_Std")){
				//finish();
			}else{
				//finish();
			}
		

			
			text_signinwith.setVisibility(View.VISIBLE);
			// Set the Textview's text to the user's name.
			userNameView.setText(customerName);
			userNameView.setVisibility(View.VISIBLE);

			layout_fblogin.setVisibility(View.GONE);
			layout_profile.setVisibility(View.VISIBLE);
			
			if(fbuserID.equals("")){	
				
				layout_fbimage.setVisibility(View.GONE);
				layout_signin.setVisibility(View.GONE);
				text_signin.setVisibility(View.GONE);

				SharedPreferences.Editor editor = settings.edit();
				editor.remove("fbuserID");	
				editor.commit();

			} else {

				if (session_state.equals("OPENED")) {
					layout_signin.setVisibility(View.GONE);
					text_signin.setVisibility(View.GONE);
					//profilePictureView.setProfileId(fbuserID);

				} else {
					layout_fbimage.setVisibility(View.GONE);
					layout_signin.setVisibility(View.GONE);
					text_signin.setVisibility(View.GONE);
				}
			}
			
			
		} else {
			layout_profile.setVisibility(View.GONE);
		}
	}

	/*private Session createSession(String taskvar) {
		Log.d(LOG_TAG, "createSession..");

		Session activeSession = Session.getActiveSession();
		if (activeSession == null || activeSession.getState().isClosed() || taskvar.equals("logout")) {
			String app_id = getResources().getString(R.string.fbapp_id);
			activeSession = new Session.Builder(this).setApplicationId(app_id).build();
			Session.setActiveSession(activeSession);
			
			if (taskvar.equals("logout")) {
				activeSession.closeAndClearTokenInformation();
				layout_fblogin.setVisibility(View.VISIBLE);		
			} 

		}
		return activeSession;
	}*/

	/*private void onSessionStateChange(Session session, SessionState state, Exception exception) {

		final SharedPreferences.Editor editor = settings.edit();
		session_state = String.valueOf(state);
		editor.putString("session_state",session_state);
		Log.w(LOG_TAG, "session_state: "+session_state);

		editor.commit();

		if (exception != null) {
			Log.e(LOG_TAG, "ERROR: "+exception.getMessage());
			session = createSession("");
		}

		else if (session != null && session.isOpened()) {
			Log.d(LOG_TAG, "FACEBOOK session is open ");

			layout_signin.setVisibility(View.GONE);
			text_signin.setVisibility(View.GONE);
			text_signinwith.setVisibility(View.VISIBLE);
			Log.d(LOG_TAG, "Access Token = " + session.getAccessToken());
			
			profilePictureView.setProfileId(fbuserID);
			// Set the Textview's text to the user's name.
			userNameView.setText(customerName);

			// get_regist_data();
			// user is already login show
			try {
				Session.OpenRequest request = new Session.OpenRequest(this);
				request.setPermissions(Arrays.asList("email", "publish_actions"));
			}

			catch (Exception e) {
				e.printStackTrace();
			}

			// make request to the /me API
			Request.newMeRequest(session, 
					//Request.executeMeRequestAsync(session,
					new Request.GraphUserCallback() {
				// callback after Graph API response with user object
				@Override
				public void onCompleted(GraphUser user, Response response) {

					if (user != null) {

						//Log.d(LOG_TAG,"Response = "+ response.getRawResponse());

						try {

							customerPhone = "";
							customerBirthday = "";
							customerGender = "";
							customerLocale = "";
							customerTimezone = "";


							profilePictureView.setProfileId(user.getId());
							// Set the Textview's text to the user's name.
							userNameView.setText(user.getName());
							fbuserID=user.getId();
							customerName = user.getName();


							if (user.asMap().get("email") != null) {
								customerEmail = user.asMap().get("email").toString();
							}

							if (user.asMap().get("gender") != null) {
								customerGender = user.asMap().get("gender").toString();
							}

							if (user.asMap().get("locale") != null) {
								customerLocale = user.asMap().get("locale").toString();
							}

							if (user.asMap().get("timezone") != null) {
								customerTimezone = user.asMap().get("timezone").toString();
							}

							if (user.getBirthday() != null) {
								customerBirthday = user.getBirthday();                   
							}                          

							Log.d(LOG_TAG,"customerName="+ customerName);
							Log.d(LOG_TAG,"customerEmail="+ customerEmail);
							Log.d(LOG_TAG,"customerGender="+ customerGender);

							SharedPreferences.Editor editor = settings.edit();
							editor.remove("fbuserID");
							editor.remove("customerName");
							editor.remove("customerPhone");
							editor.remove("customerEmail");
							editor.putString("fbuserID",fbuserID);
							editor.putString("customerName",customerName);
							editor.putString("customerPhone",customerPhone);
							editor.putString("customerEmail",customerEmail);
							editor.commit();

							if (customerEmail != null && customerEmail.length()>0) {
								updateFBUserDetail Task = new updateFBUserDetail();
								Task.execute(new String[] { });
							} else {
								getUserEmail();
							}

						}
						catch (Exception e) {
							Log.e(LOG_TAG,"FB data error");
							e.printStackTrace();

						}

					} else {
						if (isNetworkAvailable()) { // check network
							//Log.e(LOG_TAG,"FB LOGIN error="+ response.getRawResponse().toString());
							updateFBError Task = new updateFBError();
							Task.execute(new String[] { response.getRequest().toString() });
						} else {
							viewSignIn(false);
							layout_fblogin.setVisibility(View.GONE);
							Toast.makeText(Customer_MainPage.this,
									getResources().getString(R.string.MSG_NO_INTERNET),
									Toast.LENGTH_SHORT).show();
						}
					}
				}
			}).executeAsync();
			
			
		}else if (state == SessionState.CLOSED_LOGIN_FAILED){

			Log.e(LOG_TAG,"FB STATE="+ String.valueOf(state));
			session = createSession("");
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.login_failed), Toast.LENGTH_SHORT).show();
			updateFBError Task = new updateFBError();
			Task.execute(new String[] { String.valueOf(state) });

		}else{

			Log.w(LOG_TAG,"FB STATE="+ String.valueOf(state));
			viewSignIn(true);
		}


	}*/

	public void getUserEmail() {

		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle(getResources().getString(R.string.registration));
		alert.setMessage(getResources().getString(R.string.fbemail));

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				customerEmail = input.getText().toString();
				if (customerEmail != null && customerEmail.length()>0) {
					SharedPreferences.Editor editor = settings.edit();
					editor.remove("customerEmail");
					editor.putString("customerEmail",customerEmail);
					editor.commit();

					updateFBUserDetail Task = new updateFBUserDetail();
					Task.execute(new String[] { });
				} else {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.fbemail_failed), Toast.LENGTH_SHORT).show();
				}
			}
		});

		alert.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.fbemail_failed), Toast.LENGTH_SHORT).show();
			}
		});

		alert.show();

	}

	@Override
	public void onResume() {
		super.onResume();
		//uiHelper.onResume();
		viewLoginLayout();
	}

	@Override
	public void onPause() {
		super.onPause();
		//uiHelper.onPause();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		//uiHelper.onSaveInstanceState(outState);
	}


	public class updateFBUserDetail extends AsyncTask<String, Void, String> {
		JSONArray StatusJSONArray;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			Log.w(LOG_TAG, "updateFBUserDetail - doInBackground"); 
			String response = null;
			try{

				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

				String strURL = getResources().getString(R.string.FB_REGIST_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&cs="+URLEncoder.encode(slotAction.getCheckSum(), "UTF-8")
						+ "&email="+ URLEncoder.encode(customerEmail, "UTF-8")
						+ "&phone="+ URLEncoder.encode(customerPhone, "UTF-8")
						+ "&name=" +  URLEncoder.encode(customerName, "UTF-8")
						+ "&gender=" +  URLEncoder.encode(customerGender, "UTF-8")
						+ "&locale=" +  URLEncoder.encode(customerLocale, "UTF-8")
						+ "&tzone=" +  URLEncoder.encode(customerTimezone, "UTF-8")
						+ "&bday=" +  URLEncoder.encode(customerBirthday, "UTF-8")
						+ "&fbid=" +  URLEncoder.encode(fbuserID, "UTF-8")
						+ "&tag=FB";			


				HttpPost httppost = new HttpPost(strURL);
				Log.d(LOG_TAG,"FBDetails-url:"+ strURL);

				String responseBody = null;
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				responseBody = httpclient.execute(httppost, responseHandler);

				Log.d(LOG_TAG, "FBDetails-responseBody:" + responseBody);

				StatusJSONArray = new JSONArray(responseBody);

				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "updateFBUserDetail-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				if (result.equals("SUCCESS")) {
					if (StatusJSONArray.length() > 0) {
						status = StatusJSONArray.getJSONObject(0).getString("status").toString();
						status_desc = StatusJSONArray.getJSONObject(0).getString("desc").toString();
						customerID = StatusJSONArray.getJSONObject(0).getString("customer_id").toString();
						Log.d(LOG_TAG, "customerID:" + customerID);
					}


					SharedPreferences.Editor editor = settings.edit();
					editor.remove("customerID");
					editor.putString("customerID",customerID);
					editor.commit();

					String afterLogin="";
					afterLogin = settings.getString("afterLogin", "");

					if(status.equals("1") || status.equals("2")){

						if(afterLogin.equals("Loyalty")){

							Intent i=new Intent(Customer_MainPage.this, Loyalty.class);
							startActivity(i);
							finish();

						}else if(afterLogin.equals("Warranty")){

							Intent i=new Intent(Customer_MainPage.this, Warranty_List.class);
							startActivity(i);
							finish();

						}else if(afterLogin.equals("Reservation")){

							String topid_reservation = settings.getString("topid_reservation", "");
							Intent i=new Intent(Customer_MainPage.this, Reservation_List.class);
							i.putExtra("topid_reservation",topid_reservation);
							startActivity(i);
							finish();
							
						}else if(afterLogin.equals("Event")){

							String topid_event = settings.getString("topid_event", "");
							Intent i=new Intent(Customer_MainPage.this, Event_List.class);
							i.putExtra("topid_event",topid_event);
							startActivity(i);
							finish();
	
						}else if(afterLogin.equals("Comment")){
						
							String topid_comment = settings.getString("topid_comment", "");
							slotAction.CreatePluginPage(topid_comment, "content","","","");
							finish();

						}else if(afterLogin.equals("Favorite")){
							
							String topid_favorite = settings.getString("topid_favorite", "");
							slotAction.CreatePluginPage(topid_favorite, "content","","","");
							finish();

						}else if(afterLogin.equals("html5c")){
							
							String p1 = settings.getString("customerPageTitle", "");
							String p2 = settings.getString("html5c_url", "");
							
							if (!p2.startsWith("http://") && !p2.startsWith("https://")){
								p2 = "http://" + p2;
							}
								
							Intent i=new Intent(context, WebLink.class);
							i.putExtra("pagetitle", p1);
							i.putExtra("url", p2);
							i.putExtra("openweb", "no");
							context.startActivity(i);
							finish();
							
						}else if(afterLogin.equals("Theme_Std")){
							
							String themecode = settings.getString("themecode", "");
							String themepageid = settings.getString("themepageid", "");
							String noback = settings.getString("themepageid", "");
							String pagetitle = settings.getString("pagetitle", "");
							
							SharedPreferences.Editor editor_ = settings.edit();
							editor_.remove("themecode");
							editor_.remove("themepageid");
							editor_.remove("noback");
							editor_.remove("pagetitle");
							editor_.commit();
							
							finish();
							Intent i=new Intent(Customer_MainPage.this, Theme_Std.class);
							i.putExtra("themecode", themecode);
							i.putExtra("themepageid", themepageid);
							i.putExtra("noback", noback);
							i.putExtra("pagetitle", pagetitle);
							startActivity(i);
							
						}else if(afterLogin.equals("clock")){
							Intent i=new Intent(Customer_MainPage.this, Plugin_Clock_T1.class);
							startActivity(i);
							finish();
						}
						
						editor.remove("html5c_url");
						editor.remove("afterLogin");
						editor.putString("afterLogin","None");
						editor.commit();

						//Toast.makeText(getApplicationContext(), getResources().getString(R.string.login_success), Toast.LENGTH_SHORT).show();
					}else{
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.login_failed), Toast.LENGTH_SHORT).show(); 
					}
					
					viewLoginLayout();
					
				}else{
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.login_failed), Toast.LENGTH_SHORT).show(); 
				}
			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "updateFBUserDetail-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}


	public class updateFBError extends AsyncTask<String, Void, String> {
		String errstr;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			Log.w(LOG_TAG, "updateFBError - doInBackground"); 
			String response = null;
			errstr = arg0[0];
			
			try{

				String hashKey="";
				String packagename = getApplicationContext().getPackageName();
				PackageInfo info = getPackageManager().getPackageInfo(packagename, PackageManager.GET_SIGNATURES);
				for (Signature signature : info.signatures) {
					MessageDigest md = MessageDigest.getInstance("SHA");
					md.update(signature.toByteArray());
					hashKey = Base64.encodeToString(md.digest(), Base64.DEFAULT);
					Log.e("My Key", "MY KEY HASH: "+hashKey);
				}


				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

				String strURL = getResources().getString(R.string.FB_REGIST_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&cs="+URLEncoder.encode(slotAction.getCheckSum(), "UTF-8")
						+ "&email="+ URLEncoder.encode(packagename+"|"+fbuserID, "UTF-8")
						+ "&phone="+ URLEncoder.encode(hashKey, "UTF-8")
						+ "&err="+ URLEncoder.encode("error: "+errstr, "UTF-8")
						+ "&tag=FB";			


				HttpPost httppost = new HttpPost(strURL);

				String responseBody = null;
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				responseBody = httpclient.execute(httppost, responseHandler);
				Log.d(LOG_TAG, "updateFBError-responseBody:" + responseBody);
				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "updateFBError-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				viewSignIn(true);
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.login_failed), Toast.LENGTH_SHORT).show();
			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "updateFBError-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}

	public void viewSignIn(boolean logout) {
		if (logout) {
			SharedPreferences.Editor editor = settings.edit();
			editor.remove("fbuserID");	
			editor.remove("customerID");
			editor.commit();
		}
		//profilePictureView.setProfileId("");
		userNameView.setText("");
		layout_signin.setVisibility(View.VISIBLE);
		text_signin.setVisibility(View.VISIBLE);
		text_signinwith.setVisibility(View.GONE);	
	}

	
	public class GetPointsDetails extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			Log.w(LOG_TAG, "GetPointsDetails - doInBackground"); 
			String response = null;
			
			try{
				f = Helper_Themeobjectplugin.getByPluginType("points"); //get object plugin data base on action value
				if (f.moveToLast() != false){
					point_title = Helper_Themeobjectplugin.getplugin_value1(f).toString();
					customer_point_title = Helper_Themeobjectplugin.getplugin_value2(f).toString();
					level_title = Helper_Themeobjectplugin.getplugin_value3(f).toString();
					
					Log.d(LOG_TAG, "point_title="+point_title);
					Log.d(LOG_TAG, "customer_point_title="+customer_point_title);
					Log.d(LOG_TAG, "level_title="+level_title);

					response="SUCCESS";
				} else {
					response="FAILED";					
				}
				f.close();

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetPointsDetails-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				//TODO
				if (result.equals("SUCCESS")) {
					btnpoint.setText(point_title);
					btnpoint.setVisibility(View.VISIBLE);
					msglinepoint.setVisibility(View.VISIBLE);
				}
			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "GetPointsDetails-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}
	
	private View.OnClickListener onBtnLogout = new View.OnClickListener() {
		public void onClick(View v) {
			try {

				SharedPreferences.Editor editor = settings.edit();
				editor.remove("customerID");
				editor.commit();

				//Session session = createSession("logout");
				//Log.d(LOG_TAG, String.valueOf(session));
				
				layout_fbimage.setVisibility(View.VISIBLE);
				layout_signin.setVisibility(View.VISIBLE);
				text_signin.setVisibility(View.VISIBLE);
				text_signinwith.setVisibility(View.GONE);
				layout_profile.setVisibility(View.GONE);

				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_left);
			} catch (Throwable t) {
				Log.e(LOG_TAG, "onBtnLogout-Error:" + t.getMessage(), t);

			}
		}

	};

	private View.OnClickListener onBtnInbox = new View.OnClickListener() {
		public void onClick(View v) {
			try {
				SharedPreferences.Editor editor = settings.edit();
				editor.remove("InboxTitle");
				editor.putString("InboxTitle", getResources().getString(R.string.inbox));
				editor.remove("InboxTop");
				editor.putString("InboxTop", "0");
				editor.commit();

				Intent i = new Intent(Customer_MainPage.this, Customer_Inbox.class);
				i.putExtra("noback", "no");
				startActivity(i);

			} catch (Throwable t) {
				Log.e(LOG_TAG, "onBtnInbox-Error:" + t.getMessage(), t);

			}
		}

	};

	private View.OnClickListener onBtnChange = new View.OnClickListener() {
		public void onClick(View v) {
			try {
				Intent i = new Intent(Customer_MainPage.this, Customer_ChangePwd.class);
				i.putExtra("noback", "no");
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

			} catch (Throwable t) {
				Log.e(LOG_TAG, "onBtnChange-Error:" + t.getMessage(), t);

			}
		}
	};

	private View.OnClickListener onBtnUpdate = new View.OnClickListener() {
		public void onClick(View v) {
			try {
				Intent i = new Intent(Customer_MainPage.this, Customer_Update.class);
				i.putExtra("noback", "no");
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

			} catch (Throwable t) {
				Log.e(LOG_TAG, "onBtnUpdate-Error:" + t.getMessage(), t);

			}
		}
	};

	private View.OnClickListener onBtnPoint = new View.OnClickListener() {
		public void onClick(View v) {
			try {
				Intent i = new Intent(Customer_MainPage.this, Customer_Point.class);
				i.putExtra("noback", "no");
				i.putExtra("point_title", point_title);
				i.putExtra("customer_point_title", customer_point_title);
				i.putExtra("level_title", level_title);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

			} catch (Throwable t) {
				Log.e(LOG_TAG, "onBtnPoint-Error:" + t.getMessage(), t);

			}
		}

	};

	private View.OnClickListener onClickSignMyapp = new View.OnClickListener() {
		public void onClick(View v) {
			try {
				finish();
				Intent i = new Intent(Customer_MainPage.this, Customer_Login.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				
			} catch (Throwable t) {
				Log.e(LOG_TAG, "onClickSignMyapp-Error:" + t.getMessage(), t);

			}
		}

	};

	// FUNCTION NETWORK

	public boolean isNetworkAvailable() {

		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	// Function onDestroy
	@Override
	public void onDestroy() {
		super.onDestroy();
		//uiHelper.onDestroy();
		Helper_Themeobjectplugin.close();

		this.finish();

		Log.d(LOG_TAG, "onDestroy()");
	}

	public void onBackPressed() {

		String afterLogin="";
		afterLogin = settings.getString("afterLogin", "");
		
		if(afterLogin.equals("Theme_Std")){
			this.finish();
			/*Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);*/
			
		}else{
			super.onBackPressed(); // allows standard use of backbutton for page 1
			this.finish();
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
		}
	}

}
