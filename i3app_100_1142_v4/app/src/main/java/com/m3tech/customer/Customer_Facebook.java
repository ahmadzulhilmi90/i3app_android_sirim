package com.m3tech.customer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.m3tech.app_100_1474.R;
import com.m3tech.background.PageBackground;
import com.m3tech.header.Header;

public class Customer_Facebook extends Activity {

	private static final String LOG_TAG = "CUSTOMER FBUSER DETAIL";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	SharedPreferences settings;
	String app_user,app_title,app_db,themecolor,home_pagename,colorcode,udid,cust_mail;
	LinearLayout layout_textemail,layout_editemail,layout_textphone,layout_editPhone;
	ImageView page_background,tabheader;
	TextView texttitlebar,textTitle;
	Header Header = null;
	Context context = this;
	PageBackground PageBackground = null;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING "+LOG_TAG);
		setContentView(R.layout.customer_facebook);

		settings = this.getSharedPreferences(PREFS_NAME, 0);
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		home_pagename = settings.getString("home_pagename", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");

		Intent j = getIntent();
		cust_mail = j.getStringExtra("cust_mail");
		Log.d(LOG_TAG,"cust_mail="+ cust_mail);


		/*** layout linear declaration *****/

		tabheader = (ImageView) findViewById(R.id.tabheader);

		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		
		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, app_title.toUpperCase(),"");
		
		/*** Setting Page Background ***/
		page_background = (ImageView) findViewById(R.id.page_background);
		PageBackground = new PageBackground(context,page_background);

		textTitle = (TextView) findViewById(R.id.textTitle);
		layout_textemail = (LinearLayout) findViewById(R.id.layout_textemail);
		layout_editemail = (LinearLayout) findViewById(R.id.layout_editemail);
		layout_textphone = (LinearLayout) findViewById(R.id.layout_textphone);
		layout_editPhone = (LinearLayout) findViewById(R.id.layout_editPhone);

		if(!cust_mail.isEmpty()){

			layout_textemail.setVisibility(View.GONE);
			layout_editemail.setVisibility(View.GONE);
			textTitle.setText("Please enter your mobile number to proceed.");

		}else{
			textTitle.setText("Please enter your email address and mobile number to proceed.");
		}

	}



}
