package com.m3tech.customer;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.background.PageBackground;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;
import com.m3tech.web.WebBrowser;

public class Customer_Register extends Activity {

	private String LOG_TAG = "REGISTER";
	LinearLayout layout_forgotpass, layout_whatismyapp;
	TextView txtwhatismyapp;
	public static final String PREFS_NAME = "MyPrefsFile";
	String home_pagename, layout,page, themecolor, btn_menucolor, tabbar, userid, currentdate,
			filename,customerid,home_themecode,app_title,colorcode,app_user,app_db,udid,app_tnc;
	Button btnsubmit,btnshow;
	EditText editTextemail, editTextname, editTextPass, editTextPass2, editTextPhone;
	static ProgressDialog progressDialog;
	SharedPreferences settings;
	TextView texttitlebar,texthome,agreeto;
	ImageView page_background,back,tabheader;
	Helper_Themeobject Helper_Themeobject = null;
	Context context = this;
	SlotAction slotAction = null;
	Header Header = null;
	PageBackground PageBackground = null;

	AlertDialog levelDialog = null;
	//final CharSequence[] items = {" Register another email "," Reset password  "};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING "+LOG_TAG);
		setContentView(R.layout.customer_register);
		slotAction = new SlotAction(this);
		Helper_Themeobject = new Helper_Themeobject(this);

		settings = this.getSharedPreferences(PREFS_NAME, 0);
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");

		home_themecode = settings.getString("home_themecode", "");
		home_pagename = settings.getString("home_pagename", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		app_tnc = settings.getString("app_tnc", "");
		Log.d(LOG_TAG,"home_themecode="+ home_themecode);


		/*** layout linear declaration *****/

		tabheader = (ImageView) findViewById(R.id.tabheader);

		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		String title_header = getResources().getString(R.string.registration);

		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, title_header,"");
		
		/*** Setting Page Background ***/
		page_background = (ImageView) findViewById(R.id.page_background);
		PageBackground = new PageBackground(context,page_background);

		agreeto = (TextView) findViewById(R.id.agreeto);
		agreeto.setText(Html.fromHtml(getString(R.string.agreeto_tnc)));
		agreeto.setOnClickListener(onclickTnC);

		btnsubmit = (Button) findViewById(R.id.btnsubmit);
		btnsubmit.setOnClickListener(onRegister);

		btnshow = (Button) findViewById(R.id.btnshow);
		btnshow.setOnClickListener(onShowHide);

		editTextemail = (EditText) findViewById(R.id.editTextemail);
		editTextname = (EditText) findViewById(R.id.editTextname);
		editTextPass = (EditText) findViewById(R.id.editTextPass);
		editTextPass2 = (EditText) findViewById(R.id.editTextPass2);
		editTextPhone = (EditText) findViewById(R.id.editTextPhone);

		Date d = new Date();
		currentdate = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(d);
		Log.d(LOG_TAG, "currentdate=" + currentdate);
	}

	private View.OnClickListener onShowHide=new View.OnClickListener() {
		public void onClick(View v) {
			try{
				String cur_state = btnshow.getText().toString();
				if (cur_state.equals(getResources().getString(R.string.show))) {
					editTextPass.setTransformationMethod(null);
					btnshow.setText(getResources().getString(R.string.hide));
				} else {
					editTextPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
					btnshow.setText(getResources().getString(R.string.show));					
				}

			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onShowHide-Error:" + t.getMessage(), t); 

			}
		}

	};

	private View.OnClickListener onclickTnC=new View.OnClickListener() {
		public void onClick(View v) {
			try{
				if (!app_tnc.startsWith("http://") && !app_tnc.startsWith("https://")){
					app_tnc = "http://" + app_tnc;
				}

				Intent i=new Intent(getApplicationContext(), WebBrowser.class);
				i.putExtra("url", app_tnc);
				i.putExtra("openweb", "no");
				startActivity(i);
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onclickTnC-Error:" + t.getMessage(), t); 

			}
		}

	};



	private View.OnClickListener onRegister = new View.OnClickListener() {
		public void onClick(View v) {
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) { // check network

					if (editTextname.getText().toString().trim().equals("")) { // check
						// name
						editTextname.setError(getResources().getString(R.string.err_name));
						// You can Toast a message here that the Username is
						// Empty
					} else {
						editTextname.setError(null);
						
						if (editTextemail.getText().toString().trim().equals("")) { // check email
							editTextemail.setError(getResources().getString(R.string.err_email));

						} else {
							editTextemail.setError(null);
							
							if (isEmailValid(editTextemail.getText().toString()) == false) { // check valid email format
								editTextemail.setError(getResources().getString(R.string.err_invalid_email));

							} else {
								editTextemail.setError(null);
								
								if (editTextPass.getText().toString().trim().length()<6) { // check password
									editTextPass.setError(getResources().getString(R.string.err_password));
								} else {
									if (!editTextPass.getText().toString().trim().equals(editTextPass2.getText().toString().trim())) { // check password
										editTextPass2.setError(getResources().getString(R.string.err_re_password));
									} else {
										//finish();
										editTextPass.setError(null);
										InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
										imm.hideSoftInputFromWindow(editTextPass.getWindowToken(), 0);
										Register_Task Login = new Register_Task();
										Login.execute();
									}
								}

							}
						}
					}

				} else {
					Toast.makeText(Customer_Register.this,
							getResources().getString(R.string.MSG_NO_INTERNET),
							Toast.LENGTH_LONG).show();
				}
			} catch (Throwable t) {
				Log.e(LOG_TAG, "onLogin-Error:" + t.getMessage(), t);

			}
		}
	};

	private class Register_Task extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Customer_Register.this, "",
					getResources().getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String status = null;
			String status_desc = null;
			String customer_id = null;
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {


					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

					String strURL = getResources().getString(R.string.CUST_REGIST_API)
							+ "db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&name=" + URLEncoder.encode(editTextname.getText().toString(), "UTF-8")
							+ "&phone=" + URLEncoder.encode(editTextPhone.getText().toString(), "UTF-8")
							+ "&email=" + URLEncoder.encode(editTextemail.getText().toString(), "UTF-8")
							+ "&cdate=" + URLEncoder.encode(currentdate, "UTF-8")
							+ "&password=" + URLEncoder.encode(editTextPass.getText().toString(), "UTF-8")
							+ "&tag=APPS";

					
					HttpPost httppost = new HttpPost(strURL);
					Log.d(LOG_TAG, "onLogin-url:" + strURL);
					String responseBody = null;
					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					responseBody = httpclient.execute(httppost, responseHandler);

					Log.d(LOG_TAG, "onLogin-responseBody:" + responseBody);

					JSONArray StatusJSONArray = new JSONArray(responseBody);
					Log.d(LOG_TAG, "StatusJSONArray.length():"
							+ StatusJSONArray.length());
					if (StatusJSONArray.length() > 0) {
						status = StatusJSONArray.getJSONObject(0).getString("status").toString();
						status_desc = StatusJSONArray.getJSONObject(0).getString("desc").toString();
						customer_id = StatusJSONArray.getJSONObject(0).getString("customer_id").toString();
						Log.d(LOG_TAG, "customer_id:"+ customer_id);

					}

					Customer_Register.progressDialog.dismiss();

				} else {
					status = "4";
					status_desc = getResources().getString(
							R.string.MSG_NO_INTERNET);

				}


			} catch (Throwable t) {
				status = "-1";
				status_desc = getResources().getString(
						R.string.please_retry_again);
				Log.e(LOG_TAG, "doInBackground-Error:" + t.getMessage(), t);
			}
			return status + "~" + status_desc;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			AlertDialog.Builder alert;
			final String[] arr_result = TextUtils.split(result, "~");

			try{
				String toastmsg = "";
				String resultmsg = "";
				
				Customer_Register.progressDialog.dismiss();

				if (arr_result[0].equals("1")) {
					toastmsg = getResources().getString(R.string.register_success);
					resultmsg = getResources().getString(R.string.success);
				}else if (arr_result[0].equals("2")) {
					toastmsg = getResources().getString(R.string.register_failed);
					resultmsg = getResources().getString(R.string.error);
				}
				else if (arr_result[0].equals("4")) {
					toastmsg = getResources().getString(R.string.MSG_NO_INTERNET);
					resultmsg = getResources().getString(R.string.error);
				} else if (arr_result[0].equals("0")) {
					toastmsg = getResources().getString(R.string.register_failed);
					resultmsg = getResources().getString(R.string.error);
				} else {				
					toastmsg = getResources().getString(R.string.error_msg);
					resultmsg = getResources().getString(R.string.error);			
				} 

				alert = new AlertDialog.Builder(Customer_Register.this);
				
				alert.setTitle(resultmsg);
				alert.setIcon(R.drawable.icon90);
				alert.setMessage(toastmsg);

				alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						if (arr_result[0].equals("1")) {
							finish();
						} else {
							dialog.cancel();
							
						}
						
					}

				});
				
				alert.show();   

			}
			
			catch (Throwable t) {
				Log.e(LOG_TAG, "onregister-Error:" + t.getMessage(), t);
				Customer_Register.progressDialog.dismiss();
			}

		}

	}

	

	/**
	 * method is used for checking valid email id format.
	 * 
	 * @param email
	 * @return boolean true for valid false for invalid
	 */
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public static int getResourceId(Context context, String name,
			String resourceType) { // this will returns you Drawable with that
		// String Variable.
		return context.getResources().getIdentifier(name, resourceType,
				context.getPackageName());
	}

	// FUNCTION NETWORK

	public boolean isNetworkAvailable() {

		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	// Function onDestroy
	@Override
	public void onDestroy() {
		super.onDestroy();

		this.finish();

		Log.d(LOG_TAG, "onDestroy()");
	}

	@Override
	public void onBackPressed() {

		String afterLogin="";
		afterLogin = settings.getString("afterLogin", "");
		if(afterLogin.equals("Theme_Std")){
			finish();
			Intent i = new Intent(Customer_Register.this, Customer_MainPage.class);
			startActivity(i);
			//overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		}else{
			super.onBackPressed(); 
			this.finish();
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
		}
	}

}
