package com.m3tech.customer;


import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.background.PageBackground;
import com.m3tech.clock.Plugin_Clock_T1;
import com.m3tech.comments.CommentList;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.event.Event_List;
import com.m3tech.header.Header;
import com.m3tech.loyalty.Loyalty;
import com.m3tech.reservation.Reservation_List;
import com.m3tech.theme.Theme_Std;
import com.m3tech.warranty.Warranty_List;
import com.m3tech.weblink.WebLink;

public class Customer_Login extends Activity {


	private static final String LOG_TAG = "LOGIN PAGE";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	Context context = this;
	Cursor a,b,c,d,e,g,f,h;
	String pageTitle,home_pagename,app_title,themecolor,colorcode,customerID,app_user,app_db,udid,background;
	ImageView tabheader;
	TextView texttitlebar,texthome,txtforgotpss,txtregister;
	SharedPreferences settings;
	ImageView page_background,back;
	Button btnlogin;
	static ProgressDialog progressDialog;
	EditText editTextemail,editTextPass;
	Helper_Themeobject Helper_Themeobject = null;
	SlotAction slotAction = null;
	Header Header = null;
	Customer_Allow_Registration customer_allow_registration = null;
	PageBackground PageBackground = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING "+LOG_TAG);
		setContentView(R.layout.customer_login);

		slotAction = new SlotAction(this);
		customer_allow_registration = new Customer_Allow_Registration(this);
		Helper_Themeobject = new Helper_Themeobject(this);
		
		settings = this.getSharedPreferences(PREFS_NAME, 0);
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		home_pagename = settings.getString("home_pagename", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		pageTitle = settings.getString("customerPageTitle", "");
		background = settings.getString("background", "");
		
		Log.d(LOG_TAG, "background:"+background);

		if(pageTitle ==null||pageTitle.equals("")){
			pageTitle = app_title;
		}

		/*** layout linear declaration *****/
		tabheader = (ImageView) findViewById(R.id.tabheader);

		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		texttitlebar.setText(pageTitle);

		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, pageTitle,"");
		
		/*** Setting Page Background ***/
		page_background = (ImageView) findViewById(R.id.page_background);
		PageBackground = new PageBackground(context,page_background);

		btnlogin = (Button) findViewById(R.id.btnlogin);
		btnlogin.setOnClickListener(onLogin);

		editTextemail = (EditText) findViewById(R.id.editTextemail);
		editTextPass = (EditText) findViewById(R.id.editTextPass);

		txtforgotpss = (TextView) findViewById(R.id.txtforgotpss);
		txtforgotpss.setOnClickListener(onClickforgotpss);

		txtregister = (TextView) findViewById(R.id.txtregister);
		txtregister.setOnClickListener(onClickregister);
		
		if(customer_allow_registration.onResultAllowRegistration().equals("0")){
			txtregister.setVisibility(View.INVISIBLE);
		}else{
			txtregister.setVisibility(View.VISIBLE);
		}

	}


	private View.OnClickListener onLogin = new View.OnClickListener() {
		public void onClick(View v) {
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) { // check network

					if (editTextemail.getText().toString().trim().equals("")) { // check
						// email

						editTextemail.setError("Email is required!");

					} else {
						editTextemail.setError(null);
						if (isEmailValid(editTextemail.getText().toString()) == false) { // check
							// valid
							// email
							// format

							editTextemail
							.setError("Please enter valid format email address!");

						} else {
							editTextemail.setError(null);
							if (editTextPass.getText().toString().trim()
									.equals("")) { // check password
								editTextPass.setError("Password is required!");
							} else {
								//finish();
								editTextPass.setError(null);
								InputMethodManager imm = (InputMethodManager)getSystemService(
										Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(editTextPass.getWindowToken(), 0);

								Login_Task Login = new Login_Task();
								Login.execute();
							}

						}
					}

				} else {
					Toast.makeText(Customer_Login.this,
							getResources().getString(R.string.MSG_NO_INTERNET),
							Toast.LENGTH_LONG).show();
				}
			} catch (Throwable t) {
				Log.e(LOG_TAG, "onLogin-Error:" + t.getMessage(), t);

			}
		}
	};



	private class Login_Task extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Customer_Login.this, "", getResources()
					.getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String status = null;
			String status_desc = null;
			String customerName = "";
			String customerPhone = "";
			String customerEmail = "";
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {

					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(
							ConnRoutePNames.DEFAULT_PROXY, proxy);

					customerEmail = editTextemail.getText().toString();
					settings = getSharedPreferences(PREFS_NAME, 0);
					String strURL = getResources().getString(R.string.CUST_LOGIN_API)
							+ "db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&email=" + URLEncoder.encode(customerEmail, "UTF-8")
							+ "&password=" + URLEncoder.encode(editTextPass.getText().toString(), "UTF-8")
							+ "&cs="+URLEncoder.encode(slotAction.getCheckSum(), "UTF-8");

					HttpPost httppost = new HttpPost(strURL);					
					Log.d(LOG_TAG, "onLogin-url:" + strURL);

					String responseBody = null;
					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					responseBody = httpclient
							.execute(httppost, responseHandler);

					Log.d(LOG_TAG, "onLogin-responseBody:" + responseBody);

					JSONArray StatusJSONArray = new JSONArray(responseBody);
					Log.d(LOG_TAG, "StatusJSONArray.length():"
							+ StatusJSONArray.length());
					if (StatusJSONArray.length() > 0) {
						status = StatusJSONArray.getJSONObject(0).getString("status").toString();
						status_desc = StatusJSONArray.getJSONObject(0).getString("desc").toString();

						customerID = StatusJSONArray.getJSONObject(0).getString("customer_id").toString();
						
						if (status.equals("1") ) {
							customerName = StatusJSONArray.getJSONObject(0).getString("name").toString();
							customerPhone = StatusJSONArray.getJSONObject(0).getString("phone").toString();
						}
					}

					SharedPreferences.Editor editor = settings.edit();
					editor.remove("customerName");
					editor.remove("customerID");
					editor.remove("customerEmail");
					editor.remove("customerPhone");
					editor.putString("customerID",customerID);
					editor.putString("customerName",customerName);
					editor.putString("customerEmail",customerEmail);
					editor.putString("customerPhone",customerPhone);
					editor.commit();

				} else {
					status = "1";
					status_desc = getResources().getString(R.string.MSG_NO_INTERNET);
				}
			} catch (Throwable t) {
				status = "-1";
				status_desc = getResources().getString(
						R.string.please_retry_again);
				Log.e(LOG_TAG, "doInBackground-Error:" + t.getMessage(), t);
			}
			return status + "~" + status_desc;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.d(LOG_TAG, "result: "+result);
			
			String[] arr_result = TextUtils.split(result, "~");

			if (arr_result[0].equals("1")) {
				Toast.makeText(Customer_Login.this, getResources().getString(R.string.login_success), Toast.LENGTH_LONG).show();

				String afterLogin="";
				afterLogin = settings.getString("afterLogin", "");
				Log.d(LOG_TAG, "afterLogin : "+ afterLogin);

				if(afterLogin.equals("Loyalty")){
					finish();
					Log.d(LOG_TAG, "Go to Loyalty Page..");
					Intent i=new Intent(Customer_Login.this, Loyalty.class);
					startActivity(i);

				} else if(afterLogin.equals("Warranty")){
					finish();
					Log.d(LOG_TAG, "Go to Warranty Page..");
					Intent i=new Intent(Customer_Login.this, Warranty_List.class);
					startActivity(i);

				} else if(afterLogin.equals("Reservation")){
					finish();
					Log.d(LOG_TAG, "Go to Reservation Page..");
					String topid_reservation = settings.getString("topid_reservation", "");
					Intent i=new Intent(Customer_Login.this, Reservation_List.class);
					i.putExtra("topid_reservation",topid_reservation);
					startActivity(i);
				
				} else if(afterLogin.equals("Event")){
					finish();
					Log.d(LOG_TAG, "Go to Event Page..");
					String topid_event = settings.getString("topid_event", "");
					Intent i=new Intent(Customer_Login.this, Event_List.class);
					i.putExtra("topid_event",topid_event);
					startActivity(i);
					
				} else if(afterLogin.equals("Inbox")){
					finish();
					Log.d(LOG_TAG, "Go to Inbox Page..");
					Intent i=new Intent(Customer_Login.this, Customer_Inbox.class);
					i.putExtra("noback", "no");
					startActivity(i);
					
				} else if(afterLogin.equals("Comment")){
					finish();
					Log.d(LOG_TAG, "Go to Comment Page..");
					String topid_comment = settings.getString("topid_comment", "");
					String cid_comment = settings.getString("cid_comment", "");
					
					Intent toCommentList = new Intent(Customer_Login.this,CommentList.class);
					toCommentList.putExtra("cid",cid_comment.toString());
					toCommentList.putExtra("noback","no");
					toCommentList.putExtra("topid",topid_comment);
					startActivity(toCommentList);
					finish();
					
				}else if(afterLogin.equals("Favorite")){
					finish();
					Log.d(LOG_TAG, "Go to Favorite Page..");
					String topid_favorite = settings.getString("topid_favorite", "");
					String cid_favorite = settings.getString("cid_favorite", "");
					Log.d(LOG_TAG, cid_favorite);
					slotAction.CreatePluginPage(topid_favorite, "content","","","");
					finish();
					
				}else if(afterLogin.equals("Theme_Std")){
					
					String themecode = settings.getString("themecode", "");
					String themepageid = settings.getString("themepageid", "");
					String noback = settings.getString("themepageid", "");
					String pagetitle = settings.getString("pagetitle", "");
					
					SharedPreferences.Editor editor = settings.edit();
					editor.remove("themecode");
					editor.remove("themepageid");
					editor.remove("noback");
					editor.remove("pagetitle");
					editor.commit();
					
					finish();
					Intent i=new Intent(Customer_Login.this, Theme_Std.class);
					i.putExtra("themecode", themecode);
					i.putExtra("themepageid", themepageid);
					i.putExtra("noback", noback);
					i.putExtra("pagetitle", pagetitle);
					startActivity(i);
					
					
					
				}else if(afterLogin.equals("clock")){
					finish();
					Intent i=new Intent(Customer_Login.this, Plugin_Clock_T1.class);
					i.putExtra("pagetitle", pageTitle);
					startActivity(i);
					
				}else if(afterLogin.equals("html5c")){
					Log.d(LOG_TAG, "Go to WebLink Page..");
					String p1 = settings.getString("customerPageTitle", "");
					String p2 = settings.getString("html5c_url", "");
					
					if (!p2.startsWith("http://") && !p2.startsWith("https://")){
						p2 = "http://" + p2;
					}
						
					Intent i=new Intent(context, WebLink.class);
					i.putExtra("pagetitle", p1);
					i.putExtra("url", p2);
					i.putExtra("openweb", "no");
					context.startActivity(i);
					finish();

				}else {
					onBackPressed();
				} 
				
				SharedPreferences.Editor editor = settings.edit();
				editor.remove("html5c_url");
				editor.remove("afterLogin");
				editor.remove("topid_comment");
				editor.remove("cid_comment");
				editor.remove("topid_event");
				editor.remove("topid_reservation");
				editor.putString("afterLogin","None");
				editor.commit();

			} else {

				Toast.makeText(Customer_Login.this,
						getResources().getString(R.string.login_failed),
						Toast.LENGTH_LONG).show();

			}

			try {
				Customer_Login.progressDialog.dismiss();
				//dialog = null;
			} catch (Exception e) {
				// nothing
			}
		}

	}



	private View.OnClickListener onClickforgotpss = new View.OnClickListener() {
		public void onClick(View v) {
			try {
				// finish();
				
				String afterLogin="";
				afterLogin = settings.getString("afterLogin", "");
				
				if(afterLogin.equals("Theme_Std")){
					finish();
				}
				Intent i = new Intent(Customer_Login.this, Customer_ForgotPwd.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_left);
			} catch (Throwable t) {
				Log.e(LOG_TAG, "onRegister-Error:" + t.getMessage(), t);

			}
		}

	};

	private View.OnClickListener onClickregister = new View.OnClickListener() {
		public void onClick(View v) {
			try {
				// finish();
				
				String afterLogin="";
				afterLogin = settings.getString("afterLogin", "");
				
				if(afterLogin.equals("Theme_Std")){
					finish();
				}
				
				Intent i = new Intent(Customer_Login.this, Customer_Register.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_left);
			} catch (Throwable t) {
				Log.e(LOG_TAG, "onRegister-Error:" + t.getMessage(), t);

			}
		}

	};

	/**
	 * method is used for checking valid email id format.
	 * 
	 * @param email
	 * @return boolean true for valid false for invalid
	 */
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}
	
	// FUNCTION NETWORK

	public boolean isNetworkAvailable() {

		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	public void onBackPressed() {
		
		String afterLogin="";
		afterLogin = settings.getString("afterLogin", "");
		if(afterLogin.equals("Theme_Std")){
			finish();
		}else{
			super.onBackPressed(); 
			this.finish();
		}
	}


	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();
	}

}
