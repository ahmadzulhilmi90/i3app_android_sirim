package com.m3tech.customer;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.background.PageBackground;
import com.m3tech.collection.Collection_List;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

@SuppressWarnings("deprecation")
public class Customer_Point extends Activity {

	private static final String LOG_TAG = "CUSTOMER POINT PAGE";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	public static final String COMMON_FILE = "MyCommonFile"; 	
	SharedPreferences settings;
	SharedPreferences commonfile;
	
	String home_pagename, app_title,themecolor,colorcode,colorcode1,app_user,app_db,udid,customerID,nowDate;
	LinearLayout container;
	TextView texttitlebar,texthome,txt_level,txt_point;
	ImageView page_background,back,level_icon,tabheader;

	static ProgressDialog progressDialog;
	JSONArray custJSONArray;
	String point_title="", customer_point_title="", level_title="";

	List<Collection_List> model_list = new ArrayList<Collection_List>();
	ArrayAdapter<Collection_List> adapter_list = null;
	
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();
	
	Header Header = null;
	SlotAction slotAction = null;
	ProgressBar progressbar; 
	LinearLayout main_point; 
	String levellist_json="",levellist_date="";
	JSONArray levelJSONArray;
	Helper_Themeobject Helper_Themeobject = null;
	Context context = this;
	PageBackground PageBackground = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING "+LOG_TAG);
		setContentView(R.layout.customer_point);

		slotAction = new SlotAction(this);
		Helper_Themeobject = new Helper_Themeobject(this);
		commonfile = this.getSharedPreferences(COMMON_FILE, 0);
		settings = this.getSharedPreferences(PREFS_NAME, 0);
		
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		customerID = settings.getString("customerID", "");
		home_pagename = settings.getString("home_pagename", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		colorcode1 = settings.getString("colorcode1", "");
		
		Intent j = getIntent();
		point_title = j.getStringExtra("point_title");
		customer_point_title = j.getStringExtra("customer_point_title");
		level_title = j.getStringExtra("level_title");


		/*** layout linear declaration *****/
		container = (LinearLayout) findViewById(R.id.container);
		tabheader = (ImageView) findViewById(R.id.tabheader);

		progressbar = (ProgressBar) findViewById(R.id.progressbar);
		main_point = (LinearLayout) findViewById(R.id.main_point);
		
		String transcolor = colorcode1.replace("#", "#90");
		container.setBackgroundColor(Color.parseColor(transcolor));
		
		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		

		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome,img_back,img_home, texttitlebar, app_title.toUpperCase(),"");
		
		/*** Setting Page Background ***/
		page_background = (ImageView) findViewById(R.id.page_background);
		PageBackground = new PageBackground(context,page_background);
		
		if (point_title.length()>0) {
			texttitlebar.setText(point_title);
		} else {
			texttitlebar.setText(app_title.toUpperCase());
		}
		
		level_icon = (ImageView) findViewById(R.id.level_icon);

		txt_level = (TextView) findViewById(R.id.txt_level);
		txt_point = (TextView) findViewById(R.id.txt_point);

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy kk:mm");
		nowDate = df.format(cal.getTime());
		
		GetLevelList();

		Boolean Network_status = isNetworkAvailable();
		if (Network_status == true) { // check network
			getCustomerPoints Task = new getCustomerPoints();
			Task.execute();
		} else {
			Toast.makeText(Customer_Point.this,
					getResources().getString(R.string.MSG_NO_INTERNET),
					Toast.LENGTH_LONG).show();
			onBackPressed();
		}

	}


	private class getCustomerPoints extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Toast.makeText(Customer_Point.this,getResources().getString(R.string.please_wait), Toast.LENGTH_LONG).show();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;

			try{
				
				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_RSVSETTING;
				String strURL = getResources().getString(R.string.POINTS_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8");

				HTTP_POST_RSVSETTING = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);

				ResponseHandler<String> responseHandler_RSVSETTING=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_RSVSETTING, responseHandler_RSVSETTING);	
				Log.d(LOG_TAG,responseBody);

				custJSONArray = new JSONArray(responseBody);
				Log.d(LOG_TAG,"custJSONArray lenght :"+ custJSONArray.length());

				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "getCustomerPoints-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){

					if ( custJSONArray.length() > 0){
						if (custJSONArray.getJSONObject(0).getString("level_name").toString()!=null) {
							txt_level.setText(level_title+": "+custJSONArray.getJSONObject(0).getString("level_name").toString());
						}
						
						if (custJSONArray.getJSONObject(0).getString("score_total").toString()!=null) {
							txt_point.setText(customer_point_title+": "+custJSONArray.getJSONObject(0).getString("score_total").toString());
						}

						if (custJSONArray.getJSONObject(0).getString("level_icon").toString()!=null) {
							String imgicon = custJSONArray.getJSONObject(0).getString("level_icon").toString();
							LoadLevelIcon Task = new LoadLevelIcon();
							Task.execute(new String[] { imgicon });
						}

						progressbar.setVisibility(View.GONE);
						main_point.setVisibility(View.VISIBLE);

					}else{
						Toast.makeText(Customer_Point.this,getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG).show();
					}

				}else{
					Toast.makeText(Customer_Point.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "getCustomerPoints-Error:" + t.getMessage(), t);
			}
		}

	}


	/****  function LoadLevelIcon  *****/
	public class LoadLevelIcon extends AsyncTask<String, Void, String> {
		String iconimg="";		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			
			try{
				
				if (arg0.length>0) {
					iconimg = arg0[0];
					response="SUCCESS";
				} else {
					response="FAILED";
				}

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "LoadLevelIcon-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				if (result.equals("SUCCESS")){
					Log.d(LOG_TAG, "iconimg: " + iconimg);						

					imageLoader.displayImage(iconimg,  level_icon, displayOptions, new ImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) { }
						@Override
						public void onLoadingFailed(String imageUri, View view, FailReason failReason) { }
						@Override
						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
							level_icon.setVisibility(View.VISIBLE);
						}
						@Override
						public void onLoadingCancelled(String imageUri, View view) { }
					}); 	
				}
			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "LoadLevelIcon-onPostExecute-Error:" + t.getMessage(), t);

			}
		}
	}

	public void GetLevelList() {
		try{

			levellist_json = commonfile.getString("levellist_json", "");
			levellist_date = commonfile.getString("levellist_date", "00-00-0000 00:00");
			
			String subdate = nowDate.substring(0,13);
			if (subdate.equals(levellist_date.substring(0,13))) {
				UpdateLevelList Task = new UpdateLevelList();
				Task.execute();
			} else {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) { // check network
					getLevelList_API Task = new getLevelList_API();
					Task.execute();
				}
			}
			 
		} catch (Throwable t) { 
			Log.e(LOG_TAG, "GetLevelList-Error:" + t.getMessage(), t); 

		}
	}
	


	private class getLevelList_API extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;

			try{


				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_RSVSETTING;
				String strURL = getResources().getString(R.string.POINT_LEVEL_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8");

				HTTP_POST_RSVSETTING = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);

				ResponseHandler<String> responseHandler_RSVSETTING=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_RSVSETTING, responseHandler_RSVSETTING);	
				Log.d(LOG_TAG,responseBody);

				levellist_json = responseBody;
				levelJSONArray = new JSONArray(responseBody);
				Log.d(LOG_TAG,"levelJSONArray lenght :"+ levelJSONArray.length());

				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "getLevelList_API-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){
					if ( levelJSONArray.length() > 0){
						SharedPreferences.Editor editor = commonfile.edit();
						editor.remove("levellist_json");
						editor.remove("levellist_date");
						editor.putString("levellist_json",levellist_json); 
						editor.putString("levellist_date",nowDate); 
						editor.commit();
					}
					
					UpdateLevelList Task = new UpdateLevelList();
					Task.execute();
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "getLevelList_API-Error:" + t.getMessage(), t);
			}
		}

	}


	/**** start function get content list ****/
	private class UpdateLevelList extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			try{

				adapter_list = new list_Adapter();
				adapter_list.clear();

				levelJSONArray = new JSONArray(levellist_json);

				if (levelJSONArray.length()>0) {

					for (int n = 0; n < levelJSONArray.length(); n++) {
						String title = levelJSONArray.getJSONObject(n).getString("level_name").toString();	
						String image =levelJSONArray.getJSONObject(n).getString("level_icon").toString();
						String code1 =levelJSONArray.getJSONObject(n).getString("level_details").toString();
						String code2 =levelJSONArray.getJSONObject(n).getString("level_min").toString();

						Collection_List current_content = new Collection_List();
						current_content.set_title(title);
						current_content.set_image(image);
						current_content.set_code1(code1);
						current_content.set_code2(code2);
						adapter_list.add(current_content);
					}		
				}
				
				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "UpdateLevelList-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				ListView listcontent = (ListView) findViewById(R.id.listcontent);

				if (result.equals("SUCCESS")){
					listcontent.setAdapter(adapter_list);			

				}else{
					Log.d(LOG_TAG, "UpdateLevelList-Result:" + result);
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "UpdateLevelList-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}


	class list_Adapter extends ArrayAdapter<Collection_List> {
		list_Adapter() {
			super(Customer_Point.this, R.layout.plugin_content_template5_row, model_list);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View row=convertView;
			List_NotificationHolder holder=null;

			if (row==null) {													
				LayoutInflater inflater=getLayoutInflater();

				row=inflater.inflate(R.layout.plugin_content_template5_row, parent, false);
				holder=new List_NotificationHolder(row);
				row.setTag(holder);
			}
			else {
				holder=(List_NotificationHolder)row.getTag();
			}

			holder.populateFromList(model_list.get(position));

			return(row);
		}
	}


	class List_NotificationHolder {
		@SuppressWarnings("unused")
		private View row=null;
		private TextView titlecontent=null;
		private TextView var1 = null;
		private ImageView image =null;
		private LinearLayout spinner = null;
		private LinearLayout layout_image = null;
		
		List_NotificationHolder(View row) {
			this.row=row;	
			titlecontent=(TextView) row.findViewById(R.id.text);
			titlecontent.setTextColor(Color.parseColor(colorcode));

			image = (ImageView) row.findViewById(R.id.image_url);
			var1=(TextView) row.findViewById(R.id.var1);
			spinner = (LinearLayout) row.findViewById(R.id.progressbar);
			layout_image = (LinearLayout) row.findViewById(R.id.layout_image);
		}
		
		void populateFromList(Collection_List r) {

			titlecontent.setText(r.get_title());
			Log.d(LOG_TAG,"titlecontent="+r.get_title());

			var1.setText(r.get_code1());

			imageLoader.displayImage(r.get_image(), image, displayOptions, new ImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {
					spinner.setVisibility(View.VISIBLE);
					layout_image.setVisibility(View.GONE);
				}
				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					spinner.setVisibility(View.GONE);
					layout_image.setVisibility(View.VISIBLE);
				}
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					spinner.setVisibility(View.GONE);
					layout_image.setVisibility(View.VISIBLE);
				}
				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					spinner.setVisibility(View.GONE);
					layout_image.setVisibility(View.VISIBLE);
				}

			});

		}	
	}


	public static DisplayImageOptions getDisplayImageOptions() {
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}

	// FUNCTION NETWORK

	public boolean isNetworkAvailable() {

		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}


	/**
	 * method is used for checking valid email id format.
	 * 
	 * @param email
	 * @return boolean true for valid false for invalid
	 */
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public void onBackPressed() {

		super.onBackPressed(); 
		this.finish();

		overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
	}


	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();
	}
}
