package com.m3tech.customer;

import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.background.PageBackground;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;

public class Customer_ChangePwd extends Activity {

	private static final String LOG_TAG = "CUSTOMER CHANGE PASSWORD";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	SharedPreferences settings;
	String home_pagename, app_title,themecolor,colorcode,app_user,app_db,udid,customerID;
	ImageView tabheader;
	TextView texttitlebar,texthome;
	ImageView page_background,back;
	Button btnsubmit;
	EditText editText_currentpwd, editText_newpwd, editText_re_newpwd;
	static ProgressDialog progressDialog;
	Helper_Themeobject Helper_Themeobject = null;
	SlotAction slotAction = null;
	Context context = this;
	Header Header = null;
	PageBackground PageBackground = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING "+LOG_TAG);
		setContentView(R.layout.customer_changepwd);

		slotAction = new SlotAction(this);
		Helper_Themeobject = new Helper_Themeobject(this);
		settings = this.getSharedPreferences(PREFS_NAME, 0);
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		home_pagename = settings.getString("home_pagename", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		customerID = settings.getString("customerID", "");

		/*** layout linear declaration *****/

		tabheader = (ImageView) findViewById(R.id.tabheader);

		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		String title_header = getResources().getString(R.string.changepwd);

		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome,img_back,img_home, texttitlebar, title_header,"");
		
		/*** Setting Page Background ***/
		page_background = (ImageView) findViewById(R.id.page_background);
		PageBackground = new PageBackground(context,page_background);

		btnsubmit = (Button) findViewById(R.id.btnsubmit);
		btnsubmit.setOnClickListener(onClickSubmit);

		editText_currentpwd = (EditText) findViewById(R.id.editText_currentpwd);
		editText_newpwd = (EditText) findViewById(R.id.editText_newpwd);
		editText_re_newpwd = (EditText) findViewById(R.id.editText_re_newpwd);

	}


	//click button forgot pass
	private View.OnClickListener onClickSubmit = new View.OnClickListener() {
		public void onClick(View v) {
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) { // check network

					editText_currentpwd.setError(null);
					editText_newpwd.setError(null);
					editText_re_newpwd.setError(null);

					if (editText_currentpwd.getText().toString().length()<6) { // check
						editText_currentpwd.setError(getResources().getString(R.string.error_pwdshort));

					} else if (editText_newpwd.getText().toString().length()<6) { // check
							editText_newpwd.setError(getResources().getString(R.string.error_pwdshort));

					} else {
						if (editText_newpwd.getText().toString().equals(editText_re_newpwd.getText().toString())) { 

							editText_currentpwd.setError(null);
							InputMethodManager imm = (InputMethodManager)getSystemService(
									Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(editText_currentpwd.getWindowToken(), 0);

							Forgot_Task changepwd = new Forgot_Task();
							changepwd.execute();

						} else {
							editText_re_newpwd.setError(getResources().getString(R.string.error_re_newpwd));

						}
					}

				} else {
					Toast.makeText(Customer_ChangePwd.this,
							getResources().getString(R.string.MSG_NO_INTERNET),
							Toast.LENGTH_LONG).show();
				}
			} catch (Throwable t) {
				Log.e(LOG_TAG, "onLogin-Error:" + t.getMessage(), t);

			}
		}
	};



	private class Forgot_Task extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Customer_ChangePwd.this, "", getResources()
					.getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String status = null;
			String status_desc = null;
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {

					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
					
					String strURL = getResources().getString(R.string.FORGOT_API)
							+ "db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
							+ "&cs="+URLEncoder.encode(slotAction.getCheckSum(), "UTF-8")
							+ "&task=changepassword"
							+ "&password=" + URLEncoder.encode(editText_currentpwd.getText().toString(), "UTF-8")
							+ "&newpassword=" + URLEncoder.encode(editText_newpwd.getText().toString(), "UTF-8")
							+ "&newpassword1=" + URLEncoder.encode(editText_re_newpwd.getText().toString(), "UTF-8");


					HttpPost httppost = new HttpPost(strURL);
					Log.d(LOG_TAG,"strURL:"+ strURL);
					String responseBody = null;
					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					responseBody = httpclient
							.execute(httppost, responseHandler);

					Log.d(LOG_TAG, "responseBody:" + responseBody);

					JSONArray StatusJSONArray = new JSONArray(responseBody);
					Log.d(LOG_TAG, "StatusJSONArray.length():"
							+ StatusJSONArray.length());
					if (StatusJSONArray.length() > 0) {
						status = StatusJSONArray.getJSONObject(0).getString("status").toString();
						status_desc = StatusJSONArray.getJSONObject(0).getString("desc").toString();

					}

				} else {
					status = "1";
					status_desc = getResources().getString(R.string.MSG_NO_INTERNET);
				}
			} catch (Throwable t) {
				status = "-1";
				status_desc = getResources().getString(R.string.please_retry_again);
				Log.e(LOG_TAG, "doInBackground-Error:" + t.getMessage(), t);
			}
			Customer_ChangePwd.progressDialog.dismiss();			
			return status + "~" + status_desc;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			String[] arr_result = TextUtils.split(result, "~");

			if (arr_result[0].equals("1")) {
				Toast.makeText(Customer_ChangePwd.this, arr_result[1], Toast.LENGTH_LONG).show();
				onBackPressed();

			} else {

				Toast.makeText(Customer_ChangePwd.this, arr_result[1], Toast.LENGTH_LONG).show();

			}
			
		}

	}

	// FUNCTION NETWORK

	public boolean isNetworkAvailable() {

		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}


	/**
	 * method is used for checking valid email id format.
	 * 
	 * @param email
	 * @return boolean true for valid false for invalid
	 */
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public void onBackPressed() {

		super.onBackPressed(); 
		this.finish();

		overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
	}


	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();
	}
}
