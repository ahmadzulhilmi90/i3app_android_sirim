package com.m3tech.customer;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.collection.Collection_Inbox;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

@SuppressWarnings("deprecation")
public class Customer_Inbox extends Activity {

	private static final String LOG_TAG = "INBOX LIST";
	public static final String PREFS_NAME = "MyPrefsFile";

	private SharedPreferences settings;
	Boolean network_status;

	String home_pagename, layout, page, userid, currentdate, customerID,app_title,colorcode,colorcode1,app_user,app_db,udid;
	String inbox_title, top_id, newmessage, responbody;
	TextView product_cat_name,texttitlebar,texthome;
	LinearLayout layoutviewlist;
	ImageView tabheader,back,btnsubmit, btnuploadimage, imgPreview;
	EditText editmessage;
	ListView inboxlist;
	Boolean capturingImage = false;
	Helper_Themeobject Helper_Themeobject = null;
	Context context = this;
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();

	//JSON
	JSONArray dataJSONArray;
	SlotAction slotAction = null;
	Header Header = null;

	List<Collection_Inbox> model_Inbox=new ArrayList<Collection_Inbox>();
	InboxDetail_Adapter adapter_Inbox=null;
	Collection_Inbox current_Inbox=null;	

	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;

	// directory name to store captured images and videos
	private static final String IMAGE_DIRECTORY_NAME = "Camera";
	String uploadFilePath="";
	String imgFilename = "inbox_";
	private Uri fileUri;
	
	int serverResponseCode = 0;
	StringBuffer responseBuffer = new StringBuffer(); 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING "+LOG_TAG);
		setContentView(R.layout.customer_inbox);
		slotAction = new SlotAction(this);
		Helper_Themeobject = new Helper_Themeobject(this);

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
		.threadPriority(Thread.NORM_PRIORITY)
		.denyCacheImageMultipleSizesInMemory()
		.discCacheFileNameGenerator(new Md5FileNameGenerator())
		.tasksProcessingOrder(QueueProcessingType.FIFO)
		.writeDebugLogs() // Remove
		.build();
		ImageLoader.getInstance().init(config);

		settings = this.getSharedPreferences(PREFS_NAME, 0);
		home_pagename = settings.getString("home_pagename", "");
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		customerID = settings.getString("customerID", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		colorcode1 = settings.getString("colorcode1", "");

		String pageTitle = getResources().getString(R.string.inbox);
		
		top_id = settings.getString("InboxTop", "");
		inbox_title = settings.getString("InboxTitle", "");

		/*** layout linear declaration *****/

		tabheader = (ImageView) findViewById(R.id.tabheader);

		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		

		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, pageTitle,"");

		if (inbox_title.length()>0) {
			texttitlebar.setText(inbox_title);
		} else {
			texttitlebar.setText(pageTitle);
		}
		
		/**** back button *****/
		Intent j = getIntent();		
		String noback = j.getStringExtra("noback");
		if (noback.equals("yes")) {
			tabback.setVisibility(View.GONE);
		} else {
			tabback.setOnClickListener(onClickbackbutton);
		}

		network_status = isNetworkAvailable();
		if(!network_status){
			Toast.makeText(Customer_Inbox.this,getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_LONG).show();
		} 

		btnsubmit = (ImageView) findViewById(R.id.btnsubmit);
		btnsubmit.setOnClickListener(onClickSubmit);
		inboxlist = (ListView) findViewById(R.id.inboxlist);
		editmessage = (EditText) findViewById(R.id.editmessage);
		
		imgPreview = (ImageView) findViewById(R.id.imgPreview);
		btnuploadimage= (ImageView) findViewById(R.id.btnuploadimage);
		btnuploadimage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// capture picture
				captureImage();
			}
		});
		
		// External sdcard location
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
			}
		}

		uploadFilePath = mediaStorageDir.getPath() + File.separator ;
		Log.w(LOG_TAG, "getOutputMediaFile: " + uploadFilePath);

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddkkmm");
		String todayDate = df.format(cal.getTime());

		imgFilename = imgFilename + todayDate + ".jpg";

	}

	private class getInboxData extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Toast.makeText(Customer_Inbox.this,getResources().getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;
			try{

				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_TOP;

				String strURL = getResources().getString(R.string.INBOX_LIST_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&top=" + URLEncoder.encode(top_id, "UTF-8")
						+ "&cs="+URLEncoder.encode(slotAction.getCheckSum(), "UTF-8"); 


				HTTP_POST_TOP = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);
				//Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	

				dataJSONArray = new JSONArray(responseBody);
				response="SUCCESS";
				Log.d(LOG_TAG,"responseBody="+ responseBody);	


			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "getInboxData-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				ListView listView = (ListView) findViewById(R.id.inboxlist);
				adapter_Inbox=new InboxDetail_Adapter ();
				adapter_Inbox.clear();

				Log.d(LOG_TAG,"onPostExecute: "+ dataJSONArray.length());

				if (result.equals("SUCCESS")){

					if (dataJSONArray.length()>0){

						Log.d(LOG_TAG,"dataJSONArray lenght :"+ dataJSONArray.length());
						for (int i = 0; i < dataJSONArray.length(); i++) {

							current_Inbox=new Collection_Inbox();
							current_Inbox.setid(dataJSONArray.getJSONObject(i).getString("id").toString());
							current_Inbox.setmdate(dataJSONArray.getJSONObject(i).getString("m_date").toString());
							current_Inbox.setsender_id(dataJSONArray.getJSONObject(i).getString("sender_id").toString());
							current_Inbox.setsender_name(dataJSONArray.getJSONObject(i).getString("sender_name").toString());
							current_Inbox.setmessage(dataJSONArray.getJSONObject(i).getString("message").toString());
							current_Inbox.setimage_url(dataJSONArray.getJSONObject(i).getString("image_url").toString());
							adapter_Inbox.add(current_Inbox);

						}


						listView.setAdapter(adapter_Inbox);			
						listView.setOnItemClickListener(onListClick);	
						scrollMyListViewToBottom();
					}

				}else{
					Toast.makeText(Customer_Inbox.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "getInboxData-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}

	private void scrollMyListViewToBottom() {
		
		current_Inbox = model_Inbox.get(adapter_Inbox.getCount() - 1);	
		String msgid = current_Inbox.getid();
		
		SharedPreferences.Editor editor = settings.edit();
		editor.remove("latest_msgid");
		editor.putString("latest_msgid", msgid);
		editor.commit();

		inboxlist.post(new Runnable() {
	        @Override
	        public void run() {
	            // Select the last row so it will scroll into view...
	        	inboxlist.setSelection(adapter_Inbox.getCount() - 1);
	        }
	    });
	}
	

	class InboxDetail_Adapter extends ArrayAdapter<Collection_Inbox> {
		InboxDetail_Adapter() {
			super(Customer_Inbox.this, R.layout.customer_inbox_row, model_Inbox);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View row=convertView;
			NotificationHolderhome holder=null;

			if (row==null) {													
				LayoutInflater inflater=getLayoutInflater();

				row=inflater.inflate(R.layout.customer_inbox_row, parent, false);
				holder=new NotificationHolderhome(row);
				row.setTag(holder);
			} else {
				holder=(NotificationHolderhome)row.getTag();
			}

			holder.populateFromhome(model_Inbox.get(position));

			return(row);
		}
	}

	class NotificationHolderhome {
		@SuppressWarnings("unused")
		private View row=null;
		private TextView textname=null;
		private TextView textmessage=null;
		private TextView textdate=null;
		private TextView textdelete=null;
		private LinearLayout inboxrow=null;
		private ImageView image =null;
		private ProgressBar spinner = null;
		private RelativeLayout imglayout = null;

		NotificationHolderhome(View row) {
			this.row=row;	
			inboxrow=(LinearLayout) row.findViewById(R.id.inboxrow);
			textname=(TextView) row.findViewById(R.id.textname);
			textmessage=(TextView) row.findViewById(R.id.textmessage);
			textdate=(TextView) row.findViewById(R.id.textdate);
			textdelete=(TextView) row.findViewById(R.id.textdelete);	
			image = (ImageView) row.findViewById(R.id.image_url);
			spinner = (ProgressBar) row.findViewById(R.id.progress);
			imglayout = (RelativeLayout) row.findViewById(R.id.layout_image);
		}

		void populateFromhome(Collection_Inbox r) {
			String sender_id = r.getsender_id();
			String img_url = r.getimage_url();
			
			if (img_url.isEmpty() || img_url.equals("null")) {
				img_url = "";
			}
			
			if (sender_id.equals("0")) {
				//inboxrow.setBackgroundColor(Color.parseColor("#ffffd0"));
				inboxrow.setBackground(getResources().getDrawable(R.drawable.border_curve_yellow));
			}

			String date = r.getmdate();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
			Date newDate = null;
			try {
				newDate = sdf.parse(date);
			}catch(Exception ex){
				ex.printStackTrace();
			}

			SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd, yyyy hh:mm a");
			String msgdate = formatter.format(newDate);

			textname.setText(r.getsender_name());
			textmessage.setText(r.getmessage());
			textdate.setText(msgdate);			
			textdelete.setVisibility(View.GONE);

			myClickListener listener = new myClickListener(r.getid());
			textdelete.setOnClickListener(listener);

			if (img_url.length()>10) {
				imglayout.setVisibility(View.VISIBLE);
						
				imageLoader.displayImage(img_url, image, displayOptions, new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {											
						spinner.setVisibility(View.VISIBLE);
					}
					@Override
					public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
						spinner.setVisibility(View.GONE);
					}
					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
						spinner.setVisibility(View.GONE);
					}
					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						spinner.setVisibility(View.GONE);
					}
	
				});
			}
				

		}
	}



	private AdapterView.OnItemClickListener onListClick=new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent,
				View view, int position,
				long id) {

			current_Inbox = model_Inbox.get(position);							
			int intid = Integer.parseInt(current_Inbox.getid());

			TextView textdelete = (TextView) view.findViewById(R.id.textdelete);
			
			if (textdelete.getVisibility()==View.GONE && intid>0) {
				textdelete.setVisibility(View.VISIBLE);
			}
			

		}
	};

	public class myClickListener implements TextView.OnClickListener {

		 String myVar;
	     public myClickListener(String myVar) {
	          this.myVar = myVar;
	     }

	     public void onClick(View v) {
				try{
					Log.d(LOG_TAG,"onClickDelete :" + myVar);
					submitDeleteData Task = new submitDeleteData();
					Task.execute(new String[] { myVar }); 
					
				} catch (Throwable t) { 
					Log.e(LOG_TAG, "onClickDelete-Error:" + t.getMessage(), t); 

				}
	     }

	  };

	@SuppressWarnings("unused")
	private View.OnClickListener onClickSubmitOriginal=new View.OnClickListener() {
		public void onClick(View v) {
			try{

				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(editmessage.getWindowToken(), 0);

				String message = editmessage.getText().toString();

				if(message.length()>0) {
					submitInboxData Task = new submitInboxData();
					Task.execute(new String[] { message }); 
				}

			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onClickSubmit-Error:" + t.getMessage(), t); 

			}
		}

	};

	private class submitInboxData extends AsyncTask<String, Void, String> {
		String message="";
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Toast.makeText(Customer_Inbox.this,getResources().getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;
			try{
				message = arg0[0];
				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_TOP;
				//http://app.m3online.com/api/inbox_add.php?db=118&userid=118&udid=72FA8B47&cuid=3&device=ios&message=hihitest
				String strURL = getResources().getString(R.string.INBOX_SUBMIT_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&top=" + URLEncoder.encode(top_id, "UTF-8")
						+ "&message=" + URLEncoder.encode(message, "UTF-8"); 


				HTTP_POST_TOP = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);
				//Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	

				dataJSONArray = new JSONArray(responseBody);
				response="SUCCESS";
				Log.d(LOG_TAG,"responseBody="+ responseBody);	


			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "submitInboxData-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){
					editmessage.setText("");

					getInboxData Task = new getInboxData();
					Task.execute(new String[] { });
				}else{
					Toast.makeText(Customer_Inbox.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_SHORT).show();
				}



			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "submitInboxData-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}

	private class submitDeleteData extends AsyncTask<String, Void, String> {
		String msgid="";
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;
			try{
				msgid = arg0[0];
				if(msgid==null || msgid.equals("")){
					response="FAILED";
				} else {

					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

					HttpPost HTTP_POST_TOP;
					//http://app.m3online.com/api/inbox_add.php?db=118&userid=118&udid=72FA8B47&cuid=3&device=ios&message=hihitest
					String strURL = getResources().getString(R.string.INBOX_DELETE_API)
							+ "db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						    + "&top=" + URLEncoder.encode(top_id, "UTF-8")
							+ "&inboxid=" + URLEncoder.encode(msgid, "UTF-8"); 


					HTTP_POST_TOP = new HttpPost(strURL);
					Log.d(LOG_TAG,"strURL="+ strURL);
					//Execute HTTP Post Request
					ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
					responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	

					dataJSONArray = new JSONArray(responseBody);
					response="SUCCESS";
					Log.d(LOG_TAG,"responseBody="+ responseBody);	
				}

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "submitDeleteData-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){
					getInboxData Task = new getInboxData();
					Task.execute(new String[] { });
				}else{
					Toast.makeText(Customer_Inbox.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_SHORT).show();
				}



			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "submitDeleteData-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}

	/***  back button onclick ****/
	private View.OnClickListener onClickSubmit=new View.OnClickListener() {
		public void onClick(View v) {
			try{

				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(editmessage.getWindowToken(), 0);

				newmessage = editmessage.getText().toString();
				File imgFile = new File(uploadFilePath + "" + imgFilename);
				
				if(imgFile.exists() || newmessage.length()>0) {   
					uploadFile(uploadFilePath + "" + imgFilename);
				}
					
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onBackButton-Error:" + t.getMessage(), t); 

			}
		}

	};
	
	/******************upload file *************************/

	public int uploadFile(String sourceFileUri) {

		Toast.makeText(Customer_Inbox.this,getResources().getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
		String fileName = sourceFileUri;
		Log.d(LOG_TAG, "fileName="+fileName);
		HttpURLConnection conn = null;
		DataOutputStream dos = null; 
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(sourceFileUri);
		Log.d(LOG_TAG, "sourceFile="+sourceFile);

		try {
			String strURL = getResources().getString(R.string.INBOX_SUBMIT_API)
					+ "db=" + URLEncoder.encode(app_db, "UTF-8")
					+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
					+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
					+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
					+ "&top=" + URLEncoder.encode(top_id, "UTF-8")
					+ "&message=" + URLEncoder.encode(newmessage, "UTF-8"); 


			URL url = new URL(strURL);

			// Open a HTTP  connection to  the URL
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true); // Allow Inputs
			conn.setDoOutput(true); // Allow Outputs
			conn.setUseCaches(false); // Don't use a Cached Copy
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

			File imgFile = new File(sourceFileUri);

			if(imgFile.exists()) {   

				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(sourceFile);

				conn.setRequestProperty("uploaded_file", fileName);
				dos = new DataOutputStream(conn.getOutputStream());
				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
						+ fileName + "\"" + lineEnd);
				dos.writeBytes(lineEnd);

				// create a buffer of  maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize); 

				while (bytesRead > 0) {
					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);  

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				//close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			}

			// Responses from the server (code and message)
			serverResponseCode = conn.getResponseCode();
			String serverResponseMessage = conn.getResponseMessage();

			Log.i("uploadFile", "HTTP Response is : "
					+ serverResponseMessage + ": " + serverResponseCode);

			//Get Response 

			InputStream inputStream = conn.getInputStream();

			BufferedReader bufferReader = new BufferedReader(new InputStreamReader(inputStream));

			String line;

			while((line = bufferReader.readLine()) != null) {
				responseBuffer.append(line);
				responseBuffer.append('\r');
			}

			bufferReader.close();
			responbody = responseBuffer.toString();
			Log.d(LOG_TAG, "responbody="+responbody);

			if(serverResponseCode == 200){

				runOnUiThread(new Runnable() {
					public void run() {

						try {
							dataJSONArray = new JSONArray(responbody);
							String status = dataJSONArray.getJSONObject(0).getString("status").toString();
							String desc = dataJSONArray.getJSONObject(0).getString("desc").toString();
							Log.d(LOG_TAG, desc);
							if(status.equals("1")){
								//clear all data
								editmessage.setText("");
								imgPreview.setVisibility(View.GONE);
								imgFilename = "inbox_";
								
								Toast.makeText(Customer_Inbox.this,getResources().getString(R.string.message_success), Toast.LENGTH_SHORT).show();
								getInboxData Task = new getInboxData();
								Task.execute(new String[] { });
							} else {
								Toast.makeText(Customer_Inbox.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_SHORT).show();
								
							}

						} catch (JSONException e) {
							e.printStackTrace();
						}

					}
				});               
			}   


		} catch (MalformedURLException ex) {

			ex.printStackTrace();

			runOnUiThread(new Runnable() {
				public void run() {
					//Toast.makeText(Warrantyadd.this, "MalformedURLException", Toast.LENGTH_SHORT).show();
				}
			});

			Log.e("Upload file to server", "error: " + ex.getMessage(), ex); 

		} catch (Exception e) {

			e.printStackTrace();
			runOnUiThread(new Runnable() {
				public void run() {
					//Toast.makeText(Warrantyadd.this, "Got Exception : see logcat ", Toast.LENGTH_SHORT).show();
				}
			});
			Log.e("Upload file to server Exception", "Exception : "
					+ e.getMessage(), e); 
		}

		return serverResponseCode;

		//} // End else block
	} 

	/*******************end upload file*********************/
	
	//************************************ SETUP FOR IMAGE INPUT ******************************//

	/**
	 * Checking device has camera hardware or not
	 * */
	@SuppressWarnings("unused")
	private boolean isDeviceSupportCamera() {
		if (getApplicationContext().getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}

	/*
	 * Capturing Camera Image will lauch camera app requrest image capture
	 */
	private void captureImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable("file_uri", fileUri);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		// get the file url
		fileUri = savedInstanceState.getParcelable("file_uri");
	}



	/**
	 * Receiving activity result method will be called after closing the camera
	 * */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				capturingImage = true;
				resizeImg ();
				previewCapturedImage();
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		} 
	}

	/*
	 * Display image from a path to ImageView
	 */
	private void previewCapturedImage() {
		try {
			imgPreview.setVisibility(View.VISIBLE);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = 8;
			final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);
			imgPreview.setImageBitmap(bitmap);
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}



	/**
	 * ------------ Helper Methods ---------------------- 
	 * */

	/*
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/*
	 * returning image / video
	 */
	private File getOutputMediaFile(int type) {

		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(uploadFilePath + imgFilename);
			Log.w(LOG_TAG, "getOutputMediaFile: " + mediaFile);

		} else {
			return null;
		}

		return mediaFile;
	}

	private void resizeImg(){
		int maxWidth = 800;
		int maxHeight = 800;

		Log.d(LOG_TAG, "resizeImg-Start");
		String path = uploadFilePath + imgFilename;
		Log.d(LOG_TAG, "path: "+path);

		// create the options
		BitmapFactory.Options opts = new BitmapFactory.Options();

		//just decode the file
		opts.inJustDecodeBounds = true;
		Bitmap bp = BitmapFactory.decodeFile(path, opts);

		//get the original size
		int orignalHeight = opts.outHeight;
		int orignalWidth = opts.outWidth;

		Log.d(LOG_TAG, "orignalHeight: "+orignalHeight);
		Log.d(LOG_TAG, "orignalWidth: "+orignalWidth);

		//initialization of the scale
		int resizeScale = 1;

		//get the good scale
		if ( orignalWidth > maxWidth || orignalHeight > maxHeight ) {
			final int heightRatio = Math.round((float) orignalHeight / (float) maxHeight);
			final int widthRatio = Math.round((float) orignalWidth / (float) maxWidth);
			resizeScale = heightRatio < widthRatio ? heightRatio : widthRatio;
			resizeScale += 1;
		}
		Log.d(LOG_TAG, "resizeScale: "+resizeScale);

		//put the scale instruction (1 -> scale to (1/1); 8-> scale to 1/8)
		opts.inSampleSize = resizeScale;
		opts.inJustDecodeBounds = false;

		bp = BitmapFactory.decodeFile(path, opts);

		FileOutputStream out=null;
		try {
			out = new FileOutputStream(path);
			bp.compress(Bitmap.CompressFormat.JPEG, 80, out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try{
				Log.d(LOG_TAG, "resizeImg-Success");
				out.close();
			} 
			catch(Throwable t) {
				Log.e(LOG_TAG, "resizeImg-Error:" + t.getMessage(), t);

			}
		}
	}


	//************************************ END SETUP FOR IMAGE INPUT ******************************//	
	
	/***  back button onclick ****/
	private View.OnClickListener onClickbackbutton=new View.OnClickListener() {
		public void onClick(View v) {
			try{
				finish();
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onBackButton-Error:" + t.getMessage(), t); 
			}
		}

	};

	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d(LOG_TAG,"onResume.. " + capturingImage.toString());

		if (!capturingImage) {
			network_status = isNetworkAvailable();
			if(network_status){
				getInboxData Task = new getInboxData();
				Task.execute(new String[] { });  
			}
		} else {
			capturingImage = false;
		}
	}

	public static DisplayImageOptions getDisplayImageOptions() {
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}

	public void onBackPressed() {
		super.onBackPressed(); // allows standard use of backbutton for page 1
		this.finish();
		//overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
	}

	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();		
	}

}
