package com.m3tech.event;


import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.data.Helper_Content;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;

public class Event_Reservation extends Activity {

	private static final String LOG_TAG = "EVENT RESERVATION";
	public static final String PREFS_NAME = "MyPrefsFile";

	private SharedPreferences settings;
	Boolean network_status;
	Event_Setting event_setting;

	Helper_Content Helper_Content=null;
	String home_pagename, layout, page, userid, currentdate, customerID,app_title,colorcode,colorcode1,app_user,app_db,udid, tq_message;
	TextView product_cat_name,texttitlebar,texthome;
	ImageView back,tabheader;
	ImageView view_btn_cnfrm;

	//Progress
	static ProgressDialog progressDialog;

	//JSON
	JSONArray eventJSONArray,eventConfirmationJSONArray;
	SlotAction slotAction = null;
	
	Helper_Themeobject Helper_Themeobject = null;
	Context context = this;
	Header Header = null;
	
	Button btn_cnfrm_yes,btn_cnfrm_later,btn_reminder_yes,btn_reminder_no,btn_cnfrm;
	EditText edt_pax_limit;
	Spinner spinner_pax;
	
	String RESULT_CONFIRMATION = "";
	String RESULT_REMINDER = "";
	String RESULT_PAX = "";
	
	String id_,name_,email_,mobile_,event_id_,note_,pax_,notification_,top_id_,status_,is_imported_,user_id_,cdate_,mdate_,event_name_;
	String event_description_,date_start_,date_end_,time_start_,time_end_,venue_,cutoff_,pax_limit_;
	
	String MODE_COLOR = "1";
	String MODE_IMAGE = "2";
	
	String bgcolor_,fontcolor_,btncolor_,btnfontcolor_,headercolor_,headerfontcolor_,bg_img_,bg_going_img,bg_maybe_img,bg_not_going_img;
	
	ImageView layout_header_confirmation,layout_header_pax_limit,layout_header_reminder,layout_content;
	TextView txt_title_reminder,txt_title_pax,txt_title_cnfrm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING EVENT RESERVATION ");
		setContentView(R.layout.event_rsvp);
		slotAction = new SlotAction(this);
		Helper_Content = new Helper_Content(this);
		Helper_Themeobject = new Helper_Themeobject(this);
		event_setting = new Event_Setting(this);

		settings = this.getSharedPreferences(PREFS_NAME, 0);
		home_pagename = settings.getString("home_pagename", "");
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		customerID = settings.getString("customerID", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		colorcode1 = settings.getString("colorcode1", "");

		String pageTitle = settings.getString("eventPageTitle", "");
		if(pageTitle ==null||pageTitle.equals("")){
			pageTitle = app_title;
		}


		/*** layout linear declaration *****/

		tabheader = (ImageView) findViewById(R.id.tabheader);
		layout_header_confirmation = (ImageView)findViewById(R.id.layout_header_confirmation);
		layout_header_pax_limit = (ImageView)findViewById(R.id.layout_header_pax_limit);
		layout_header_reminder = (ImageView)findViewById(R.id.layout_header_reminder);
		layout_content = (ImageView)findViewById(R.id.layout_content);
		

		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		txt_title_cnfrm = (TextView) findViewById(R.id.txt_title_cnfrm);
		txt_title_pax = (TextView) findViewById(R.id.txt_title_pax);
		txt_title_reminder = (TextView) findViewById(R.id.txt_title_reminder);

		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, pageTitle,"");
		
		//declare variable
		btn_cnfrm_yes = (Button)findViewById(R.id.btn_cnfrm_yes);
		btn_cnfrm_later = (Button)findViewById(R.id.btn_cnfrm_later);
		btn_reminder_yes = (Button)findViewById(R.id.btn_reminder_yes);
		btn_reminder_no = (Button)findViewById(R.id.btn_reminder_no);
		btn_cnfrm = (Button)findViewById(R.id.btn_cnfrm);
		edt_pax_limit = (EditText)findViewById(R.id.edt_pax_limit);
		spinner_pax = (Spinner) findViewById(R.id.spinner_pax);
		
		//ImageView for background
		view_btn_cnfrm = (ImageView)findViewById(R.id.view_btn_cnfrm);
		
		//getIntent Data
		Intent r = getIntent();
		String event_id = r.getStringExtra("event_id");
		
		network_status = isNetworkAvailable();
		if(network_status == true){
			GetEventReservation Task = new GetEventReservation();
			Task.execute(new String[] { event_id });  
		}else{
			Toast.makeText(Event_Reservation.this,getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_LONG).show();
		} 
		
		
		
	}
	
	private View.OnClickListener onConfirmation=new View.OnClickListener() {

		public void onClick(View v) {
			try{
				
				Log.d(LOG_TAG, "click confirm!");
				
				int pax = 0;
				int pax_limit = Integer.parseInt(pax_limit_);
				
				if(pax_limit == 0){
					pax = Integer.parseInt(edt_pax_limit.getText().toString());
				}else{
					pax = Integer.parseInt(RESULT_PAX.toString());
				}
				
				if(pax_limit != 0){
					if(pax > pax_limit){
						Toast.makeText(context,"Sorry,Your pax only "+ pax_limit_+".",Toast.LENGTH_SHORT).show();
					}else{
						if(RESULT_CONFIRMATION.length()>0 && RESULT_REMINDER.length()>0 ){
							
							network_status = isNetworkAvailable();
							if(network_status == true){
								String pax_ = String.valueOf(pax);
								GetEventConfirmation Task = new GetEventConfirmation();
								Task.execute(new String[] { event_id_,RESULT_CONFIRMATION,RESULT_REMINDER,pax_ });  
							}else{
								Toast.makeText(Event_Reservation.this,getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_LONG).show();
							}
							
						}else{
							Toast.makeText(context,"Confirmation,Reminder and Pax is required.",Toast.LENGTH_SHORT).show();
						}
					}
				}else{
					if(RESULT_CONFIRMATION.length()>0 && RESULT_REMINDER.length()>0 ){
						
						network_status = isNetworkAvailable();
						if(network_status == true){
							String pax_ = String.valueOf(pax);
							GetEventConfirmation Task = new GetEventConfirmation();
							Task.execute(new String[] { event_id_,RESULT_CONFIRMATION,RESULT_REMINDER,pax_ });  
						}else{
							Toast.makeText(Event_Reservation.this,getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_LONG).show();
						}
						
					}else{
						Toast.makeText(context,"Confirmation,Reminder and Pax is required.",Toast.LENGTH_SHORT).show();
					}
				}
				
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onConfirmation-Error:" + t.getMessage(), t); 
			}
		}
	};
	
	private View.OnClickListener onPreConfirmation=new View.OnClickListener() {

		public void onClick(View v) {
			try{
				String value = v.getTag().toString();
				
				if(value.equals("btn_cnfrm_later")){
					Log.d(LOG_TAG, "value : " + value);
					
					//setting background selected
					if(headercolor_.length()>0){
						btn_cnfrm_later.setBackgroundColor(Color.parseColor(headercolor_));
					}else{
						btn_cnfrm_later.setBackgroundResource(R.color.row_even);
					}
					
					//setting textcolor selected
					if(headerfontcolor_.length()>0){
						btn_cnfrm_later.setTextColor(Color.parseColor(headerfontcolor_));
					}else{
						btn_cnfrm_later.setTextColor(Color.WHITE);
					}
					
					//setting background default
					btn_cnfrm_yes.setBackgroundResource(R.drawable.border_black_white);
					//set text color
					btn_cnfrm_yes.setTextColor(Color.BLACK);
					//set result
					RESULT_CONFIRMATION = "0";
					
				}else if(value.equals("btn_cnfrm_yes")){
					
					//setting background selected
					if(headercolor_.length()>0){
						btn_cnfrm_yes.setBackgroundColor(Color.parseColor(headercolor_));
					}else{
						btn_cnfrm_yes.setBackgroundResource(R.color.row_even);
					}
					
					//setting textcolor selected
					if(headerfontcolor_.length()>0){
						btn_cnfrm_yes.setTextColor(Color.parseColor(headerfontcolor_));
					}else{
						btn_cnfrm_yes.setTextColor(Color.WHITE);
					}
					
					//setting background default
					btn_cnfrm_later.setBackgroundResource(R.drawable.border_black_white);
					//set text color
					btn_cnfrm_later.setTextColor(Color.BLACK);
					//set result
					RESULT_CONFIRMATION = "1";
					
				}else if(value.equals("btn_reminder_yes")){
					
					//setting background selected
					if(headercolor_.length()>0){
						btn_reminder_yes.setBackgroundColor(Color.parseColor(headercolor_));
					}else{
						btn_reminder_yes.setBackgroundResource(R.color.row_even);
					}
					
					//setting textcolor selected
					if(headerfontcolor_.length()>0){
						btn_reminder_yes.setTextColor(Color.parseColor(headerfontcolor_));
					}else{
						btn_reminder_yes.setTextColor(Color.WHITE);
					}
					
					//setting background default
					btn_reminder_no.setBackgroundResource(R.drawable.border_black_white);
					//set text color
					btn_reminder_no.setTextColor(Color.BLACK);
					//set result
					RESULT_REMINDER = "1";
					
				}else if(value.equals("btn_reminder_no")){
					
					//setting background selected
					if(headercolor_.length()>0){
						btn_reminder_no.setBackgroundColor(Color.parseColor(headercolor_));
					}else{
						btn_reminder_no.setBackgroundResource(R.color.row_even);
					}
					
					//setting textcolor selected
					if(headerfontcolor_.length()>0){
						btn_reminder_no.setTextColor(Color.parseColor(headerfontcolor_));
					}else{
						btn_reminder_no.setTextColor(Color.WHITE);
					}
					
					//setting background default
					btn_reminder_yes.setBackgroundResource(R.drawable.border_black_white);
					//set text color
					btn_reminder_yes.setTextColor(Color.BLACK);
					//set result
					RESULT_REMINDER = "0";
					
				}else{
					//set all default
					btn_cnfrm_yes.setBackgroundResource(R.drawable.border_black_white);
					btn_cnfrm_later.setBackgroundResource(R.drawable.border_black_white);
					btn_reminder_yes.setBackgroundResource(R.drawable.border_black_white);
					btn_reminder_no.setBackgroundResource(R.drawable.border_black_white);
				}
				
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onPreConfirmation-Error:" + t.getMessage(), t); 
			}
		}
	};
	
	private class GetEventReservation extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Event_Reservation.this, "", getResources().getString(R.string.please_wait));
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;
			try{
				
				String event_id = arg0[0];
				responseBody = "";
				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_TOP;

				String strURL = getResources().getString(R.string.EVENT_RSVP_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&event_id=" + URLEncoder.encode(event_id, "UTF-8");

				HTTP_POST_TOP = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);
				//Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	
				eventJSONArray = new JSONArray(responseBody);
				response="SUCCESS";
				//Log.d(LOG_TAG,"responseBody="+ responseBody);	

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetEventList-doInBackground-Error:" + t.getMessage(), t); 
			}
			progressDialog.dismiss();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){

					if (eventJSONArray.length()>0){

						Log.d(LOG_TAG,"eventJSONArray lenght :"+ eventJSONArray.length());
						for (int i = 0; i < eventJSONArray.length(); i++) {

							id_ = eventJSONArray.getJSONObject(i).getString("id").toString();
							name_ = eventJSONArray.getJSONObject(i).getString("name").toString();
							email_ = eventJSONArray.getJSONObject(i).getString("email").toString();
							mobile_ = eventJSONArray.getJSONObject(i).getString("mobile").toString();
							event_id_ = eventJSONArray.getJSONObject(i).getString("event_id").toString();
							note_ = eventJSONArray.getJSONObject(i).getString("note").toString();
							pax_ = eventJSONArray.getJSONObject(i).getString("pax").toString();
							notification_ = eventJSONArray.getJSONObject(i).getString("notification").toString();
							top_id_ = eventJSONArray.getJSONObject(i).getString("top_id").toString();
							status_ = eventJSONArray.getJSONObject(i).getString("status").toString();
							is_imported_ = eventJSONArray.getJSONObject(i).getString("is_imported").toString();
							user_id_ = eventJSONArray.getJSONObject(i).getString("user_id").toString();
							cdate_ = eventJSONArray.getJSONObject(i).getString("cdate").toString();
							mdate_ = eventJSONArray.getJSONObject(i).getString("mdate").toString();
							event_name_ = eventJSONArray.getJSONObject(i).getString("event_name").toString();
							event_description_ = eventJSONArray.getJSONObject(i).getString("event_description").toString();
							date_start_ = eventJSONArray.getJSONObject(i).getString("date_start").toString();
							date_end_ = eventJSONArray.getJSONObject(i).getString("date_end").toString();
							time_start_ = eventJSONArray.getJSONObject(i).getString("time_start").toString();
							time_end_ = eventJSONArray.getJSONObject(i).getString("time_end").toString();
							venue_ = eventJSONArray.getJSONObject(i).getString("venue").toString();
							cutoff_ = eventJSONArray.getJSONObject(i).getString("cutoff").toString();
							pax_limit_ = eventJSONArray.getJSONObject(i).getString("pax_limit").toString();
							
							//14/9/2016 - nemi // add setting event
							bgcolor_ = eventJSONArray.getJSONObject(i).getString("bgcolor").toString();
							fontcolor_ = eventJSONArray.getJSONObject(i).getString("fontcolor").toString();
							btncolor_ = eventJSONArray.getJSONObject(i).getString("btncolor").toString();
							btnfontcolor_ = eventJSONArray.getJSONObject(i).getString("btnfontcolor").toString();
							headercolor_ = eventJSONArray.getJSONObject(i).getString("headercolor").toString();
							headerfontcolor_ = eventJSONArray.getJSONObject(i).getString("headerfontcolor").toString();
							bg_img_ = eventJSONArray.getJSONObject(i).getString("bg_img").toString();
							bg_going_img = eventJSONArray.getJSONObject(i).getString("bg_going_img").toString();
							bg_maybe_img = eventJSONArray.getJSONObject(i).getString("bg_maybe_img").toString();
							bg_not_going_img = eventJSONArray.getJSONObject(i).getString("bg_not_going_img").toString();
							
						}
						
						//setting background layout
						if(bg_img_.length()>0){
							event_setting.SetBackgroundSlot(layout_content, MODE_IMAGE, bg_img_);
						}else if(bgcolor_.length()>0){
							event_setting.SetBackgroundSlot(layout_content, MODE_COLOR, bgcolor_);
						}

						
						//Setting background header
						if(headercolor_.length()>0){
							event_setting.SetBackgroundSlot(view_btn_cnfrm, MODE_COLOR, headercolor_);
							event_setting.SetBackgroundSlot(layout_header_confirmation, MODE_COLOR, headercolor_);
							event_setting.SetBackgroundSlot(layout_header_pax_limit, MODE_COLOR, headercolor_);
							event_setting.SetBackgroundSlot(layout_header_reminder, MODE_COLOR, headercolor_);
						}
						
						//setting textcolor header
						if(headerfontcolor_.length()>0){
							btn_cnfrm.setTextColor(Color.parseColor(headerfontcolor_));
							txt_title_reminder.setTextColor(Color.parseColor(headerfontcolor_));
							txt_title_pax.setTextColor(Color.parseColor(headerfontcolor_));
							txt_title_cnfrm.setTextColor(Color.parseColor(headerfontcolor_));
						}
						
						Log.d(LOG_TAG, "Pax Limit : " + pax_limit_);
						//set TextView Data
						if(pax_limit_.length()>0 && pax_limit_ != null){
							
							if(pax_limit_.equals("0") || pax_limit_ == "0"){
								
								edt_pax_limit.setVisibility(View.VISIBLE);
								spinner_pax.setVisibility(View.GONE);
								edt_pax_limit.setText(pax_limit_.toString());
							}else{
								
								edt_pax_limit.setVisibility(View.GONE);
								spinner_pax.setVisibility(View.VISIBLE);
								addItemsOnSpinner(Integer.parseInt(pax_limit_.toString()));
							}
							
						}
						
						//set default button confirmation
						if(status_.equals("1") || status_ == "1"){ //confirmation == yes
							
							//setting background selected
							if(headercolor_.length()>0){
								btn_cnfrm_yes.setBackgroundColor(Color.parseColor(headercolor_));
							}else{
								btn_cnfrm_yes.setBackgroundResource(R.color.row_even);
							}
							
							//setting textcolor selected
							if(headerfontcolor_.length()>0){
								btn_cnfrm_yes.setTextColor(Color.parseColor(headerfontcolor_));
							}else{
								btn_cnfrm_yes.setTextColor(Color.WHITE);
							}
							
							//setting background default
							btn_cnfrm_later.setBackgroundResource(R.drawable.border_black_white);
							//set text color
							btn_cnfrm_later.setTextColor(Color.BLACK);
							//set result
							RESULT_CONFIRMATION = "1";
							
						}else if(status_.equals("0")  || status_ == "0"){ //confirmation == later
							
							//setting background selected
							if(headercolor_.length()>0){
								btn_cnfrm_later.setBackgroundColor(Color.parseColor(headercolor_));
							}else{
								btn_cnfrm_later.setBackgroundResource(R.color.row_even);
							}
							
							//setting textcolor selected
							if(headerfontcolor_.length()>0){
								btn_cnfrm_later.setTextColor(Color.parseColor(headerfontcolor_));
							}else{
								btn_cnfrm_later.setTextColor(Color.WHITE);
							}
							
							//setting background default
							btn_cnfrm_yes.setBackgroundResource(R.drawable.border_black_white);
							//set text color
							btn_cnfrm_yes.setTextColor(Color.BLACK);
							//set result
							RESULT_CONFIRMATION = "0";
						}else{
							
							
							//setting background default
							btn_cnfrm_later.setBackgroundResource(R.drawable.border_black_white);
							btn_cnfrm_yes.setBackgroundResource(R.drawable.border_black_white);
							//set text color
							btn_cnfrm_later.setTextColor(Color.BLACK);
							btn_cnfrm_yes.setTextColor(Color.BLACK);
							//set result
							RESULT_CONFIRMATION = "0";
						}
						
						//set default button reminder
						if(notification_.equals("1") || notification_ == "1"){ //reminder == yes
							
							//setting background selected
							if(headercolor_.length()>0){
								btn_reminder_yes.setBackgroundColor(Color.parseColor(headercolor_));
							}else{
								btn_reminder_yes.setBackgroundResource(R.color.row_even);
							}
							
							//setting textcolor selected
							if(headerfontcolor_.length()>0){
								btn_reminder_yes.setTextColor(Color.parseColor(headerfontcolor_));
							}else{
								btn_reminder_yes.setTextColor(Color.WHITE);
							}
							
							//setting background default
							btn_reminder_no.setBackgroundResource(R.drawable.border_black_white);
							//set text color
							btn_reminder_no.setTextColor(Color.BLACK);
							//set result
							RESULT_REMINDER = "1";
							
						}else if(notification_.equals("0") || notification_ == "0"){ //reminder == no
							
							//setting background selected
							if(headercolor_.length()>0){
								btn_reminder_no.setBackgroundColor(Color.parseColor(headercolor_));
							}else{
								btn_reminder_no.setBackgroundResource(R.color.row_even);
							}
							
							//setting textcolor selected
							if(headerfontcolor_.length()>0){
								btn_reminder_no.setTextColor(Color.parseColor(headerfontcolor_));
							}else{
								btn_reminder_no.setTextColor(Color.WHITE);
							}
							
							//setting background default
							btn_reminder_yes.setBackgroundResource(R.drawable.border_black_white);
							//set text color
							btn_reminder_yes.setTextColor(Color.BLACK);
							//set result
							RESULT_REMINDER = "0";
						}
						
						
						//onclick
						btn_cnfrm_yes.setOnClickListener(onPreConfirmation);
						btn_cnfrm_later.setOnClickListener(onPreConfirmation);
						btn_reminder_yes.setOnClickListener(onPreConfirmation);
						btn_reminder_no.setOnClickListener(onPreConfirmation);
						btn_cnfrm.setOnClickListener(onConfirmation);
						

					}else{
						Toast.makeText(Event_Reservation.this,"No Event Invitation data.", Toast.LENGTH_LONG).show();
					}

				}else{
					Toast.makeText(Event_Reservation.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "GetEventReservation-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}
	
	private class GetEventConfirmation extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;
			try{
				
				String event_id = arg0[0];
				String confirmation = arg0[1];
				String reminder = arg0[2];
				String pax = arg0[3];
				
				responseBody = "";
				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_TOP;

				String strURL = getResources().getString(R.string.EVENT_RSVP_UPDATE_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&event_id=" + URLEncoder.encode(event_id, "UTF-8")
						+ "&confirmation=" + URLEncoder.encode(confirmation, "UTF-8")
						+ "&reminder=" + URLEncoder.encode(reminder, "UTF-8")
						+ "&pax=" + URLEncoder.encode(pax, "UTF-8");

				HTTP_POST_TOP = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);
				//Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	
				eventConfirmationJSONArray = new JSONArray(responseBody);
				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetEventConfirmation-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){

					if (eventConfirmationJSONArray.length()>0){

						Log.d(LOG_TAG,"eventConfirmationJSONArray lenght :"+ eventConfirmationJSONArray.length());
						for (int i = 0; i < eventConfirmationJSONArray.length(); i++) {

							String status = eventConfirmationJSONArray.getJSONObject(i).getString("status").toString();
							String desc = eventConfirmationJSONArray.getJSONObject(i).getString("desc").toString();
							Log.d(LOG_TAG, desc);
							
							if(status.equals("1") || status == "1"){
								finish();
							}
							Toast.makeText(context,"Thank You.",Toast.LENGTH_SHORT).show();
						}
						
					}else{
						Toast.makeText(Event_Reservation.this,"Failed.", Toast.LENGTH_LONG).show();
					}

				}else{
					Toast.makeText(Event_Reservation.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "GetEventConfirmation-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}
	
	//Spinner Pax Number
	public void addItemsOnSpinner(int max) {

        List<String> list = new ArrayList<String>();
        for (int i=1; i<=max; i++){
            list.add(String.valueOf(i));
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_pax.setAdapter(dataAdapter);
        spinner_pax.setOnItemSelectedListener(new Pax_Selected());
    }
	
	public class Pax_Selected implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

			try {
				RESULT_PAX = parent.getItemAtPosition(pos).toString();
				//Toast.makeText(Event_Reservation.this,parent.getItemAtPosition(pos).toString(), Toast.LENGTH_SHORT).show();

			} catch (Throwable t) {
				Log.e(LOG_TAG, "Getproduct-doInBackground-Error :" + t.getMessage(), t);
			}

		}

		@SuppressWarnings("rawtypes")
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}
	
	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;
	}
	
	public void onBackPressed() {
		super.onBackPressed(); // allows standard use of backbutton for page 1
		this.finish();
		overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
	}

	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();		
	}

}
