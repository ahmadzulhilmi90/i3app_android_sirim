package com.m3tech.event;


import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.collection.Collection_Event;
import com.m3tech.data.Helper_Content;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;

public class Event_List extends Activity {

	private static final String LOG_TAG = "EVENT LIST";
	public static final String PREFS_NAME = "MyPrefsFile";

	private SharedPreferences settings;
	Boolean network_status;

	Helper_Content Helper_Content=null;
	String top_id,home_pagename, layout, page, userid, currentdate, customerID,app_title,colorcode,colorcode1,app_user,app_db,udid, tq_message;
	TextView product_cat_name,texttitlebar,texthome;
	ImageView back,tabheader;

	//Progress
	static ProgressDialog progressDialog;

	//JSON
	JSONArray eventGetGuestEventJSONArray,eventJSONArray,eventGeneralSettingJSONArray;
	SlotAction slotAction = null;
	
	Helper_Themeobject Helper_Themeobject = null;
	Context context = this;
	Header Header = null;
	
	List<Collection_Event> model_event=new ArrayList<Collection_Event>();
	Event_Adapter adapter_event=null;
	Collection_Event current_event=null;
	
	ImageView layout_content;
	String event_gps_img;
	String event_fontcolor;
	String event_alternatefontcolor;
	String event_rowbgcolor;
	String event_alternaterowbgcolor;
	
	Event_Setting Event_Setting;
	
	String MODE_COLOR = "1";
	String MODE_IMAGE = "2";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING EVENT LIST ");
		setContentView(R.layout.event_list);
		slotAction = new SlotAction(this);
		Helper_Content = new Helper_Content(this);
		Helper_Themeobject = new Helper_Themeobject(this);
		Event_Setting = new Event_Setting(this);

		settings = this.getSharedPreferences(PREFS_NAME, 0);
		home_pagename = settings.getString("home_pagename", "");
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		customerID = settings.getString("customerID", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		colorcode1 = settings.getString("colorcode1", "");

		String pageTitle = settings.getString("eventPageTitle", "");
		if(pageTitle ==null||pageTitle.equals("")){
			pageTitle = app_title;
		}


		/*** layout linear declaration *****/

		tabheader = (ImageView) findViewById(R.id.tabheader);
		layout_content = (ImageView)findViewById(R.id.layout_content);

		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);

		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, pageTitle,"");
		
		Intent r = getIntent();
		top_id = r.getStringExtra("topid_event");
		Log.d(LOG_TAG, "TOP ID EVENT PARENT : " + top_id);

		network_status = isNetworkAvailable();
		if(network_status == true){
			GetEventGeneralSetting Task = new GetEventGeneralSetting();
			Task.execute(new String[] { });  
			
		}else{
			
			Toast.makeText(Event_List.this,getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_LONG).show();
		} 
	}
	
	//1. Get Guest Event
	
	private class GetEventGetGuest extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Event_List.this, "", getResources().getString(R.string.please_wait));
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;
			try{
				responseBody = "";
				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_TOP;

				String strURL = getResources().getString(R.string.EVENT_GET_GUEST_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&top=" + URLEncoder.encode(top_id, "UTF-8");

				HTTP_POST_TOP = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);
				//Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	
				eventGetGuestEventJSONArray = new JSONArray(responseBody);
				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetEventGetGuest-doInBackground-Error:" + t.getMessage(), t); 
			}
			progressDialog.dismiss();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){

					if (eventGetGuestEventJSONArray.length()>0){

						Log.d(LOG_TAG,"eventJSONArray lenght :"+ eventGetGuestEventJSONArray.length());
						for (int i = 0; i < eventGetGuestEventJSONArray.length(); i++) {

							String status = eventGetGuestEventJSONArray.getJSONObject(i).getString("status").toString();
							//String desc = eventGetGuestEventJSONArray.getJSONObject(i).getString("desc").toString();
							
							if(status.equals("1")){
								
								String event_id = eventGetGuestEventJSONArray.getJSONObject(i).getString("event_id").toString();
								
								if(event_id.length()>0){
									// Get Event List
									GetEventList Task = new GetEventList();
									Task.execute(new String[] { event_id });
									
								}else{
									Toast.makeText(Event_List.this,"You currently have no events!", Toast.LENGTH_LONG).show();
								}
							}else{
								Toast.makeText(Event_List.this,"You currently have no events!", Toast.LENGTH_LONG).show();
							}
						}
					}else{
						Toast.makeText(Event_List.this,"You currently have no event!", Toast.LENGTH_LONG).show();
					}
				}else{
					Toast.makeText(Event_List.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}
			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "GetEventGetGuest-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}
	
	//CALL API - EVENT GENERAL SETTING
	
	private class GetEventGeneralSetting extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Event_List.this, "", getResources().getString(R.string.please_wait));
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;
			try{
				
				responseBody = "";
				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_TOP;

				String strURL = getResources().getString(R.string.EVENT_GENERAL_SETTING_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&top=" + URLEncoder.encode(top_id, "UTF-8");

				HTTP_POST_TOP = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);
				//Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	
				eventGeneralSettingJSONArray = new JSONArray(responseBody);
				response="SUCCESS";
				//Log.d(LOG_TAG,"responseBody="+ responseBody);	

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetEventGeneralSetting-doInBackground-Error:" + t.getMessage(), t); 
			}
			progressDialog.dismiss();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){

					if (eventGeneralSettingJSONArray.length()>0){

						Log.d(LOG_TAG,"eventGeneralSettingJSONArray lenght :"+ eventGeneralSettingJSONArray.length());
						for (int i = 0; i < eventGeneralSettingJSONArray.length(); i++) {

							//String id = eventGeneralSettingJSONArray.getJSONObject(i).getString("id").toString();
							String bg_img = eventGeneralSettingJSONArray.getJSONObject(i).getString("bg_img").toString();
							String bgcolor = eventGeneralSettingJSONArray.getJSONObject(i).getString("bgcolor").toString();
							String fontcolor = eventGeneralSettingJSONArray.getJSONObject(i).getString("fontcolor").toString();
							String alternatefontcolor = eventGeneralSettingJSONArray.getJSONObject(i).getString("alternatefontcolor").toString();
							String rowbgcolor = eventGeneralSettingJSONArray.getJSONObject(i).getString("rowbgcolor").toString();
							String alternaterowbgcolor = eventGeneralSettingJSONArray.getJSONObject(i).getString("alternaterowbgcolor").toString();
							String gps_img = eventGeneralSettingJSONArray.getJSONObject(i).getString("gps_img").toString();
							//String top_id = eventGeneralSettingJSONArray.getJSONObject(i).getString("top_id").toString();
							//String user_id = eventGeneralSettingJSONArray.getJSONObject(i).getString("user_id").toString();
							
							event_gps_img = gps_img;
							event_fontcolor = fontcolor;
							event_alternatefontcolor = alternatefontcolor;
							event_rowbgcolor = rowbgcolor;
							event_alternaterowbgcolor = alternaterowbgcolor;
							
							String file = "#ffffff";
							String mode = null;
							if(bg_img.length()>0){
								mode = MODE_IMAGE;
								file = bg_img.toString();
							}else if(bgcolor.length()>0){
								mode = MODE_COLOR;
								file = bgcolor.toString();
							}else{
								mode = MODE_COLOR;
								file = "#ffffff";
							}
							
							Event_Setting.SetBackgroundSlot(layout_content, mode, file);
							
							//Get GetEventGetGuest
							GetEventGetGuest Task = new GetEventGetGuest();
							Task.execute(new String[] { }); 
							
						}	

					}else{
						Toast.makeText(Event_List.this,"Sorry, General Setting have problem.", Toast.LENGTH_LONG).show();
					}
				}
			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "GetEventGeneralSetting-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}
	
	private class GetEventList extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;
			try{
				
				String event_id = arg0[0];
				responseBody = "";
				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_TOP;

				String strURL = getResources().getString(R.string.EVENT_LIST_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&top=" + URLEncoder.encode(top_id, "UTF-8")
						+ "&eventlist=" + URLEncoder.encode(event_id, "UTF-8");

				HTTP_POST_TOP = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);
				//Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	
				eventJSONArray = new JSONArray(responseBody);
				response="SUCCESS";
				//Log.d(LOG_TAG,"responseBody="+ responseBody);	

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetEventList-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				ListView listView = (ListView) findViewById(R.id.listevent);
				adapter_event=new Event_Adapter ();
				adapter_event.clear();
				adapter_event.notifyDataSetChanged();
				listView.setAdapter(null);

				if (result.equals("SUCCESS")){

					if (eventJSONArray.length()>0){

						Log.d(LOG_TAG,"eventJSONArray lenght :"+ eventJSONArray.length());
						for (int i = 0; i < eventJSONArray.length(); i++) {

							current_event = new Collection_Event();
							current_event.setrow_id(eventJSONArray.getJSONObject(i).getString("id").toString());
							current_event.setevent_name(eventJSONArray.getJSONObject(i).getString("event_name").toString());
							current_event.setevent_description(eventJSONArray.getJSONObject(i).getString("event_description").toString());
							current_event.setvenue(eventJSONArray.getJSONObject(i).getString("venue").toString());
							current_event.setdate_start(eventJSONArray.getJSONObject(i).getString("date_start").toString());
							current_event.setdate_end(eventJSONArray.getJSONObject(i).getString("date_end").toString());
							current_event.settime_start(eventJSONArray.getJSONObject(i).getString("time_start").toString());
							current_event.settime_end(eventJSONArray.getJSONObject(i).getString("time_end").toString());
							current_event.setpax_limit(eventJSONArray.getJSONObject(i).getString("pax_limit").toString());
							current_event.setlatitude(eventJSONArray.getJSONObject(i).getString("latitude").toString());
							current_event.setlongitude(eventJSONArray.getJSONObject(i).getString("longitude").toString());
							current_event.seturl(eventJSONArray.getJSONObject(i).getString("url").toString());
							current_event.setcutoff(eventJSONArray.getJSONObject(i).getString("cutoff").toString());
							current_event.setnotification(eventJSONArray.getJSONObject(i).getString("notification").toString());
							current_event.setstatus(eventJSONArray.getJSONObject(i).getString("status").toString());
							current_event.setcdate(eventJSONArray.getJSONObject(i).getString("cdate").toString());
							current_event.setmdate(eventJSONArray.getJSONObject(i).getString("mdate").toString());
							current_event.setuser_id(eventJSONArray.getJSONObject(i).getString("user_id").toString());
							current_event.settop_id(eventJSONArray.getJSONObject(i).getString("top_id").toString());
							current_event.setschedule_date(eventJSONArray.getJSONObject(i).getString("schedule_date").toString());
							current_event.setpage_id(eventJSONArray.getJSONObject(i).getString("page_id").toString());
							current_event.setnum(String.valueOf(i));
							
							adapter_event.add(current_event);

						}
						listView.setAdapter(adapter_event);			
						listView.setOnItemClickListener(onListClickEvent);	

					}else{
						Toast.makeText(Event_List.this,"You currently have no events!", Toast.LENGTH_LONG).show();
					}

				}else{
					Toast.makeText(Event_List.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "GetEventList-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}



	class Event_Adapter extends ArrayAdapter<Collection_Event> {
		Event_Adapter() {
			super(Event_List.this, R.layout.event_row, model_event);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View row=convertView;
			NotificationHolderhome holder=null;

			if (row==null) {													
				LayoutInflater inflater=getLayoutInflater();

				row=inflater.inflate(R.layout.event_row, parent, false);
				holder=new NotificationHolderhome(row);
				row.setTag(holder);
			} else {
				holder=(NotificationHolderhome)row.getTag();
			}
			
			Log.d(LOG_TAG,"Event Name :"+ model_event.get(position).getevent_name());
			holder.populateFromhome(model_event.get(position));

			return(row);
		}
	}

	class NotificationHolderhome {
		@SuppressWarnings("unused")
		private View row=null;
		private TextView txt_date = null;
		private TextView txt_title = null;
		private ImageView txt_gps = null;
		private ImageView row_content = null;

		NotificationHolderhome(View row) {
			this.row=row;	
			txt_date=(TextView) row.findViewById(R.id.txt_date);
			txt_title=(TextView) row.findViewById(R.id.txt_title);
			txt_gps=(ImageView) row.findViewById(R.id.txt_gps);
			row_content = (ImageView)row.findViewById(R.id.row_content);
		}

		void populateFromhome(final Collection_Event r) {
			
			//setting row color
			if(event_alternaterowbgcolor.length()>0 && event_rowbgcolor.length()>0){
				int num = Integer.parseInt(r.getnum())+1;
				if (num % 2 == 1) {
					Event_Setting.SetBackgroundSlot(row_content, MODE_COLOR, event_rowbgcolor);
			    } else {
			    	Event_Setting.SetBackgroundSlot(row_content, MODE_COLOR, event_alternaterowbgcolor);
			    }
			}else{
				int num = Integer.parseInt(r.getnum())+1;
				if (num % 2 == 1) {
					row_content.setBackgroundResource(R.color.row_odd);
			    } else {
			    	row_content.setBackgroundResource(R.color.row_even);
			    }
			}
			
			//setting row text color
			if(event_fontcolor.length()>0 && event_alternatefontcolor.length()>0){
				int num = Integer.parseInt(r.getnum())+1;
				if (num % 2 == 1) {
					txt_date.setTextColor(Color.parseColor(event_fontcolor));
					txt_title.setTextColor(Color.parseColor(event_fontcolor));
			    } else {
			    	txt_date.setTextColor(Color.parseColor(event_alternatefontcolor));
					txt_title.setTextColor(Color.parseColor(event_alternatefontcolor));
			    }
			}
			
			txt_date.setText(r.getdate_start().toString());
			txt_title.setText(r.getevent_name().toString());
			String gps_loc = r.getlatitude().toString()+","+r.getlongitude().toString();
			txt_gps.setTag(gps_loc);
			
			//setting gps img
			if(event_gps_img.length()>0){
				Event_Setting.SetBackgroundSlot(txt_gps, MODE_IMAGE, event_gps_img);
			}
			
			//onclick
			txt_gps.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					double latitude = Double.parseDouble(r.getlatitude().toString());
					double longitude = Double.parseDouble(r.getlongitude().toString());
					String label = r.getevent_name().toString();
					String uriBegin = "geo:" + latitude + "," + longitude;
					String query = latitude + "," + longitude + "(" + label + ")";
					String encodedQuery = Uri.encode(query);
					String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
					Uri uri = Uri.parse(uriString);
					Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
					startActivity(intent);
				
				}
			});
			
		}

	}
	

	private AdapterView.OnItemClickListener onListClickEvent=new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent,View view, int position,long id) {
			
			current_event=model_event.get(position);	
			Log.d(LOG_TAG,"onListClickEvent ="+current_event.getevent_name());
			slotAction.CreateNextThemePage(current_event.getpage_id().toString(),current_event.getrow_id().toString());
		}
	};

	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;
	}
	
	public void onBackPressed() {
		super.onBackPressed(); // allows standard use of backbutton for page 1
		this.finish();
		overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
	}

	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();		
	}

}
