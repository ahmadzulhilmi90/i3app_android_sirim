package com.m3tech.event;


import java.net.URLEncoder;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.data.Helper_Content;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.header.Header;

public class Event_Detail extends Activity {

	private static final String LOG_TAG = "EVENT DETAIL";
	public static final String PREFS_NAME = "MyPrefsFile";

	private SharedPreferences settings;
	Boolean network_status;

	Helper_Content Helper_Content=null;
	String home_pagename, layout, page, userid, currentdate, customerID,app_title,colorcode,colorcode1,app_user,app_db,udid, tq_message;
	TextView product_cat_name,texttitlebar,texthome;
	ImageView back,tabheader;

	//Progress
	static ProgressDialog progressDialog;

	//JSON
	JSONArray eventConfirmationJSONArray,eventJSONArray;
	SlotAction slotAction = null;
	
	Helper_Themeobject Helper_Themeobject = null;
	Context context = this;
	Header Header = null;
	
	TextView txt_event_detail,txt_date,txt_time,txt_address,txt_cut_off_date;
	Button btn_yes,btn_no,btn_maybe;
	
	String RESULT_CONFIRMATION_NO = "3",RESULT_REMINDER_NO = "3", RESULT_PAX_NO = "1";
	
	String row_id,cutoff_,venue_,pax_limit_,time_end_,time_start_,date_end_,date_start_,event_description_,id_,name_,email_,mobile_,event_id_,note_,pax_,notification_,top_id_,status_,is_imported_,user_id_,cdate_,mdate_,event_name_;
	String bgcolor_,fontcolor_,btncolor_,btnfontcolor_,headercolor_,headerfontcolor_,bg_img_,bg_going_img,bg_maybe_img,bg_not_going_img;
	//String row_id,event_name,event_description,venue,date_start,date_end,time_start,time_end,pax_limit,latitude,longitude,url,cutoff,notification,status,cdate,mdate,user_id,top_id,schedule_date,page_id;
	
	
	ImageView layout_content,layout_event_btn_no,layout_event_btn_yes,layout_event_btn_maybe;
	TextView title_1,title_2,title_3,title_4,title_5;
	
	Event_Setting Event_Setting;
	
	String MODE_COLOR = "1";
	String MODE_IMAGE = "2";
	
	String ROW_COLOR_EVENT = "#82DACA";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING EVENT DETAIL ");
		setContentView(R.layout.event_details);
		slotAction = new SlotAction(this);
		Helper_Content = new Helper_Content(this);
		Helper_Themeobject = new Helper_Themeobject(this);
		Event_Setting = new Event_Setting(this);

		settings = this.getSharedPreferences(PREFS_NAME, 0);
		home_pagename = settings.getString("home_pagename", "");
		app_user = getResources().getString(R.string.app_user);
		app_db = getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		customerID = settings.getString("customerID", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		colorcode1 = settings.getString("colorcode1", "");

		String pageTitle = settings.getString("eventPageTitle", "");
		if(pageTitle ==null||pageTitle.equals("")){
			pageTitle = app_title;
		}


		/*** layout linear declaration *****/

		tabheader = (ImageView) findViewById(R.id.tabheader);
		layout_content = (ImageView) findViewById(R.id.layout_content);
		layout_event_btn_yes = (ImageView) findViewById(R.id.layout_event_btn_yes);
		layout_event_btn_no = (ImageView) findViewById(R.id.layout_event_btn_no);
		layout_event_btn_maybe = (ImageView) findViewById(R.id.layout_event_btn_maybe);

		/*** text view *******/
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);

		/**** home & back button *****/
		LinearLayout tabback = (LinearLayout) findViewById(R.id.tabback);
		LinearLayout tabhome = (LinearLayout) findViewById(R.id.tabhome);
		ImageView img_back = (ImageView) findViewById(R.id.back);
		ImageView img_home = (ImageView) findViewById(R.id.home);
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, pageTitle,"");
		
		//declare variable
		txt_event_detail = (TextView)findViewById(R.id.txt_event_detail);
		txt_date = (TextView)findViewById(R.id.txt_date);
		txt_time = (TextView)findViewById(R.id.txt_time);
		txt_address = (TextView)findViewById(R.id.txt_address);
		txt_cut_off_date = (TextView)findViewById(R.id.txt_cut_off_date);
		btn_yes = (Button)findViewById(R.id.btn_yes);
		btn_no = (Button)findViewById(R.id.btn_no);
		btn_maybe = (Button)findViewById(R.id.btn_maybe);
		
		title_1 = (TextView)findViewById(R.id.title_1);
		title_2 = (TextView)findViewById(R.id.title_2);
		title_3 = (TextView)findViewById(R.id.title_3);
		title_4 = (TextView)findViewById(R.id.title_4);
		title_5 = (TextView)findViewById(R.id.title_5);
		
		//Get Intent Data // 
		
		Intent r = getIntent();
		row_id = r.getStringExtra("event_id");
		
		network_status = isNetworkAvailable();
		if(network_status == true){
			GetEventReservation task = new GetEventReservation();
			task.execute(new String[] { row_id });
		}else{
			Toast.makeText(Event_Detail.this,getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_LONG).show();
		}
	}
	
	private View.OnClickListener onClickEvent=new View.OnClickListener() {

		public void onClick(View v) {
			try{
				
				String value = v.getTag().toString();
				Log.d(LOG_TAG, "Tag Value : " + value);
				
				network_status = isNetworkAvailable();
				if(network_status == true){
					
					if(!value.equals("no")){
						Intent to = new Intent(Event_Detail.this,Event_Reservation.class);
						to.putExtra("event_id", row_id);
						startActivity(to);
						
					}else if(value.equals("no")){
					
						GetEventConfirmation Task = new GetEventConfirmation();
						Task.execute(new String[] { row_id,RESULT_CONFIRMATION_NO,RESULT_REMINDER_NO,RESULT_PAX_NO });  
						
					}else{
						finish();
					}
					
				}else{
					Toast.makeText(Event_Detail.this,getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_LONG).show();
				}
				
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onClickEvent-Error:" + t.getMessage(), t); 

			}
		}
	};
	
	private class GetEventConfirmation extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Event_Detail.this, "", getResources().getString(R.string.please_wait));
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;
			try{
				
				String event_id = arg0[0];
				String confirmation = arg0[1];
				String reminder = arg0[2];
				String pax = arg0[3];
				
				responseBody = "";
				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_TOP;

				String strURL = getResources().getString(R.string.EVENT_RSVP_UPDATE_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&event_id=" + URLEncoder.encode(event_id, "UTF-8")
						+ "&confirmation=" + URLEncoder.encode(confirmation, "UTF-8")
						+ "&reminder=" + URLEncoder.encode(reminder, "UTF-8")
						+ "&pax=" + URLEncoder.encode(pax, "UTF-8");

				HTTP_POST_TOP = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);
				//Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	
				eventConfirmationJSONArray = new JSONArray(responseBody);
				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetEventConfirmation-doInBackground-Error:" + t.getMessage(), t); 
			}
			progressDialog.dismiss();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){

					if (eventConfirmationJSONArray.length()>0){

						Log.d(LOG_TAG,"eventConfirmationJSONArray lenght :"+ eventConfirmationJSONArray.length());
						for (int i = 0; i < eventConfirmationJSONArray.length(); i++) {

							String status = eventConfirmationJSONArray.getJSONObject(i).getString("status").toString();
							String desc = eventConfirmationJSONArray.getJSONObject(i).getString("desc").toString();
							Log.d(LOG_TAG, desc);
							
							if(status.equals("1") || status == "1"){
								//finish();
							}
							Toast.makeText(context,"Thank You.",Toast.LENGTH_SHORT).show();
						}
						
					}else{
						Toast.makeText(Event_Detail.this,"Failed.", Toast.LENGTH_LONG).show();
					}

				}else{
					Toast.makeText(Event_Detail.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "GetEventConfirmation-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}
	private class GetEventReservation extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Event_Detail.this, "", getResources().getString(R.string.please_wait));
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;
			try{
				
				String event_id = arg0[0];
				responseBody = "";
				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_TOP;

				String strURL = getResources().getString(R.string.EVENT_RSVP_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(customerID, "UTF-8")
						+ "&event_id=" + URLEncoder.encode(event_id, "UTF-8");

				HTTP_POST_TOP = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);
				//Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	
				eventJSONArray = new JSONArray(responseBody);
				response="SUCCESS";
				//Log.d(LOG_TAG,"responseBody="+ responseBody);	

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetEventList-doInBackground-Error:" + t.getMessage(), t); 
			}
			progressDialog.dismiss();
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){

					if (eventJSONArray.length()>0){

						Log.d(LOG_TAG,"eventJSONArray lenght :"+ eventJSONArray.length());
						for (int i = 0; i < eventJSONArray.length(); i++) {

							id_ = eventJSONArray.getJSONObject(i).getString("id").toString();
							name_ = eventJSONArray.getJSONObject(i).getString("name").toString();
							email_ = eventJSONArray.getJSONObject(i).getString("email").toString();
							mobile_ = eventJSONArray.getJSONObject(i).getString("mobile").toString();
							event_id_ = eventJSONArray.getJSONObject(i).getString("event_id").toString();
							note_ = eventJSONArray.getJSONObject(i).getString("note").toString();
							pax_ = eventJSONArray.getJSONObject(i).getString("pax").toString();
							notification_ = eventJSONArray.getJSONObject(i).getString("notification").toString();
							top_id_ = eventJSONArray.getJSONObject(i).getString("top_id").toString();
							status_ = eventJSONArray.getJSONObject(i).getString("status").toString();
							is_imported_ = eventJSONArray.getJSONObject(i).getString("is_imported").toString();
							user_id_ = eventJSONArray.getJSONObject(i).getString("user_id").toString();
							cdate_ = eventJSONArray.getJSONObject(i).getString("cdate").toString();
							mdate_ = eventJSONArray.getJSONObject(i).getString("mdate").toString();
							event_name_ = eventJSONArray.getJSONObject(i).getString("event_name").toString();
							event_description_ = eventJSONArray.getJSONObject(i).getString("event_description").toString();
							date_start_ = eventJSONArray.getJSONObject(i).getString("date_start").toString();
							date_end_ = eventJSONArray.getJSONObject(i).getString("date_end").toString();
							time_start_ = eventJSONArray.getJSONObject(i).getString("time_start").toString();
							time_end_ = eventJSONArray.getJSONObject(i).getString("time_end").toString();
							venue_ = eventJSONArray.getJSONObject(i).getString("venue").toString();
							cutoff_ = eventJSONArray.getJSONObject(i).getString("cutoff").toString();
							pax_limit_ = eventJSONArray.getJSONObject(i).getString("pax_limit").toString();
							
							//14/9/2016 - nemi // add setting event
							bgcolor_ = eventJSONArray.getJSONObject(i).getString("bgcolor").toString();
							fontcolor_ = eventJSONArray.getJSONObject(i).getString("fontcolor").toString();
							btncolor_ = eventJSONArray.getJSONObject(i).getString("btncolor").toString();
							btnfontcolor_ = eventJSONArray.getJSONObject(i).getString("btnfontcolor").toString();
							headercolor_ = eventJSONArray.getJSONObject(i).getString("headercolor").toString();
							headerfontcolor_ = eventJSONArray.getJSONObject(i).getString("headerfontcolor").toString();
							bg_img_ = eventJSONArray.getJSONObject(i).getString("bg_img").toString();
							bg_going_img = eventJSONArray.getJSONObject(i).getString("bg_going_img").toString();
							bg_maybe_img = eventJSONArray.getJSONObject(i).getString("bg_maybe_img").toString();
							bg_not_going_img = eventJSONArray.getJSONObject(i).getString("bg_not_going_img").toString();
							
							Log.d(LOG_TAG, "bgcolor_ : " + bgcolor_);
							Log.d(LOG_TAG, "fontcolor_ : " + fontcolor_);
							Log.d(LOG_TAG, "btncolor_ : " + btncolor_);
							Log.d(LOG_TAG, "headercolor_ : " + headercolor_);
							Log.d(LOG_TAG, "headerfontcolor_ : " + headerfontcolor_);
							Log.d(LOG_TAG, "bg_img_ : " + bg_img_);
							Log.d(LOG_TAG, "bg_going_img_ : " + bg_going_img);
							Log.d(LOG_TAG, "bg_maybe_img_ : " + bg_maybe_img);
							Log.d(LOG_TAG, "bg_not_going_img_ : " + bg_not_going_img);
							
						}
						
						//setting layout button
						if(bg_going_img.length()>0){
							Event_Setting.SetBackgroundSlot(layout_event_btn_yes, MODE_IMAGE, bg_going_img.trim());
						}
						
						if(bg_not_going_img.length()>0){
							Log.d(LOG_TAG, "bg_not_going_img_ in");
							Event_Setting.SetBackgroundSlot(layout_event_btn_no, MODE_IMAGE, bg_not_going_img.trim());
							
						}
						
						if(bg_maybe_img.length()>0){
							Log.d(LOG_TAG, "bg_maybe_img_ in");
							Event_Setting.SetBackgroundSlotNextImage(layout_event_btn_maybe, MODE_IMAGE, bg_maybe_img.trim());
						}
						
						//setting layout_content background
						if(bg_img_.length()>0){
							Event_Setting.SetBackgroundSlot(layout_content, MODE_IMAGE, bg_img_);
						}else if(bgcolor_.length()>0){
							Event_Setting.SetBackgroundSlot(layout_content, MODE_COLOR, bgcolor_);
						}
						
						
						
						//Setting color title
						if(fontcolor_.length()>0){
							title_1.setTextColor(Color.parseColor(fontcolor_));
							title_2.setTextColor(Color.parseColor(fontcolor_));
							title_3.setTextColor(Color.parseColor(fontcolor_));
							title_4.setTextColor(Color.parseColor(fontcolor_));
							title_5.setTextColor(Color.parseColor(fontcolor_));
							
							//setting color edittext
							txt_event_detail.setTextColor(Color.parseColor(fontcolor_));
							txt_date.setTextColor(Color.parseColor(fontcolor_));
							txt_time.setTextColor(Color.parseColor(fontcolor_));
							txt_address.setTextColor(Color.parseColor(fontcolor_));
							txt_cut_off_date.setTextColor(Color.parseColor(fontcolor_));
						}
						
						if(btnfontcolor_.length()>0){
							btn_yes.setTextColor(Color.parseColor(btnfontcolor_));
							btn_maybe.setTextColor(Color.parseColor(btnfontcolor_));
							btn_no.setTextColor(Color.parseColor(btnfontcolor_));
						}
						
						
						//Setting Data to textview
						txt_event_detail.setText(event_description_);
						txt_date.setText(": " + date_start_+ " ~ " + date_end_);
						txt_time.setText(": " + time_start_ + " ~ "+ time_end_);
						txt_address.setText(": " + venue_);
						txt_cut_off_date.setText(": " + cutoff_);
								
						//onclick
						btn_yes.setOnClickListener(onClickEvent);
						btn_maybe.setOnClickListener(onClickEvent);
						btn_no.setOnClickListener(onClickEvent);
						
						

					}else{
						Toast.makeText(Event_Detail.this,"Sorry.No event available!", Toast.LENGTH_LONG).show();
					}

				}else{
					Toast.makeText(Event_Detail.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "GetEventReservation-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}
	
	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;
	}
	
	public void onBackPressed() {
		super.onBackPressed(); // allows standard use of backbutton for page 1
		this.finish();
		overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
	}

	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();		
	}

}
