package com.m3tech.event;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class Event_Setting {

	private static final String LOG_TAG = "EVENT-SETTING";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();
	SharedPreferences settings;
	String colorcode,background;
	String MODE_COLOR = "1";
	String MODE_IMAGE = "2";
	
	public Event_Setting(Context c){
		Log.d(LOG_TAG, LOG_TAG);
	}
	
	public void SetBackgroundSlot(ImageView v,String mode,String file){
		if(mode.equals(MODE_COLOR)){
			
			v.setBackgroundColor(Color.parseColor(file));
			
		}else if(mode.equals(MODE_IMAGE)){
			
			imageLoader.displayImage(file, v, displayOptions, new ImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {		
				}
				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					
				}
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				}
				@Override
				public void onLoadingCancelled(String imageUri, View view) {
				}

			});
		}else{
			v.setBackgroundColor(Color.parseColor("#FFFFFF"));
		}
	}
	
	public void SetBackgroundSlotNextImage(ImageView v,String mode,String file){
		if(mode.equals(MODE_COLOR)){
			
			v.setBackgroundColor(Color.parseColor(file));
			
		}else if(mode.equals(MODE_IMAGE)){
			
			imageLoader.displayImage(file, v, displayOptions, new ImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {		
				}
				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					
				}
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				}
				@Override
				public void onLoadingCancelled(String imageUri, View view) {
				}

			});
		}else{
			v.setBackgroundColor(Color.parseColor("#FFFFFF"));
		}
	}
	
	@SuppressWarnings("deprecation")
	public static DisplayImageOptions getDisplayImageOptions() {
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}
}
