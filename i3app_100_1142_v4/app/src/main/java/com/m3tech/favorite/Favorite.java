package com.m3tech.favorite;

import java.io.IOException;
import java.net.URLEncoder;

import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.m3tech.data.Helper_Favorite;
import com.m3tech.app_100_1474.R;

public class Favorite {

	Context context;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static final String LOG_TAG = "FAVORITE";
	private SharedPreferences settings;
	private String app_user,app_db,udid,cuid,responseBody;
	JSONArray dataJSONArray;
	Boolean network_status;
	Helper_Favorite helper_favorite = null;
	
	public Favorite(Context mycontext,String topid){
		context = mycontext;
		helper_favorite = new Helper_Favorite(context);
		settings = context.getSharedPreferences(PREFS_NAME, 0);
		app_user = context.getResources().getString(R.string.app_user);
		app_db = context.getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		cuid = settings.getString("customerID", "");
		
		/*** Call onReturnTotalFavorite ***/
		
		network_status = isNetworkAvailable();
		if(!network_status){
			Toast.makeText(context,context.getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_LONG).show();
		} else{
			try {
				
				onCallTotalFavorite(topid);
				GetListFavorite(topid);
				
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/*** Call GetListFavorite ***/
	
	public void GetListFavorite(String topid) throws NotFoundException, ClientProtocolException, IOException, JSONException{
		
			String response;
			HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
			HttpClient httpclient = new DefaultHttpClient();
			httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

			HttpPost HTTP_POST_TOP;
			String strURL = context.getResources().getString(R.string.FAVORITE_LIST_API)
					+ "db=" + URLEncoder.encode(app_db, "UTF-8")
					+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
					+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
					+ "&cuid=" + URLEncoder.encode(cuid, "UTF-8")
					+ "&top=" + URLEncoder.encode(topid, "UTF-8");


			HTTP_POST_TOP = new HttpPost(strURL);
			Log.d(LOG_TAG,"strURL="+ strURL);
			//Execute HTTP Post Request
			ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
			responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	

			dataJSONArray = new JSONArray(responseBody);
			response="SUCCESS";
			Log.d(LOG_TAG,"responseBody="+ responseBody +"|"+response);	
			
			if(dataJSONArray.length() > 0){
				helper_favorite.DeleteListFavorite();
				for(int i = 0;i < dataJSONArray.length();i++ ) {
				     JSONObject jsonObj = dataJSONArray.getJSONObject(i);
		
				     //now get id & value
				     String content_id = jsonObj.getString("content_id");
				     String desc = "1";
				     helper_favorite.insert(content_id,"1",desc);
				     
				 }
				helper_favorite.close();
			}
		
	}
	
	/*** Call onCallTotalFavorite ***/
	
	public void onCallTotalFavorite(String topid) throws NotFoundException, ClientProtocolException, IOException, JSONException{
		
			String response;
			HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
			HttpClient httpclient = new DefaultHttpClient();
			httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

			HttpPost HTTP_POST_TOP;
			String strURL = context.getResources().getString(R.string.FAVORITE_COUNTER_API)
					+ "db=" + URLEncoder.encode(app_db, "UTF-8")
					+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
					+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
					+ "&cuid=" + URLEncoder.encode(cuid, "UTF-8")
					+ "&top=" + URLEncoder.encode(topid, "UTF-8");


			HTTP_POST_TOP = new HttpPost(strURL);
			Log.d(LOG_TAG,"strURL="+ strURL);
			//Execute HTTP Post Request
			ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
			responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	

			dataJSONArray = new JSONArray(responseBody);
			response="SUCCESS";
			Log.d(LOG_TAG,"responseBody="+ responseBody +"|"+response);	
			
			if(dataJSONArray.length() > 0){
				helper_favorite.DeleteAllTotal();
				for(int i = 0;i < dataJSONArray.length();i++ ) {
				     JSONObject jsonObj = dataJSONArray.getJSONObject(i);
		
				     //now get id & value
				     String content_id = jsonObj.getString("content_id");
				     String cntFav = jsonObj.getString("cntFav");
				     helper_favorite.insert(content_id,"0",cntFav);
				     
				 }
				helper_favorite.close();
			}
	}
	
	/*** Call onProcessFavorite ***/
	
	public String onProcessFavorites(String topid,String content_id) throws NotFoundException, ClientProtocolException, IOException, JSONException{
		
			String response,result = null;
			
			if(!network_status){
				Toast.makeText(context,context.getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_LONG).show();
				return "0-error";
			} else{
			
				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	
				//http://app.getsnapps.com/api/favorite_process.php?db=200&userid=192&udid=80f20d8b2968a9f8&cuid=3&top=255&cid=0
				HttpPost HTTP_POST_TOP;
				String strURL = context.getResources().getString(R.string.FAVORITE_PROCESS_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(cuid, "UTF-8")
						+ "&top=" + URLEncoder.encode(topid, "UTF-8")
						+ "&cid=" + URLEncoder.encode(content_id, "UTF-8");
	
	
				HTTP_POST_TOP = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);
				//Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	
	
				dataJSONArray = new JSONArray(responseBody);
				response="SUCCESS";
				Log.d(LOG_TAG,"responseBody="+ responseBody +"|"+response);	
				
				if(dataJSONArray.length() > 0){
					for(int i = 0;i < dataJSONArray.length();i++ ) {
					     JSONObject jsonObj = dataJSONArray.getJSONObject(i);
			
					     //now get id & value
					     String status = jsonObj.getString("status");
					     String desc = jsonObj.getString("desc");
					     Log.d(LOG_TAG, "onProcessFavorites : " + status + "|" + desc);
					     result = status+"-"+desc;
					     
					 }
				}else{
					result = "0-error";
				}
				return result;
			}
		
	}
	
	public String onReturnTotalFavorite(String cid) throws JSONException{
		
		Cursor c = helper_favorite.getTotalByContentID(cid);
		int total = c.getCount();
		helper_favorite.close();
		String total_favorite = null,total_ = null;
		
		if(total>0){
			
			if (c.moveToLast() != false ){
		 		//Get from DB
		 		c.moveToFirst();
		 		do {
		 			
		 			total_ = helper_favorite.gettotal(c).toString();
		 			
		 		} while (c.moveToNext());
			 		
		 	}
		 	c.close();
		 	total_favorite = total_;
		 	Log.d(LOG_TAG, "total_favorite : "+ total_favorite);
		 	return total_favorite;
		 			
		}else{
			return total_favorite = "0";
		}
	}
	
	public boolean onFavoriteList(String content_id){
		
		Cursor c = helper_favorite.getListFavorite(content_id);
		int total = c.getCount();
		c.close();
		helper_favorite.close();
		if(total>0){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;


	}
	
}
