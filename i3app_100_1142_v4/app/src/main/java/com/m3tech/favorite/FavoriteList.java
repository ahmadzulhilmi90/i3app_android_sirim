package com.m3tech.favorite;

import java.net.URLEncoder;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.m3tech.adapter.Favorite_Adapter;
import com.m3tech.app_100_1474.R;
import com.m3tech.collection.Collection_Favorite;
import com.m3tech.content.Plugin_Content_Detail;
import com.m3tech.header.Header;
import com.m3tech.widget.Favorite_Widget;


public class FavoriteList extends Favorite_Widget {

	private static final String LOG_TAG = "FAVORITE LIST";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LOG(LOG_TAG,"Class",LOG_TAG);
		
		//*** Setting getIntent() ***//
		
		Intent j = getIntent();
		topid = j.getStringExtra("top_id");
		favoritePageTitle =  j.getStringExtra("favoritePageTitle");
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, favoritePageTitle,"");

		
		//*** Call Function ***//
		
		GetContentList Task = new GetContentList();
		Task.execute();
		
	}
	
	/**** start function get content list ****/
	private class GetContentList extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			try{

				String responseBody;
				
				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_TOP;
				String strURL = context.getResources().getString(R.string.FAVORITE_LIST_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(cuid, "UTF-8")
						+ "&top=" + URLEncoder.encode(topid, "UTF-8");


				HTTP_POST_TOP = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);
				//Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	

				dataJSONArray = new JSONArray(responseBody);
				Log.d(LOG_TAG,"responseBody="+ responseBody);	
				response="SUCCESS";

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetContentList-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{
				
				Favorite_Adapter favorite_adapter = new Favorite_Adapter(context, R.layout.plugin_favorite_list_row,model_favorite);
				favorite_adapter.clear();
				favorite_adapter.notifyDataSetChanged();
				
				if (result.equals("SUCCESS")){
					
					if(dataJSONArray.length() > 0){
					
						for(int n = 0; n < dataJSONArray.length(); n++){
							
							current_favorite = new Collection_Favorite();
							current_favorite.setid(dataJSONArray.getJSONObject(n).getString("id").toString());
							current_favorite.setuser_id(dataJSONArray.getJSONObject(n).getString("user_id").toString());
							current_favorite.setcustomer_id(dataJSONArray.getJSONObject(n).getString("customer_id").toString());
							current_favorite.setm_date(dataJSONArray.getJSONObject(n).getString("m_date").toString());
							current_favorite.setcontent_id(dataJSONArray.getJSONObject(n).getString("content_id").toString());
							current_favorite.settop_id(dataJSONArray.getJSONObject(n).getString("top_id").toString());
							favorite_adapter.add(current_favorite);
						}
						listview.setAdapter(favorite_adapter);
						listview.setOnItemClickListener(onListClick3);
						
					}else{
						Toast.makeText(FavoriteList.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
					}
				}else{
					Toast.makeText(FavoriteList.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}
			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "Getcontentlist-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}
	private AdapterView.OnItemClickListener onListClick3=new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent,
				View view, int position,
				long id) {
				
				current_favorite=model_favorite.get(position);	
				Log.d(LOG_TAG,"onListClick-"+current_favorite.getcontent_id());
				
				c = Helper_Content.getByContID(current_favorite.getcontent_id());
				
				if(c.getCount() > 0){
					if(c.moveToLast() != false){
						do{
							String category_id = Helper_Content.getcategory_id(c).toString();
							Cursor d = Helper_Contentcategory.getByCatId(category_id);
							if(d.getCount() > 0){
								if(d.moveToLast() != false){
									do{
										
										String list_template = Helper_Contentcategory.getlist_template(d).toString();
										String plugin_id = Helper_Contentcategory.getplugin_id(d).toString();
										String top_id = Helper_Contentcategory.gettop_id(d).toString();
										String category_name = Helper_Contentcategory.getname(d).toString();
										String allow_comments = Helper_Contentcategory.getallow_comments(d).toString();
										String allow_favorite = Helper_Contentcategory.getallow_favorite(d).toString();
										
										Intent i=new Intent(FavoriteList.this, Plugin_Content_Detail.class);
										i.putExtra("contentcategorytitle", category_name);
										i.putExtra("contentid", current_favorite.getcontent_id());
										i.putExtra("list_template", list_template);
										i.putExtra("plugin_id", plugin_id);
										i.putExtra("top_id", top_id);
										i.putExtra("allow_comments", allow_comments);
										i.putExtra("allow_favorite", allow_favorite);
										i.putExtra("from", "favorite");
										startActivity(i);
										
									}while(d.moveToNext());
								}d.close();
							}
							
						}while(c.moveToNext());
					}
				}c.close();
		}
	};

	public void onBackPressed() {
		super.onBackPressed(); 
		this.finish();
	}

	public void onDestroy() {
		this.finish();
		super.onStop();
		super.onDestroy();
		Helper_Contentcategory.close();
		Helper_Themeobject.close();
		Helper_Content.close();		
	}
}
