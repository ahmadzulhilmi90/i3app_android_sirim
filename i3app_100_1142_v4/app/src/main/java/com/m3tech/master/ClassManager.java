package com.m3tech.master;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.m3tech.appv3.SlotAction;
import com.m3tech.collection.Collection_AppSetup;
import com.m3tech.collection.Collection_BookSet;
import com.m3tech.collection.Collection_BookingDetail;
import com.m3tech.collection.Collection_Comment;
import com.m3tech.collection.Collection_Favorite;
import com.m3tech.collection.Collection_Ibeaconcontent;
import com.m3tech.collection.Collection_Ibeacondevice;
import com.m3tech.collection.Collection_Ibeaconxref;
import com.m3tech.collection.Collection_Inbox;
import com.m3tech.collection.Collection_List;
import com.m3tech.collection.Collection_Loyalty;
import com.m3tech.collection.Collection_Notes;
import com.m3tech.collection.Collection_PointSettings;
import com.m3tech.collection.Collection_Warrantylist;
import com.m3tech.collection.Collection_content;
import com.m3tech.collection.Collection_contentcategory;
import com.m3tech.collection.Collection_themecolor;
import com.m3tech.collection.Collection_themeobject;
import com.m3tech.collection.Collection_themeobjectplugin;
import com.m3tech.collection.Collection_themepage;
import com.m3tech.comments.Comment;
import com.m3tech.data.Helper_Appsetup;
import com.m3tech.data.Helper_BookSet;
import com.m3tech.data.Helper_Clock;
import com.m3tech.data.Helper_ClockLog;
import com.m3tech.data.Helper_Comment;
import com.m3tech.data.Helper_Content;
import com.m3tech.data.Helper_Contentcategory;
import com.m3tech.data.Helper_Ibeaconcontent;
import com.m3tech.data.Helper_Ibeacondevice;
import com.m3tech.data.Helper_Ibeaconxref;
import com.m3tech.data.Helper_Loyalty;
import com.m3tech.data.Helper_Notes;
import com.m3tech.data.Helper_Notification;
import com.m3tech.data.Helper_PreviousdeviceID;
import com.m3tech.data.Helper_Stamp;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.data.Helper_Themeobjectplugin;
import com.m3tech.data.Helper_Themepage;
import com.m3tech.data.Helper_Warrantylist;
import com.m3tech.favorite.Favorite;
import com.m3tech.header.Header;
import com.m3tech.app_100_1474.R;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class ClassManager extends Activity {

	private static final String LOG_TAG = "ActivityManagerLog";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	
	//****** Initialize Class ********//
	protected Context context = this;
	protected Activity activity = this;
	protected Network networkManager;
	protected SharedPreferences settings;
	protected SlotAction slotAction = null;
	protected Comment comment = null;
	protected Favorite favorite = null;
	protected Header Header = null;
	
	//****** Initialize Helper *******//
	protected Helper_Appsetup Helper_Appsetup = null;
	protected Helper_Themeobject Helper_Themeobject = null;
	protected Helper_Themepage Helper_Themepage = null;
	protected Helper_Themeobjectplugin Helper_Themeobjectplugin =null;
	protected Helper_Contentcategory Helper_Contentcategory= null;
	protected Helper_Content Helper_Content=null;
	protected Helper_BookSet Helper_BookSet = null;
	protected Helper_Ibeaconcontent Helper_Ibeaconcontent = null;
	protected Helper_Ibeacondevice Helper_Ibeacondevice = null;
	protected Helper_Ibeaconxref Helper_Ibeaconxref = null;
	protected Helper_Loyalty Helper_Loyalty = null;
	protected Helper_Notification Helper_Notification = null;
	protected Helper_PreviousdeviceID Helper_PreviousdeviceID = null;
	protected Helper_Stamp Helper_Stamp  = null;
	protected Helper_Warrantylist Helper_Warrantylist = null;
	protected Helper_Comment Helper_Comment = null;
	protected Helper_Clock Helper_Clock = null;
	protected Helper_ClockLog Helper_ClockLog = null;
	protected Helper_Notes Helper_Notes = null;
	
	//****** Initialize Collection - current *******//
	protected Collection_AppSetup current_AppSetup = null;	
	protected Collection_BookingDetail current_BookingDetail = null;	
	protected Collection_BookSet current_BookSet = null;	
	protected Collection_Comment current_comment = null;	
	protected Collection_content current_content = null;	
	protected Collection_contentcategory current_contentcategory = null;	
	protected Collection_Favorite current_favorite = null;	
	protected Collection_Ibeaconcontent current_Ibeaconcontent = null;	
	protected Collection_Ibeacondevice current_Ibeacondevice = null;	
	protected Collection_Ibeaconxref current_Ibeaconxref = null;	
	protected Collection_Inbox current_Inbox = null;	
	protected Collection_List current_List = null;
	protected Collection_Loyalty current_Loyalty = null;
	protected Collection_PointSettings current_PointSettings = null;
	protected Collection_themecolor current_themecolor = null;
	protected Collection_themeobject current_themeobject = null;
	protected Collection_themeobjectplugin current_themeobjectplugin = null;
	protected Collection_themepage current_themepage = null;
	protected Collection_Warrantylist current_warrantylist = null;
	protected Collection_Notes current_notes = null;
	
	//****** Initialize <List> Model Collection *****//
	protected List<Collection_themeobject> model_themeobject = new ArrayList<Collection_themeobject>();
	protected List<Collection_Favorite> model_favorite =new ArrayList<Collection_Favorite>();
	protected List<Collection_Comment> model_comment=new ArrayList<Collection_Comment>();
	protected List<Collection_content> model_content=new ArrayList<Collection_content>();
	protected List<Collection_Warrantylist> model_warrantylist=new ArrayList<Collection_Warrantylist>();
	protected List<Collection_Notes> model_notes=new ArrayList<Collection_Notes>();
	
	//****** Initialize Variable *******//
	protected String currentTextFilter="",key="",favoritePageTitle;
	protected String list_template,plugin_id,cont_cat_name,cont_category_id,top_id,cid,background;
	protected String customerName,topid,app_user,app_db,home_themecode,themecolor,home_pagename,app_title,colorcode,udid,cuid,currentDateTime,allow_favorite,allow_comments;
	protected String colorcode1,home_pageid,imei;
	protected boolean network = false;
	
	//****** Initialize JSONArray *******//
	protected JSONArray dataJSONArray;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//*** Initialize ImageLoader Library ***//
		ImageLoader.getInstance().destroy();
		@SuppressWarnings("deprecation")
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
		.threadPriority(Thread.NORM_PRIORITY)
		.denyCacheImageMultipleSizesInMemory()
		.discCacheFileNameGenerator(new Md5FileNameGenerator())
		.tasksProcessingOrder(QueueProcessingType.FIFO)
		.writeDebugLogs()
		.build();
		ImageLoader.getInstance().init(config);
		
		//*** Initialize Helper ***/
		
		Helper_Appsetup = new Helper_Appsetup(context);
		Helper_Themeobject = new Helper_Themeobject(context);
		Helper_Themepage = new Helper_Themepage(context);
		Helper_Themeobjectplugin = new Helper_Themeobjectplugin(context);
		Helper_Contentcategory = new Helper_Contentcategory(context);
		Helper_Content = new Helper_Content(context);
		Helper_BookSet = new Helper_BookSet(context);
		Helper_Ibeaconcontent = new Helper_Ibeaconcontent(context);
		Helper_Ibeacondevice = new Helper_Ibeacondevice(context);
		Helper_Ibeaconxref = new Helper_Ibeaconxref(context);
		Helper_Loyalty = new Helper_Loyalty(context);
		Helper_Notification = new Helper_Notification(context);
		Helper_PreviousdeviceID = new Helper_PreviousdeviceID(context);
		Helper_Stamp = new Helper_Stamp(context);
		Helper_Warrantylist = new Helper_Warrantylist(context);
		Helper_Comment = new Helper_Comment(context);
		Helper_Clock = new Helper_Clock(context);
		Helper_ClockLog = new Helper_ClockLog(context);
		Helper_Notes = new Helper_Notes(context);
		
		//*** Initialize Class ***/
		slotAction = new SlotAction(context);
		
		//*** Call Function onGet.. ***/
		onGetSharedPreferences();
		onGetNetworkStatus();
		onGetCurrentDateTime();
		onGetImei();
	}
	
	
	/*** getSharedPreferences ***/
	
	public void onGetSharedPreferences(){
		settings = this.getSharedPreferences(PREFS_NAME, 0);
		app_user = context.getResources().getString(R.string.app_user);
		app_db = context.getResources().getString(R.string.app_db);
		home_themecode = settings.getString("home_themecode", "");
		home_pagename = settings.getString("home_pagename", "");
		colorcode1 = settings.getString("colorcode1", "");		
		home_pageid = settings.getString("home_pageid", "");
		app_title = settings.getString("app_title", "");
		colorcode = settings.getString("colorcode", "");
		udid = settings.getString("udid", "");
		cuid = settings.getString("customerID", "");
		customerName = settings.getString("customerName", "");
		
	}
	
	/*** Get Network Connection Status ***/
	
	public void onGetNetworkStatus(){
		networkManager = new Network(context);
		if(networkManager.isNetworkAvailable()){
			network = true;
		}else{ network = false; }
	}
	
	/*** Get CurrentDateTime ***/
	
	public void onGetCurrentDateTime(){
		 SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		 String formattedDate = dateFormat.format(new Date()).toString();
		 currentDateTime = formattedDate;
		 Log.d(LOG_TAG, "currentDateTime : " + currentDateTime);
		
	}
	
	/*** Get IMEI Device ***/
	
	public void onGetImei(){
		//TelephonyManager mngr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE); 
		//imei = mngr.getDeviceId();
		//Log.d(LOG_TAG, "IMEI : " + mngr.getDeviceId());
	}
	
	/*** LOG_TAG ***/
	public void LOG(String context,String parent,String parameter){
		Log.d(context, parent + ":" + parameter);
	}
	
	@Override
	protected void onDestroy() {
		
		//*** Close All Helper ***//
		Helper_Appsetup.close();
		Helper_Themeobject.close();
		Helper_Themepage.close();
		Helper_Themeobjectplugin.close();
		Helper_Contentcategory.close();
		Helper_Content.close();
		Helper_BookSet.close();
		Helper_Ibeaconcontent.close();
		Helper_Ibeacondevice.close();
		Helper_Ibeaconxref.close();
		Helper_Loyalty.close();
		Helper_Notification.close();
		Helper_PreviousdeviceID.close();
		Helper_Stamp.close();
		Helper_Warrantylist.close();
		Helper_Comment.close();
		Helper_Notes.close();
		
		super.onDestroy();
		finish();
	}
}
