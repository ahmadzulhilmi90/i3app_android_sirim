package com.m3tech.master;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Network
{
	Context mycontext;
	static Boolean network_status = false;
	
	public Network(Context context){
		
		mycontext = context;
		
	}
	
	public boolean isNetworkAvailable() {

		ConnectivityManager connectivityManager = (ConnectivityManager) mycontext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	
}
