package com.m3tech.widget;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.m3tech.master.ClassManager;
import com.m3tech.app_100_1474.R;

public class SlideMenu_Widget extends ClassManager{

	protected LinearLayout layout_darkslidemenu;
	protected ListView listview;
	protected Cursor c;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.plugin_content_slidemenu);
		
		layout_darkslidemenu = (LinearLayout)findViewById(R.id.layout_darkslidemenu);
		listview = (ListView) findViewById(R.id.listmenu);
	}
}
