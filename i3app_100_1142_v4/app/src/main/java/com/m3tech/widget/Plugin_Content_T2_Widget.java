package com.m3tech.widget;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.m3tech.master.ClassManager;
import com.m3tech.app_100_1474.R;

public class Plugin_Content_T2_Widget extends ClassManager{

	protected Cursor content,g,a;
	protected LinearLayout tabback,tabhome;
	protected TextView texttitlebar,texthome;
	protected ListView listview;
	protected ImageView tabheader,img_back,img_home;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.plugin_content_template2);
		
		tabheader = (ImageView) findViewById(R.id.tabheader);
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		tabback = (LinearLayout) findViewById(R.id.tabback);
		tabhome = (LinearLayout) findViewById(R.id.tabhome);
		img_back = (ImageView) findViewById(R.id.back);
		img_home = (ImageView) findViewById(R.id.home);
		listview = (ListView) findViewById(R.id.listcontent);
		
	}
}
