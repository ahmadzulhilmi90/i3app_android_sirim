package com.m3tech.widget;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.m3tech.app_100_1474.R;
import com.m3tech.master.ClassManager;

public class Notes_Widget extends ClassManager{
	
	protected Button btn_save_note;
	protected EditText edit_note;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.plugin_notes);
		
		btn_save_note = (Button)findViewById(R.id.btn_save_note);
		edit_note = (EditText)findViewById(R.id.edit_note);
	}
}
