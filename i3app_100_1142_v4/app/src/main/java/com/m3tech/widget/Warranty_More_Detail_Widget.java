package com.m3tech.widget;

import org.json.JSONArray;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.m3tech.app_100_1474.R;
import com.m3tech.master.ClassManager;

public class Warranty_More_Detail_Widget extends ClassManager{
	
	protected ImageView tabheader,imgPreview,img_back,img_home;
	protected TextView texttitlebar,textdone;
	protected LinearLayout tabback,tabhome,layouttitleheader;
	protected JSONArray warrantyJSONArray;
	protected Button btnsubmit,btnuploadimage;
	protected EditText editserialno,editmodelsku,editstorename;
	protected String warrantyID,uploadFilePath="";
	protected String imgFilename = "product_",serial_no,model_sku,store_name,uploadFileName,responbody;
	protected int serverResponseCode = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.warranty_moredetail);
		
		tabheader = (ImageView) findViewById(R.id.tabheader);
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		tabback = (LinearLayout) findViewById(R.id.tabback);
		tabhome = (LinearLayout) findViewById(R.id.tabhome);
		img_back = (ImageView) findViewById(R.id.back);
		img_home = (ImageView) findViewById(R.id.home);
		layouttitleheader = (LinearLayout) findViewById(R.id.layouttitleheader);
		btnsubmit = (Button) findViewById(R.id.btnsubmit);
		textdone = (TextView) findViewById(R.id.textDone);
		imgPreview = (ImageView) findViewById(R.id.imgPreview);
		editserialno = (EditText) findViewById(R.id.editserialno);
		editmodelsku = (EditText) findViewById(R.id.editmodelsku);
		editstorename = (EditText) findViewById(R.id.editstorename);
		btnuploadimage = (Button) findViewById(R.id.btnuploadimage);
		
	}
}
