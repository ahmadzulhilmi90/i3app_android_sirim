package com.m3tech.widget;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.m3tech.app_100_1474.R;
import com.m3tech.master.ClassManager;

public class Warranty_Content_Detail_Widget extends ClassManager{
	
	protected ImageView tabheader,image_url,image_receipt,image_warranty,img_back,img_home;
	protected TextView texttitlebar,textheader;
	protected LinearLayout tabback,tabhome,layouttitleheader;
	protected LinearLayout layout_productname,layout_storename,layout_purcahsedate,layout_expirydate,
	layout_serialno,layout_modelsku,layout_ketiga,layoutwarrantyimage,
	layoutproductimage,layoutreceiptimage;
	protected String prodName,storeName,purchaseDate,expiryDate,serialNo,modelsku,proPicture,receiptPicture,warrantyPicture;
	protected ProgressBar spinner,spinner2,spinner3;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.warranty_contentdetail);
		
		tabheader = (ImageView) findViewById(R.id.tabheader);
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		tabback = (LinearLayout) findViewById(R.id.tabback);
		tabhome = (LinearLayout) findViewById(R.id.tabhome);
		img_back = (ImageView) findViewById(R.id.back);
		img_home = (ImageView) findViewById(R.id.home);
		layouttitleheader = (LinearLayout) findViewById(R.id.layouttitleheader);
		textheader = (TextView) findViewById(R.id.textheader);
		layout_productname = (LinearLayout) findViewById(R.id.layout_productname);
		layout_storename = (LinearLayout) findViewById(R.id.layout_storename);
		layout_purcahsedate = (LinearLayout) findViewById(R.id.layout_purcahsedate);
		layout_expirydate = (LinearLayout) findViewById(R.id.layout_expirydate);
		layout_serialno = (LinearLayout) findViewById(R.id.layout_serialno);
		layout_modelsku = (LinearLayout) findViewById(R.id.layout_modelsku);
		layout_ketiga = (LinearLayout) findViewById(R.id.layout_ketiga);
		layoutproductimage  = (LinearLayout) findViewById(R.id.layoutproductimage); 
		layoutreceiptimage = (LinearLayout) findViewById(R.id.layoutreceiptimage); 
		layoutwarrantyimage = (LinearLayout) findViewById(R.id.layoutwarrantyimage);

		image_url = (ImageView) findViewById(R.id.image_url);
		image_receipt = (ImageView) findViewById(R.id.image_receipt);
		image_warranty = (ImageView) findViewById(R.id.image_warranty);
		spinner = (ProgressBar) findViewById(R.id.progressbar);
		spinner2 = (ProgressBar) findViewById(R.id.progressbarreceipt);
		spinner3 = (ProgressBar) findViewById(R.id.progressbarwarranty);
		
	}
}
