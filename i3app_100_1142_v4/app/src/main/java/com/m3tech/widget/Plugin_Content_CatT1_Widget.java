package com.m3tech.widget;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.m3tech.master.ClassManager;
import com.m3tech.app_100_1474.R;

public class Plugin_Content_CatT1_Widget extends ClassManager{

	protected Cursor content,detail,g,a;
	protected TextView texttitlebar,texthome,text_parent,text_child;
	protected LinearLayout tabback,tabhome;
	protected ImageView tabheader,img_back,img_home;
	protected Spinner spinner_parent,spinner_child;
	protected Button submit;
	protected String prod_cat_name,parent_name,child_name;
	protected static ProgressDialog progressdialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.plugin_content_template1);
		
		tabheader = (ImageView) findViewById(R.id.tabheader);
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		tabback = (LinearLayout) findViewById(R.id.tabback);
		tabhome = (LinearLayout) findViewById(R.id.tabhome);
		img_back = (ImageView) findViewById(R.id.back);
		img_home = (ImageView) findViewById(R.id.home);
		spinner_parent = (Spinner) findViewById(R.id.spinner_parent);
		spinner_child = (Spinner) findViewById(R.id.spinner_child);
		submit = (Button)findViewById(R.id.btn_submit);
		text_parent = (TextView)findViewById(R.id.text_parent);
		text_child = (TextView)findViewById(R.id.text_child);
		
	}
}
