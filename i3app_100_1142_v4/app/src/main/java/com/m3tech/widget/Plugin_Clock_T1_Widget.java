package com.m3tech.widget;

import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.m3tech.app_100_1474.R;
import com.m3tech.master.ClassManager;

public class Plugin_Clock_T1_Widget extends ClassManager{

	protected Cursor content,g,a;
	protected EditText text_remark;
	protected LinearLayout tabback,tabhome;
	protected TextView text_customername,texttitlebar,texthome,text_desc,text_total_hour,text_timer_in,text_status,text_timer_out;
	protected ImageView tabheader,image_status_in,image_status_out,img_home,img_back;
	protected String currentlat="",currentlong="";
	protected boolean canGetLocation;
	protected String IN = "in";
	protected String OUT = "out";
	protected String pagetitle, timer_resume;
	protected Location currentLocation;
	protected LocationManager locationManager;
	public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
	public static final long MIN_TIME_BW_UPDATES = 1000*60*1; // 1 minute
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.plugin_clock_template1);
		
		tabheader = (ImageView) findViewById(R.id.tabheader);
		image_status_in = (ImageView) findViewById(R.id.image_status_in);
		image_status_out = (ImageView) findViewById(R.id.image_status_out);
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		text_desc = (TextView) findViewById(R.id.text_desc);
		text_total_hour = (TextView) findViewById(R.id.text_total_hour);
		text_timer_in = (TextView) findViewById(R.id.text_timer_in);
		text_timer_out = (TextView) findViewById(R.id.text_timer_out);
		text_status = (TextView) findViewById(R.id.text_status);
		text_customername = (TextView) findViewById(R.id.text_customername);
		tabback = (LinearLayout) findViewById(R.id.tabback);
		tabhome = (LinearLayout) findViewById(R.id.tabhome);
		img_back = (ImageView) findViewById(R.id.back);
		img_home = (ImageView) findViewById(R.id.home);
		text_remark = (EditText)findViewById(R.id.text_remark);
		
	}
}
