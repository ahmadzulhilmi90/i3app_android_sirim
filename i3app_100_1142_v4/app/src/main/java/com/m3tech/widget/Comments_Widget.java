package com.m3tech.widget;

import android.net.Uri;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.m3tech.master.ClassManager;
import com.m3tech.app_100_1474.R;

public class Comments_Widget extends ClassManager{
	
	protected LinearLayout tabback,tabhome;
	protected TextView texttitlebar;
	protected Boolean network_status;
	protected ImageView tabheader,btnsubmit,imgPreview,btnuploadimage,img_back,img_home;
	protected ListView inboxlist;
	protected EditText editmessage;
	protected String inbox_title,newmessage,responbody;
	protected Boolean capturingImage = false;
	protected int serverResponseCode = 0;
	protected StringBuffer responseBuffer = new StringBuffer(); 
	protected String uploadFilePath="";
	protected String imgFilename = "comment_";
	protected Uri fileUri;
	protected ListView listView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comment_inbox);
		
		tabheader = (ImageView) findViewById(R.id.tabheader);
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		tabback = (LinearLayout) findViewById(R.id.tabback);
		tabhome = (LinearLayout) findViewById(R.id.tabhome);
		img_back = (ImageView) findViewById(R.id.back);
		img_home = (ImageView) findViewById(R.id.home);
		btnsubmit = (ImageView) findViewById(R.id.btnsubmit);
		inboxlist = (ListView) findViewById(R.id.inboxlist);
		editmessage = (EditText) findViewById(R.id.editmessage);
		imgPreview = (ImageView) findViewById(R.id.imgPreview);
		btnuploadimage= (ImageView) findViewById(R.id.btnuploadimage);
		listView = (ListView) findViewById(R.id.inboxlist);
		
	}
}
