package com.m3tech.widget;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.m3tech.app_100_1474.R;
import com.m3tech.master.ClassManager;

public class WebLink_Widget extends ClassManager{
	
	protected WebView webView;
	protected ImageView tabheader,img_back,img_home;
	protected TextView texttitlebar;
	protected LinearLayout tabback,tabhome,tabbottom,btnopen;
	protected ProgressBar progressbar,spinner;
	protected String cusid,url,openweb,pagetitle;
	protected Boolean isPageLoadedComplete=false;
	protected static ProgressDialog progressDialog;
	protected AlertDialog.Builder alert;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webbrowser);
		
		webView = (WebView) findViewById(R.id.WebView);
		tabheader = (ImageView) findViewById(R.id.tabheader);
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		tabback = (LinearLayout) findViewById(R.id.tabback);
		tabhome = (LinearLayout) findViewById(R.id.tabhome);
		img_back = (ImageView) findViewById(R.id.back);
		img_home = (ImageView) findViewById(R.id.home);
		tabbottom = (LinearLayout) findViewById(R.id.tabbottom);
		btnopen = (LinearLayout) findViewById(R.id.btnopen);
		progressbar = (ProgressBar) findViewById(R.id.progressbar);
		spinner = (ProgressBar) findViewById(R.id.spinner);

		tabback.setVisibility(View.VISIBLE);
		tabhome.setVisibility(View.VISIBLE);
	}
}
