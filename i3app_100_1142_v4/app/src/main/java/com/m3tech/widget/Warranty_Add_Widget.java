package com.m3tech.widget;

import org.json.JSONArray;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.m3tech.app_100_1474.R;
import com.m3tech.master.ClassManager;

public class Warranty_Add_Widget extends ClassManager{
	
	protected ImageView tabheader,imgPreview,img_back,img_home;
	protected TextView texttitlebar;
	protected LinearLayout tabback,tabhome,layouttitleheader;
	protected Button btnsubmit,btnuploadimage;
	protected EditText editdate,editproductName;
	protected String uploadFilePath="";
	protected String imgFilename = "receipt_",prod_name,prod_date,responbody;
	protected int serverResponseCode = 0;
	protected JSONArray warrantyJSONArray;
	protected Cursor a,g;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.warranty_add);
		
		tabheader = (ImageView) findViewById(R.id.tabheader);
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		tabback = (LinearLayout) findViewById(R.id.tabback);
		tabhome = (LinearLayout) findViewById(R.id.tabhome);
		img_back = (ImageView) findViewById(R.id.back);
		img_home = (ImageView) findViewById(R.id.home);
		layouttitleheader = (LinearLayout) findViewById(R.id.layouttitleheader);
		btnsubmit = (Button) findViewById(R.id.btnsubmit);
		imgPreview = (ImageView) findViewById(R.id.imgPreview);
		editdate = (EditText) findViewById(R.id.editdate);
		editproductName = (EditText) findViewById(R.id.editproductName);
		btnuploadimage = (Button) findViewById(R.id.btnuploadimage);
		
	}
}
