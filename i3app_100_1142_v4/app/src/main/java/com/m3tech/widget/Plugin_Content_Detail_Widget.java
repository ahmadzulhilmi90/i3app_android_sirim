package com.m3tech.widget;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.m3tech.app_100_1474.R;
import com.m3tech.master.ClassManager;

public class Plugin_Content_Detail_Widget extends ClassManager{
	
	protected ImageView tabheader,img_fav,img_comment,img_home,img_back;
	protected TextView texttitlebar,text_total_fav,text_total_comment;
	protected LinearLayout layout_fav_comment,tabback,tabhome;
	protected String orderno,price_title2,price, var1_title,var1,var2_title,var2,var3_title,var3, var4_title, var4,var5_title,var5,home_pagename;
	protected static ProgressDialog progressDialog;
	protected String title,image,thumbnail,call_title,telephone,email_title,email,details,link_title,link_url,map_title,
	map_lat,map_lon,video_title,video_url,fb_title,fb_url;
	protected Cursor a,c,d,g;
	protected String contentcategorytitle,contentid,from;
	protected ProgressBar spinner;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.plugin_content_detail);
		
		tabheader = (ImageView) findViewById(R.id.tabheader);
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		layout_fav_comment = (LinearLayout)findViewById(R.id.layout_fav_comment);
		img_fav = (ImageView)findViewById(R.id.img_fav);
		img_comment = (ImageView)findViewById(R.id.img_comment);
		text_total_fav = (TextView)findViewById(R.id.text_total_fav);
		text_total_comment = (TextView)findViewById(R.id.text_total_comment);
		tabback = (LinearLayout) findViewById(R.id.tabback);
		tabhome = (LinearLayout) findViewById(R.id.tabhome);
		img_back = (ImageView) findViewById(R.id.back);
		img_home = (ImageView) findViewById(R.id.home);
		
	}
}
