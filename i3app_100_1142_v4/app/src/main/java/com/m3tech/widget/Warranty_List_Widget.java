package com.m3tech.widget;

import java.util.Date;

import org.json.JSONArray;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.m3tech.app_100_1474.R;
import com.m3tech.master.ClassManager;

public class Warranty_List_Widget extends ClassManager{
	
	protected ImageView tabheader,img_back,img_home;
	protected TextView texttitlebar;
	protected LinearLayout layouteditsearch,tabback,tabhome,layoutaddnew,layoutsearch,layoutexpired;
	protected EditText editTextsearch;
	protected ListView listView;
	protected JSONArray warrantyJSONArray;
	protected static ProgressDialog progressDialog;
	protected String currentTextFilter = null;
	protected String expired_date1,new_date1,now_date1,strAPI_WARRANTYLIST,currentdate,tabvalue,key;
	protected Date expired_date,new_date,now_date;
	protected Cursor c;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.warranty_list);
		
		tabheader = (ImageView) findViewById(R.id.tabheader);
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		tabback = (LinearLayout) findViewById(R.id.tabback);
		tabhome = (LinearLayout) findViewById(R.id.tabhome);
		img_back = (ImageView) findViewById(R.id.back);
		img_home = (ImageView) findViewById(R.id.home);
		layoutaddnew = (LinearLayout) findViewById(R.id.layoutaddnew);
		layoutsearch = (LinearLayout) findViewById(R.id.layoutsearch);
		layoutexpired = (LinearLayout) findViewById(R.id.layoutexpired);
		layouteditsearch = (LinearLayout) findViewById(R.id.layouteditsearch);
		editTextsearch = (EditText) findViewById(R.id.editTextsearch);
		listView = (ListView) findViewById(R.id.listcategory);
		
	}
}
