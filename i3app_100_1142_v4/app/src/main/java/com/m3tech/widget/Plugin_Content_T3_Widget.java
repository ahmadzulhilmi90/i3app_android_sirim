package com.m3tech.widget;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.m3tech.comments.Comment;
import com.m3tech.favorite.Favorite;
import com.m3tech.master.ClassManager;
import com.m3tech.app_100_1474.R;

public class Plugin_Content_T3_Widget extends ClassManager{

	protected Cursor content,g,a;
	protected LinearLayout tabback,tabhome;
	protected TextView texttitlebar,texthome;
	protected GridView gridView;
	protected Comment comment = null;
	protected Favorite favorite = null;
	protected ImageView tabheader,img_back,img_home;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.plugin_content_template3);
		
		tabheader = (ImageView) findViewById(R.id.tabheader);
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		tabback = (LinearLayout) findViewById(R.id.tabback);
		tabhome = (LinearLayout) findViewById(R.id.tabhome);
		img_back = (ImageView) findViewById(R.id.back);
		img_home = (ImageView) findViewById(R.id.home);
		gridView = (GridView) findViewById(R.id.gridview);
		
	}
}
