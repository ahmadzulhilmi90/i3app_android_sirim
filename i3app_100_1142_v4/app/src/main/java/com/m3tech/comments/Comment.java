package com.m3tech.comments;

import java.io.IOException;
import java.net.URLEncoder;

import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.m3tech.data.Helper_Comment;
import com.m3tech.app_100_1474.R;

public class Comment {
	
	Context context;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static final String LOG_TAG = "COMMENT";
	private SharedPreferences settings;
	private String app_user,app_db,udid,cuid,responseBody;
	JSONArray dataJSONArray;
	Boolean network_status;
	Helper_Comment helper_comment = null;
	
	public Comment(Context mycontext,String topid){
		context = mycontext;
		helper_comment = new Helper_Comment(context);
		settings = context.getSharedPreferences(PREFS_NAME, 0);
		app_user = context.getResources().getString(R.string.app_user);
		app_db = context.getResources().getString(R.string.app_db);
		udid = settings.getString("udid", "");
		cuid = settings.getString("customerID", "");
		
		/*** Call onReturnTotalComment ***/
		
		network_status = isNetworkAvailable();
		if(!network_status){
			Toast.makeText(context,context.getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_LONG).show();
		} else{
			try {
				onCallTotalComment(topid);
				
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
	/*** Call onCallTotalComment ***/
	
	public void onCallTotalComment(String topid) throws NotFoundException, ClientProtocolException, IOException, JSONException{
		
			String response;
			HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
			HttpClient httpclient = new DefaultHttpClient();
			httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

			HttpPost HTTP_POST_TOP;
			String strURL = context.getResources().getString(R.string.COMMENT_COUNTER_API)
					+ "db=" + URLEncoder.encode(app_db, "UTF-8")
					+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
					+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
					+ "&cuid=" + URLEncoder.encode(cuid, "UTF-8")
					+ "&top=" + URLEncoder.encode(topid, "UTF-8");


			HTTP_POST_TOP = new HttpPost(strURL);
			Log.d(LOG_TAG,"strURL="+ strURL);
			//Execute HTTP Post Request
			ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
			responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	

			dataJSONArray = new JSONArray(responseBody);
			response="SUCCESS";
			Log.d(LOG_TAG,"responseBody="+ responseBody +"|"+response);	
			
			if(dataJSONArray.length() > 0){
				helper_comment.Delete();
				for(int i = 0;i < dataJSONArray.length();i++ ) {
				     JSONObject jsonObj = dataJSONArray.getJSONObject(i);
		
				     //now get id & value
				     String content_id = jsonObj.getString("content_id");
				     String cntComments = jsonObj.getString("cntComments");
				     helper_comment.insert(content_id,cntComments);
				     
				 }
				helper_comment.close();
			}
		
	}
	
	public String onReturnTotalComment(String cid){
		
		Cursor c = helper_comment.getTotalByContentID(cid);
		int total = c.getCount();
		String total_comment = null,total_ = null;
		
		if(total>0){
			
			if (c.moveToLast() != false ){
		 		//Get from DB
		 		c.moveToFirst();
		 		do {
		 			
		 			total_ = helper_comment.gettotal(c).toString();
		 			
		 		} while (c.moveToNext());
			 		
		 	}
		 	
		 	total_comment = total_;
		 	Log.d(LOG_TAG, "total_comment : "+ total_comment);
		 	c.close();
		 	return total_comment;
		 			
		}else{
			return total_comment = "0";
		}
	}
	
	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;
	}
}
