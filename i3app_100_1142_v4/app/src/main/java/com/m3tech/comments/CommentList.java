package com.m3tech.comments;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.collection.Collection_Comment;
import com.m3tech.header.Header;
import com.m3tech.widget.Comments_Widget;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

@SuppressWarnings("deprecation")
public class CommentList extends Comments_Widget {

	private static final String LOG_TAG = "COMMENTS LIST";
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();
	Comment_Adapter adapter_comment=null;
	
	// directory name to store captured images and videos
	private static final String IMAGE_DIRECTORY_NAME = "Camera";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LOG(LOG_TAG,"Class",LOG_TAG);

		/**** back button *****/
		Intent j = getIntent();		
		cid = j.getStringExtra("cid");
		topid = j.getStringExtra("topid");
		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome,img_back,img_home, texttitlebar, "Inbox","");
		
		Log.d(LOG_TAG, "cid : " + cid);

		network_status = isNetworkAvailable();
		if(!network_status){
			Toast.makeText(CommentList.this,getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_LONG).show();
		} 

		btnsubmit.setOnClickListener(onClickSubmit);
	
		btnuploadimage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// capture picture
				captureImage();
			}
		});
		
		// External sdcard location
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
			}
		}

		uploadFilePath = mediaStorageDir.getPath() + File.separator ;
		Log.w(LOG_TAG, "getOutputMediaFile: " + uploadFilePath);

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddkkmm");
		String todayDate = df.format(cal.getTime());

		imgFilename = imgFilename + todayDate + ".jpg";
		
	}
	
	private class getInboxData extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Toast.makeText(CommentList.this,getResources().getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;
			try{

				HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

				HttpPost HTTP_POST_TOP;
				String strURL = getResources().getString(R.string.COMMENT_LIST_API)
						+ "db=" + URLEncoder.encode(app_db, "UTF-8")
						+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
						+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
						+ "&cuid=" + URLEncoder.encode(cuid, "UTF-8")
						+ "&top=" + URLEncoder.encode(topid, "UTF-8")
						+ "&cid="+URLEncoder.encode(cid, "UTF-8"); 


				HTTP_POST_TOP = new HttpPost(strURL);
				Log.d(LOG_TAG,"strURL="+ strURL);
				//Execute HTTP Post Request
				ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
				responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	

				dataJSONArray = new JSONArray(responseBody);
				response="SUCCESS";
				Log.d(LOG_TAG,"responseBody="+ responseBody);	


			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "getInboxData-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				
				adapter_comment=new Comment_Adapter();
				adapter_comment.clear();

				Log.d(LOG_TAG,"onPostExecute: "+ dataJSONArray.length());

				if (result.equals("SUCCESS")){

					if (dataJSONArray.length()>0){

						Log.d(LOG_TAG,"dataJSONArray lenght :"+ dataJSONArray.length());
						for (int i = 0; i < dataJSONArray.length(); i++) {

							current_comment=new Collection_Comment();
							current_comment.setid(dataJSONArray.getJSONObject(i).getString("id").toString());
							current_comment.setuser_id(dataJSONArray.getJSONObject(i).getString("user_id").toString());
							current_comment.setcustomer_id(dataJSONArray.getJSONObject(i).getString("customer_id").toString());
							current_comment.setm_date(dataJSONArray.getJSONObject(i).getString("m_date").toString());
							current_comment.setmessage(dataJSONArray.getJSONObject(i).getString("message").toString());
							current_comment.setstatus(dataJSONArray.getJSONObject(i).getString("status").toString());
							current_comment.settop_id(dataJSONArray.getJSONObject(i).getString("top_id").toString());
							current_comment.setimage_url(dataJSONArray.getJSONObject(i).getString("image_url").toString());
							current_comment.setcontent_id(dataJSONArray.getJSONObject(i).getString("content_id").toString());
							current_comment.setname(dataJSONArray.getJSONObject(i).getString("name").toString());
							adapter_comment.add(current_comment);

						}
						listView.setAdapter(adapter_comment);			
						listView.setOnItemClickListener(onListClick);	
						scrollMyListViewToBottom();
					}

				}else{
					Toast.makeText(CommentList.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "getInboxData-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}

	private void scrollMyListViewToBottom() {
		
		current_comment = model_comment.get(adapter_comment.getCount() - 1);	
		String msgid = current_comment.getid();
		
		SharedPreferences.Editor editor = settings.edit();
		editor.remove("latest_msgid");
		editor.putString("latest_msgid", msgid);
		editor.commit();

		inboxlist.post(new Runnable() {
	        @Override
	        public void run() {
	            // Select the last row so it will scroll into view...
	        	inboxlist.setSelection(adapter_comment.getCount() - 1);
	        }
	    });
	}
	

	class Comment_Adapter extends ArrayAdapter<Collection_Comment> {
		Comment_Adapter() {
			super(CommentList.this, R.layout.comment_inbox_row, model_comment);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View row=convertView;
			NotificationHolderhome holder=null;

			if (row==null) {													
				LayoutInflater inflater=getLayoutInflater();

				row=inflater.inflate(R.layout.comment_inbox_row, parent, false);
				holder=new NotificationHolderhome(row);
				row.setTag(holder);
			} else {
				holder=(NotificationHolderhome)row.getTag();
			}

			holder.populateFromhome(model_comment.get(position));

			return(row);
		}
	}

	class NotificationHolderhome {
		private TextView textname=null;
		private TextView textmessage=null;
		private TextView textdate=null;
		private TextView textdelete=null;
		private LinearLayout inboxrow=null;
		private ImageView image =null;
		private ProgressBar spinner = null;
		private RelativeLayout imglayout = null;

		NotificationHolderhome(View row) {
			inboxrow=(LinearLayout) row.findViewById(R.id.inboxrow);
			textname=(TextView) row.findViewById(R.id.textname);
			textmessage=(TextView) row.findViewById(R.id.textmessage);
			textdate=(TextView) row.findViewById(R.id.textdate);
			textdelete=(TextView) row.findViewById(R.id.textdelete);	
			image = (ImageView) row.findViewById(R.id.image_url);
			spinner = (ProgressBar) row.findViewById(R.id.progress);
			imglayout = (RelativeLayout) row.findViewById(R.id.layout_image);
		}

		void populateFromhome(Collection_Comment r) {
			String sender_id = "1";//r.getsender_id();
			String img_url = r.getimage_url();
			
			if (img_url.isEmpty() || img_url.equals("null")) {
				img_url = "";
			}
			
			if (sender_id.equals("0")) {
				//inboxrow.setBackgroundColor(Color.parseColor("#ffffd0"));
				inboxrow.setBackground(getResources().getDrawable(R.drawable.border_curve_yellow));
			}

			String date = r.getm_date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
			Date newDate = null;
			try {
				newDate = sdf.parse(date);
			}catch(Exception ex){
				ex.printStackTrace();
			}

			SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd, yyyy hh:mm a");
			String msgdate = formatter.format(newDate);

			textname.setText(r.getname());
			textmessage.setText(r.getmessage());
			textdate.setText(msgdate);			
			textdelete.setVisibility(View.VISIBLE);

			myClickListener listener = new myClickListener(r.getid());
			textdelete.setOnClickListener(listener);

			if (img_url.length()>10) {
				imglayout.setVisibility(View.VISIBLE);
						
				imageLoader.displayImage(img_url, image, displayOptions, new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {											
						spinner.setVisibility(View.VISIBLE);
					}
					@Override
					public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
						spinner.setVisibility(View.GONE);
					}
					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
						spinner.setVisibility(View.GONE);
					}
					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						spinner.setVisibility(View.GONE);
					}
	
				});
			}
				

		}
	}



	private AdapterView.OnItemClickListener onListClick=new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent,
				View view, int position,
				long id) {

			current_comment = model_comment.get(position);							
			int intid = Integer.parseInt(current_comment.getid());

			TextView textdelete = (TextView) view.findViewById(R.id.textdelete);
			
			if (textdelete.getVisibility()==View.GONE && intid>0) {
				textdelete.setVisibility(View.VISIBLE);
			}
		}
	};

	public class myClickListener implements TextView.OnClickListener {

		 String myVar;
	     public myClickListener(String myVar) {
	          this.myVar = myVar;
	     }

	     public void onClick(View v) {
			try{
				Log.d(LOG_TAG,"onClickDelete :" + myVar);
				submitDeleteData Task = new submitDeleteData();
				Task.execute(new String[] { myVar }); 
				
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onClickDelete-Error:" + t.getMessage(), t); 

			}
	     }

	  };

	private class submitDeleteData extends AsyncTask<String, Void, String> {
		String msgid="";
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String responseBody = null;
			try{
				msgid = arg0[0];
				if(msgid==null || msgid.equals("")){
					response="FAILED";
				} else {

					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST); 
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);	

					HttpPost HTTP_POST_TOP;
					String strURL = getResources().getString(R.string.COMMENT_DELETE_API)
							+ "db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&cuid=" + URLEncoder.encode(cuid, "UTF-8")
						    + "&commentsid=" + URLEncoder.encode(msgid, "UTF-8")
							+ "&cid=" + URLEncoder.encode(cid, "UTF-8"); 


					HTTP_POST_TOP = new HttpPost(strURL);
					Log.d(LOG_TAG,"strURL="+ strURL);
					//Execute HTTP Post Request
					ResponseHandler<String> responseHandler_TOP=new BasicResponseHandler();
					responseBody=httpclient.execute(HTTP_POST_TOP, responseHandler_TOP);	

					dataJSONArray = new JSONArray(responseBody);
					response="SUCCESS";
					Log.d(LOG_TAG,"responseBody="+ responseBody);	
				}

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "submitDeleteData-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){
					getInboxData Task = new getInboxData();
					Task.execute(new String[] { });
				}else{
					Toast.makeText(CommentList.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_SHORT).show();
				}
			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "submitDeleteData-onPostExecute-Error:" + t.getMessage(), t);
			}
		}

	}

	/***  back button onclick ****/
	private View.OnClickListener onClickSubmit=new View.OnClickListener() {
		public void onClick(View v) {
			try{

				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(editmessage.getWindowToken(), 0);

				newmessage = editmessage.getText().toString();
				File imgFile = new File(uploadFilePath + "" + imgFilename);
				
				if(imgFile.exists() || newmessage.length()>0) {   
					uploadFile(uploadFilePath + "" + imgFilename);
				}
					
			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onBackButton-Error:" + t.getMessage(), t); 

			}
		}

	};
	
	/******************upload file *************************/

	public int uploadFile(String sourceFileUri) {

		Toast.makeText(CommentList.this,getResources().getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
		String fileName = sourceFileUri;
		Log.d(LOG_TAG, "fileName="+fileName);
		HttpURLConnection conn = null;
		DataOutputStream dos = null; 
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(sourceFileUri);
		Log.d(LOG_TAG, "sourceFile="+sourceFile);

		try {
			
			//http://app.getsnapps.com/api/comments_add.php?db=%@&userid=%@&udid=%@&cuid=%@&message=%@&top=%@&cid=%@
			String strURL = getResources().getString(R.string.COMMENT_SUBMIT_API)
					+ "db=" + URLEncoder.encode(app_db, "UTF-8")
					+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
					+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
					+ "&cuid=" + URLEncoder.encode(cuid, "UTF-8")
					+ "&message=" + URLEncoder.encode(newmessage, "UTF-8")
					+ "&top=" + URLEncoder.encode(topid, "UTF-8")
					+ "&cid=" + URLEncoder.encode(cid, "UTF-8");

			Log.d(LOG_TAG, "URL : " + strURL);
			
			URL url = new URL(strURL);

			// Open a HTTP  connection to  the URL
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true); // Allow Inputs
			conn.setDoOutput(true); // Allow Outputs
			conn.setUseCaches(false); // Don't use a Cached Copy
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

			File imgFile = new File(sourceFileUri);

			if(imgFile.exists()) {   

				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(sourceFile);

				conn.setRequestProperty("uploaded_file", fileName);
				dos = new DataOutputStream(conn.getOutputStream());
				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
						+ fileName + "\"" + lineEnd);
				dos.writeBytes(lineEnd);

				// create a buffer of  maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize); 

				while (bytesRead > 0) {
					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);  

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				//close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			}

			// Responses from the server (code and message)
			serverResponseCode = conn.getResponseCode();
			String serverResponseMessage = conn.getResponseMessage();

			Log.i("uploadFile", "HTTP Response is : "
					+ serverResponseMessage + ": " + serverResponseCode);

			//Get Response 

			InputStream inputStream = conn.getInputStream();

			BufferedReader bufferReader = new BufferedReader(new InputStreamReader(inputStream));

			String line;

			while((line = bufferReader.readLine()) != null) {
				responseBuffer.append(line);
				responseBuffer.append('\r');
			}

			bufferReader.close();
			responbody = responseBuffer.toString();
			Log.d(LOG_TAG, "responbody="+responbody);

			if(serverResponseCode == 200){

				runOnUiThread(new Runnable() {
					public void run() {

						try {
							dataJSONArray = new JSONArray(responbody);
							String status = dataJSONArray.getJSONObject(0).getString("status").toString();
							String desc = dataJSONArray.getJSONObject(0).getString("desc").toString();
							Log.d(LOG_TAG, "RunOnUiThread : desc" + desc);

							if(status.equals("1")){
								//clear all data
								editmessage.setText("");
								imgPreview.setVisibility(View.GONE);
								imgFilename = "inbox_";
								
								Toast.makeText(CommentList.this,getResources().getString(R.string.message_success), Toast.LENGTH_SHORT).show();
								getInboxData Task = new getInboxData();
								Task.execute(new String[] { });
							} else {
								Toast.makeText(CommentList.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_SHORT).show();
								
							}

						} catch (JSONException e) {
							e.printStackTrace();
						}

					}
				});               
			}   


		} catch (MalformedURLException ex) {

			ex.printStackTrace();

			runOnUiThread(new Runnable() {
				public void run() {
					//Toast.makeText(Warrantyadd.this, "MalformedURLException", Toast.LENGTH_SHORT).show();
				}
			});

			Log.e("Upload file to server", "error: " + ex.getMessage(), ex); 

		} catch (Exception e) {

			e.printStackTrace();
			runOnUiThread(new Runnable() {
				public void run() {
					//Toast.makeText(Warrantyadd.this, "Got Exception : see logcat ", Toast.LENGTH_SHORT).show();
				}
			});
			Log.e("Upload file to server Exception", "Exception : "
					+ e.getMessage(), e); 
		}

		return serverResponseCode;

		//} // End else block
	} 

	/*******************end upload file*********************/
	
	//************************************ SETUP FOR IMAGE INPUT ******************************//

	/**
	 * Checking device has camera hardware or not
	 * */
	@SuppressWarnings("unused")
	private boolean isDeviceSupportCamera() {
		if (getApplicationContext().getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}

	/*
	 * Capturing Camera Image will lauch camera app requrest image capture
	 */
	private void captureImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable("file_uri", fileUri);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		// get the file url
		fileUri = savedInstanceState.getParcelable("file_uri");
	}



	/**
	 * Receiving activity result method will be called after closing the camera
	 * */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				capturingImage = true;
				resizeImg ();
				previewCapturedImage();
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		} 
	}

	/*
	 * Display image from a path to ImageView
	 */
	private void previewCapturedImage() {
		try {
			imgPreview.setVisibility(View.VISIBLE);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = 8;
			final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);
			imgPreview.setImageBitmap(bitmap);
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/*
	 * returning image / video
	 */
	private File getOutputMediaFile(int type) {

		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(uploadFilePath + imgFilename);
			Log.w(LOG_TAG, "getOutputMediaFile: " + mediaFile);

		} else {
			return null;
		}

		return mediaFile;
	}

	private void resizeImg(){
		int maxWidth = 800;
		int maxHeight = 800;

		Log.d(LOG_TAG, "resizeImg-Start");
		String path = uploadFilePath + imgFilename;
		Log.d(LOG_TAG, "path: "+path);

		// create the options
		BitmapFactory.Options opts = new BitmapFactory.Options();

		//just decode the file
		opts.inJustDecodeBounds = true;
		Bitmap bp = BitmapFactory.decodeFile(path, opts);

		//get the original size
		int orignalHeight = opts.outHeight;
		int orignalWidth = opts.outWidth;

		Log.d(LOG_TAG, "orignalHeight: "+orignalHeight);
		Log.d(LOG_TAG, "orignalWidth: "+orignalWidth);

		//initialization of the scale
		int resizeScale = 1;

		//get the good scale
		if ( orignalWidth > maxWidth || orignalHeight > maxHeight ) {
			final int heightRatio = Math.round((float) orignalHeight / (float) maxHeight);
			final int widthRatio = Math.round((float) orignalWidth / (float) maxWidth);
			resizeScale = heightRatio < widthRatio ? heightRatio : widthRatio;
			resizeScale += 1;
		}
		Log.d(LOG_TAG, "resizeScale: "+resizeScale);

		//put the scale instruction (1 -> scale to (1/1); 8-> scale to 1/8)
		opts.inSampleSize = resizeScale;
		opts.inJustDecodeBounds = false;

		bp = BitmapFactory.decodeFile(path, opts);

		FileOutputStream out=null;
		try {
			out = new FileOutputStream(path);
			bp.compress(Bitmap.CompressFormat.JPEG, 80, out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try{
				Log.d(LOG_TAG, "resizeImg-Success");
				out.close();
			} 
			catch(Throwable t) {
				Log.e(LOG_TAG, "resizeImg-Error:" + t.getMessage(), t);

			}
		}
	}


	//************************************ END SETUP FOR IMAGE INPUT ******************************//	

	public boolean isNetworkAvailable(){
		Log.d(LOG_TAG,"isNetworkAvailable-Start!");
		ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo !=null;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d(LOG_TAG,"onResume.. " + capturingImage.toString());

		if (!capturingImage) {
			network_status = isNetworkAvailable();
			if(network_status){
				getInboxData Task = new getInboxData();
				Task.execute(new String[] { });  
			}
		} else {
			capturingImage = false;
		}
	}

	public static DisplayImageOptions getDisplayImageOptions() {
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}

	public void onBackPressed() {
		this.finish();
		slotAction.CreatePluginPage(topid,"content","","","");
	}

	public void onDestroy() {
		
		this.finish();
		super.onStop();
		super.onDestroy();		
	}

}
