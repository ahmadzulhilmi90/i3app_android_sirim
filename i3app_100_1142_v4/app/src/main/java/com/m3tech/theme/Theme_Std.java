package com.m3tech.theme;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebChromeClient;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.m3tech.app_100_1474.R;
import com.m3tech.appv3.SlotAction;
import com.m3tech.collection.Collection_themeobject;
import com.m3tech.customer.Customer_MainPage;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.data.Helper_Themeobjectplugin;
import com.m3tech.data.Helper_Themepage;
import com.m3tech.header.AppHeader;
import com.m3tech.web.WebBrowser;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("deprecation")
public class Theme_Std extends Activity {
	private static final String LOG_TAG = "THEME_STD";
	public static final String PREFS_NAME = "MyPrefsFile";
	public static final String PREFS_FEED = "MyFeedsFile";
	private static final int PERMISSION_REQUEST_CODE = 200;
	ImageView appstitle;
	TextView texttitlebar, texthome;
	static ProgressDialog progressDialog, progressDialog2;

	String themepageid, theme_code, object_id, themecolor, object_type;
	String listtemplate, cont_cat_name, cont_category_id, contentid,
			app_background = "";
	String slot_gal = "", titlebar_img, show_title;
	Cursor ca, cb, cc, cm;
	LinearLayout tabheader, tabback, tabhome, container;
	String objecttype, value, prev_page, object_type2, attribute,
			object_plugin_id, imageUri;
	String udid,app_title, home_themecode, pagetitle, home_pagename, home_pageid,
			colorcode, colorcode1, customerID, prev_pageid, themecode;
	private int PicPosition = 0;
	Gallery gallery;
	RelativeLayout galLayout;
	Runnable runnable;
	boolean rungallery = true, pausegallery = false;

	SharedPreferences settings;
	SharedPreferences feeds;
	AlertDialog.Builder alert;
	String[] values;
	InputStream ims;
	long diffMinutes, diffSeconds;
	String k = "0";
	Handler handler = new Handler();
	Runnable refresh;

	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();

	Helper_Themeobject Helper_Themeobject = null;
	Helper_Themepage Helper_Themepage = null;
	Helper_Themeobjectplugin Helper_Themeobjectplugin = null;
	SlotAction slotAction = null;
	String noback = "", app_db;

	List<Collection_themeobject> slotData = new ArrayList<Collection_themeobject>();
	List<Collection_themeobject> galleryData = new ArrayList<Collection_themeobject>();
	Collection_themeobject current_list = null;
	Collection_themeobject current_slotData = null;

	View headerLayout;
	AppHeader appHeader;

	View menuLayout;
	FloatMenu floatMenu;
	Context context = this;
	Activity activity = this;

	WebView item = null;
	
	private FrameLayout frameWebview,frameLargeview;
	private CustomViewCallback mCustomViewCallback;	
	private View mCustomView;
	private MyChromeClient mClient;
	private ProgressBar spinner = null;
	private static final int SMS_PERMISSION_CODE = 200;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Android StrictMode
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				this)
				.threadPriority(Thread.NORM_PRIORITY)
				// .threadPriority(Thread.MAX_PRIORITY-1)
				.denyCacheImageMultipleSizesInMemory()
				.discCacheFileNameGenerator(new Md5FileNameGenerator())
				.tasksProcessingOrder(QueueProcessingType.FIFO)
				.writeDebugLogs().build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);

		Helper_Themeobject = new Helper_Themeobject(this);
		Helper_Themepage = new Helper_Themepage(this);
		Helper_Themeobjectplugin = new Helper_Themeobjectplugin(this);
		slotAction = new SlotAction(this);

		feeds = this.getSharedPreferences(PREFS_FEED, 0);
		settings = this.getSharedPreferences(PREFS_NAME, 0);

		app_db = context.getResources().getString(R.string.app_db);

		home_themecode = settings.getString("home_themecode", "");
		home_pagename = settings.getString("home_pagename", "");
		app_background = settings.getString("app_background", "");
		customerID = settings.getString("customerID", "");
		app_title = settings.getString("app_title", "");
		udid = settings.getString("udid", "");
		colorcode = settings.getString("colorcode", "");
		colorcode1 = settings.getString("colorcode1", "");
		home_pageid = settings.getString("home_pageid", "");
		prev_pageid = settings.getString("current_pageid", "");
		titlebar_img = settings.getString("titlebar_img", "");
		show_title = settings.getString("show_title", "");

		String ntf_action_type = settings.getString("ntf_action_type", "");
		String ntf_action_value = settings.getString("ntf_action_value", "");
		String ntf_content_id = settings.getString("ntf_content_id", "");

		themepageid = "";
		Intent j = getIntent();
		themecode = j.getStringExtra("themecode");
		themepageid = j.getStringExtra("themepageid");
		noback = j.getStringExtra("noback");
		pagetitle = j.getStringExtra("pagetitle");
		
		Log.e(LOG_TAG, "ENTERING pagetitle .. " + pagetitle);
		Log.e(LOG_TAG, "ENTERING themepageid .. " + themepageid);
		Log.e(LOG_TAG, "ENTERING THEME .. " + themecode);
		
		if (themecode.equals("1")) {
			setContentView(R.layout.theme1);
		} else if (themecode.equals("2")) {
			setContentView(R.layout.theme2);
		} else if (themecode.equals("3")) {
			setContentView(R.layout.theme3);
		} else if (themecode.equals("4")) {
			setContentView(R.layout.theme4);
		} else if (themecode.equals("5")) {
			setContentView(R.layout.theme5);
		} else if (themecode.equals("6")) {
			setContentView(R.layout.theme6);
		} else if (themecode.equals("7")) {
			setContentView(R.layout.theme7);
		} else if (themecode.equals("8")) {
			setContentView(R.layout.theme8);
		} else if (themecode.equals("9")) {
			setContentView(R.layout.theme9);
		} else if (themecode.equals("10")) {
			setContentView(R.layout.theme10);
		} else if (themecode.equals("11")) {
			setContentView(R.layout.theme11);
		} else if (themecode.equals("12")) {
			setContentView(R.layout.theme12);
		} else if (themecode.equals("13")) {
			setContentView(R.layout.theme13);
		} else if (themecode.equals("14")) {
			setContentView(R.layout.theme14);
		} else if (themecode.equals("15")) {
			setContentView(R.layout.theme15);
		} else if (themecode.equals("16")) {
			setContentView(R.layout.theme16);
		} else if (themecode.equals("17")) {
			setContentView(R.layout.theme17);
		} else if (themecode.equals("18")) {
			setContentView(R.layout.theme18);
		} else if (themecode.equals("19")) {
			setContentView(R.layout.theme19);
		} else if (themecode.equals("20")) {
			setContentView(R.layout.theme20);
		} else if (themecode.equals("21")) {
			setContentView(R.layout.theme21);
		} else if (themecode.equals("22")) {
			setContentView(R.layout.theme22);
		} else if (themecode.equals("23")) {
			setContentView(R.layout.theme23);
		} else if (themecode.equals("24")) {
			setContentView(R.layout.theme24);
		} else if (themecode.equals("25")) {
			setContentView(R.layout.theme25);
		} else if (themecode.equals("26")) {
			setContentView(R.layout.theme26);
		}
		else {
			setContentView(R.layout.theme8);
		}

		SharedPreferences.Editor editor = settings.edit();
		editor.remove("current_pageid");
		editor.putString("current_pageid", themepageid);

		if (ntf_action_type.length() > 0) {
			editor.remove("ntf_action_type");
			editor.remove("ntf_action_value");
			editor.remove("ntf_content_id");
		}
		editor.commit();

		if (themepageid.equals(home_pageid)) {
			prev_page = "0";
		} else {
			prev_page = "1";
		}

		Log.d(LOG_TAG, "home_themecode=" + home_themecode);
		Log.d(LOG_TAG, "home_pageid=" + home_pageid);
		Log.d(LOG_TAG, "themepageid=" + themepageid);
		Log.d(LOG_TAG, "prev_page=" + prev_page);
		Log.d(LOG_TAG, "noback=" + noback);

		if (pagetitle.length() == 0) {
			pagetitle = app_title;
		}

		//Toast.makeText(context, pagetitle+"| show_title = " + show_title, Toast.LENGTH_LONG).show();
		
		View headerLayout = (View) findViewById(R.id.headerLayout);
		appHeader = new AppHeader(Theme_Std.this,Theme_Std.this, headerLayout, pagetitle,prev_page, noback);

		if (PageShouldLogin()) {
			Log.d(LOG_TAG, "PageShouldLogin() : YES");
			SharedPreferences.Editor editor1 = settings.edit();
			editor1.remove("afterLogin");
			editor1.putString("themecode", themecode);
			editor1.putString("themepageid", themepageid);
			editor1.putString("noback", noback);
			editor1.putString("pagetitle", pagetitle);
			editor1.putString("afterLogin", "Theme_Std");
			editor1.commit();
			
			Intent i = new Intent(context.getApplicationContext(),Customer_MainPage.class);
			context.startActivity(i);
			finish();
		}else{
			Log.d(LOG_TAG, "PageShouldLogin() : NO");
			LoadMenu();
			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);
	
			// find background image for container
			LoadBackground taskBg = new LoadBackground();
			taskBg.execute();
	
			// get Slot data
			GetSlotData Task = new GetSlotData();
			Task.execute();
	
			if (ntf_action_type.length() > 0) {
				Log.e(LOG_TAG, "Go to Notification Page .. " + ntf_action_type);
				slotAction.CreateNotificationPage(ntf_action_type,ntf_action_value, ntf_content_id);
			}
		}

		if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

			if (ActivityCompat.shouldShowRequestPermissionRationale((activity), Manifest.permission.CAMERA)) {


			} else {
				ActivityCompat.requestPermissions((activity),
						new String[]{Manifest.permission.CAMERA},
						PERMISSION_REQUEST_CODE);
			}

		}



	}

	public void LoadMenu() {
		String viewmenu = "0";
		try {

			cm = Helper_Themeobject.getByslotcodepageid("MENU", themepageid);
			Log.d(LOG_TAG, "Loading Menu.. " + cm.getCount());

			for (cm.moveToFirst(); !cm.isAfterLast(); cm.moveToNext()) {
				viewmenu = Helper_Themeobject.getobject_value(cm).toString();
			}

			if (viewmenu.equals("1")) {
				View menuLayout = (View) findViewById(R.id.menuLayout);
				floatMenu = new FloatMenu(Theme_Std.this, menuLayout);
			}

		} catch (Throwable t) {
			Log.e(LOG_TAG, "LoadMenu-Error:" + t.getMessage(), t);
		}

	}

	public boolean PageShouldLogin() {

		if (customerID.length() > 0) {
			return false;
		} else {
			Cursor c = Helper_Themeobject.getByPageLogin("LOGIN", themepageid);
			Log.d(LOG_TAG, "Loading LOGIN.. " + c.getCount());

			if (c.getCount() > 0) {
				return true;
			} else {
				return false;
			}
		}
	}

	/**** function LoadBackground *****/
	public class LoadBackground extends AsyncTask<String, Void, String> {
		String bgimg = "";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;

			try {
				ca = Helper_Themeobject.getByslotcodepageid("BG", themepageid);
				Log.d(LOG_TAG, "Loading Background.. " + ca.getCount());

				for (ca.moveToFirst(); !ca.isAfterLast(); ca.moveToNext()) {
					bgimg = Helper_Themeobject.getobject_value(ca).toString();
				}

				if (bgimg.length() < 10) {
					bgimg = app_background;
				}

				response = "SUCCESS";

			} catch (Throwable t) {
				response = "FAILED";
				Log.e(LOG_TAG,"LoadBackground-doInBackground-Error:" + t.getMessage(),t);
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if (result.equals("SUCCESS") && bgimg.length() > 10) {
					String bgUri = slotAction.getImageURI(bgimg);
					Log.d(LOG_TAG, "Background: " + bgUri);
					container = (LinearLayout) findViewById(R.id.container);

					String draw = "drawable://";
					if (bgUri.toLowerCase().contains(draw.toLowerCase())) {
						bgUri = bgUri.replace("drawable://", "");
						Drawable d = getResources().getDrawable(Integer.parseInt(bgUri));
						container.setBackground(d);
					} else {
						imageLoader.loadImage(bgUri,new SimpleImageLoadingListener() {
							@Override
							public void onLoadingComplete(String imageUri, View view,Bitmap loadedImage) {
								super.onLoadingComplete(imageUri, view,loadedImage);
								container.setBackground(new BitmapDrawable(loadedImage));
							}
						});
					}
				}
			}

			catch (Throwable t) {
				Log.e(LOG_TAG,"LoadBackground-onPostExecute-Error:" + t.getMessage(),t);

			}
		}
	}

	/**** function get Gallery data *****/
	public class GetGalleryData extends AsyncTask<String, Void, String> {
		int handledelay = 10;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// progressDialog = ProgressDialog.show(Theme5.this, "",
			// getResources().getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;
			String galSlot = arg0[0];

			try {
				ca = Helper_Themeobject.getByslotcodepageid(themecode + galSlot, themepageid);
				Log.d(LOG_TAG, "Loading Gallery.. " + ca.getCount());

				for (ca.moveToFirst(); !ca.isAfterLast(); ca.moveToNext()) {
					current_list = new Collection_themeobject();

					current_list.settheme_code(Helper_Themeobject.gettheme_code(ca).toString());
					current_list.setslot_code(Helper_Themeobject.getslot_code(ca).toString());
					current_list.setpage_id(Helper_Themeobject.getpage_id(ca).toString());
					current_list.setobject_type(Helper_Themeobject.getobject_type(ca).toString());
					current_list.setobject_value(Helper_Themeobject.getobject_value(ca).toString());
					current_list.setaction_type(Helper_Themeobject.getaction_type(ca).toString());
					current_list.setaction_value(Helper_Themeobject.getaction_value(ca).toString());
					current_list.setattribute(Helper_Themeobject.getattribute(ca).toString());
					objecttype = Helper_Themeobject.getobject_type(ca).toString();
					value = Helper_Themeobject.getobject_value(ca).toString();

					galleryData.add(current_list);

				}

				response = galSlot;
			} catch (Throwable t) {
				response = "FAILED";
				Log.e(LOG_TAG,"GetSchedule-doInBackground-Error:" + t.getMessage(), t);
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				Log.d(LOG_TAG, "GetGalleryData :: " + result);
				if (!result.equals("FAILED")) {
					galLayout = SelectSlotLayout(result);

						if (result.equals("A")) {
							gallery = (Gallery) findViewById(R.id.GalA);
						} else if (result.equals("B")) {
							gallery = (Gallery) findViewById(R.id.GalB);
						} else if (result.equals("C")) {
							gallery = (Gallery) findViewById(R.id.GalC);
						} else if (result.equals("D")) {
							gallery = (Gallery) findViewById(R.id.GalD);
						} else if (result.equals("E")) {
							gallery = (Gallery) findViewById(R.id.GalE);
						} else if (result.equals("F")) {
							gallery = (Gallery) findViewById(R.id.GalF);
						} else if (result.equals("G")) {
							gallery = (Gallery) findViewById(R.id.GalG);
						} else if (result.equals("H")) {
							gallery = (Gallery) findViewById(R.id.GalH);
						} else if (result.equals("I")) {
							gallery = (Gallery) findViewById(R.id.GalI);
						} else if (result.equals("J")) {
							gallery = (Gallery) findViewById(R.id.GalJ);
						} else if (result.equals("K")) {
							gallery = (Gallery) findViewById(R.id.GalK);
						} else if (result.equals("L")) {
							gallery = (Gallery) findViewById(R.id.GalL);
						} else if (result.equals("M")) {
							gallery = (Gallery) findViewById(R.id.GalM);
						} else if (result.equals("N")) {
							gallery = (Gallery) findViewById(R.id.GalN);
						} else if (result.equals("O")) {
							gallery = (Gallery) findViewById(R.id.GalO);
						} else if (result.equals("P")) {
							gallery = (Gallery) findViewById(R.id.GalP);
						} else if (result.equals("Q")) {
							gallery = (Gallery) findViewById(R.id.GalQ);
						} else if (result.equals("R")) {
							gallery = (Gallery) findViewById(R.id.GalR);
						}
					

					gallery.setVisibility(View.VISIBLE);

					/***** gallery function ****************/
					if (objecttype.equals("img")) {

						gallery.setAdapter(new ImageAdapter(Theme_Std.this));
						if (galleryData.size() == 1) {
							Log.d(LOG_TAG, "Gallery: 1 image ONLY");
							gallery.setSelection(0);

							gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
								public void onItemClick(AdapterView<?> parent,View v, int position, long id) {
									rungallery = false;
									slotAction.CreateNextPage(galleryData.get(position));
								}
							});

						} else {
							runnable = new Runnable() {
								public void run() {
									if (PicPosition >= galleryData.size()) {
										PicPosition = 0; // start again
									}

									if (rungallery) {
										handledelay = 3000;
										String strdelayed = galleryData.get(PicPosition).getattribute();
										Log.d(LOG_TAG, "Gallery Position: "+ PicPosition + ": "+ strdelayed);

										if (!strdelayed.equals("")) {
											int delay = Integer.parseInt(strdelayed);

											if (delay > 1) {
												handledelay = delay * 1000;
											}
										}

										if (pausegallery) {
											Log.d(LOG_TAG, "pausegallery:"+ handledelay);
											pausegallery = false;
										} else {
											gallery.setSelection(PicPosition);
											gallery.setOnTouchListener(new OnTouchListener() {
												@Override
												public boolean onTouch(View v,MotionEvent event) {
													pausegallery = true;
													return false;
												}
											});

											gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
												public void onItemClick(AdapterView<?> parent,View v, int position,long id) {
													slotAction.CreateNextPage(galleryData.get(position));
												}
											});

										}
										PicPosition = gallery.getSelectedItemPosition() + 1;
									}
									handler.postDelayed(runnable, handledelay);
								}
							};

							handler = new Handler();
							handler.postDelayed(runnable, handledelay);
						}

					} else {

						gallery.setVisibility(View.GONE);
					}

				}

				// progressDialog.dismiss();

			} catch (Throwable t) {
				Log.e(LOG_TAG,"Getcontenthome-onPostExecute-Error:" + t.getMessage(),t);

			}
		}

	}

	/**** gallery image *********/

	public class ImageAdapter extends BaseAdapter {
		private Context context;

		public ImageAdapter(Context c) {
			context = c;
		}

		// ---returns the number of images---
		public int getCount() {
			return galleryData.size();
		}

		// ---returns the ID of an item---
		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		// ---returns an ImageView view---

		@SuppressWarnings("unused")
		public View getView(final int position, View convertView,ViewGroup parent) {

			ImageView imageView = new ImageView(context);
			String object_value = null;

			object_value = galleryData.get(position).getobject_value();
			imageUri = slotAction.getImageURI(object_value);
			Log.d(LOG_TAG, "Image : " + imageUri);

			String draw = "drawable://";
			if (imageUri.toLowerCase().contains(draw.toLowerCase())) {
				imageUri = imageUri.replace("drawable://", "");
				imageView.setImageResource(Integer.parseInt(imageUri));
			} else {

				imageLoader.displayImage(imageUri, imageView, displayOptions,
						new ImageLoadingListener() {
							@Override
							public void onLoadingStarted(String imageUri,View view) {
							}

							@Override
							public void onLoadingFailed(String imageUri,View view, FailReason failReason) {
							}

							@Override
							public void onLoadingComplete(String imageUri,View view, Bitmap loadedImage) {
							}

							@Override
							public void onLoadingCancelled(String imageUri,View view) {
							}
						});

			}

			int header_height = dpToPx(getResources().getInteger(R.integer.header_height));

			int screen_height = getWindowManager().getDefaultDisplay().getHeight() - header_height;
			int screen_width = getWindowManager().getDefaultDisplay().getWidth();

			imageView.setLayoutParams(new Gallery.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

			imageView.setScaleType(ImageView.ScaleType.FIT_XY);

			return imageView;
		}
	}

	/**** function get Tab data *****/
	public class GetSlotData extends AsyncTask<String, Void, String> {
		String slotcode = "", slotprev = "";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// progressDialog2 = ProgressDialog.show(Theme5.this, "",
			// getResources().getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;

			try {
				cb = Helper_Themeobject.getBypageid(themepageid);
				Log.d(LOG_TAG, "Loading Slot.. " + cb.getCount());

				slot_gal = "0";
				for (cb.moveToFirst(); !cb.isAfterLast(); cb.moveToNext()) {
					current_slotData = new Collection_themeobject();

					slotcode = Helper_Themeobject.getslot_code(cb).toString();
					slotcode = slotcode.substring(slotcode.length() - 1);

					if (slotprev.equals(slotcode)) {
						if (!slotcode.equals(slot_gal.substring(slot_gal.length() - 1))) {
							slot_gal = slot_gal + "," + slotcode;
						}

					}

					current_slotData.settheme_code(Helper_Themeobject.gettheme_code(cb).toString());
					current_slotData.setslot_code(slotcode);
					current_slotData.setpage_id(Helper_Themeobject.getpage_id(cb).toString());
					current_slotData.setobject_type(Helper_Themeobject.getobject_type(cb).toString());
					current_slotData.setobject_value(Helper_Themeobject.getobject_value(cb).toString());
					current_slotData.setaction_type(Helper_Themeobject.getaction_type(cb).toString());
					current_slotData.setaction_value(Helper_Themeobject.getaction_value(cb).toString());
					current_slotData.setthemeobject_id(Helper_Themeobject.getthemeobject_id(cb).toString());
					current_slotData.setattribute(Helper_Themeobject.getattribute(cb).toString());
					slotData.add(current_slotData);

					slotprev = slotcode;
				}
				Log.d(LOG_TAG, "Gallery.. " + slot_gal);
				response = "SUCCESS";

				// progressDialog2.dismiss();
			} catch (Throwable t) {
				response = "FAILED";
				Log.e(LOG_TAG,"GetSlotData-doInBackground-Error:" + t.getMessage(), t);
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				if (result.equals("SUCCESS")) {

					Log.d(LOG_TAG, "slotcode :: " + slotcode);
					List<String> items = Arrays.asList(slot_gal.split("\\s*,\\s*"));
					Log.d(LOG_TAG, "Gallery Item X :: "+ items);
					for (String x : items) {
						// get Gallery data
						Log.d(LOG_TAG, "Gallery Item X :: "+ x);
						if (!x.equals("0")) {
							GetGalleryData GalTask = new GetGalleryData();
							GalTask.execute(new String[] { x });
						}
					}
					
					//Setting Data Size
					/*int num = 0, data = 0;
					num = slotData.size();
					if (num % 2 == 1) {
						// odd
						data = slotData.size()-1;
					} else {
						data = slotData.size();
					}
					*//*
					int data = 0;
					if(theme_code.equals("2")){
						data = slotData.size();
					}else{
						data = slotData.size()-1;
					}
					*/
					int data;
					if (themecode.equals("22") || themecode.equals("21") || themecode.equals("20") || themecode.equals("19") || themecode.equals("16") || themecode.equals("15") || themecode.equals("11") || themecode.equals("10") || themecode.equals("9") || themecode.equals("8") || themecode.equals("7") || themecode.equals("4") || themecode.equals("24") || themecode.equals("17") || themecode.equals("14") || themecode.equals("5") || themecode.equals("6") || themecode.equals("3") || themecode.equals("13") || themecode.equals("1") || themecode.equals("2") || themecode.equals("26") || themecode.equals("23") || themecode.equals("18") || themecode.equals("25") || themecode.equals("12")) {
						data = slotData.size();
					}else{
						data = slotData.size()-1;
					}
					/*
					int data;
					if (themecode.equals("12") || themecode.equals("6") || themecode.equals("3") || themecode.equals("13") || themecode.equals("1") || themecode.equals("2") || themecode.equals("26") || themecode.equals("23") || themecode.equals("18") || themecode.equals("25")) {
						data = slotData.size();
					}else{
						data = slotData.size()-1;
					}*/
					for (int g = 0; g < data; g++) {
						Log.d(LOG_TAG, "slot data size : " + String.valueOf(slotData.size()));
						slotcode = slotData.get(g).getslot_code();
						
						if (slot_gal.indexOf(slotcode.charAt(0)) > 0) {
							Log.d(LOG_TAG, slotcode + " is Gallery ");
							// is Gallery
							// Nothing to do							
						} else {
							Log.d(LOG_TAG, slotcode + " is SLOT ");
							SetSlotParameters(slotcode, g);

						}
					}					
				} else {
					// Toast.makeText(Theme_Std.this,getResources().getString(R.string.please_retry_again),
					// Toast.LENGTH_LONG).show();
				}

			} catch (Throwable t) {
				Log.e(LOG_TAG,"GetSlotData-onPostExecute-Error:" + t.getMessage(), t);

			}
		}
	}

	private void SetSlotParameters(final String slotcode, final int position) {

		String imageUri = "";
		RelativeLayout RL = SelectSlotLayout(slotcode);
		ImageView imageSlot = SelectSlotImage(slotcode);
		TextView textSlot = SelectSlotText(slotcode);

		final String object_type = slotData.get(position).getobject_type();
		final ProgressBar spinner = SelectSlotProgress(slotcode);
		String object_value = slotData.get(position).getobject_value();
		String attribute = slotData.get(position).getattribute();
		 
		if (object_type.equals("img")) {
			Log.d(LOG_TAG, "object type-img");
			imageSlot.setVisibility(View.VISIBLE);
			imageUri = slotAction.getImageURI(object_value);
			imageSlot.setScaleType(ScaleType.FIT_XY);
			Log.d(LOG_TAG, "Slot " + slotcode + " : " + imageUri);

			String draw = "drawable://";
			if (imageUri.toLowerCase().contains(draw.toLowerCase())) {
				imageUri = imageUri.replace("drawable://", "");
				imageSlot.setImageResource(Integer.parseInt(imageUri));
				spinner.setVisibility(View.GONE);

			} else {
				imageLoader.displayImage(imageUri, imageSlot, displayOptions,new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri,View view) {
						spinner.setVisibility(View.VISIBLE);
					}

					@Override
					public void onLoadingFailed(String imageUri,View view, FailReason failReason) {
						spinner.setVisibility(View.GONE);

					}

					@Override
					public void onLoadingComplete(String imageUri,View view, Bitmap loadedImage) {
						spinner.setVisibility(View.GONE);
					}

					@Override
					public void onLoadingCancelled(String imageUri,View view) {
						spinner.setVisibility(View.GONE);
					}

				});
			}

		} else if (object_type.equals("feed")) {
			imageSlot.setVisibility(View.GONE);
			textSlot.setVisibility(View.GONE);

			String feedurl = "";
			String feedname = "";
			if (attribute.length() > 0) {

				// Get the URL from Plugin (attribute=plugin_id)

				cc = Helper_Themeobjectplugin.getByActvalue(attribute); 
				if (cc.moveToLast() != false) {
					feedurl = Helper_Themeobjectplugin.getplugin_value3(cc).toString();
					feedname = Helper_Themeobjectplugin.getplugin_value1(cc).toString();
				}
				cc.close();

				Log.d(LOG_TAG, "Slot " + slotcode + " : " + feedname + " > "+ feedurl);

				if (feedurl.length() > 0) {
					// CALL GetFeedData Async
					getFeedData feedTask = new getFeedData();
					feedTask.execute(new String[] { slotcode, feedurl, feedname });
				}

			}

		} else {
			imageSlot.setVisibility(View.GONE);
			textSlot.setVisibility(View.VISIBLE);
			Spanned text = Html.fromHtml(object_value);

			Log.d(LOG_TAG, "Slot " + slotcode + " : Text " + attribute);

			if (attribute.length() > 12) {
				String[] attr = attribute.split("\\|");
				Log.d(LOG_TAG, "Attribute : " + attribute + " - " + attr.length);

				if (attr.length > 3) {
					String bgcolor = "#" + attr[0];
					String fontcolor = "#" + attr[1];
					String fonsize = attr[2];
					String txtalign = attr[3];

					int fontsize_i = Integer.parseInt(fonsize) + 4;

					int gravity = 17;

					if (txtalign.equals("left")) {
						gravity = 3;
					} else if (txtalign.equals("right")) {
						gravity = 5;
					}

					if (bgcolor.length() > 6) {
						textSlot.setBackgroundColor(Color.parseColor(bgcolor));
					}
					textSlot.setTextColor(Color.parseColor(fontcolor));
					textSlot.setTextSize((float) fontsize_i);
					textSlot.setGravity(17 | gravity);
					textSlot.setPadding(10, 10, 10, 10);
				}
			}
			spinner.setVisibility(View.GONE);
			textSlot.setText(text);
		}

		RL.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (!object_type.equals("feed")) {
					rungallery = false;
					Log.d(LOG_TAG, "slotcode :: " + slotData.get(position).getslot_code() + " ---| object type :: " + slotData.get(position).getobject_type() + " ---| action value :: "+ slotData.get(position).getaction_value());
					slotAction.CreateNextPage(slotData.get(position));
				}
			}
		});
	}

	private class getFeedData extends AsyncTask<String, Void, String> {
		WebView webSlot;
		
		String slotname = "";

		// save the data in PREF FILES

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {

			String slotcode = arg0[0];
			String feedurl = arg0[1];
			slotname = arg0[2];

			if (customerID.length() < 1) {
				customerID = "";
			}

			feedurl = feedurl + "&db=" + app_db + "&cid=" + customerID+"&udid=" + udid;
			Log.d(LOG_TAG, "feedurl 1 : " + feedurl);

			String response = feeds.getString(feedurl, "");
			String responseBody = null;

			try {
				webSlot = SelectSlotWeb(slotcode);
				spinner = SelectSlotProgress(slotcode);
				frameWebview = SelectFrameWebView(slotcode);
				frameLargeview = SelectFrameLayoutLargeView(slotcode);

				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {
					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

					HttpPost HTTP_POST_RSVSETTING;
					HTTP_POST_RSVSETTING = new HttpPost(feedurl);
					// Log.d(LOG_TAG,"feedurl="+ feedurl);

					ResponseHandler<String> responseHandler_RSVSETTING = new BasicResponseHandler();
					responseBody = httpclient.execute(HTTP_POST_RSVSETTING,responseHandler_RSVSETTING);
					Log.d(LOG_TAG, "responbody : " + responseBody);

					response = responseBody;

					SharedPreferences.Editor feeditor = feeds.edit();
					feeditor.remove(feedurl);
					feeditor.putString(feedurl, responseBody);
					feeditor.commit();
				}

			} catch (Throwable t) {
				if (response.length() == 0) {
					response = getResources().getString(R.string.error_msg);
				}
				Log.e(LOG_TAG,
						"getFeedData-doInBackground-Error:" + t.getMessage(), t);
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {

				if (result.length() > 0) {
					
					String htmlData = "<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0\"></head>"
							+ "<body><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />"
							+ result + "</body>";
					Log.d(LOG_TAG, "htmlData : "+ htmlData);
					
					webSlot.setVisibility(View.VISIBLE);
					webSlot.setBackgroundColor(Color.TRANSPARENT);
					
					webSlot.canGoBack();
					WebSettings settings = webSlot.getSettings();
					webSlot.getSettings().setAllowFileAccess(true);
					webSlot.getSettings().setPluginState(WebSettings.PluginState.ON);
					webSlot.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);
					settings.setMinimumFontSize(14);
					settings.setLoadWithOverviewMode(true);
					settings.setUseWideViewPort(true);
					settings.setDisplayZoomControls(false);
					settings.setAppCacheEnabled(true);
					settings.setJavaScriptEnabled(true);
					
					mClient = new MyChromeClient();
					webSlot.setWebChromeClient(mClient);
					//webSlot.loadData(htmlData, "text/html", "UTF-8");
					webSlot.loadDataWithBaseURL("file:///android_asset/",htmlData, "text/html", "UTF-8", null);
					//webSlot.loadUrl("file:///android_asset/test.html");

					/*
					
					webSlot.canGoBack();
					WebSettings settings = webSlot.getSettings();
					webSlot.getSettings().setAllowFileAccess(true);
					webSlot.getSettings().setPluginState(WebSettings.PluginState.ON);
					webSlot.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);
					settings.setMinimumFontSize(14);
					settings.setLoadWithOverviewMode(true);
					settings.setUseWideViewPort(true);
					settings.setDisplayZoomControls(false);
					settings.setAppCacheEnabled(true);
					settings.setJavaScriptEnabled(true);

					String htmlData = "<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0\"></head>"
										+ "<body><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />"
										+ result + "</body>";
					Log.d(LOG_TAG, "htmlData : "+ htmlData);
					//webSlot.loadUrl("file:///android_asset/"+htmlData);
					//webSlot.loadDataWithBaseURL("file:///android_asset/",htmlData, "text/html", "UTF-8", null);
					webSlot.loadData(htmlData, "text/html", "UTF-8");
					// webSlot.loadData(result, "text/html", "UTF-8");
					webSlot.setVisibility(View.VISIBLE);
					webSlot.setBackgroundColor(Color.TRANSPARENT);

					webSlot.setWebChromeClient(new WebChromeClient() {
						
						public void onProgressChanged(WebView view, int progress) {
							Log.i(LOG_TAG,
									"setWebChromeClient - onProgressChanged: "
											+ progress);

							if (progress == 100) {
								spinner.setVisibility(View.GONE);
							} else {
								spinner.setVisibility(View.VISIBLE);
							}
						}
					});
				*/

					// prevent opening new window
					webSlot.setWebViewClient(new WebViewClient() {
						@Override
						public boolean shouldOverrideUrlLoading(WebView view,
								String url) {
							Log.d(LOG_TAG, "URL: " + url);
							
							if (url.toLowerCase().startsWith("tel:")) {
								//Intent intent = new Intent(Intent.ACTION_DIAL,Uri.parse(url));
								//startActivity(intent);
								return true;
							}

							else if (url.toLowerCase().startsWith("mailto:")) {
								String email = url.toLowerCase();
								email = url.replace("mailto:", "");

								Intent emailIntent = new Intent(
										android.content.Intent.ACTION_SEND);
								emailIntent.setType("plain/text");
								emailIntent.putExtra(
										android.content.Intent.EXTRA_EMAIL,
										new String[] { email });
								emailIntent.putExtra(
										android.content.Intent.EXTRA_SUBJECT,
										app_title);
								emailIntent.putExtra(
										android.content.Intent.EXTRA_TEXT,
										Html.fromHtml(""));

								startActivity(Intent.createChooser(emailIntent,
										"Send email..."));
								return true;
							}

							else if (!url.startsWith("http://")
									&& !url.startsWith("https://")) {
								url = "http://" + url;
							}

							Intent i = new Intent(Theme_Std.this,
									WebBrowser.class);
							i.putExtra("pagetitle", slotname);
							i.putExtra("url", url);
							i.putExtra("openweb", "no");
							Theme_Std.this.startActivity(i);

							return true;
						}

						@Override
						public void onPageFinished(WebView view, String url) {
							super.onPageFinished(view, url);
							Log.d(LOG_TAG,"url : "+ url);
						}

						@Override
						public void onReceivedError(WebView view, int errorCod,
								String description, String failingUrl) {
								Log.d(LOG_TAG,"description : "+ description);
								Log.d(LOG_TAG,"failingUrl : "+ failingUrl);
								Log.d(LOG_TAG,"errorCod : "+ errorCod);
						}

					});
				}

			} catch (Throwable t) {
				Log.e(LOG_TAG, "getFeedData-Error:" + t.getMessage(), t);
			}
			spinner.setVisibility(View.GONE);

		}

	}

	class MyChromeClient extends WebChromeClient {
		
		@Override
		public void onShowCustomView(View view, CustomViewCallback callback) {		
		    mCustomViewCallback = callback;
		    frameLargeview.addView(view);
			mCustomView = view;
			frameWebview.setVisibility(View.GONE);
			frameLargeview.setVisibility(View.VISIBLE);
			frameLargeview.bringToFront();
		}
		
		@Override
		public void onProgressChanged(WebView view, int progress) {
			Log.i(LOG_TAG,"setWebChromeClient - onProgressChanged: "+ progress);

			if (progress == 100) {
				spinner.setVisibility(View.GONE);
			} else {
				spinner.setVisibility(View.VISIBLE);
			}
		}

		@Override
		public void onHideCustomView() {
			if (mCustomView == null)
				return;

			mCustomView.setVisibility(View.GONE);
			frameLargeview.removeView(mCustomView);
			mCustomView = null;
			frameLargeview.setVisibility(View.GONE);			
			mCustomViewCallback.onCustomViewHidden();			
			frameWebview.setVisibility(View.VISIBLE);
		}
	}
	
	private RelativeLayout SelectSlotLayout(final String slotcode) {
		RelativeLayout Lay = null;

		if (slotcode.equals("A")) {
			Lay = (RelativeLayout) findViewById(R.id.LayA);
		} else if (slotcode.equals("B")) {
			Lay = (RelativeLayout) findViewById(R.id.LayB);
		} else if (slotcode.equals("C")) {
			Lay = (RelativeLayout) findViewById(R.id.LayC);
		} else if (slotcode.equals("D")) {
			Lay = (RelativeLayout) findViewById(R.id.LayD);
		} else if (slotcode.equals("E")) {
			Lay = (RelativeLayout) findViewById(R.id.LayE);
		} else if (slotcode.equals("F")) {
			Lay = (RelativeLayout) findViewById(R.id.LayF);
		} else if (slotcode.equals("G")) {
			Lay = (RelativeLayout) findViewById(R.id.LayG);
		} else if (slotcode.equals("H")) {
			Lay = (RelativeLayout) findViewById(R.id.LayH);
		} else if (slotcode.equals("I")) {
			Lay = (RelativeLayout) findViewById(R.id.LayI);
		} else if (slotcode.equals("J")) {
			Lay = (RelativeLayout) findViewById(R.id.LayJ);
		} else if (slotcode.equals("K")) {
			Lay = (RelativeLayout) findViewById(R.id.LayK);
		} else if (slotcode.equals("L")) {
			Lay = (RelativeLayout) findViewById(R.id.LayL);
		} else if (slotcode.equals("M")) {
			Lay = (RelativeLayout) findViewById(R.id.LayM);
		} else if (slotcode.equals("N")) {
			Lay = (RelativeLayout) findViewById(R.id.LayN);
		} else if (slotcode.equals("O")) {
			Lay = (RelativeLayout) findViewById(R.id.LayO);
		} else if (slotcode.equals("P")) {
			Lay = (RelativeLayout) findViewById(R.id.LayP);
		} else if (slotcode.equals("Q")) {
			Lay = (RelativeLayout) findViewById(R.id.LayQ);
		} else if (slotcode.equals("R")) {
			Lay = (RelativeLayout) findViewById(R.id.LayR);
		}

		if (themecode.equals("4")) {

			int header_height = dpToPx(getResources().getInteger(
					R.integer.header_height));

			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);

			int screen_height = getWindowManager().getDefaultDisplay()
					.getHeight();
			Log.d(LOG_TAG, "screen_height=" + screen_height);
			int screen_width = getWindowManager().getDefaultDisplay()
					.getWidth();
			Log.d(LOG_TAG, "screen_width=" + screen_width);

			int template_height = screen_height - header_height;
			Log.d(LOG_TAG, "template_height=" + template_height);

			int big_square = (template_height / 2);
			Log.d(LOG_TAG, "big_square=" + big_square);

			int small_square = (template_height / 4);
			Log.d(LOG_TAG, "small_square=" + small_square);

			LayoutParams paramsLay;
			paramsLay = Lay.getLayoutParams();

			if (slotcode.equals("A") || slotcode.equals("H")
					|| slotcode.equals("K")) {
				paramsLay.height = big_square;
				paramsLay.width = big_square;
			}

			else if (slotcode.equals("I") || slotcode.equals("J")
					|| slotcode.equals("B") || slotcode.equals("F")
					|| slotcode.equals("E") || slotcode.equals("N")) {
				paramsLay.height = small_square;
				paramsLay.width = small_square;
			}

			else if (slotcode.equals("L") || slotcode.equals("D")
					|| slotcode.equals("M")) {
				paramsLay.height = small_square;
				paramsLay.width = big_square;
			}

			else if (slotcode.equals("C") || slotcode.equals("G")) {
				paramsLay.height = big_square;
				paramsLay.width = small_square;
			}

		}

		return Lay;
	}

	private ProgressBar SelectSlotProgress(final String slotcode) {
		ProgressBar item = null;

		if (slotcode.equals("A")) {
			item = (ProgressBar) findViewById(R.id.ProgA);
		} else if (slotcode.equals("B")) {
			item = (ProgressBar) findViewById(R.id.ProgB);
		} else if (slotcode.equals("C")) {
			item = (ProgressBar) findViewById(R.id.ProgC);
		} else if (slotcode.equals("D")) {
			item = (ProgressBar) findViewById(R.id.ProgD);
		} else if (slotcode.equals("E")) {
			item = (ProgressBar) findViewById(R.id.ProgE);
		} else if (slotcode.equals("F")) {
			item = (ProgressBar) findViewById(R.id.ProgF);
		} else if (slotcode.equals("G")) {
			item = (ProgressBar) findViewById(R.id.ProgG);
		} else if (slotcode.equals("H")) {
			item = (ProgressBar) findViewById(R.id.ProgH);
		} else if (slotcode.equals("I")) {
			item = (ProgressBar) findViewById(R.id.ProgI);
		} else if (slotcode.equals("J")) {
			item = (ProgressBar) findViewById(R.id.ProgJ);
		} else if (slotcode.equals("K")) {
			item = (ProgressBar) findViewById(R.id.ProgK);
		} else if (slotcode.equals("L")) {
			item = (ProgressBar) findViewById(R.id.ProgL);
		} else if (slotcode.equals("M")) {
			item = (ProgressBar) findViewById(R.id.ProgM);
		} else if (slotcode.equals("N")) {
			item = (ProgressBar) findViewById(R.id.ProgN);
		} else if (slotcode.equals("O")) {
			item = (ProgressBar) findViewById(R.id.ProgO);
		} else if (slotcode.equals("P")) {
			item = (ProgressBar) findViewById(R.id.ProgP);
		} else if (slotcode.equals("Q")) {
			item = (ProgressBar) findViewById(R.id.ProgQ);
		} else if (slotcode.equals("R")) {
			item = (ProgressBar) findViewById(R.id.ProgR);
		}

		return item;
	}

	private FrameLayout SelectFrameWebView(final String slotcode) {
		FrameLayout item = null;

		if (slotcode.equals("A")) {
			item = (FrameLayout) findViewById(R.id.frameWebA);
		} else if (slotcode.equals("B")) {
			item = (FrameLayout) findViewById(R.id.frameWebB);
		} else if (slotcode.equals("C")) {
			item = (FrameLayout) findViewById(R.id.frameWebC);
		} else if (slotcode.equals("D")) {
			item = (FrameLayout) findViewById(R.id.frameWebD);
		} else if (slotcode.equals("E")) {
			item = (FrameLayout) findViewById(R.id.frameWebE);
		} else if (slotcode.equals("F")) {
			item = (FrameLayout) findViewById(R.id.frameWebF);
		} else if (slotcode.equals("G")) {
			item = (FrameLayout) findViewById(R.id.frameWebG);
		} else if (slotcode.equals("H")) {
			item = (FrameLayout) findViewById(R.id.frameWebH);
		} else if (slotcode.equals("I")) {
			item = (FrameLayout) findViewById(R.id.frameWebI);
		} else if (slotcode.equals("J")) {
			item = (FrameLayout) findViewById(R.id.frameWebJ);
		} else if (slotcode.equals("K")) {
			item = (FrameLayout) findViewById(R.id.frameWebK);
		} else if (slotcode.equals("L")) {
			item = (FrameLayout) findViewById(R.id.frameWebL);
		} else if (slotcode.equals("M")) {
			item = (FrameLayout) findViewById(R.id.frameWebM);
		} else if (slotcode.equals("N")) {
			item = (FrameLayout) findViewById(R.id.frameWebN);
		} else if (slotcode.equals("O")) {
			item = (FrameLayout) findViewById(R.id.frameWebO);
		} else if (slotcode.equals("P")) {
			item = (FrameLayout) findViewById(R.id.frameWebP);
		} else if (slotcode.equals("Q")) {
			item = (FrameLayout) findViewById(R.id.frameWebQ);
		} else if (slotcode.equals("R")) {
			item = (FrameLayout) findViewById(R.id.frameWebR);
		}
		return item;
	}
	
	private FrameLayout SelectFrameLayoutLargeView(final String slotcode) {
		FrameLayout item = null;
		item = (FrameLayout) findViewById(R.id.framelayout_large_view);
		return item;
	}
	
	private TextView SelectSlotText(final String slotcode) {
		TextView item = null;

		if (slotcode.equals("A")) {
			item = (TextView) findViewById(R.id.TextA);
		} else if (slotcode.equals("B")) {
			item = (TextView) findViewById(R.id.TextB);
		} else if (slotcode.equals("C")) {
			item = (TextView) findViewById(R.id.TextC);
		} else if (slotcode.equals("D")) {
			item = (TextView) findViewById(R.id.TextD);
		} else if (slotcode.equals("E")) {
			item = (TextView) findViewById(R.id.TextE);
		} else if (slotcode.equals("F")) {
			item = (TextView) findViewById(R.id.TextF);
		} else if (slotcode.equals("G")) {
			item = (TextView) findViewById(R.id.TextG);
		} else if (slotcode.equals("H")) {
			item = (TextView) findViewById(R.id.TextH);
		} else if (slotcode.equals("I")) {
			item = (TextView) findViewById(R.id.TextI);
		} else if (slotcode.equals("J")) {
			item = (TextView) findViewById(R.id.TextJ);
		} else if (slotcode.equals("K")) {
			item = (TextView) findViewById(R.id.TextK);
		} else if (slotcode.equals("L")) {
			item = (TextView) findViewById(R.id.TextL);
		} else if (slotcode.equals("M")) {
			item = (TextView) findViewById(R.id.TextM);
		} else if (slotcode.equals("N")) {
			item = (TextView) findViewById(R.id.TextN);
		} else if (slotcode.equals("O")) {
			item = (TextView) findViewById(R.id.TextO);
		} else if (slotcode.equals("P")) {
			item = (TextView) findViewById(R.id.TextP);
		} else if (slotcode.equals("Q")) {
			item = (TextView) findViewById(R.id.TextQ);
		} else if (slotcode.equals("R")) {
			item = (TextView) findViewById(R.id.TextR);
		}

		return item;
	}

	private ImageView SelectSlotImage(final String slotcode) {
		ImageView item = null;

			if(slotcode.equals("A")) {
				item = (ImageView) findViewById(R.id.ImgA);
			} else if (slotcode.equals("B")) {
				item = (ImageView) findViewById(R.id.ImgB);
			} else if (slotcode.equals("C")) {
				item = (ImageView) findViewById(R.id.ImgC);
			} else if (slotcode.equals("D")) {
				item = (ImageView) findViewById(R.id.ImgD);
			} else if (slotcode.equals("E")) {
				item = (ImageView) findViewById(R.id.ImgE);
			} else if (slotcode.equals("F")) {
				item = (ImageView) findViewById(R.id.ImgF);
			} else if (slotcode.equals("G")) {
				item = (ImageView) findViewById(R.id.ImgG);
			} else if (slotcode.equals("H")) {
				item = (ImageView) findViewById(R.id.ImgH);
			} else if (slotcode.equals("I")) {
				item = (ImageView) findViewById(R.id.ImgI);
			} else if (slotcode.equals("J")) {
				item = (ImageView) findViewById(R.id.ImgJ);
			} else if (slotcode.equals("K")) {
				item = (ImageView) findViewById(R.id.ImgK);
			} else if (slotcode.equals("L")) {
				item = (ImageView) findViewById(R.id.ImgL);
			} else if (slotcode.equals("M")) {
				item = (ImageView) findViewById(R.id.ImgM);
			} else if (slotcode.equals("N")) {
				item = (ImageView) findViewById(R.id.ImgN);
			} else if (slotcode.equals("O")) {
				item = (ImageView) findViewById(R.id.ImgO);
			} else if (slotcode.equals("P")) {
				item = (ImageView) findViewById(R.id.ImgP);
			} else if (slotcode.equals("Q")) {
				item = (ImageView) findViewById(R.id.ImgQ);
			} else if (slotcode.equals("R")) {
				item = (ImageView) findViewById(R.id.ImgR);
			}
		

		return item;
	}

	private WebView SelectSlotWeb(final String slotcode) {

		if (slotcode.equals("A")) {
			item = (WebView) findViewById(R.id.WebA);
		} else if (slotcode.equals("B")) {
			item = (WebView) findViewById(R.id.WebB);
		} else if (slotcode.equals("C")) {
			item = (WebView) findViewById(R.id.WebC);
		} else if (slotcode.equals("D")) {
			item = (WebView) findViewById(R.id.WebD);
		} else if (slotcode.equals("E")) {
			item = (WebView) findViewById(R.id.WebE);
		} else if (slotcode.equals("F")) {
			item = (WebView) findViewById(R.id.WebF);
		} else if (slotcode.equals("G")) {
			item = (WebView) findViewById(R.id.WebG);
		} else if (slotcode.equals("H")) {
			item = (WebView) findViewById(R.id.WebH);
		} else if (slotcode.equals("I")) {
			item = (WebView) findViewById(R.id.WebI);
		} else if (slotcode.equals("J")) {
			item = (WebView) findViewById(R.id.WebJ);
		} else if (slotcode.equals("K")) {
			item = (WebView) findViewById(R.id.WebK);
		} else if (slotcode.equals("L")) {
			item = (WebView) findViewById(R.id.WebL);
		} else if (slotcode.equals("M")) {
			item = (WebView) findViewById(R.id.WebM);
		} else if (slotcode.equals("N")) {
			item = (WebView) findViewById(R.id.WebN);
		} else if (slotcode.equals("O")) {
			item = (WebView) findViewById(R.id.WebO);
		} else if (slotcode.equals("P")) {
			item = (WebView) findViewById(R.id.WebP);
		} else if (slotcode.equals("R")) {
			item = (WebView) findViewById(R.id.WebR);
		}

		return item;
	}

	/*** SAME FOR EACH THEME PAGE ****************************************************/

	public static DisplayImageOptions getDisplayImageOptions() {
		return new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();
	}

	public int dpToPx(int dp) {
		DisplayMetrics displayMetrics = getBaseContext().getResources().getDisplayMetrics();
		int px = Math.round(dp* (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return px;
	}

	/*
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			switch (keyCode) {
			case KeyEvent.KEYCODE_BACK:
				
				if (themecode.equals("2")) {
					
					if(framelayouttarget.getVisibility() == View.VISIBLE){
						mClient.onHideCustomView();
					}else{
						if (item.canGoBack()) {
							item.goBack();
						} else {
							onBackPressed();
						}
					}
					
				} else {
					onBackPressed();
				}
				return true;
			}

		}
		return super.onKeyDown(keyCode, event);
	}*/


	@Override
	public void onBackPressed() {
		
		if(prev_page != null){
		
			if (prev_page.equals("0") || noback.equals("yes")) {
	
				if (themecode.equals("2")) {
					
					if(frameLargeview.getVisibility() == View.VISIBLE){
						mClient.onHideCustomView();
					}else{
						if (item.canGoBack()) {
							item.goBack();
						} else {
							onAlert();
						}
					}
					
				} else {
					onAlert();
				}
	
			} else {
				
				this.finish();
				//finishAffinity();
			}
		}else{
			//finishAffinity();
			this.finish();
		}

	}
	
	public void onAlert(){
		alert = new AlertDialog.Builder(this);
		alert.setTitle(getResources().getString(R.string.want_to_exit));
		alert.setIcon(R.drawable.icon90);

		alert.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int whichButton) {
						Intent startMain = new Intent(Intent.ACTION_MAIN);
						startMain.addCategory(Intent.CATEGORY_HOME);
						startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(startMain);
					}

				});

		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int whichButton) {
						dialog.cancel();
					}
				});
		alert.show();
	}
	
	private void toggleWebViewState(WebView webview,boolean pause)
	{           
	    try
	    {
	    	webview.stopLoading();
	    	//webview.destroy();
	        Class.forName("android.webkit.WebView")
	        .getMethod(pause ? "onPause" : "onResume", (Class[]) null)
	        //.getMethod("onPause", (Class[]) null)
	        .invoke(webview, (Object[]) null);
	    
	    }
	    catch (Exception e){}
	}

	@Override
	protected void onPause() {
		rungallery = false;

		WebView A = (WebView)findViewById(R.id.WebA);
		
		if(A.getVisibility() == View.VISIBLE){
			
			if (mCustomView != null){
				mClient.onHideCustomView();
			}
			toggleWebViewState(item,true);
		}
		
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		String reCreateFirstPage = settings.getString("reCreateFirstPage", "");

		if (reCreateFirstPage.equals("true")) {
			Log.d(LOG_TAG, "reCreateFirstPage TRUE");

			finish();
			startActivity(getIntent());

			SharedPreferences.Editor editor = settings.edit();
			editor.remove("reCreateFirstPage");
			editor.putString("reCreateFirstPage", "false");
			editor.commit();
		}
		rungallery = true;
		
		WebView A = (WebView)findViewById(R.id.WebA);
		
		if(A.getVisibility() == View.VISIBLE){
			
			if (mCustomView != null){
				mClient.onHideCustomView();
			}
			toggleWebViewState(item,false);
			
		}
	}

	// FUNCTION NETWORK

	public boolean isNetworkAvailable() {

		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	/*** END SAME FOR EACH THEME PAGE ****************************************************/

	public void onDestroy() {
		rungallery = false;
		
		Helper_Themepage.close();
		Helper_Themeobject.close();
		Helper_Themeobjectplugin.close();
		slotAction.close();
		
		this.finish();
		super.onStop();
		super.onDestroy();

	}
}
