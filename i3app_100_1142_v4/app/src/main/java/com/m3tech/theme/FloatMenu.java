package com.m3tech.theme;

import java.util.ArrayList;
import java.util.List;

import com.m3tech.app_100_1474.R;

import com.m3tech.appv3.SlotAction;
import com.m3tech.collection.Collection_themeobject;
import com.m3tech.data.Helper_Themeobject;
import com.m3tech.data.Helper_Themeobjectplugin;
import com.m3tech.data.Helper_Themepage;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FloatMenu {

	private static final String LOG_TAG = "FLOAT MENU";
	public static final String PREFS_NAME = "MyPrefsFile"; 
	ImageView appstitle;
	TextView texttitlebar,texthome;
	static ProgressDialog progressDialog,progressDialog2;

	String theme_code = "MENU";
	Cursor cb;
	LinearLayout tabmenu1,tabmenu2;
	String object_id,object_plugin_id,themecolor,object_type;

	Context mycontext;
	View layout;

	Helper_Themeobject Helper_Themeobject = null;
	Helper_Themepage Helper_Themepage = null;
	Helper_Themeobjectplugin Helper_Themeobjectplugin = null;
	SlotAction slotAction = null;

	List<Collection_themeobject> slotData=new ArrayList<Collection_themeobject>();
	Collection_themeobject current_list=null;	
	Collection_themeobject current_slotData=null;	


	@SuppressLint("NewApi")
	
	public FloatMenu(Context context, View lay) {
		// TODO Auto-generated constructor stub

		mycontext = context;
		layout = lay;
		slotAction = new SlotAction(mycontext);


		Helper_Themeobject = new Helper_Themeobject(mycontext);
		Helper_Themepage = new Helper_Themepage(mycontext);
		Helper_Themeobjectplugin = new Helper_Themeobjectplugin(mycontext);
		slotAction = new SlotAction(mycontext);

		Log.e(LOG_TAG,"ENTERING THEME .. "+LOG_TAG);

		//get Slot data
		GetSlotData Task = new GetSlotData();
		Task.execute();


	}


	/****  function get Tab data  *****/
	public class GetSlotData extends AsyncTask<String, Void, String> {
		String  slotcode="";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//progressDialog2 = ProgressDialog.show(Theme5.this, "", getResources().getString(R.string.please_wait));
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;

			try{
				cb = Helper_Themeobject.getMenu();
				Log.d(LOG_TAG, "Loading Slot.. " + cb.getCount());

				for(cb.moveToFirst(); !cb.isAfterLast(); cb.moveToNext()){
					current_slotData=new Collection_themeobject();

					slotcode = Helper_Themeobject.getslot_code(cb).toString();
					slotcode = slotcode.substring(slotcode.length() - 1);

					current_slotData.settheme_code(Helper_Themeobject.gettheme_code(cb).toString());
					current_slotData.setslot_code(slotcode);
					current_slotData.setpage_id(Helper_Themeobject.getpage_id(cb).toString());
					current_slotData.setobject_type(Helper_Themeobject.getobject_type(cb).toString());
					current_slotData.setobject_value(Helper_Themeobject.getobject_value(cb).toString());
					current_slotData.setaction_type(Helper_Themeobject.getaction_type(cb).toString());
					current_slotData.setaction_value(Helper_Themeobject.getaction_value(cb).toString());
					current_slotData.setthemeobject_id(Helper_Themeobject.getthemeobject_id(cb).toString());
					current_slotData.setattribute(Helper_Themeobject.getattribute(cb).toString());
					slotData.add(current_slotData);

				}
				response="SUCCESS";

				//progressDialog2.dismiss();	
			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetSlotData-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{

				if (result.equals("SUCCESS")){

					for(int g=0; g < slotData.size(); g++){
						slotcode = slotData.get(g).getslot_code();
						SetSlotParameters(slotcode,g);
					}


				} else {
					//NO MENU;
					Log.d(LOG_TAG, "Menu not available.. ");
				}

			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "GetSlotData-onPostExecute-Error:" + t.getMessage(), t);

			}
		}
	}

	@SuppressWarnings("deprecation")
	private void SetSlotParameters(final String slotcode, final int position) {

		LinearLayout RL = SelectSlotLayout(slotcode);
		TextView textSlot = SelectSlotText(slotcode);
		
		String object_value = slotData.get(position).getobject_value();							
		String attribute = slotData.get(position).getattribute();

		if (object_value.length()>0) {
			
			RL.setVisibility(View.VISIBLE);
			textSlot.setVisibility(View.VISIBLE);
			Spanned text = Html.fromHtml(object_value);

			Log.d(LOG_TAG, "Slot " + slotcode + " : Text " + attribute);				

			if (attribute.length()>12) {
				String[] attr = attribute.split("\\|");
				Log.d(LOG_TAG, "Attribute : " + attribute + " - " + attr.length);

				if (attr.length>3) {				
					String bgcolor = "#99"+attr[0];
					String fontcolor = "#"+attr[1];
					String fonsize = attr[2];
					String txtalign = attr[3];

					int fontsize_i = Integer.parseInt(fonsize)+4;

					int gravity = 17;

					if (txtalign.equals("left")) {
						gravity = 3;
					} else if (txtalign.equals("right")) {
						gravity = 5;
					}

					if (bgcolor.length()<6) {
						bgcolor = "#99FFFFFF"; 
					}
					
					GradientDrawable gd = new GradientDrawable();
			        gd.setColor(Color.parseColor(bgcolor)); 
			        gd.setCornerRadius(0);
			        gd.setStroke(2, Color.parseColor(fontcolor));
			        
			        textSlot.setBackgroundDrawable(gd);				        
					//textSlottextSlot.setBackgroundColor(Color.parseColor(bgcolor));
					
					
					textSlot.setTextColor(Color.parseColor(fontcolor));
					textSlot.setTextSize((float)fontsize_i);
					textSlot.setGravity(17|gravity);
				}
			}
			textSlot.setText(text);
		}		

		textSlot.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {					
				slotAction.CreateNextPage(slotData.get(position));		
				
			}
		});
	}



	private LinearLayout SelectSlotLayout(final String slotcode) {
		LinearLayout Lay = null;

		if(slotcode.equals("A") || slotcode.equals("B") || slotcode.equals("C")){ 
			Lay = (LinearLayout)layout.findViewById(R.id.tabmenu1);											
		} else if(slotcode.equals("D") || slotcode.equals("E") || slotcode.equals("F")){ 
			Lay = (LinearLayout)layout.findViewById(R.id.tabmenu2);											
		} 
		
		return Lay;
	}


	private TextView SelectSlotText(final String slotcode) {
		TextView item = null;

		if(slotcode.equals("A")){ 
			item = (TextView)layout.findViewById(R.id.TextA);											
		} else if(slotcode.equals("B")){ 
			item = (TextView)layout.findViewById(R.id.TextB);											
		} else if(slotcode.equals("C")){ 
			item = (TextView)layout.findViewById(R.id.TextC);											
		} else if(slotcode.equals("D")){ 
			item = (TextView)layout.findViewById(R.id.TextD);											
		} else if(slotcode.equals("E")){ 
			item = (TextView)layout.findViewById(R.id.TextE);											
		} else if(slotcode.equals("F")){ 
			item = (TextView)layout.findViewById(R.id.TextF);											
		} 		

		return item;
	}


	/***  END SAME FOR EACH THEME PAGE ****************************************************/

	public void onDestroy() {
		Helper_Themepage.close();		
		Helper_Themeobject.close();
		Helper_Themeobjectplugin.close();
		slotAction.close();

	}
}

