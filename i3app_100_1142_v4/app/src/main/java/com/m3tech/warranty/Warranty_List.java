package com.m3tech.warranty;


import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.collection.Collection_Warrantylist;
import com.m3tech.header.Header;
import com.m3tech.widget.Warranty_List_Widget;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

@SuppressWarnings("deprecation")
@SuppressLint("NewApi")
public class Warranty_List extends Warranty_List_Widget {

	private static final String LOG_TAG = "WARRANTYLIST";
	warrantylist_Adapter adapter_warrantylist=null;
	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		String pageTitle = settings.getString("warrantyPageTitle", "");
		if(pageTitle ==null||pageTitle.equals("")){
			pageTitle = app_title;
		}

		Intent j = getIntent();
		tabvalue = j.getStringExtra("tabvalue");
		if(tabvalue ==null||tabvalue.equals("")){
			tabvalue = "normal";
		}

		Log.d(LOG_TAG, "tabvalue="+tabvalue);

		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome,img_back,img_home, texttitlebar, pageTitle,tabvalue);

		
		layoutaddnew.setBackgroundColor(Color.parseColor(colorcode1));
		layoutaddnew.setOnClickListener(onClickaddnew);

		
		if(tabvalue.equals("search")){
			layoutsearch.setBackgroundColor(Color.parseColor(colorcode));
		} else {
			layoutsearch.setBackgroundColor(Color.parseColor(colorcode1));
		}
		layoutsearch.setOnClickListener(onClicksearch);

		
		if(tabvalue.equals("expiring")){
			layoutexpired.setBackgroundColor(Color.parseColor(colorcode));			
		} else {
			layoutexpired.setBackgroundColor(Color.parseColor(colorcode1));
		}
		layoutexpired.setOnClickListener(onClickexpiring);

		

		Date d = new Date();
		currentdate = (new SimpleDateFormat("yyyy-MM-dd")).format(d);
		Log.d(LOG_TAG,"Check existing log date:"+currentdate);

		if(tabvalue.equals("normal") || tabvalue.equals("expiring")){

			layouteditsearch.setVisibility(View.GONE);

			Getwarrantylist Task = new Getwarrantylist();
			Task.execute();

		}else{


			layouteditsearch.setVisibility(View.VISIBLE);

			//*** text search change *******//

			editTextsearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_SEARCH) {
						GetSearch Task = new GetSearch();
						Task.execute(new String[] { currentTextFilter });
						return true;
					}
					return false;
				}


			});

			editTextsearch.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
					// When user changed the Text

				}

				@Override
				public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
						int arg3) {
					// TODO Auto-generated method stub
				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub   

					Log.i(LOG_TAG, "Typed=" + s.toString());
					GetSearch Task = new GetSearch();
					currentTextFilter = s.toString();
					Log.i(LOG_TAG, "currentTextFilter == " + currentTextFilter);
					Log.i(LOG_TAG, "currentTextFilterLength == " + currentTextFilter.length());

					if (currentTextFilter != null || currentTextFilter.length() > 0) {
						Task.execute(new String[] { currentTextFilter });
						listView.setVisibility(View.VISIBLE);
					}


					if(currentTextFilter.equals("")){
						listView.setVisibility(View.GONE);

					}

				}

			});

		}

	}


	private class Getwarrantylist extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Warranty_List.this, "", getResources().getString(R.string.please_wait));
		}
		@Override
		protected String doInBackground(String... arg0) {
			String response;


			try{

				if (tabvalue.equals("normal")) {
					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

					HttpPost HTTP_POST_WARRANTYLIST;
					String strURL = getResources().getString(R.string.WARRANTYLIST_API)
							+ "db=" + URLEncoder.encode(app_db, "UTF-8")
							+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
							+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
							+ "&cuid=" + URLEncoder.encode(cuid, "UTF-8");


					HTTP_POST_WARRANTYLIST = new HttpPost(strURL);
					Log.d(LOG_TAG,"strURL="+ strURL);

					ResponseHandler<String> responseHandler_WARRANTYLIST=new BasicResponseHandler();
					strAPI_WARRANTYLIST=httpclient.execute(HTTP_POST_WARRANTYLIST, responseHandler_WARRANTYLIST);	
					Log.d(LOG_TAG,"strAPI_WARRANTYLIST="+ strAPI_WARRANTYLIST);
					Log.d(LOG_TAG,"Insert into DB");

					warrantyJSONArray=new JSONArray(strAPI_WARRANTYLIST);
					Log.d(LOG_TAG,"warrantyJSONArray lenght :"+ warrantyJSONArray.length());

					Helper_Warrantylist.Delete();

					for (int n = 0; n < warrantyJSONArray.length(); n++) {

						String warrantylistid= warrantyJSONArray.getJSONObject(n).getString("id").toString();	
						String userid =warrantyJSONArray.getJSONObject(n).getString("user_id").toString();
						String customerid = warrantyJSONArray.getJSONObject(n).getString("customer_id").toString();
						String unregister_id= warrantyJSONArray.getJSONObject(n).getString("unregister_id").toString();
						String title= warrantyJSONArray.getJSONObject(n).getString("title").toString();
						String dealer= warrantyJSONArray.getJSONObject(n).getString("dealer").toString();
						String serial_number= warrantyJSONArray.getJSONObject(n).getString("serial_number").toString();
						String model_number= warrantyJSONArray.getJSONObject(n).getString("model_number").toString();
						String purchase_date= warrantyJSONArray.getJSONObject(n).getString("purchase_date").toString();
						String warranty_period= warrantyJSONArray.getJSONObject(n).getString("warranty_period").toString();
						String icon= warrantyJSONArray.getJSONObject(n).getString("icon").toString();
						String img_product= warrantyJSONArray.getJSONObject(n).getString("img_product").toString();
						String img_receipt= warrantyJSONArray.getJSONObject(n).getString("img_receipt").toString();
						String img_warranty= warrantyJSONArray.getJSONObject(n).getString("img_warranty").toString();
						String comments= warrantyJSONArray.getJSONObject(n).getString("comments").toString();
						String date_inserted= warrantyJSONArray.getJSONObject(n).getString("date_inserted").toString();
						String date_updated= warrantyJSONArray.getJSONObject(n).getString("date_updated").toString();
						String status= warrantyJSONArray.getJSONObject(n).getString("status").toString();
						String brand= warrantyJSONArray.getJSONObject(n).getString("brand").toString();
						String category= warrantyJSONArray.getJSONObject(n).getString("category").toString();


						Log.d(LOG_TAG,"Insert into DB " + n + ", Status: " +status); 
						Helper_Warrantylist.insert(warrantylistid,userid,customerid,unregister_id,title,dealer,serial_number,model_number,purchase_date,warranty_period,icon,img_product,img_receipt,img_warranty,comments,date_inserted,date_updated,status,brand,category,currentdate);
					}

				}

				response="SUCCESS";
				progressDialog.dismiss();

			} catch (Throwable t) { 
				response="FAILED";
				Log.e(LOG_TAG, "GetWarantyList-doInBackground-Error:" + t.getMessage(), t); 
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try{


				if (result.equals("SUCCESS")){


					adapter_warrantylist=new warrantylist_Adapter();
					adapter_warrantylist.clear();

					c=Helper_Warrantylist.getAll();

					if (c.getCount()>0){

						if (c.moveToLast() != false ){
							c.moveToFirst();
							do {

								String expirystatus = "";

								String purchase_date = Helper_Warrantylist.getpurchase_date(c).toString();

								if(purchase_date.length()>0 && !purchase_date.equals("null") && !purchase_date.equals("0000-00-00"))  {

									String warranty_period = Helper_Warrantylist.getwarranty_period(c).toString();							
									if(warranty_period.length()>0 && !warranty_period.equals("null") && !warranty_period.equals("0")){

										expired_date1 = addMonth(purchase_date,Integer.parseInt(warranty_period)); //get expired date
										new_date1 = minusMonth(expired_date1); //get new date
										now_date1 = parseDateToddMMyyyy(currentdate);

										expired_date = stringtodate(expired_date1);
										new_date = stringtodate( new_date1 );
										now_date = stringtodate( now_date1 );

										if(now_date.compareTo(expired_date)>0){
											expirystatus = "expired";
											//expired
										} else if(now_date.compareTo(new_date)>0){
											Log.d(LOG_TAG,"Status: Expiring"); 
											expirystatus = "expiring";
										}
									}

								} 

								if(tabvalue.equals("normal")||tabvalue.equals("search")||expirystatus.equals("expiring")){


									current_warrantylist=new Collection_Warrantylist();

									current_warrantylist.setid(Helper_Warrantylist.getwarrantylist_id(c).toString());
									current_warrantylist.setuser_id(Helper_Warrantylist.getuser_id(c).toString());
									current_warrantylist.setcustomer_id(Helper_Warrantylist.getcustomer_id(c).toString());
									current_warrantylist.setunregister_id(Helper_Warrantylist.getunregister_id(c).toString());
									current_warrantylist.settitle(Helper_Warrantylist.gettitle(c).toString());
									current_warrantylist.setdealer(Helper_Warrantylist.getdealer(c).toString());
									current_warrantylist.setserial_number(Helper_Warrantylist.getserial_number(c).toString());
									current_warrantylist.setmodel_number(Helper_Warrantylist.getmodel_number(c).toString());
									current_warrantylist.setpurchase_date(Helper_Warrantylist.getpurchase_date(c).toString());
									current_warrantylist.setwarranty_period(Helper_Warrantylist.getwarranty_period(c).toString());
									current_warrantylist.seticon(Helper_Warrantylist.geticon(c).toString());
									current_warrantylist.setimg_product(Helper_Warrantylist.getimg_product(c).toString());
									current_warrantylist.setimg_receipt(Helper_Warrantylist.getimg_receipt(c).toString());
									current_warrantylist.setimg_warranty(Helper_Warrantylist.getimg_warranty(c).toString());
									current_warrantylist.setcomments(Helper_Warrantylist.getcomments(c).toString());
									current_warrantylist.setbrand(Helper_Warrantylist.getbrand(c).toString());
									current_warrantylist.setcategory(Helper_Warrantylist.getcategory(c).toString());
									current_warrantylist.setstatus(Helper_Warrantylist.getstatus(c).toString());
									current_warrantylist.setexpirystatus(expirystatus);

									adapter_warrantylist.add(current_warrantylist);
								}

							}while (c.moveToNext());
						}

						listView.setAdapter(adapter_warrantylist);			
						listView.setOnItemClickListener(onListClickwarrantyList);	


					}else{
						Toast.makeText(Warranty_List.this,getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG).show();
					}

				}else{
					Toast.makeText(Warranty_List.this,getResources().getString(R.string.please_retry_again), Toast.LENGTH_LONG).show();
				}


				progressDialog.dismiss();


			}
			catch (Throwable t) {
				Log.e(LOG_TAG, "onloyalty-Error:" + t.getMessage(), t);
				progressDialog.dismiss();
			}
		}

	}


	class warrantylist_Adapter extends ArrayAdapter<Collection_Warrantylist> {
		warrantylist_Adapter() {
			super(Warranty_List.this, R.layout.warranty_list_row, model_warrantylist);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View row=convertView;
			NotificationHolderhome holder=null;

			if (row==null) {													
				LayoutInflater inflater=getLayoutInflater();

				row=inflater.inflate(R.layout.warranty_list_row, parent, false);
				holder=new NotificationHolderhome(row);
				row.setTag(holder);
			}
			else {
				holder=(NotificationHolderhome)row.getTag();
			}

			Log.d(LOG_TAG,"warrantylist_Adapter: populateFromhome"); 
			holder.populateFromhome(model_warrantylist.get(position));

			return(row);
		}
	}


	class NotificationHolderhome {
		@SuppressWarnings("unused")
		private View row=null;
		private TextView texttitle=null;
		private TextView purchase_date=null;
		private TextView dealer=null;
		private ImageView image_list =null;
		private LinearLayout spinner = null;
		private TextView textexpired = null;
		private TextView textexpiring =null;
		private TextView textverified = null;

		NotificationHolderhome(View row) {
			this.row=row;	

			texttitle=(TextView) row.findViewById(R.id.texttitle);
			purchase_date=(TextView) row.findViewById(R.id.purchase_date);

			dealer=(TextView) row.findViewById(R.id.dealer);
			image_list = (ImageView) row.findViewById(R.id.image_list);
			spinner = (LinearLayout) row.findViewById(R.id.progressbar);
			textexpired=(TextView) row.findViewById(R.id.textexpired);
			textexpiring=(TextView) row.findViewById(R.id.textexpiring);
			textverified=(TextView) row.findViewById(R.id.textverified);

		}

		void populateFromhome(Collection_Warrantylist r) {
			//SET NON-REQUIRED DATA TO BE GONE FIRST 
			image_list.setVisibility(View.GONE);
			dealer.setVisibility(View.GONE);
			textexpiring.setVisibility(View.GONE);
			textexpired.setVisibility(View.GONE);
			
			String purchasedate = r.getpurchase_date();
			String expirystatus =  r.getexpirystatus();

			if(!r.gettitle().equals("null")&&!r.gettitle().isEmpty()){
				Log.d(LOG_TAG,"Title: " + r.gettitle()); 
				texttitle.setText(r.gettitle());
			}

			if(!r.getdealer().equals("null")&&!r.getdealer().isEmpty()){
				Log.d(LOG_TAG,"Dealer: " + r.getdealer()); 
				dealer.setText(r.getdealer());
				dealer.setVisibility(View.VISIBLE);
			}


			if(purchasedate.length()>0 && !purchasedate.equals("null") && !purchasedate.equals("0000-00-00"))  {

				purchase_date.setText(parseDateToddMMyyyy(r.getpurchase_date()));
				Log.d(LOG_TAG,"Purchase Date: " + parseDateToddMMyyyy(r.getpurchase_date())); 

				if(expirystatus.equals("expired")){
					Log.d(LOG_TAG,"Status: Expired"); 
					textexpiring.setVisibility(View.GONE);
					textexpired.setVisibility(View.VISIBLE);

				} else if(expirystatus.equals("expiring")){
					Log.d(LOG_TAG,"Status: Expiring"); 
					textexpired.setVisibility(View.GONE);
					textexpiring.setVisibility(View.VISIBLE);
				}
			} 

			if(r.getstatus().equals("2")){
				textverified.setVisibility(View.VISIBLE);
				Log.d(LOG_TAG,"Status: Verified"); 

			}else{
				textverified.setVisibility(View.GONE);
			}


			if (r.geticon().length()>10) {
				image_list.setVisibility(View.VISIBLE);
				imageLoader.displayImage(r.geticon(), image_list, displayOptions, new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {
						spinner.setVisibility(View.VISIBLE);
					}
					@Override
					public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
						spinner.setVisibility(View.GONE);

					}
					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
						spinner.setVisibility(View.GONE);
					}
					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						spinner.setVisibility(View.GONE);
					}

				});
			}

		}


	}

	private AdapterView.OnItemClickListener onListClickwarrantyList=new AdapterView.OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent,
				View view, int position,
				long id) {
			current_warrantylist=model_warrantylist.get(position);	
			Log.d(LOG_TAG,"onListClick table name="+current_warrantylist.gettitle());

			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
			Intent i=new Intent(Warranty_List.this, Warranty_Content_Detail.class);
			i.putExtra("prodName", current_warrantylist.gettitle());
			i.putExtra("storeName", current_warrantylist.getdealer());
			i.putExtra("purchaseDate", current_warrantylist.getpurchase_date());
			i.putExtra("expiryDate", expired_date1);
			i.putExtra("serialNo", current_warrantylist.getserial_number());
			i.putExtra("modelsku", current_warrantylist.getmodel_number());
			i.putExtra("proPicture", current_warrantylist.getimg_product());
			i.putExtra("receiptPicture", current_warrantylist.getimg_receipt());
			i.putExtra("warrantyPicture", current_warrantylist.getimg_warranty());
			startActivity(i);

		}
	};


	/****************************search function*******************************/



	private class GetSearch extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			String response;
			try {
				//String responseBody = null;
				key = arg0[0];
				Log.d(LOG_TAG, "KEY =" + key);
				response = "SUCCESS";

			} catch (Throwable t) {
				response = "FAILED";
				Log.e(LOG_TAG,
						"GetSearch-doInBackground-Error:"
								+ t.getMessage(), t);
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {


				adapter_warrantylist=new warrantylist_Adapter();
				adapter_warrantylist.clear();
				int num = 0;
				if (result.equals("SUCCESS")) {

					Cursor c=Helper_Warrantylist.getSearchByName(key);
					int count_ = c.getCount();
					Log.d(LOG_TAG, "count_product="+ count_);

					if(count_ == 0){

						Toast.makeText(Warranty_List.this,getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG).show();
					}
					else{
						Log.d(LOG_TAG, "count_Product_Mode="+ count_);
						if (c.moveToLast() != false ){
							//Get from DB
							c.moveToFirst();
							do {
								num++;
								Log.d(LOG_TAG, String.valueOf(num));
								current_warrantylist=new Collection_Warrantylist();

								current_warrantylist.setid(Helper_Warrantylist.getwarrantylist_id(c).toString());
								current_warrantylist.setuser_id(Helper_Warrantylist.getuser_id(c).toString());
								current_warrantylist.setcustomer_id(Helper_Warrantylist.getcustomer_id(c).toString());
								current_warrantylist.setunregister_id(Helper_Warrantylist.getunregister_id(c).toString());
								current_warrantylist.settitle(Helper_Warrantylist.gettitle(c).toString());
								current_warrantylist.setdealer(Helper_Warrantylist.getdealer(c).toString());
								current_warrantylist.setserial_number(Helper_Warrantylist.getserial_number(c).toString());
								current_warrantylist.setmodel_number(Helper_Warrantylist.getmodel_number(c).toString());
								current_warrantylist.setpurchase_date(Helper_Warrantylist.getpurchase_date(c).toString());
								current_warrantylist.setwarranty_period(Helper_Warrantylist.getwarranty_period(c).toString());
								current_warrantylist.seticon(Helper_Warrantylist.geticon(c).toString());
								current_warrantylist.setimg_product(Helper_Warrantylist.getimg_product(c).toString());
								current_warrantylist.setimg_receipt(Helper_Warrantylist.getimg_receipt(c).toString());
								current_warrantylist.setimg_warranty(Helper_Warrantylist.getimg_warranty(c).toString());
								current_warrantylist.setcomments(Helper_Warrantylist.getcomments(c).toString());
								current_warrantylist.setbrand(Helper_Warrantylist.getbrand(c).toString());
								current_warrantylist.setcategory(Helper_Warrantylist.getcategory(c).toString());
								current_warrantylist.setstatus(Helper_Warrantylist.getstatus(c).toString());

								adapter_warrantylist.add(current_warrantylist);

							} while (c.moveToNext());


						}
					}

				} else {
					Toast.makeText(Warranty_List.this,"No product found.",Toast.LENGTH_SHORT).show();
				}
				listView.setAdapter(adapter_warrantylist);
				listView.setOnItemClickListener(onListClickwarrantyList);	

			} catch (Throwable t) {
				Log.e(LOG_TAG,"GetSalesOrder-onPostExecute-Error:" + t.getMessage(),t);

			}
		}
	}

	/*************************search function**********************************/





	public static String minusMonth(String time)  //function calculate expired date
	{
		String inputPattern = "dd-MM-yyyy";
		String outputPattern = "dd-MM-yyyy";
		SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
		SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

		Date date = null;
		String str = null;

		try {
			date = inputFormat.parse(time);

		} catch (ParseException e) {
			e.printStackTrace();
		}


		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1); //minus number would decrement the days
		str = outputFormat.format(cal.getTime());

		return str;
	}



	public static String addMonth(String time, int month)  //function calculate expired date
	{
		String inputPattern = "yyyy-MM-dd";
		String outputPattern = "dd-MM-yyyy";
		SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
		SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

		Date date = null;
		String str = null;

		try {
			date = inputFormat.parse(time);

		} catch (ParseException e) {
			e.printStackTrace();
		}


		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, month); //minus number would decrement the days
		str = outputFormat.format(cal.getTime());

		return str;
	}


	public static String parseDateToddMMyyyy(String time) {  // change date format
		String inputPattern = "yyyy-MM-dd";
		String outputPattern = "dd-MM-yyyy";
		SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
		SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

		Date date = null;
		String str = null;

		try {
			date = inputFormat.parse(time);
			str = outputFormat.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return str;
	}

	public static Date stringtodate(String time) {  // change date string to date
		String inputPattern = "dd-MM-yyyy";

		SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);


		Date date = null;


		try {

			date = inputFormat.parse(time);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static DisplayImageOptions getDisplayImageOptions()
	{
		return new DisplayImageOptions.Builder()

		.cacheInMemory(true)
		.cacheOnDisc(true)


		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();


	}


	/***  addnew button onclick ****/
	private View.OnClickListener onClickaddnew=new View.OnClickListener() {
		public void onClick(View v) {
			try{

				Intent i=new Intent(Warranty_List.this, Warranty_Add.class);
				startActivity(i);

			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onBackButton-Error:" + t.getMessage(), t); 

			}
		}

	};


	/***  addnew button onclick ****/
	private View.OnClickListener onClickexpiring=new View.OnClickListener() {
		public void onClick(View v) {
			try{

				Intent i=new Intent(Warranty_List.this, Warranty_List.class);
				i.putExtra("tabvalue", "expiring");
				startActivity(i);

			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onBackButton-Error:" + t.getMessage(), t); 

			}
		}

	};


	/***  search button onclick ****/
	private View.OnClickListener onClicksearch=new View.OnClickListener() {
		public void onClick(View v) {
			try{

				Intent i=new Intent(Warranty_List.this, Warranty_List.class);
				i.putExtra("tabvalue", "search");
				startActivity(i);

			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onBackButton-Error:" + t.getMessage(), t); 

			}
		}

	};

}
