package com.m3tech.warranty;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.header.Header;
import com.m3tech.widget.Warranty_Add_Widget;

public class Warranty_Add extends Warranty_Add_Widget implements OnClickListener{

	private static final String LOG_TAG = "WARRANTYADD";
	private Uri fileUri;
	private Calendar cal;
	private int day;
	private int month;
	private int year;
	StringBuffer responseBuffer = new StringBuffer(); 
	static ProgressDialog progressDialog=null;
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;
	private static final String IMAGE_DIRECTORY_NAME = "Camera";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		String pageTitle = settings.getString("warrantyPageTitle", "");
		if(pageTitle ==null||pageTitle.equals("")){
			pageTitle = app_title;
		}

		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,img_home,texttitlebar, pageTitle,"");
	
		layouttitleheader.setBackgroundColor(Color.parseColor(colorcode1));
		btnsubmit.setOnClickListener(onclicksubmit);
		editdate.setKeyListener(null);
		
		btnuploadimage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// capture picture
				captureImage();
			}
		});

		cal = Calendar.getInstance();
		day = cal.get(Calendar.DAY_OF_MONTH);
		month = cal.get(Calendar.MONTH);
		year = cal.get(Calendar.YEAR);

		// set current date into textview
		editdate.setText(new StringBuilder()
			// Month is 0 based, just add 1
			.append(day).append("-").append(month + 1).append("-")
			.append(year).append(" "));

		editdate.setOnClickListener(this);

		// External sdcard location
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
			}
		}

		uploadFilePath = mediaStorageDir.getPath() + File.separator ;
		Log.w(LOG_TAG, "getOutputMediaFile: " + uploadFilePath);

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddkkmm");
		String todayDate = df.format(cal.getTime());

		imgFilename = imgFilename + todayDate + ".jpg";

	}


	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		showDialog(0);
	}

	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id) {
		return new DatePickerDialog(this, datePickerListener, year, month, day);
	}
	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			editdate.setText(selectedDay + "-" + (selectedMonth + 1) + "-"
					+ selectedYear);
		}
	};

	//************************************ SETUP FOR IMAGE INPUT ******************************//

	/**
	 * Checking device has camera hardware or not
	 * */
	@SuppressWarnings("unused")
	private boolean isDeviceSupportCamera() {
		if (getApplicationContext().getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}

	/*
	 * Capturing Camera Image will lauch camera app requrest image capture
	 */
	private void captureImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
	}

	/*
	 * Here we store the file url as it will be null after returning from camera
	 * app
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// save file url in bundle as it will be null on scren orientation
		// changes
		outState.putParcelable("file_uri", fileUri);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		// get the file url
		fileUri = savedInstanceState.getParcelable("file_uri");
	}



	/**
	 * Receiving activity result method will be called after closing the camera
	 * */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// successfully captured the image
				// resize first
				resizeImg ();
				// display it in image view
				previewCapturedImage();
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				//Toast.makeText(getApplicationContext(),
				//		"User cancelled image capture", Toast.LENGTH_SHORT)
				//		.show();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		} 
	}

	/*
	 * Display image from a path to ImageView
	 */
	private void previewCapturedImage() {
		try {
			// hide video preview
			//videoPreview.setVisibility(View.GONE);

			imgPreview.setVisibility(View.VISIBLE);

			// bimatp factory
			BitmapFactory.Options options = new BitmapFactory.Options();

			// downsizing image as it throws OutOfMemory Exception for larger
			// images
			options.inSampleSize = 8;
			final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);

			imgPreview.setImageBitmap(bitmap);
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}



	/**
	 * ------------ Helper Methods ---------------------- 
	 * */

	/*
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/*
	 * returning image / video
	 */
	private File getOutputMediaFile(int type) {

		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(uploadFilePath + imgFilename);
			Log.w(LOG_TAG, "getOutputMediaFile: " + mediaFile);

		} else {
			return null;
		}

		return mediaFile;
	}

	private void resizeImg(){
		int maxWidth = 800;
		int maxHeight = 800;

		Log.d(LOG_TAG, "resizeImg-Start");
		String path = uploadFilePath + imgFilename;
		Log.d(LOG_TAG, "path: "+path);

		// create the options
		BitmapFactory.Options opts = new BitmapFactory.Options();

		//just decode the file
		opts.inJustDecodeBounds = true;
		Bitmap bp = BitmapFactory.decodeFile(path, opts);

		//get the original size
		int orignalHeight = opts.outHeight;
		int orignalWidth = opts.outWidth;

		Log.d(LOG_TAG, "orignalHeight: "+orignalHeight);
		Log.d(LOG_TAG, "orignalWidth: "+orignalWidth);

		//initialization of the scale
		int resizeScale = 1;

		//get the good scale
		if ( orignalWidth > maxWidth || orignalHeight > maxHeight ) {
			final int heightRatio = Math.round((float) orignalHeight / (float) maxHeight);
			final int widthRatio = Math.round((float) orignalWidth / (float) maxWidth);
			resizeScale = heightRatio < widthRatio ? heightRatio : widthRatio;
			resizeScale += 1;
		}
		Log.d(LOG_TAG, "resizeScale: "+resizeScale);

		//put the scale instruction (1 -> scale to (1/1); 8-> scale to 1/8)
		opts.inSampleSize = resizeScale;
		opts.inJustDecodeBounds = false;

		bp = BitmapFactory.decodeFile(path, opts);

		FileOutputStream out=null;
		try {
			out = new FileOutputStream(path);
			bp.compress(Bitmap.CompressFormat.JPEG, 80, out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try{
				Log.d(LOG_TAG, "resizeImg-Success");
				out.close();
			} 
			catch(Throwable t) {
				Log.e(LOG_TAG, "resizeImg-Error:" + t.getMessage(), t);

			}
		}
	}


	//************************************ END SETUP FOR IMAGE INPUT ******************************//

	/***  back button onclick ****/
	private View.OnClickListener onclicksubmit=new View.OnClickListener() {
		public void onClick(View v) {
			try{

				prod_name = editproductName.getText().toString();
				Log.d(LOG_TAG,"prod_name="+prod_name);
				prod_date = editdate.getText().toString();
				Log.d(LOG_TAG,"prod_date="+prod_date);

				Log.d(LOG_TAG,"uploadFilePath="+uploadFilePath);

				if(prod_name.equals(""))  {
					editproductName.setError("Product Name is required!");

				}else{

					if (prod_date.equals("")) { // check email
						editdate.setError("Purchase date is required!");

					} else {
						File imgFile = new File(uploadFilePath + "" + imgFilename);

						if(imgFile.exists()) {   
							uploadFile(uploadFilePath + "" + imgFilename);
						} else {
							btnuploadimage.setError("Please upload your product receipt.");
						}
					}

				}

			} catch (Throwable t) { 
				Log.e(LOG_TAG, "onBackButton-Error:" + t.getMessage(), t); 

			}
		}

	};

	/******************upload file *************************/

	public int uploadFile(String sourceFileUri) {

		progressDialog = ProgressDialog.show(this, "", "Please Wait..", true,false); 
		String fileName = sourceFileUri;
		Log.d(LOG_TAG, "fileName="+fileName);
		HttpURLConnection conn = null;
		DataOutputStream dos = null; 
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(sourceFileUri);
		Log.d(LOG_TAG, "sourceFile="+sourceFile);

		try {
			String strURL = getResources().getString(R.string.WARRANTYPROCESS_API)
					+ "db=" + URLEncoder.encode(app_db, "UTF-8")
					+ "&userid=" + URLEncoder.encode(app_user, "UTF-8")
					+ "&udid=" + URLEncoder.encode(udid, "UTF-8")
					+ "&cuid=" + URLEncoder.encode(cuid, "UTF-8")
					+ "&task=" + URLEncoder.encode("step1", "UTF-8")
					+ "&title=" + URLEncoder.encode(prod_name, "UTF-8")
					+ "&purchase_date=" + URLEncoder.encode(prod_date, "UTF-8");

			URL url = new URL(strURL);

			// Open a HTTP  connection to  the URL
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true); // Allow Inputs
			conn.setDoOutput(true); // Allow Outputs
			conn.setUseCaches(false); // Don't use a Cached Copy
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

			File imgFile = new File(sourceFileUri);

			if(imgFile.exists()) {   

				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(sourceFile);

				conn.setRequestProperty("uploaded_file", fileName);
				dos = new DataOutputStream(conn.getOutputStream());
				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
						+ fileName + "\"" + lineEnd);
				dos.writeBytes(lineEnd);

				// create a buffer of  maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize); 

				while (bytesRead > 0) {
					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);  

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				//close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			}

			// Responses from the server (code and message)
			serverResponseCode = conn.getResponseCode();
			String serverResponseMessage = conn.getResponseMessage();

			Log.i("uploadFile", "HTTP Response is : "
					+ serverResponseMessage + ": " + serverResponseCode);

			//Get Response 

			InputStream inputStream = conn.getInputStream();

			BufferedReader bufferReader = new BufferedReader(new InputStreamReader(inputStream));

			String line;

			while((line = bufferReader.readLine()) != null) {
				responseBuffer.append(line);
				responseBuffer.append('\r');
			}

			bufferReader.close();
			responbody = responseBuffer.toString();
			Log.d(LOG_TAG, "responbody="+responbody);

			if(serverResponseCode == 200){

				runOnUiThread(new Runnable() {
					public void run() {

						progressDialog.dismiss();

						try {
							warrantyJSONArray = new JSONArray(responbody);
							String status = warrantyJSONArray.getJSONObject(0).getString("status").toString();
							String desc = warrantyJSONArray.getJSONObject(0).getString("desc").toString();
							String warrantyID = warrantyJSONArray.getJSONObject(0).getString("warranty_id").toString();
							Log.d(LOG_TAG, warrantyID+"|"+desc);
							if(status.equals("1")){

								finish();

								Intent r = new Intent(Warranty_Add.this, Warranty_More_Detail.class);
								r.putExtra("warrantyID",warrantyID);

								startActivity(r);
							}

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				});               
			}   


		} catch (MalformedURLException ex) {


			ex.printStackTrace();

			runOnUiThread(new Runnable() {
				public void run() {

					progressDialog.dismiss();
					//Toast.makeText(Warrantyadd.this, "MalformedURLException", Toast.LENGTH_SHORT).show();
				}
			});

			Log.e("Upload file to server", "error: " + ex.getMessage(), ex); 

		} catch (Exception e) {

			e.printStackTrace();
			runOnUiThread(new Runnable() {
				public void run() {

					progressDialog.dismiss();

					//Toast.makeText(Warrantyadd.this, "Got Exception : see logcat ", Toast.LENGTH_SHORT).show();
				}
			});
			Log.e("Upload file to server Exception", "Exception : "
					+ e.getMessage(), e); 
		}

		return serverResponseCode;

		//} // End else block
	} 

	/*******************end upload file*********************/

	public void onBackPressed() {

		super.onBackPressed(); 
		finish();
	}

}
