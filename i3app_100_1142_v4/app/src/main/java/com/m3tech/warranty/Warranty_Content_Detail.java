package com.m3tech.warranty;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.m3tech.app_100_1474.R;
import com.m3tech.header.Header;
import com.m3tech.widget.Warranty_Content_Detail_Widget;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class Warranty_Content_Detail extends Warranty_Content_Detail_Widget {

	private static final String LOG_TAG = "WARRANTYCONTENTDETAIL";


	static ImageLoader imageLoader = ImageLoader.getInstance();
	static DisplayImageOptions displayOptions = getDisplayImageOptions();

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(LOG_TAG,"ENTERING WARRANTY DETAILS ");
		

		String pageTitle = settings.getString("warrantyPageTitle", "");
		if(pageTitle ==null||pageTitle.equals("")){
			pageTitle = app_title;
		}

		Intent j = getIntent();
		prodName = j.getStringExtra("prodName");
		storeName = j.getStringExtra("storeName");
		purchaseDate = j.getStringExtra("purchaseDate");
		expiryDate = j.getStringExtra("expiryDate");
		serialNo = j.getStringExtra("serialNo");
		modelsku = j.getStringExtra("modelsku");
		proPicture = j.getStringExtra("proPicture");
		receiptPicture = j.getStringExtra("receiptPicture");
		warrantyPicture = j.getStringExtra("warrantyPicture");

		if(expiryDate==null){
			expiryDate ="";
		}

		System.out.println("prodName="+prodName);
		System.out.println("storeName="+storeName);
		System.out.println("purchaseDate="+purchaseDate);
		System.out.println("expiryDate="+expiryDate);
		System.out.println("serialNo="+serialNo);
		System.out.println("modelsku="+modelsku);
		System.out.println("proPicture="+proPicture);
		System.out.println("receiptPicture="+receiptPicture);
		System.out.println("warrantyPicture="+warrantyPicture);

		
		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome,img_back,img_home, texttitlebar, pageTitle,"");

		
		layouttitleheader.setBackgroundColor(Color.parseColor(colorcode1));

		
		textheader.setText(prodName);

		

		if(!prodName.isEmpty() ||  !storeName.isEmpty() || !purchaseDate.isEmpty() || !expiryDate.isEmpty() || !serialNo.isEmpty() || !modelsku.isEmpty()){

			layout_ketiga.setVisibility(View.VISIBLE);

			if(!prodName.isEmpty() && !prodName.equals("null")){  //*** product name

				layout_productname.setVisibility(View.VISIBLE);
				TextView productname = (TextView) findViewById(R.id.productname);
				productname.setText(prodName);

				if(storeName.length() < 1  && purchaseDate.length() < 1 && expiryDate.length()< 1 && serialNo.length()< 1 && modelsku.length()< 1){

					ImageView lineproductname = (ImageView) findViewById(R.id.lineproductname);
					lineproductname.setVisibility(View.GONE);
				}
			}

			if(!storeName.isEmpty() && !storeName.equals("null")){  //*** store name

				layout_storename.setVisibility(View.VISIBLE);
				TextView storename = (TextView) findViewById(R.id.storename);
				storename.setText(storeName);

				if(purchaseDate.length() < 1 && expiryDate.length()< 1 && serialNo.length()< 1 && modelsku.length()< 1){
					ImageView linestorename = (ImageView) findViewById(R.id.linestorename);
					linestorename.setVisibility(View.GONE);
				}
			}

			if(!purchaseDate.isEmpty() && !purchaseDate.equals("null")){  //*** purchase date

				layout_purcahsedate.setVisibility(View.VISIBLE);
				TextView purchasedate = (TextView) findViewById(R.id.purchasedate);
				purchasedate.setText(parseDateToddMMyyyy(purchaseDate));

				if(expiryDate.length()< 1 && serialNo.length()< 1 && modelsku.length()< 1){

					ImageView linepurchasedate = (ImageView) findViewById(R.id.linepurchasedate);
					linepurchasedate.setVisibility(View.GONE);
				}
			}


			if(!expiryDate.isEmpty() && !expiryDate.equals("null")){  //*** expiry date

				layout_expirydate.setVisibility(View.VISIBLE);
				TextView expirydate = (TextView) findViewById(R.id.expirydate);
				expirydate.setText(expiryDate);
				expirydate.setTextColor(Color.parseColor("#34B57F"));

				if(serialNo.length()< 1 && modelsku.length()< 1){

					ImageView lineexpirydate = (ImageView) findViewById(R.id.lineexpirydate);
					lineexpirydate.setVisibility(View.GONE);
				}
			}


			if(!serialNo.isEmpty() && !serialNo.equals("null")){  //*** serial no

				layout_serialno.setVisibility(View.VISIBLE);
				TextView serialno = (TextView) findViewById(R.id.serialno);
				serialno.setText(serialNo);

				if( modelsku.length()< 1){

					ImageView lineserialno = (ImageView) findViewById(R.id.lineserialno);
					lineserialno.setVisibility(View.GONE);
				}
			}

			if(!modelsku.isEmpty() && !modelsku.equals("null")){  //*** model/sku 
				layout_modelsku.setVisibility(View.VISIBLE);
				TextView textmodelsku = (TextView) findViewById(R.id.textmodelsku);
				textmodelsku.setText(modelsku);
			}


			if(!proPicture.isEmpty() && !proPicture.equals("null")){  //*** product picture

				layoutproductimage.setVisibility(View.VISIBLE);
				imageLoader.displayImage(proPicture, image_url, displayOptions, new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {
						spinner.setVisibility(View.VISIBLE);
					}
					@Override
					public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
						spinner.setVisibility(View.GONE);

					}
					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
						spinner.setVisibility(View.GONE);
					}
					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						spinner.setVisibility(View.GONE);
					}

				});


			}


			if(!receiptPicture.isEmpty() && !receiptPicture.equals("null")){  //*** receipt picture

				layoutreceiptimage.setVisibility(View.VISIBLE);
				imageLoader.displayImage(receiptPicture, image_receipt, displayOptions, new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {
						spinner2.setVisibility(View.VISIBLE);
					}
					@Override
					public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
						spinner2.setVisibility(View.GONE);

					}
					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
						spinner2.setVisibility(View.GONE);
					}
					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						spinner2.setVisibility(View.GONE);
					}

				});

			}

			if(!warrantyPicture.isEmpty() && !warrantyPicture.equals("null")){  //*** receipt picture

				layoutwarrantyimage.setVisibility(View.VISIBLE);
				imageLoader.displayImage(warrantyPicture, image_warranty, displayOptions, new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri, View view) {
						spinner3.setVisibility(View.VISIBLE);
					}
					@Override
					public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
						spinner3.setVisibility(View.GONE);

					}
					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
						spinner3.setVisibility(View.GONE);
					}
					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						spinner3.setVisibility(View.GONE);
					}

				});

			}
		}
	}


	public static String parseDateToddMMyyyy(String time) {  // change date format
		String inputPattern = "yyyy-MM-dd";
		String outputPattern = "dd-MM-yyyy";
		SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
		SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

		Date date = null;
		String str = null;

		try {
			date = inputFormat.parse(time);
			str = outputFormat.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return str;
	}

	@SuppressWarnings("deprecation")
	public static DisplayImageOptions getDisplayImageOptions()
	{
		return new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}
}
