package com.m3tech.net;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.m3tech.app_100_1474.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HttpsRequestSirimResult {

	private final static String LOG_TAG = "HTTP-REQUEST";

	public static String toHttpPostAPI(Context c, String code) {

		SSL ssl = new SSL(c);
		String result = "";
		Uri.Builder builder = new Uri.Builder();
		builder.scheme(c.getResources().getString(R.string.HTTPS))
				.authority(c.getResources().getString(R.string.HOSTING))
				.appendQueryParameter("l", code);
		//String myUrl = builder.build().toString();
		String myUrl = "https://sqasiapps.sirim.my/label?l="+code;
		//Toast.makeText(c, myUrl, Toast.LENGTH_LONG).show();
		Log.d(LOG_TAG, "HttpsRequestSirimResult URL = " + myUrl);
		HttpPost httppost = new HttpPost(myUrl);

		try {

			HttpClient httpClient = ssl.getNewHttpClient();
			HttpResponse response = httpClient.execute(httppost);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			String line = null;
			while ((line = reader.readLine()) != null) {
				result += line + "\n";
			}
			Log.d(LOG_TAG, "HttpsRequestSirimResult (result) = " + result);

			return result;
		} catch (ClientProtocolException e) {
			return "ERROR";
		} catch (IOException e) {
			return "ERROR";
		}
	}
}
