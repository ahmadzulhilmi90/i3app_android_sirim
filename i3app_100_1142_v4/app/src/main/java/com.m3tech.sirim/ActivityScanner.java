package com.m3tech.sirim;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.SurfaceView;

import com.google.zxing.Result;
import com.m3tech.app_100_1474.R;
import com.m3tech.scanner.IScanResult;
import com.m3tech.scanner.QRCodeScannerImpl;
import com.m3tech.scanner.scanview.ScanView;

import java.io.IOException;



public class ActivityScanner extends Activity implements IScanResult {
    private ScanView mScanView;
    private SurfaceView mSurfaceView;
    private QRCodeScannerImpl mQRCodeScanner;
    public static final String PREFS_NAME = "MyPrefsFile"; 
    SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        settings = this.getSharedPreferences(PREFS_NAME, 0);
        initView();

        try {
            mQRCodeScanner = new QRCodeScannerImpl(mSurfaceView, mScanView, this);
        } catch (IOException e) {
            finish();
        }

    }

    private void initView() {
        mSurfaceView = (SurfaceView) findViewById(R.id.surface);
        mScanView = (ScanView) findViewById(R.id.viewfinder);
    }

    @Override
    public void onResume() {
        super.onResume();
        mQRCodeScanner.startScan();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mQRCodeScanner.quit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onScanResult(Result result, Bitmap bitmap) {
        final String code = result.getText().trim();
        if (!TextUtils.isEmpty(code)) {
            //Toast.makeText(this, "QRCode is " + code, Toast.LENGTH_LONG).show();
            
        	String key = String.valueOf(code);//.replaceAll(" ", "+");
            SharedPreferences.Editor editor = settings.edit();
			editor.remove("result_scanner_qrcode");
			editor.remove("result_scanner_qrcode_status");
			editor.putString("result_scanner_qrcode",key);
			editor.putString("result_scanner_qrcode_status","1");
			editor.commit();
            
            mQRCodeScanner.quit();
            finish();
        }

    }

}
