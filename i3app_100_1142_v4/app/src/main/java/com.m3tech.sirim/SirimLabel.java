package com.m3tech.sirim;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.m3tech.app_100_1474.R;
import com.m3tech.header.Header;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;

@SuppressLint("SetJavaScriptEnabled")
public class SirimLabel extends Activity {
	private static final int PERMISSION_REQUEST_CODE = 200;
	public static final String PREFS_NAME = "MyPrefsFile";
	private static final String LOG_TAG = "SirimLabel";
	protected ImageView tabheader, img_back, img_home;
	TextView txt_product_status2,txt_product_status3,txt_product_status4,texttitlebar,txt_error,txt_serialno,txt_product_details,txt_product_status,txt_this_label_assigned_to,txt_unconfirm_product;
	protected LinearLayout tabback, tabhome, tabbottom, btnopen,lay_details;
	static ProgressDialog progressDialog;
	protected EditText editTextserialno;
	Button btn_scan_qrcode,btn_search;
	LinearLayout layout_qrscan;

	String pagetitle, app_title, responbody = "";
	Context context = this;
	Activity activity = this;
	protected Header Header = null;
	SharedPreferences settings;
	String resultScanner = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		if(android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}

		super.onCreate(savedInstanceState);
		setContentView(R.layout.plugin_sirim_template1);

		tabheader = (ImageView) findViewById(R.id.tabheader);
		texttitlebar = (TextView) findViewById(R.id.texttitlebar);
		tabback = (LinearLayout) findViewById(R.id.tabback);
		tabhome = (LinearLayout) findViewById(R.id.tabhome);
		img_back = (ImageView) findViewById(R.id.back);
		img_home = (ImageView) findViewById(R.id.home);
		editTextserialno = (EditText) findViewById(R.id.editTextserialno);
		txt_error = (TextView) findViewById(R.id.txt_error);
		txt_serialno = (TextView) findViewById(R.id.txt_serialno);
		txt_product_details = (TextView) findViewById(R.id.txt_product_details);
		lay_details = (LinearLayout) findViewById(R.id.lay_details);
		txt_product_status = (TextView) findViewById(R.id.txt_product_status);
		txt_product_status2 = (TextView) findViewById(R.id.txt_product_status2);
		txt_product_status3 = (TextView) findViewById(R.id.txt_product_status3);
		txt_product_status4 = (TextView) findViewById(R.id.txt_product_status4);
		txt_this_label_assigned_to = (TextView) findViewById(R.id.txt_this_label_assigned_to);
		txt_unconfirm_product = (TextView) findViewById(R.id.txt_unconfirm_product);
		btn_scan_qrcode = (Button) findViewById(R.id.btn_scan_qrcode);
		layout_qrscan = (LinearLayout) findViewById(R.id.layout_qrscan);
		btn_search = (Button) findViewById(R.id.btn_search);

		settings = this.getSharedPreferences(PREFS_NAME, 0);
		
		Intent i = getIntent();
		pagetitle = i.getStringExtra("pagetitle");
		Log.i(LOG_TAG, "pagetitle: " + pagetitle);

		if (pagetitle.length() == 0) {
			pagetitle = app_title;
		}

		/*** Setting Header ***/
		Header = new Header(context, tabheader, tabback, tabhome, img_back,
				img_home, texttitlebar, pagetitle, "");
		
		lay_details.setVisibility(View.GONE);
		
		
		String txt_phone = txt_product_status2.getText().toString();
		if(txt_phone.length()>0){
			txt_product_status2.setOnClickListener(new View.OnClickListener() {

		        @Override
		        public void onClick(View arg0) {
		            // TODO Auto-generated method stub
		            String phone_no= txt_product_status2.getText().toString().replaceAll("-", "");
		            /*Intent callIntent = new Intent(Intent.ACTION_CALL);
		            callIntent.setData(Uri.parse("tel:"+phone_no));
		            startActivity(callIntent);*/
		            
		            if(isPermissionGranted()){
		                call_action(phone_no);
		            }
		        }
		    });
		}
		
		final String txt_email = txt_product_status4.getText().toString();
		if(txt_email.length()>0){
			
			txt_product_status4.setOnClickListener(new View.OnClickListener() {

		        @Override
		        public void onClick(View arg0) {
		            // TODO Auto-generated method stub
		            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
				            "mailto",txt_email, null));
		    		intent.putExtra(Intent.EXTRA_SUBJECT, "SIRIM SERIAL NUMBER");
		    		intent.putExtra(Intent.EXTRA_TEXT, "");
		    		startActivity(Intent.createChooser(intent, "Choose an Email client :"));
		        }
		    });
		
		}
		
		// for testing only
		//GET_SUBMIT_VB ex = new GET_SUBMIT_VB();
        //ex.execute(new String[] {"+nMscirDRBrDU/XJMlzPcQ=="});
		
		
		/*String result_scanner_qrcode = "https://sqasiapps.sirim.my/index.html?+nMscirDRBrDU/XJMlzPcQ==";
		result_scanner_qrcode = result_scanner_qrcode.replaceAll("\\?", "707");
		Log.d(LOG_TAG, "result_scanner_qrcode = " + result_scanner_qrcode);
		String[] arr = result_scanner_qrcode.split("707");
		String encrypt_serial_num = arr[1];
		
		if(encrypt_serial_num.length() > 0){
		
			Log.d(LOG_TAG, "final code = " + encrypt_serial_num);
			//run execute
    		GET_SUBMIT_VB ex = new GET_SUBMIT_VB();
            ex.execute(new String[] {encrypt_serial_num});
			
		}*/

		if (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

			if (ActivityCompat.shouldShowRequestPermissionRationale((activity), Manifest.permission.CALL_PHONE)) {


			} else {
				ActivityCompat.requestPermissions((activity),
						new String[]{Manifest.permission.CALL_PHONE},
						1);
			}

		}
/*
		 if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

	            if (ActivityCompat.shouldShowRequestPermissionRationale((activity), Manifest.permission.CAMERA)) {


	            } else {
	                ActivityCompat.requestPermissions((activity),
	                        new String[]{Manifest.permission.CAMERA},
	                        PERMISSION_REQUEST_CODE);
	            }

	        }
		 */

		
		/* if (ContextCompat.checkSelfPermission(context,Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

		        if (ActivityCompat.shouldShowRequestPermissionRationale((activity), Manifest.permission.CAMERA)) {
		        	

		        } else {
		            ActivityCompat.requestPermissions((activity), 
		                    new String[]{Manifest.permission.CAMERA},
		                    PERMISSION_REQUEST_CODE);
		        }

		  }*/
		 
		 //isPermissionGranted();
		 
		 /*if (ContextCompat.checkSelfPermission(context,Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

		        if (ActivityCompat.shouldShowRequestPermissionRationale((activity), Manifest.permission.CALL_PHONE)) {
		        	

		        } else {
		            ActivityCompat.requestPermissions((activity), 
		                    new String[]{Manifest.permission.CALL_PHONE},
		                    PERMISSION_REQUEST_CODE);
		        }

		  }*/

		btn_search.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				//onHideKeyboard();
				lay_details.setVisibility(View.GONE);
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {
					String key = editTextserialno.getText().toString();

					if(key.length() > 0){

						GET_SUBMIT ex = new GET_SUBMIT();
						ex.execute();

					}else {
						Toast.makeText(context, "Serial Number is required.", Toast.LENGTH_SHORT).show();
					}

				}else{
					Toast.makeText(context, getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	public void onSubmit(View v){
		//Toast.makeText(context, getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_SHORT).show();
		/*onHideKeyboard();
		lay_details.setVisibility(View.GONE);
		Boolean Network_status = isNetworkAvailable();
		if (Network_status == true) {
			String key = editTextserialno.getText().toString();
			
			if(key.length() > 0){
				
				GET_SUBMIT ex = new GET_SUBMIT();
				ex.execute();
				
			}else {
				Toast.makeText(context, "Serial Number is required.", Toast.LENGTH_SHORT).show();
			}
			
		}else{
			Toast.makeText(context, getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_SHORT).show();
		}*/
	}

	private class GET_SUBMIT extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(SirimLabel.this, "",
					getResources().getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String status = null;
			String key = "";
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {

					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(
							ConnRoutePNames.DEFAULT_PROXY, proxy);

					key = editTextserialno.getText().toString();
					String strURL = getResources().getString(R.string.SIRIM_LABEL_API)+ "key="+key;

					HttpPost httppost = new HttpPost(strURL);
					Log.d(LOG_TAG, "GET_SUBMIT-url:" + strURL);

					String responseBody = null;
					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					responseBody = httpclient.execute(httppost, responseHandler);

					Log.d(LOG_TAG, "GET_SUBMIT-responseBody:" + responseBody);
					
					if(responseBody.length()>0){
						status = "1";
						responbody = responseBody;
					}else{
						status = "0";
					}
					
				} else {
					status = "0";
				}
			} catch (Throwable t) {
				status = "-1";
			}
			return status;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.d(LOG_TAG, "result: " + result);
			progressDialog.dismiss();
			
			if(result == "1"){
				
				if(responbody.length() > 0){
				
					try {
						
						lay_details.setVisibility(View.VISIBLE);
						
						//txt_error.setText("success" + responbody);
						
						//JSONArray json = new JSONArray("["+responbody+"]");
						
						JSONObject JsonObj = new JSONObject(responbody); //JSONObject is an unordered collection of name/value pairs

						if (JsonObj.has("Response")) { 
							
							txt_product_status.setText("Certified");
							txt_product_status2.setText("");
							txt_product_status3.setText("");
							txt_product_status4.setText("");
							txt_unconfirm_product.setVisibility(View.GONE);
							
							lay_details.setVisibility(View.VISIBLE);
						    String response = JsonObj .getString("Response");
						    JSONArray json = new JSONArray(response);
						    for(int n = 0; n < json.length(); n++){
						    	
						    	String serialno = json.getJSONObject(n).getString("Serial No.").toString();
						    	String product = json.getJSONObject(n).getString("Product").toString();
						    	String company_name = json.getJSONObject(n).getString("Company Name").toString();
						    	txt_serialno.setText(serialno);
						    	txt_product_details.setText(product);
						    	txt_this_label_assigned_to.setText(company_name);
						    	txt_error.setText("");
						    }
						    
						    
						}
						else {
							
							// Version 1.1
							txt_product_status.setText("For this serial number, please contact us at  ");
							txt_product_status2.setText("03-55446400");
							txt_product_status3.setText(" or email us at ");
							txt_product_status4.setText("cserviceqas@sirim.my");
							txt_serialno.setText("");
					    	txt_product_details.setText("");
					    	txt_error.setText("");
					    	txt_this_label_assigned_to.setText("");
					    	txt_unconfirm_product.setVisibility(View.GONE);
							
							/*  Version 1.0
							lay_details.setVisibility(View.GONE);
						    //Do Your Staff
							String response = JsonObj .getString("Error");
							txt_error.setText(" -->"+ response);
							
							JSONArray json = new JSONArray("["+response+"]");
						    for(int n = 0; n < json.length(); n++){
						    	
						    	String status_desc = json.getJSONObject(n).getString("status_desc").toString();
						    	txt_serialno.setText("");
						    	txt_product_details.setText("");
						    	txt_error.setText(status_desc);
						    }*/
							
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					
				}
				
			}else{
				
			}
			
		}

	}

	public void onHideKeyboard(){
		InputMethodManager inputMethodManager = 
		        (InputMethodManager) SirimLabel.this.getSystemService(
		            Activity.INPUT_METHOD_SERVICE);
		    inputMethodManager.hideSoftInputFromWindow(
		        SirimLabel.this.getCurrentFocus().getWindowToken(), 0);
	}
	
	
	public void clickButtonScan(View v){
		try {
			
			Intent to = new Intent(context,ActivityScanner.class);
			startActivity(to);
			//onHideKeyboard();
			/*if(isPermissionGranted()){
				
				lay_details.setVisibility(View.GONE);
				editTextserialno.setText("");
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {
	
					//Intent to = new Intent(context,ActivityScanner.class);
					//startActivity(to);
					//finish();
					
					//Intent intent = new Intent("com.google.zxing.client.android.SCAN");
					//intent.putExtra("SCAN_MODE", "QR_CODE_MODE"); // "PRODUCT_MODE for bar codes
	
					//startActivityForResult(intent, 0);
				}else{
					Toast.makeText(context, getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_SHORT).show();
				}
			}*/

		} catch (Exception e) {

		    Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
		    Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
		    startActivity(marketIntent);

		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {           
	    super.onActivityResult(requestCode, resultCode, data);
	    if (requestCode == 0) {

	        if (resultCode == RESULT_OK) {
	            String contents = data.getStringExtra("SCAN_RESULT");
	            Log.d(LOG_TAG, "SCAN_RESULT = " + contents);
	            
	            Boolean Network_status = isNetworkAvailable();
	    		if (Network_status == true) {
	    			
	    			GET_SUBMIT_VB ex = new GET_SUBMIT_VB();
		            ex.execute(new String[] {contents});
	    			
	    		}else{
	    			Toast.makeText(context, getResources().getString(R.string.MSG_NO_INTERNET), Toast.LENGTH_SHORT).show();
	    		}
	            
	        }
	        if(resultCode == RESULT_CANCELED){
	            //handle cancel
	        }
	    }
	}

	private class GET_SUBMIT_VB extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(SirimLabel.this, "",
					getResources().getString(R.string.please_wait));
		}

		@Override
		protected String doInBackground(String... arg0) {
			String status = null;
			String key = "";
			try {
				Boolean Network_status = isNetworkAvailable();
				if (Network_status == true) {

					Log.d(LOG_TAG, "1");
					
					key = arg0[0];
					Log.d(LOG_TAG, "2=>" + key);
					/*key = key.replaceAll("\\+", "111");
					key = key.replaceAll("\\=", "110");*/
					
					HttpHost proxy = new HttpHost(ConnRouteParams.NO_HOST);
					HttpClient httpclient = new DefaultHttpClient();
					httpclient.getParams().setParameter(
							ConnRoutePNames.DEFAULT_PROXY, proxy);

					String strURL = getResources().getString(R.string.SIRIM_LABEL_API_SIRIM)+ "key="+ URLEncoder.encode(key, "UTF-8");

					HttpPost httppost = new HttpPost(strURL);
					Log.d(LOG_TAG, "GET_SUBMIT_VB-url:" + strURL);

					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					resultScanner = httpclient.execute(httppost, responseHandler);

					Log.d(LOG_TAG, "GET_SUBMIT_VB-resultScanner:" + resultScanner);
					
					/*resultScanner = String.valueOf(HttpsRequestSirimResult.toHttpPostAPI(context,key));
					Log.d(LOG_TAG, "resultScanner : " + resultScanner);
					
					Log.d(LOG_TAG, "GET_SUBMIT_VB-resultScanner:" + resultScanner);
					Toast.makeText(context, resultScanner, Toast.LENGTH_LONG).show();*/
					
					if(resultScanner.length()>0){
						status = "1";
					}else{
						status = "0";
					}
					
				} else {
					status = "0";
				}
			} catch (Throwable t) {
				//Toast.makeText(context, "-1", Toast.LENGTH_LONG).show();
				status = "-1";
			}
			return status;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.d(LOG_TAG, "result: " + result);
			progressDialog.dismiss();
			
			if(result == "1"){
				
				if(resultScanner.length() > 0){
					lay_details.setVisibility(View.VISIBLE);
					try {
						JSONArray StatusJSONArray = new JSONArray(resultScanner);
						Log.d(LOG_TAG, "StatusJSONArray.length():"+ StatusJSONArray.length());
						
						if (StatusJSONArray.length() > 0) {
						
							//[{"serialNo":"PEA0000001","Product":"SIDE AND REAR MARKING FOR ROAD VEHICLE","ApplicantName":"ORAFOL REFLECTIVE SOLUTIONS (MALAYSIA) SDN. BHD."}]
							
							String SerialNoFrom = StatusJSONArray.getJSONObject(0).getString("serialNo").toString();//serialNo
							String Product = StatusJSONArray.getJSONObject(0).getString("Product").toString();
							String ApplicantName = StatusJSONArray.getJSONObject(0).getString("ApplicantName").toString();
							
							if(Product.equals("0")){ //scan wrong qrcode
								String desc = StatusJSONArray.getJSONObject(0).getString("desc").toString();
								//txt_product_status.setText(desc);
								txt_product_details.setText("");
								txt_product_status.setText(Html.fromHtml(desc));
								
							}else{
								
								if(SerialNoFrom.length()>0){
									editTextserialno.setText(SerialNoFrom);
								}
								
								if(Product.length()>3){
									txt_product_status.setText("Certified");
									txt_product_details.setText(Product);
								}
								
							}
							
							txt_product_status2.setText("");
							txt_product_status3.setText("");
							txt_product_status4.setText("");
							txt_unconfirm_product.setVisibility(View.GONE);
							
							lay_details.setVisibility(View.VISIBLE);
							txt_serialno.setText(SerialNoFrom);
					    	txt_this_label_assigned_to.setText(ApplicantName);
					    	txt_error.setText("");
							
						
						}else{
							
							// Version 1.1
							txt_product_status.setText("For this serial number, please contact us at  ");
							txt_product_status2.setText("03-55446400");
							txt_product_status3.setText(" or email us at ");
							txt_product_status4.setText("cserviceqas@sirim.my");
							txt_serialno.setText("");
					    	txt_product_details.setText("");
					    	txt_error.setText("");
					    	txt_this_label_assigned_to.setText("");
					    	txt_unconfirm_product.setVisibility(View.GONE);
							
						}
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}

			}else{
				lay_details.setVisibility(View.VISIBLE);
				// Version 1.1
				txt_product_status.setText("For this serial number, please contact us at  ");
				txt_product_status2.setText("03-55446400");
				txt_product_status3.setText(" or email us at ");
				txt_product_status4.setText("cserviceqas@sirim.my");
				txt_serialno.setText("");
		    	txt_product_details.setText("");
		    	txt_error.setText("");
		    	txt_this_label_assigned_to.setText("");
		    	txt_unconfirm_product.setVisibility(View.GONE);
				
			}
			
		}

	}
	
	@Override
    public void onResume() {
        super.onResume();
       
        String result_scanner_qrcode_status = settings.getString("result_scanner_qrcode_status", "");
        String result_scanner_qrcode = settings.getString("result_scanner_qrcode", "");
        
        if(result_scanner_qrcode_status.equals("1")){
        	
        	SharedPreferences.Editor editor = settings.edit();
    		editor.remove("result_scanner_qrcode");
    		editor.remove("result_scanner_qrcode_status");
    		editor.putString("result_scanner_qrcode_status","0");
    		editor.commit();
        	
    		/*Log.d(LOG_TAG, "result_scanner_qrcode = " + result_scanner_qrcode);
    		result_scanner_qrcode = result_scanner_qrcode.replaceAll("\\?", "707");
    		Log.d(LOG_TAG, "result_scanner_qrcode = " + result_scanner_qrcode);
    		String[] arr = result_scanner_qrcode.split("707");
    		String encrypt_serial_num = arr[1];*/
    		
    		//Toast.makeText(context, encrypt_serial_num, Toast.LENGTH_LONG).show();
    		
    		if(result_scanner_qrcode.length() > 0){
    		
    			Log.d(LOG_TAG, "final code = " + result_scanner_qrcode);
    			//run execute
        		GET_SUBMIT_VB ex = new GET_SUBMIT_VB();
                ex.execute(new String[] {result_scanner_qrcode});
    			
    		}
    		
        }
        
    }
	
	public void call_action(String txt){

		if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return;
		}else {
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse("tel:" + txt));
			startActivity(callIntent);
		}

	}
	
	public  boolean isPermissionGranted() {
	    if (Build.VERSION.SDK_INT >= 23) {
	        if (ContextCompat.checkSelfPermission(context,Manifest.permission.CALL_PHONE)== PackageManager.PERMISSION_GRANTED) {
	            Log.v("TAG","Permission is granted");
	            return true;
	        } else {

	            Log.v("TAG","Permission is revoked");
	            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
	            return false;
	        }
	    }
	    else { //permission is automatically granted on sdk<23 upon installation
	        Log.v("TAG","Permission is granted");
	        return true;
	    }
	}

	
	public boolean isNetworkAvailable() {

		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		SharedPreferences.Editor editor = settings.edit();
		editor.remove("result_scanner_qrcode");
		editor.remove("result_scanner_qrcode_status");
		editor.putString("result_scanner_qrcode_status","0");
		editor.commit();
		finish();
	}

	public void onDestroy() {
		SharedPreferences.Editor editor = settings.edit();
		editor.remove("result_scanner_qrcode");
		editor.remove("result_scanner_qrcode_status");
		editor.putString("result_scanner_qrcode_status","0");
		editor.commit();
		this.finish();
		super.onStop();
		super.onDestroy();
	}

}
